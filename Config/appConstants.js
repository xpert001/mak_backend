
'use strict';

var SERVER = {
    APP_NAME: 'mak',
    PORTS: {
        HAPI: 8004
    },
    TOKEN_EXPIRATION_IN_MINUTES: 600,
    JWT_SECRET_KEY: '1233',
    GOOGLE_API_KEY : '',
    COUNTRY_CODE : '+91',
    MAX_DISTANCE_RADIUS_TO_SEARCH : '1',
    THUMB_WIDTH : 300,
    THUMB_HEIGHT : 300,
    DOMAIN_NAME : 'http://localhost:8000/',
    SUPPORT_EMAIL : '',
    SECRET_KEY_PAYTAB : 'T9IseO4RJWi3e3dpryzGMmcFkhvYAk4uuNWIC1AlYSpcVCBmba3YxgDRx8Q7sUCWHyFjAfFy74L3HnqTxtR4KuWhDMp6G13oAIIb',
    SECRET_KEY_PAYTAB_DEBIT : 'w7hkhgI94U1nDjU7ptaGL1udCeiu37Ozf6UxmCHrxIOa2Jirds8ZuDc7RvCVDquSxppT16kyP9JRT2j6ur6Z4i6WOJRr9mkHLJAN'

};

var DATABASE = {
    PROFILE_PIC_PREFIX : {
        ORIGINAL : 'profilePic_',
        THUMB : 'profileThumb_'
    },

    LOGO_PREFIX : {
        ORIGINAL : 'logo_',
        THUMB : 'logoThumb_'
    },

    USER_ROLES: {
        ADMIN: 'ADMIN',
        AGENCY:'AGENCY',
        MAID:"MAID",
        USER:'USER',
    },

    DEVICE_TYPES: {
        IOS: 'IOS',
        ANDROID: 'ANDROID'
    },

    GENDER:
    {
        MALE:'MALE',
        FEMALE:'FEMALE',
        OTHERS:'OTHERS'
    },

    LANGUAGE: {
        EN: 'EN',
        AR: 'AR',
        ES_MX: 'ES_MX'
    },

    PAYMENT_OPTIONS : {
        CREDIT_DEBIT_CARD : 'CREDIT_DEBIT_CARD',
        PAYPAL : 'PAYPAL',
        BITCOIN : 'BITCOIN',
        GOOGLE_WALLET : 'GOOGLE_WALLET',
        APPLE_PAY : 'APPLE_PAY',
        EIYA_CASH : 'EIYA_CASH'
    },

    EXPERIENCE : {
        UPTO_2_YEARS : 2,
        UPTO_5_YEARS : 5,
        ABOVE_6_YEARS :6,
    },

    ACTION: {
        PENDING: 'PENDING',
        DECLINE: 'DECLINE',
        ACCEPT: 'ACCEPT',
        DELETED: 'DELETED',
        COMPLETED: 'COMPLETED',

    },

    AGENCY_TYPE: {
        MAK_REGISTERED_UAE: 'MAK_REGISTERED_UAE',
        MAK_REGISTERED_BAHRAIN: 'MAK_REGISTERED_BAHRAIN',
        NORMAL: 'NORMAL',
    },

    NOTIFICATION_TYPE: {
        SERVICE_REQUEST: 'SERVICE_REQUEST',
        SERVICE_ACCEPT: 'SERVICE_ACCEPT',
        SERVICE_REJECT: 'SERVICE_REJECT',
        SERVICE_COMPLETE: 'SERVICE_COMPLETE',
        SERVICE_EXTEND: 'SERVICE_EXTEND',
        SERVICE_EXTEND_ACCEPT: 'SERVICE_EXTEND_ACCEPT',
        SERVICE_EXTEND_CANCEL: 'SERVICE_EXTEND_CANCEL',
        SERVICE_DELETE: 'SERVICE_DELETE',
        MESSAGE: 'MESSAGE',
        ALL_USER_TYPE: 'ALL_USER_TYPE',
        NEW_MAID: 'NEW_MAID',
    },
};

var STATUS_MSG = {
    ERROR: {

        PLEASE_SIGNUP:{
            statusCode:400,
            type: 'PLEASE_SIGNUP',
            customMessage : 'You have to sign up for accessing this feature. '
        },
        REJECT:{
            statusCode:400,
            type: 'REJECT',
            customMessage : 'Unfortunately your application for a MAK Registered Maid was rejected on this occasion. Please contact us for further information on reasons for rejection. '
        },
        COUNTRY_NOT_AVAILABLE:{
            statusCode:400,
            type: 'COUNTRY_NOT_AVAILABLE',
            customMessage : 'The country for which you have initiated stripe payment is not yet supported. '
        },

        AGGREMENT_NOT_ACCEPTED:{
            statusCode:400,
            type: 'AGGREMENT_NOT_ACCEPTED',
            customMessage : 'Your account has been verified by the agency. Please accept the licence aggrement sent to your email. '
        },

        AGGREMENT_NOT_ACCEPTED_AR:{
            statusCode:400,
            type: 'AGGREMENT_NOT_ACCEPTED',
            customMessage : 'تم التحقق من حسابك من قِبل الوكالة. الرجاء قبول طلب الترخيص المرسل إلى بريدك الإلكتروني.'
        },

        NOT_VERIFIED:{
            statusCode:400,
            type: 'NOT_VERIFIED',
            customMessage : 'Your account has been setup. Please wait until agency has verified your account'
        },
        NOT_VERIFIED_AR:{
            statusCode:400,
            type: 'NOT_VERIFIED_AR',
            customMessage : 'تم إعداد حسابك. الرجاء الانتظار حتى تتحقق الوكالة من حسابك'
        },
        ALREADY_VERIFY:{
            statusCode:400,
            type: 'ALREADY_VERIFY',
            customMessage : 'Your account is already verify.'
        },
        ALREADY_VERIFY_AR:{
            statusCode:400,
            type: 'ALREADY_VERIFY_AR',
            customMessage : 'حسابك هو بالفعل التحقق.'
        },

        MAID_CANNOT_REVIEW_AGAIN:{
            statusCode:400,
            type: 'MAID_CANNOT_REVIEW_AGAIN',
            customMessage : 'You cannot review this user for the same service again.'
        },
        MAID_CANNOT_REVIEW_AGAIN_AR:{
            statusCode:400,
            type: 'MAID_CANNOT_REVIEW_AGAIN',
            customMessage : 'لا يمكنك مراجعة هذا المستخدم لنفس الخدمة مرة أخرى.'
                },

        USER_CANNOT_REVIEW_AGAIN:{
            statusCode:400,
            type: 'USER_CANNOT_REVIEW_AGAIN',
            customMessage : 'You cannot review this maid for the same service again.'
        },

        USER_CANNOT_REVIEW_AGAIN_AR:{
            statusCode:400,
            type: 'USER_CANNOT_REVIEW_AGAIN_AR',
            customMessage : 'لا يمكنك مراجعة هذه الخادمة لنفس الخدمة مرة أخرى.'
        },

        UAE_REGISTERED_AGENCY_EXIST:{
            statusCode:400,
            type: 'UAE_REGISTERED_AGENCY_EXIST',
            customMessage : 'UAE registered agency has been already added'
        },

        BAHRAIN_REGISTERED_AGENCY_EXIST:{
            statusCode:400,
            type: 'BAHRAIN_REGISTERED_AGENCY_EXIST',
            customMessage : 'Bahrain registered agency has been already added'
        },

        CANNOT_BLOCK_AGENCY:{
            statusCode:400,
            type: 'CANNOT_BLOCK_AGENCY',
            customMessage : 'You cannot block this agency, because it has related informations'
        },

        FIRST_BOOKING_DONE: {
            statusCode:400,
            customMessage : 'Sorry, you have done a booking as guest previously. Please register yourself.',
            type : 'FIRST_BOOKING_DONE'
        },
        FIRST_BOOKING_DONE_AR: {
            statusCode:400,
            customMessage : 'عذرًا ، لقد قمت بإجراء حجز كضيف سابقًا. يرجى تسجيل نفسك.',
            type : 'FIRST_BOOKING_DONE_AR'
        },

        NO_MAID_AVAILABLE: {
            statusCode:400,
            customMessage : 'No maids available on your requested query',
            type : 'NO_MAID_AVAILABLE'
        },

        MAID_BUSY: {
            statusCode:400,
            customMessage : 'Sorry! This maid is already booked for the selected date or time. Please choose different date or time.',
            type : 'MAID_BUSY'
        },
        MAID_BUSY_AR: {
            statusCode:400,
            customMessage : 'الخادمة التي اخترتها مشغولة ، يرجى اختيار خادمة أخرى.',
            type : 'MAID_BUSY'
        },

        MAID_EXIST: {
            statusCode:400,
            customMessage : 'Email you have entered is already registered with us. Please login to continue.',
            type : 'MAID_EXIST'
        },

        REASON_REQUIRED: {
            statusCode:400,
            customMessage : 'Reason for declining service request is required',
            type : 'REASON_REQUIRED'
        },

        DELETE_REASON_REQUIRED: {
            statusCode:400,
            customMessage : 'Reason for canceling service request is required',
            type : 'DELETE_REASON_REQUIRED'
        },

        MAID_ID_REQUIRED: {
            statusCode:400,
            customMessage : 'Maid ID is required',
            type : 'MAID_ID_REQUIRED'
        },
        RELIGION: {
            statusCode:400,
            customMessage : 'This religion is already exist.',
            type : 'RELIGION'
        },

        NATIONALITY_REQUIRED: {
            statusCode:400,
            customMessage : 'Nationality is required',
            type : 'NATIONALITY_REQUIRED'
        },

        DOB_REQUIRED: {
            statusCode:400,
            customMessage : 'Date of birth is required',
            type : 'DOB_REQUIRED'
        },

        EXPERIENCE_REQUIRED: {
            statusCode:400,
            customMessage : 'Experience is required',
            type : 'EXPERIENCE_REQUIRED'
        },

        EXPERIENCE_WITH_CHILDREN_REQUIRED: {
            statusCode:400,
            customMessage : 'Experience with children is required',
            type : 'EXPERIENCE_WITH_CHILDREN_REQUIRED'
        },

        LANGUAGES_REQUIRED: {
            statusCode:400,
            customMessage : 'Languages is required',
            type : 'LANGUAGES_REQUIRED'
        },

        RATING_REQUIRED: {
            statusCode:400,
            customMessage : 'Rating is required',
            type : 'RATING_REQUIRED'
        },

        FEEDBACK_REQUIRED: {
            statusCode:400,
            customMessage : 'FeedBack is required',
            type : 'FEEDBACK_REQUIRED'
        },

        PROFILE_PIC_URL_REQUIRED: {
            statusCode:400,
            customMessage : 'Profile picture is required',
            type : 'PROFILE_PIC_URL_REQUIRED'
        },

        PRICE_REQUIRED: {
            statusCode:400,
            customMessage : 'Price is required',
            type : 'PRICE_REQUIRED'
        },

        INCOMPLETE_PROFILE_SETUP1: {
            statusCode:400,
            customMessage : 'Profile setup step 1 is incomplete',
            type : 'INCOMPLETE_PROFILE_SETUP1'
        },

        INCOMPLETE_PROFILE_SETUP2: {
            statusCode:400,
            customMessage : 'Profile setup step 2 is incomplete',
            type : 'INCOMPLETE_PROFILE_SETUP2'
        },

        PROFILE_SETUP_COMPLETED: {
            statusCode:400,
            customMessage : 'Profile setup is already completed',
            type : 'PROFILE_SETUP_COMPLETED'
        },

        ALREADY_EXIST: {
            statusCode:400,
            type: 'ALREADY_EXIST',
            customMessage : 'Already Exist '
        },

        DATA_NOT_FOUND: {
            statusCode:401,
            type: 'DATA_NOT_FOUND',
            customMessage : 'Result not data'
        },

        INVALID_USER_PASS: {
            statusCode:401,
            type: 'INVALID_USER_PASS',
            customMessage : 'Invalid username or password'
        },

        TOKEN_ALREADY_EXPIRED: {
            statusCode:401,
            customMessage : 'Your login session expired!',
            type : 'TOKEN_ALREADY_EXPIRED'
        },

        DB_ERROR: {
            statusCode:400,
            customMessage : 'DB Error : ',
            type : 'DB_ERROR'
        },

        INVALID_ID: {
            statusCode:400,
            customMessage : 'Invalid Id Provided : ',
            type : 'INVALID_ID'
        },

        APP_ERROR: {
            statusCode:400,
            customMessage : 'Application Error',
            type : 'APP_ERROR'
        },

        ADDRESS_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Address not found',
            type : 'ADDRESS_NOT_FOUND'
        },

        SAME_ADDRESS_ID: {
            statusCode:400,
            customMessage : 'Pickup and Delivery Address Cannot Be Same',
            type : 'SAME_ADDRESS_ID'
        },

        IMP_ERROR: {
            statusCode:500,
            customMessage : 'Implementation Error',
            type : 'IMP_ERROR'
        },

        APP_VERSION_ERROR: {
            statusCode:400,
            customMessage : 'One of the latest version or updated version value must be present',
            type : 'APP_VERSION_ERROR'
        },

        INVALID_TOKEN: {
            statusCode:401,
            customMessage : 'Invalid token provided',
            type : 'INVALID_TOKEN'
        },

        INVALID_CODE: {
            statusCode:400,
            customMessage : 'Invalid Verification Code',
            type : 'INVALID_CODE'
        },

        DEFAULT: {
            statusCode:400,
            customMessage : 'Error',
            type : 'DEFAULT'
        },

        PHONE_NO_EXIST: {
            statusCode:400,
            customMessage : 'Phone No Already Exist',
            type : 'PHONE_NO_EXIST'
        },

        NATIONAL_ID_EXIST: {
            statusCode:400,
            customMessage : 'National Id already exists. Please enter unique Id.',
            type : 'NATIONAL_ID_EXIST'
        },
        CR_ID_EXIST: {
            statusCode:400,
            customMessage : 'CR number you have entered is already exist.',
            type : 'CR_ID_EXIST'
        },
        NATIONAL_ID_EXIST_AR: {
            statusCode:400,
            customMessage : 'الهوية الوطنية موجودة بالفعل',
            type : 'NATIONAL_ID_EXIST'
        },

        EMAIL_EXIST: {
            statusCode:400,
            customMessage : 'Email you have entered is already registered with us. Please login to continue.',
            type : 'EMAIL_EXIST'
        },
        MEMEBR_EMAIL_EXIST: {
            statusCode:400,
            customMessage : 'This email is already used by one of the member of the MAK. Please use a different email account.',
            type : 'MEMEBR_EMAIL_EXIST'
        },
        // EMAIL_EXIST: {
        //     statusCode:400,
        //     customMessage : 'Email address you have entered is already registered with us. Please login to continue',
        //     type : 'EMAIL_EXIST'
        // },

        DUPLICATE_CARD: {
            statusCode:400,
            customMessage : 'Duplicate Card Entry',
            type : 'DUPLICATE_CARD'
        },

        DUPLICATE: {
            statusCode:400,
            customMessage : 'Duplicate Entry',
            type : 'DUPLICATE'
        },

        DUPLICATE_ADDRESS: {
            statusCode:400,
            customMessage : 'Address Already Exist',
            type : 'DUPLICATE_ADDRESS'
        },

        UNIQUE_CODE_LIMIT_REACHED: {
            statusCode:400,
            customMessage : 'Cannot Generate Unique Code, All combinations are used',
            type : 'UNIQUE_CODE_LIMIT_REACHED'
        },

        INVALID_REFERRAL_CODE: {
            statusCode:400,
            customMessage : 'Invalid Referral Code',
            type : 'INVALID_REFERRAL_CODE'
        },

        FACEBOOK_ID_PASSWORD_ERROR: {
            statusCode:400,
            customMessage : 'Only one field should be filled at a time, either facebookId or password',
            type : 'FACEBOOK_ID_PASSWORD_ERROR'
        },

        INVALID_EMAIL: {
            statusCode:400,
            customMessage : 'Invalid Email Address',
            type : 'INVALID_EMAIL'
        },

        INVALID_EMAIL_OR_PHONE_NUMBER: {
            statusCode:400,
            customMessage : 'Invalid Email Address or Phone Number',
            type : 'INVALID_EMAIL_OR_PHONE_NUMBER'
        },

        PASSWORD_REQUIRED: {
            statusCode:400,
            customMessage : 'Password is required',
            type : 'PASSWORD_REQUIRED'
        },

        MINIMUM_AGE: {
            statusCode:400,
            customMessage : 'Minimum age to sign up is 18',
            type : 'MINIMUM_AGE'
        },

        OTP_REQUIRED: {
            statusCode:400,
            customMessage : 'Otp is required',
            type : 'OTP_REQUIRED'
        },

        PHONE_NO_REQUIRED: {
            statusCode:400,
            customMessage : 'Phone number is required',
            type : 'PHONE_NO_REQUIRED'
        },

        EMAIL_REQUIRED: {
            statusCode:400,
            customMessage : 'Email is required',
            type : 'EMAIL_REQUIRED'
        },

        INVALID_OTP: {
            statusCode:400,
            customMessage : 'OTP you have entered does not match',
            type : 'INVALID_OTP'
        },

        FIRSTNAME_REQUIRED: {
            statusCode:400,
            customMessage : 'First Name is required',
            type : 'FIRSTNAME_REQUIRED'
        },

        PHONE_NO_NOT_VERIFIED: {
            statusCode:400,
            customMessage : 'Phone Number is not verified',
            type : 'PHONE_NO_NOT_VERIFIED'
        },

        INVALID_COUNTRY_CODE: {
            statusCode:400,
            customMessage : 'Invalid Country Code, Should be in the format +52',
            type : 'INVALID_COUNTRY_CODE'
        },

        INVALID_PHONE_NO_FORMAT: {
            statusCode:400,
            customMessage : 'Phone no. cannot start with 0',
            type : 'INVALID_PHONE_NO_FORMAT'
        },

        COUNTRY_CODE_MISSING: {
            statusCode:400,
            customMessage : 'You forgot to enter the country code',
            type : 'COUNTRY_CODE_MISSING'
        },

        INVALID_PHONE_NO: {
            statusCode:400,
            customMessage : 'Phone No. & Country Code does not match to which the OTP was sent',
            type : 'INVALID_PHONE_NO'
        },

        PHONE_NO_MISSING: {
            statusCode:400,
            customMessage : 'You forgot to enter the phone no.',
            type : 'PHONE_NO_MISSING'
        },

        NOTHING_TO_UPDATE: {
            statusCode:400,
            customMessage : 'Nothing to update',
            type : 'NOTHING_TO_UPDATE'
        },

        NOT_FOUND: {
            statusCode:400,
            customMessage : 'User Not Found',
            type : 'NOT_FOUND'
        },

        INVALID_RESET_PASSWORD_TOKEN: {
            statusCode:400,
            customMessage : 'Invalid Reset Password Token',
            type : 'INVALID_RESET_PASSWORD_TOKEN'
        },

        CURRENT_PASSWORD: {
            statusCode:400,
            customMessage : 'Current password is not matched',
            type : 'CURRENT_PASSWORD'
        },

        TEMPORARY_PASSWORD: {
            statusCode:400,
            customMessage : 'Temporary password is not matched',
            type : 'TEMPORARY_PASSWORD'
        },

        INCORRECT_OLD_PASSWORD: {
            statusCode:400,
            customMessage : 'Incorrect old password',
            type : 'INCORRECT_OLD_PASSWORD'
        },
        INCORRECT_OLD_PASSWORD_AR: {
            statusCode:400,
            customMessage : 'كلمة السر القديمة غير صحيحة',
            type : 'INCORRECT_OLD_PASSWORD'
        },

        INCORRECT_PASSWORD: {
            statusCode:400,
            customMessage : 'The password you have entered does not match.',
            type : 'INCORRECT_PASSWORD'
        },
        INCORRECT_PASSWORD_AR: {
            statusCode:400,
            customMessage : 'كلمة المرور التي أدخلتها غير متطابقة.',
            type : 'INCORRECT_PASSWORD'
        },
        EMPTY_VALUE: {
            statusCode:400,
            customMessage : 'Empty String Not Allowed',
            type : 'EMPTY_VALUE'
        },

        PHONE_NOT_MATCH: {
            statusCode:400,
            customMessage : "Phone No. Doesn't Match",
            type : 'PHONE_NOT_MATCH'
        },

        SAME_PASSWORD: {
            statusCode:400,
            customMessage : 'Old password and new password are same',
            type : 'SAME_PASSWORD'
        },

        ACTIVE_PREVIOUS_SESSIONS: {
            statusCode:400,
            customMessage : 'You already have previous active sessions, confirm for flush',
            type : 'ACTIVE_PREVIOUS_SESSIONS'
        },

        EMAIL_ALREADY_EXIST: {
            statusCode:400,
            customMessage : 'Email Address Already Exists',
            type : 'EMAIL_ALREADY_EXIST'
        },

        ERROR_PROFILE_PIC_UPLOAD: {
            statusCode:400,
            customMessage : 'Profile pic is not a valid file',
            type : 'ERROR_PROFILE_PIC_UPLOAD'
        },

        PHONE_ALREADY_EXIST: {
            statusCode:400,
            customMessage : 'The phone number you have entered is already in use.',
            type : 'PHONE_ALREADY_EXIST'
        },
        PHONE_ALREADY_EXIST_AR: {
            statusCode:400,
            customMessage : 'رقم الهاتف الذي أدخلته موجود بالفعل.',
            type : 'PHONE_ALREADY_EXIST'
        },

        EMAIL_NOT_REGISTERED: {
            statusCode:400,
            customMessage : 'The email you have entered has not been registered.',
            type : 'EMAIL_NOT_REGISTERED'
        },
        EMAIL_NOT_REGISTERED_AR: {
            statusCode:400,
            customMessage : 'البريد الإلكتروني الذي أدخلته غير مسجل.',
            type : 'EMAIL_NOT_REGISTERED'
        },

        FACEBOOK_ID_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Facebook Id Not Found',
            type : 'FACEBOOK_ID_NOT_FOUND'
        },

        PHONE_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Phone No. Not Found',
            type : 'PHONE_NOT_FOUND'
        },

        INCORRECT_OLD_PASS: {
            statusCode:400,
            customMessage : 'Incorrect Old Password',
            type : 'INCORRECT_OLD_PASS'
        },

        USER_DELETED: {
            statusCode:403,
            customMessage : 'This User has been deleted by the admin',
            type : 'USER_DELETED'
        },

        USER_DELETED_AR: {
            statusCode:403,
            customMessage : 'تم حذف هذا المستخدم من قبل المشرف',
            type : 'USER_DELETED'
        },

        BLOCK_USER: {
            statusCode:403,
            customMessage : 'This account is blocked by Admin. Please contact support team to activate your account.',
            type : 'BLOCK_USER'
        },
        BLOCK_USER_AR: {
            statusCode:403,
            customMessage : 'تم حظر هذا الحساب من قبل المسؤول. يرجى الاتصال بفريق الدعم لتنشيط حسابك.',
            type : 'BLOCK_USER'
        },
        VERIFY_USER: {
            statusCode:403,
            customMessage : 'Please verify your email first.',
            type : 'VERIFY_USER'
        },
        VERIFY_USER_BOOKING: {
            statusCode:400,
            customMessage : 'Please verify your email to continue with your booking.',
            type : 'VERIFY_USER'
        },
        VERIFY_USER_AR: {
            statusCode:403,
            customMessage : 'يرجى التحقق من بريدك الإلكتروني أولاً.',
            type : 'VERIFY_USER_AR'
        },
        UNAUTHORIZED: {
            statusCode:401,
            customMessage : 'You are not authorized to perform this action',
            type : 'UNAUTHORIZED'
        },
        NOT_ABLE_TO_RAISE: {
            statusCode:400,
            customMessage : 'You are not able to send issue,as you are not registered with us.',
            type : 'NOT_ABLE_TO_RAISE'
        },

        TIME_ERROR: {
            statusCode:400,
            customMessage : 'Please select time at least 2 hours after current time and between 9am and 9pm.',
            type : 'TIME_ERROR'
        },
        TIME_ERROR_AR: {
            statusCode:400,
            customMessage : 'يرجى تحديد الوقت على الأقل بعد ساعتين من الوقت الحالي وبين الساعة 9 صباحًا و 9 مساءً.',
            type : 'TIME_ERROR'
        },
        NOT_ABLE_TO_BOOK: {
            statusCode:400,
            customMessage : 'You are not able to make a booking where service ends after 11pm',
            type : 'NOT_ABLE_TO_BOOK'
        },
        ALREADY_BOOK: {
            statusCode:400,
            customMessage : 'This maid is already booked.',
            type : 'ALREADY_BOOK'
        },
        NOT_ABLE_TO_BOOK_AR: {
            statusCode:400,
            customMessage : 'لا يمكنك حجز الخادمة لأكثر من ساعتين بعد التاسعة مساءً',
            type : 'NOT_ABLE_TO_BOOK'
        },
        DUPLICATE_DATA: {
            statusCode:400,
            customMessage : 'You are entering the address that you added previously',
            type : 'DUPLICATE_DATA'
        },
        NOT_PROVIDE_SERVICE: {
            statusCode:400,
            customMessage : 'Selected maid does not serve in your area.',
            type : 'NOT_PROVIDE_SERVICE'
        },
        MAID_NOT_AVAILABLE: {
            statusCode:400,
            customMessage : 'Maid is not available in the selected time slots.',
            type : 'MAID_NOT_AVAILABLE'
        },

    },
    SUCCESS: {
        CREATED: {
            statusCode:201,
            customMessage : 'Created Successfully',
            type : 'CREATED'
        },
        DEFAULT: {
            statusCode:200,
            customMessage : 'Success',
            type : 'DEFAULT'
        },
        UPDATED: {
            statusCode:200,
            customMessage : 'Updated Successfully',
            type : 'UPDATED'
        },
        LOGOUT: {
            statusCode:200,
            customMessage : 'Logged Out Successfully',
            type : 'LOGOUT'
        },
        DELETED: {
            statusCode:200,
            customMessage : 'Deleted Successfully',
            type : 'DELETED'
        },
        BLOCKED: {
            statusCode:200,
            customMessage : 'Blocked Successfully',
            type : 'BLOCKED'
        },
        OTP_VERIFIED: {
            statusCode:200,
            customMessage : 'Otp verified',
            type : 'OTP_VERIFIED'
        },
        ACCOUNT_DEACTIVATED: {
            statusCode:200,
            customMessage : 'Account Deactivated Successfully',
            type : 'ACCOUNT_DEACTIVATED'
        },
    }
};

var swaggerDefaultResponseMessages = [
    {code: 200, message: 'OK'},
    {code: 400, message: 'Bad Request'},
    {code: 401, message: 'Unauthorized'},
    {code: 404, message: 'Data Not Found'},
    {code: 500, message: 'Internal Server Error'}
];

var SCREEN_TO_SHOW = {
    HOMEPAGE : 'HOMEPAGE',
    TRACKING : 'TRACKING',
    FEEDBACK : 'FEEDBACK'
};

var notificationMessages = {
    verificationCodeMsg: 'Your 4 digit verification code for Max is {{four_digit_verification_code}}',
    registrationEmail: {
        emailMessage : "Dear {{user_name}}, <br><br> Please  <a href='{{verification_url}}'>click here</a> to verify your email address",
        emailSubject: "Welcome to Seed Project"
    },
    contactDriverForm: {
        emailMessage : "A new driver has showed interest <br><br> Details : <br><br> Name : {{fullName}} <br><br> Email : {{email}} <br><br> Phone No : {{phoneNo}} <br><br> Vehicle Type : {{vehicleType}} <br><br> Bank Account : {{bankAccountBoolean}} <br><br> Heard From : {{heardFrom}}",
        emailSubject: "New Driver Contact Request"
    },
    contactBusinessForm: {
        emailMessage : "A new business has showed interest <br><br> Details : <br><br> Name : {{fullName}} <br><br> Email : {{email}} <br><br> Phone No : {{phoneNo}} <br><br> Business Name: {{businessName}} <br><br> Business Address: {{businessAddress}}  <br><br> Delivery Service : {{ownDeliveryService}} <br><br> Heard From : {{heardFrom}}",
        emailSubject: "New Business Contact Request"
    },
    forgotPassword: {
        emailMessage : "Dear {{user_name}}, <br><br>  Your reset password token is <strong>{{password_reset_token}}</strong> , <a href='{{password_reset_link}}'> Click Here </a> To Reset Your Password",
        emailSubject: "Password Reset Notification For Seed Project"
    }
};

var languageSpecificMessages = {
    verificationCodeMsg : {
        EN : 'Your 4 digit verification code for Seed Project is {{four_digit_verification_code}}',
        ES_MX : 'Your 4 digit verification code for Seed Project is {{four_digit_verification_code}}'
    }
};

var APP_CONSTANTS = {
    SERVER: SERVER,
    DATABASE: DATABASE,
    SCREEN_TO_SHOW : SCREEN_TO_SHOW,
    STATUS_MSG: STATUS_MSG,
    notificationMessages: notificationMessages,
    languageSpecificMessages: languageSpecificMessages,
    swaggerDefaultResponseMessages: swaggerDefaultResponseMessages
};

module.exports = APP_CONSTANTS;
