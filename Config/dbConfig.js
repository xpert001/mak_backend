'use strict';

var mongo = {
    URI: process.env.MONGO_URI || 'mongodb://mak:mak@52.36.127.111/mak',
    port: 27017
};

module.exports = {
    mongo: mongo
};