/*
'use strict';

const async = require('async');

const FCM = require('fcm-node');
const serverKey = 'AIzaSyCeH3DwTqYhK2BswkNpaa2_6VPnI_8itMk';
const fcm = new FCM(serverKey);


const sendPush = function (dtoken, type, typeId, title, body, firstName, lastName, dob, profilePicURL, userId, description, deviceType, callback) {
    async.auto({
        sendPush: function (cb) {
            const message = {
                to: dtoken,
                // collapse_key: 'demo',

                data: {
                    title: title,
                    body: body,
                    userId:userId,
                    firstName:firstName,
                    lastName:lastName,
                    dob:dob,
                    profilePicURL:profilePicURL || '',
                    description:description || "",
                    type: type,
                    typeId: typeId || '',
                    sound: 'default',
                    badge: 1
                },
                priority: 'high'
            };
            if (!(deviceType == "ANDROID")) {
                message.notification = {
                    title: title,
                    body: body,
                    userId: userId,
                    firstName: firstName,
                    lastName: lastName,
                    dob: dob,
                    profilePicURL: profilePicURL || '',
                    description: description || "",
                    type: type,
                    typeId: typeId || '',
                    sound: 'default',
                    badge: 1
                }
            }


            console.log("message**************************", message);
            fcm.send(message, function (err, result) {
                if (err) {
                    console.log("Something has gone wrong!", err);
                    callback(err);
                } else {
                    console.log("Successfully sent with response: ", result);
                    cb(null, result);
                }
            });
        }
    }, function (err, result) {
        callback(err, result);
    })
};

module.exports = {
    sendPush: sendPush,
};
*/
