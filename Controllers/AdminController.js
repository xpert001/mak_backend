'use strict';
const Service = require('../Services');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const async = require('async');
const TokenManager = require('../Lib/TokenManager');
const MailManager = require('../Lib/MailManager');
const NotificationManager = require('../Lib/NotificationManager');
const Config = require('../Config');
const Models = require('../Models');
const moment = require('moment');
const FCM = require('fcm-node');
let fs= require('fs');
let fsExtra = require('fs-extra');
let path = require('path');
const mongoose = require('mongoose');
const _ = require('lodash');
const SocketManager = require('../Lib/SocketManager');
const UploadMultipart = require('../Lib/UploadMultipart');
const UploadManager = require('../Lib/UploadManager');
const momentTz = require('moment-timezone');
let mongoXlsx = require('mongo-xlsx');
const otpGenerator = require('otp-generator');
const MaidController = require('../Controllers/MaidController');

const adminLogin = function (userData, callback) {
    let tokenToSend = null;
    let responseToSend = {};
    let tokenData = null;
    async.series([
        function (cb) {
            let uniquieAppKey = userData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        function (cb) {
            let getCriteria = {
                email: userData.email,
                password: UniversalFunctions.CryptData(userData.password)
            };
            Service.AdminServices.getAdmin(getCriteria, {}, {}, function (err, data) {
                if (err) {
                    cb({errorMessage: 'DB Error: ' + err})
                } else {
                    if (data && data.length > 0 && data[0].email) {
                        tokenData = {
                            id: data[0]._id,
                            username: data[0].name,
                            type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN
                        };
                        cb()
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS)
                    }
                }
            });
        },

        function (cb) {
            let setCriteria = {
                email: userData.email
            };
            let setQuery = {
                $push: {
                    loginAttempts: {
                        validAttempt: (tokenData != null),
                        ipAddress: userData.ipAddress
                    }
                }
            };
            Service.AdminServices.updateAdmin(setCriteria, setQuery, function (err, data) {
                cb(err, data);
            });
        },

        function (cb) {
            if (tokenData && tokenData.id) {
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        tokenToSend = output && output.accessToken || null;
                        cb();
                    }
                });
            } else {
                cb()
            }
        }
    ], function (err, result) {
        responseToSend = {access_token: tokenToSend, ipAddress: userData.ipAddress};
        if (err) {
            callback(err);
        } else {
            callback(null, responseToSend)
        }
    });
};

const adminLogout = function (token, callback) {
    async.series([
        function (cb) {
            let uniquieAppKey = userData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        function (cb) {
            TokenManager.expireToken(token, function (err, data) {
                if (!err && data == 1) {
                    cb(null, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT);
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_ALREADY_EXPIRED)
                }
            })
        }
    ], function (err, res) {
        callback(err, res)
    })
};

const agencySignUp = function (payloadData, adminData, callback) {
    let dataToInsert = {};
    let responseObject = {};
    let string
    var content
    async.series([
        function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        function (cb) {
            // validate email
            if (!UniversalFunctions.verifyEmailFormat(payloadData.email)) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
            } else {
                dataToInsert.email = payloadData.email;
                cb(null)
            }
        },

        function (cb) {
            //checkEmail in db
            let criteria = {
                email: payloadData.email,
                isDeleted: false,
            };
            Service.MaidServices.getMaid(criteria, {},{lean: true}, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MEMEBR_EMAIL_EXIST);
                    } else {
                        cb(null)
                    }
                }
            });
        },
        function (cb) {
            let criteria = {
                nationalId: payloadData.nationalId,
                isDeleted: false,
            };
            Service.AgencyServices.getAgency(criteria, {},{lean: true}, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.CR_ID_EXIST);
                    } else {
                        cb(null)
                    }
                }
            });
        },
        function (cb) {
            //checkEmail in db
            let criteria = {
                email: payloadData.email,
                isDeleted: false,
            };

            let projection = {};
            let options = {
                lean: true
            };
            Service.AgencyServices.getAgency(criteria, projection, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_EXIST);
                    } else {
                        cb(null)
                    }
                }
            });
        },

        function (cb) {
            if (payloadData.agencyType == "MAK_REGISTERED_UAE" && payloadData.agencyType == "MAK_REGISTERED_BAHRAIN") {
                let criteria = {
                    agencyType: {$in: ["MAK_REGISTERED_UAE", "MAK_REGISTERED_BAHRAIN"]},
                    isDeleted: false,
                };

                let projection = {};
                let options = {
                    lean: true
                };
                Service.AgencyServices.getAgency(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        if (data && data.length) {
                            if (payloadData.agencyType === "MAK_REGISTERED_UAE") {
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.UAE_REGISTERED_AGENCY_EXIST);
                            }
                            else {
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.BAHRAIN_REGISTERED_AGENCY_EXIST);
                            }
                        } else {
                            cb(null)
                        }
                    }
                });
            } else {
                cb(null)
            }
        },
        function (cb) {
            // insert user in db
             string = UniversalFunctions.generateRandomString()
            dataToInsert.agencyName = payloadData.agencyName;
            dataToInsert.agencyOwnerName = payloadData.agencyOwnerName;
            dataToInsert.adminNote = payloadData.adminNote;
            dataToInsert.agencyWebsite = payloadData.agencyWebsite||"";
            dataToInsert.email = payloadData.email;
            dataToInsert.password = UniversalFunctions.CryptData(string);
            dataToInsert.address = payloadData.address;

            dataToInsert.currentLocation = [payloadData.long,payloadData.lat];
            dataToInsert.locationName = payloadData.locationName;
            dataToInsert.radius = payloadData.radius;
            dataToInsert.countryENCode = payloadData.countryENCode;
            dataToInsert.countryCode = payloadData.countryCode;
            dataToInsert.countryName = payloadData.countryName;
            dataToInsert.phoneNo = payloadData.phoneNo;
            dataToInsert.nationalId = payloadData.nationalId;
            dataToInsert.documentPicURL = payloadData.documentPicURL;
            dataToInsert.profilePicURL = payloadData.profilePicURL;
            dataToInsert.agencyType = payloadData.agencyType;
            dataToInsert.isVerified = true;
            dataToInsert.commission = payloadData.commission || process.env.commission;
            dataToInsert.uniquieAppKey = UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME);

            Service.AgencyServices.createAgency(dataToInsert, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result) {
                        responseObject = result;
                        cb(null)
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                    }
                }
            })
        },
        function(cb){
        let data={};
            data.password=string;
            data.email=payloadData.email;
            data.fullName = payloadData.agencyName;
            agencyTemplate(data,(err,result)=>{
            if(err){
                cb(null)
            }
            else{
                content=result;
                cb(null)
            }
        })

    },
        function (cb) {

            // let subject = "Welcome to MAK!!";
            let subject = "MAK - New Agency Registration";
            MailManager.sendMailBySES(payloadData.email, subject, content, function (err, res) {
            });
            cb(null)
        }

    ], function (err, result) {
        callback(err, responseObject)
    })
};

function agencyTemplate(data,callback) {
    let result =   ` 
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style type="text/css">
        
        body,
        #bodyTable {
            height: 100%!important;
            margin: 0;
            padding: 0;
            width: 100%!important
        }

        #bodyTable {
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            min-height: 100%;
            background: #eee;
            color: #333
        }

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0
        }

        img {
            -ms-interpolation-mode: bicubic
        }

        body {
            margin: 0;
            padding: 0;
            background: #eee
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: 0;
            text-decoration: none
        }

        table {
            border-collapse: collapse!important;
            max-width: 100%!important
        }

        img {
            border: 0!important
        }

        h1 {
            font-family: Helvetica;
            font-size: 42px;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            margin: 30px auto 0
        }

        h2 {
            font-size: 32px;
            font-style: normal;
            font-weight: bold;
            margin: 50px auto 0
        }

        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 25px 0;
            letter-spacing: normal;
            text-align: left
        }

        a h3 {
            color: #444!important;
            text-decoration: none!important
        }

        .titleLink {
            text-decoration: none!important
        }

        .preheaderContent {
            color: #808080;
            font-size: 10px;
            line-height: 125%
        }

        .preheaderContent a:link,
        .preheaderContent a:visited,
        .preheaderContent a .yshortcuts {
            color: #606060;
            font-weight: normal;
            text-decoration: underline
        }

        #emailHeader,
        #tacoTip {
            color: #fff
        }

        #emailHeader {
            background-color: #fff
        }

        #content p {
            color: #4d4d4d;
            margin: 20px 70px 30px;
            font-size: 24px;
            line-height: 32px;
            text-align: center
        }

        #button {
            display: inline-block;
            margin: 10px auto;
            background: #fff;
            border-radius: 4px;
            font-weight: bold;
            font-size: 18px;
            padding: 15px 20px;
            cursor: pointer;
            color: #0079bf;
            margin-bottom: 50px
        }

        #socialIconWrap img {
            line-height: 35px!important;
            padding: 0 5px
        }

        .footerContent div {
            color: #707070;
            font-family: Arial;
            font-size: 12px;
            line-height: 125%;
            text-align: center;
            max-width: 100%!important
        }

        .footerContent div a:link,
        .footerContent div a:visited {
            color: #369;
            font-weight: normal;
            text-decoration: underline
        }

        .footerContent img {
            display: inline
        }

        #socialLinks img {
            margin: 0 2px
        }

        #utility {
            border-top: 1px solid #ddd
        }

        #utility div {
            text-align: center
        }

        #monkeyRewards img {
            max-width: 160px
        }

        #emailFooter {
            max-width: 100%!important
        }

        #footerTwitter a,
        #footerFacebook a {
            text-decoration: none!important;
            color: #fff!important;
            font-size: 14px
        }

        #emailButton {
            border-radius: 6px;
            background: #70b500!important;
            margin: 0 auto 60px;
            box-shadow: 0 4px 0 #578c00
        }

        #socialLinks a {
            width: 40px
        }

        #socialLinks #blogLink {
            width: 80px!important
        }

        .sectionWrap {
            text-align: center
        }

        #header {
            color: #fff!important
        }

    </style>
</head>

<body>
    <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px">
                                        <tbody>
                                            <tr>
                                                <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="http://52.36.127.111/mak_web/img/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120">
                                                    <h1 style="font-size: 30px;">Welcome to MAK</h1>
                                                    <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="background:#FFFFFF;">
                                                    <br>
                                                    Dear <b>${data.fullName}</b><br> <br>Congratulations!! You are successfully registered with MAK. 
 
<br><br>
Please login into the Agency Panel using below details:<br><br>
 

Link : <a href:'https://mak.today/MAK/AgencyPanel/#/login'>https://mak.today/MAK/AgencyPanel/#/login</a><br><br>
Email : ${data.email}<br><br>
Password : ${data.password}
                                                    </p>
                                                </td>
                                            </tr>
                                       
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px">
                                        <tbody>
                                            <tr>
                                                <td class="footerContent" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0" style="width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">&nbsp;
                                                                    <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

`
    callback(null,result)
}

const updateAgencyProfile = function (payloadData, userData, callback) {
    let response = {};

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        updateAgencyInfo: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.agencyId,
                isDeleted: false,
            };
            let dataToUpdate = {};

            if (payloadData.agencyName) {
                dataToUpdate.agencyName = payloadData.agencyName;
            }

            if (payloadData.agencyOwnerName) {
                dataToUpdate.agencyOwnerName = payloadData.agencyOwnerName;
            }
            if (payloadData.adminNote) {
                dataToUpdate.adminNote = payloadData.adminNote;
            }

            if (payloadData.agencyWebsite) {
                dataToUpdate.agencyWebsite = payloadData.agencyWebsite;
            }

            if (payloadData.address) {
                dataToUpdate.address = payloadData.address
            }

            if (payloadData.nationalId) {
                dataToUpdate.nationalId = payloadData.nationalId
            }

            if (payloadData.countryENCode && payloadData.countryCode && payloadData.phoneNo) {
                dataToUpdate.countryENCode = payloadData.countryENCode;
                dataToUpdate.countryCode = payloadData.countryCode;
                dataToUpdate.phoneNo = payloadData.phoneNo;
            }

            if (payloadData.profilePicURL) {
                dataToUpdate.profilePicURL = payloadData.profilePicURL;
            }

            if (payloadData.documentPicURL) {
                dataToUpdate.documentPicURL = payloadData.documentPicURL;
            }
            if (payloadData.radius) {
                dataToUpdate.radius = payloadData.radius;
            }

            let option = {new: true};
            Service.AgencyServices.updateAgency(criteria, dataToUpdate, option, function (err, data) {
                if (err) {
                    cb(err)
                }
                else {
                    if (data && data._id) {

                        delete data.__v;
                        delete data.password;
                        delete data.uniquieAppKey;
                        response = data;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        updateCommission:function (checkForUniqueKey, cb) {
            console.log("*********** enter in update commission");
            if(payloadData.commission){
                console.log("*********** enter in update commission second");
                let payload={
                    uniquieAppKey:payloadData.uniquieAppKey,
                    agencyId:payloadData.agencyId,
                    commission:payloadData.commission
                };
                addAndUpdateCommissionFeeForTheAgency({},payload,function (err, result) {
                    if(err){
                        cb(err)
                    }else{
                        cb(null)
                    }
                })
            }else{
                cb(null)
            }
        }

    }, function (err, result) {
        callback(err, response)
    })
};

const addAndUpdateCommissionFeeForTheAgency = function (userData, payloadData, callback) {
    console.log("********** enter in update commision function ****************")
    let maidData = [];
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        updateAgencyInfo: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.agencyId,
                isDeleted: false,
            };
            let dataToUpdate = {
                commission: payloadData.commission,
            };

            let option = {new: true};
            Service.AgencyServices.updateAgency(criteria, dataToUpdate, option, function (err, data) {
                if (err) {
                    cb(err)
                }
                else {
                    if (data && data._id) {
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getMaidsData: function (updateAgencyInfo, cb) {
            console.log("********** enter in update getMaidsData  ****************")
            let criteria = {
                agencyId: payloadData.agencyId,
                isDeleted: false
            };

            let option = {lean: true};            
            console.log("********** enter in get criteria2222  ****************"+JSON.stringify(criteria))
            Service.MaidServices.getMaid(criteria, {}, option, function (err, data) {
                // console.log("********** enter in get getMaidsData2222  ****************"+JSON.stringify(data))
                if (err) {
                    cb(err)
                }
                else {
                    maidData = data;
                    // console.log("********** maidData ****************"+JSON.stringify(data))
                    if (data && data._id) {
                      
                        console.log("********** maidData33333 ****************"+JSON.stringify(data))
                        maidData = data;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        updateMaidInfo: function (getMaidsData, cb) {
            console.log("********** enter in update updateMaidInfo  ****************"+JSON.stringify(getMaidsData))
            
            
            async.each(maidData, function (obj, cb1) {
                let criteria = {
                    _id: obj._id,
                    isDeleted: false
                };

                let dataToUpdate = {
                    commission: payloadData.commission,
                    actualPrice: (obj.price + (obj.price * payloadData.commission / 100)),
                    makPrice: ((obj.price + (obj.price * payloadData.commission / 100)) - obj.price)
                };
                console.log("******* update maid criteria ********"+JSON.stringify(criteria));
                console.log("******* update maid info ********"+JSON.stringify(dataToUpdate));
                let option = {new: true};
                Service.MaidServices.updateMaid(criteria, dataToUpdate, option, function (err, data) {
                    if (err) {
                        cb1(err)
                    }
                    else {
                        if (data && data._id) {
                            cb1(null)
                        } else {
                            cb1(null)
                        }
                    }
                })
            }, function (e1, r1) {
                if (e1) {
                    cb(e1)
                } else {
                    cb(null)
                }
            })

        },
    }, function (err, result) {
        callback(err, {})
    })
};

const getAllLanguages = function (payloadData, callback) {
    let response = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getLanguageData: function (checkForUniqueKey, cb) {
            let criteria = {
                isDeleted: false,
            };

            Service.LanguageServices.getLanguage(criteria, {
                __v: 0,
                uniquieAppKey: 0
            }, {lean: true}, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    response = result;
                    cb(null);
                }
            })
        }
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, response)
        }
    })
};

const getAllAgency = function (payloadData, callback) {

    let response = {};
    let criteria ={}
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        listAgency: function (checkForUniqueKey, cb) {
             criteria = {
                isDeleted: false,
                // isBlocked:false
            };
            if(payloadData.agencyId){
                criteria._id=payloadData.agencyId
            }
            if(payloadData.searchAgencyName){
                criteria ={
                    $or:[{agencyName:new RegExp(payloadData.searchAgencyName,'i')},
                        {agencyOwnerName:new RegExp(payloadData.searchAgencyName,'i')},
                        {phoneNo:new RegExp(payloadData.searchAgencyName,'i')},
                        {nationalId:new RegExp(payloadData.searchAgencyName,'i')},
                        ]
                }
            }
            if (payloadData.searchByCountryName) {
                criteria.countryName = {$regex: payloadData.searchByCountryName, $options: "si"}
            }

            let projection = {__v: 0, uniquieAppKey: 0};
            let options = {
                lean: true,
                sort:{_id:-1}
            };

            if(payloadData.pageNo && payloadData.limit){
                options.skip=(((payloadData.pageNo) - 1) * payloadData.limit)
                options.limit=payloadData.limit
            }
            Service.AgencyServices.getAgency(criteria, projection, options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },
        getCount: function (listAgency, cb) {

            Service.AgencyServices.count(criteria, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    response.count = result;
                    cb()
                }
            })
        },

    }, function (err, result) {
        callback(err, response)
    });
};

const getAllReligion = function (payloadData, callback) {

    let response = {};
    let criteria ={}
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        listAgency: function (checkForUniqueKey, cb) {
             criteria = {
                isDeleted: false,
                // isBlocked:false
            };
             if(payloadData.search) criteria.name = new RegExp(payloadData.search,'i')
             let option={
                 lean:true,
             }
             if(payloadData.skip)option.skip = payloadData.skip;
             if(payloadData.limit)option.limit = payloadData.limit;
            Service.ReligionServices.getReligion(criteria, {},option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },
        getCount: function (listAgency, cb) {
            Service.ReligionServices.count(criteria, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    response.count = result;
                    cb()
                }
            })
        },

    }, function (err, result) {
        callback(err, response)
    });
};

const addEditReligion = function (payloadData, userData,callback) {

    let response = {};
    let criteria ={}
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        checkReligion: function (checkForUniqueKey, cb) {
            let criteria ={};
            if(payloadData.religionId) {
                criteria.languageName = payloadData.name;
                criteria._id={$ne:payloadData.religionId}
            }
            else  criteria.languageName = payloadData.name;
            Service.LanguageServices.getLanguage(criteria, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                     if (result && result.length) {
                         cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.RELIGION)
                     } else {
                         cb(null)
                     }
                }
            })
        },
        addReligion: function (checkForUniqueKey,checkReligion, cb) {
            let criteria ={},dataToAdd={};
            dataToAdd.languageName = payloadData.name;
            if(payloadData.religionId) {
                criteria._id=payloadData.religionId;

                Service.LanguageServices.updateLanguage(criteria,dataToAdd,{} ,function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb()
                    }
                })
            }
            else {
                dataToAdd.uniquieAppKey = payloadData.uniquieAppKey
                Service.LanguageServices.createLanguage(dataToAdd, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb()
                    }
                })
            }
        }
    }, function (err, result) {
        callback(err, response)
    });
};


const blockAgencyByAdmin = function (userData, payloadData, callback) {
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkIfAgencyCanBeBlocked: function (checkForUniqueKey, cb) {
            let criteria = {
                agencyId: payloadData.agencyId,
                isDeleted: false,
            };
            let projection = {__v: 0};
            let options = {
                lean: true,
            };
            Service.MaidServices.getMaid(criteria, projection, options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        // callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.CANNOT_BLOCK_AGENCY)
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },
        disableMaids: function (checkForUniqueKey, cb) {
            let dataToUpdate = {};
            if (payloadData.action == "BLOCK") {
                dataToUpdate.isBlocked = true
            } else {
                dataToUpdate.isBlocked = false
            }

            let criteria = {
                agencyId: payloadData.agencyId,
                // isDeleted: false,
            };          
          
          
            let option = {};
            Service.MaidServices.updateAll(criteria, dataToUpdate, option, function (err, data) {
                if (err) {
                    cb(err)
                }
                else {
                    if (data && data._id) {
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
           
        },
        blockAndUnblockUserInDb: function (checkIfAgencyCanBeBlocked, cb) {
            let criteria = {
                _id: payloadData.agencyId
            };
            let dataToUpdate = {};
            if (payloadData.action == "BLOCK") {
                dataToUpdate.isBlocked = true
            } else {
                dataToUpdate.isBlocked = false
            }
            let option = {new: true};
            Service.AgencyServices.updateAgency(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        }

    }, function (err, result) {
        if (payloadData.action == "BLOCK") {
            callback(err, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.BLOCKED)
        } else {
            callback(err, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT)
        }
    })
};


const blockAgencyByAdminOld = function (userData, payloadData, callback) {
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkIfAgencyCanBeBlocked: function (checkForUniqueKey, cb) {
            let criteria = {
                agencyId: payloadData.agencyId,
                isDeleted: false,
            };
            let projection = {__v: 0};
            let options = {
                lean: true,
            };
            Service.MaidServices.getMaid(criteria, projection, options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.CANNOT_BLOCK_AGENCY)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        blockAndUnblockUserInDb: function (checkIfAgencyCanBeBlocked, cb) {
            let criteria = {
                _id: payloadData.agencyId
            };
            let dataToUpdate = {};
            if (payloadData.action == "BLOCK") {
                dataToUpdate.isBlocked = true
            } else {
                dataToUpdate.isBlocked = false
            }
            let option = {new: true};
            Service.AgencyServices.updateAgency(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        }

    }, function (err, result) {
        if (payloadData.action == "BLOCK") {
            callback(err, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.BLOCKED)
        } else {
            callback(err, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT)
        }
    })
};

const getAllCustomerDetails = function (userData, payloadData, callback) {
    let response = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getUsersCount: function (checkForUniqueKey, cb) {
            let finalPipeline = [];
            let pipeline = [];

            if (payloadData.searchCustomerName) {
                pipeline.push({
                    $match: {$or:[
                        {fullName: {$regex: payloadData.searchCustomerName, $options: "si"}},
                        {phoneNo: {$regex: payloadData.searchCustomerName, $options: "si"}},
                        {nationalId: {$regex: payloadData.searchCustomerName, $options: "si"}},
                    ]},
                })
            }

            if (payloadData.searchByCountryName) {
                pipeline.push({
                    $match: {"usersAddress.country": {$regex: payloadData.searchByCountryName, $options: "si"}}
                })
            }

            if(payloadData.userType===2) {
                pipeline.push({
                    $match: {"isGuestFlag": true}
                });
            }
            else {
                pipeline.push({
                    $match: {"isGuestFlag": false}
                });
            }

            let pipeline2 = [
                {
                    $match: {
                        isDeleted: false,
                        isDeactivated: false,
                    }
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "userId",
                        as: "serviceData",
                        restrictSearchWithMatch: {
                            $or: [
                                {agencyAction: {$ne: Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE}},
                                {deleteAction: {$ne: Config.APP_CONSTANTS.DATABASE.ACTION.DELETED}}
                            ]
                        }
                    }
                },
                {
                    $addFields: {
                        totalService: {$size: "$serviceData"}
                    }
                },
               /* {
                    $project: {
                        serviceData: 0
                    }
                },*/
            ];

            if (payloadData.searchCustomerName || payloadData.searchByCountryName || payloadData.userType) {
                finalPipeline = pipeline.concat(pipeline2)
            } else {
                finalPipeline = pipeline2
            }

            Models.Users.aggregate(finalPipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        response.count = 0;
                        cb(null)
                    }
                }
            })
        },

        listUsers: function (checkForUniqueKey, cb) {
            let finalPipeline = [];
            let pipeline = [];

            if (payloadData.searchCustomerName) {
                pipeline.push({
                    $match: {$or:[
                        {fullName: {$regex: payloadData.searchCustomerName, $options: "si"}},
                        {phoneNo: {$regex: payloadData.searchCustomerName, $options: "si"}},
                        {nationalId: {$regex: payloadData.searchCustomerName, $options: "si"}},
                    ]},
                })
            }
            if (payloadData.searchByCountryName) {
                pipeline.push({
                    $match: {"usersAddress.country": {$regex: payloadData.searchByCountryName, $options: "si"}}
                })
            }
            if(payloadData.userType===2) {
                pipeline.push({
                    $match: {"isGuestFlag": true}
                });
            }
            else {
                pipeline.push({
                    $match: {"isGuestFlag": false}
                });
            }

            let pipeline2 = [
                {
                    $match: {
                        isDeleted: false,
                        isDeactivated: false,
                    }
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "userId",
                        as: "serviceData",
                        restrictSearchWithMatch: {}
                    }
                },
                {
                    $addFields: {
                        totalService: {
                            $size: {
                                $filter: {
                                    input: "$serviceData",
                                    as: "item",
                                    cond: {$eq: ["$$item.agencyAction", Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT]}
                                }
                            }
                        },
                        totalExtendedService: {
                            $size: {
                                $filter: {
                                    input: "$serviceData",
                                    as: "item",
                                    cond: {$eq: ["$$item.isExtend.requested", true]}
                                }
                            }
                        },
                        totalCanceledService: {
                            $size: {
                                $filter: {
                                    input: "$serviceData",
                                    as: "item",
                                    cond: {
                                        $or: [
                                            {$eq: ["$$item.agencyAction", Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                                            {$eq: ["$$item.deleteAction", Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                                        ]
                                    }
                                }
                            }
                        }
                    }
                },
                /*{
                    $project: {
                        serviceData: 0
                    }
                },*/
                // {$skip: ((payloadData.pageNo - 1) * payloadData.limit)},
                // {$limit: payloadData.limit}
            ];
            if(payloadData.pageNo && payloadData.limit){
                pipeline2.push({
                    $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                });
                pipeline2.push({
                    $limit: payloadData.limit
                })
            }

            if (payloadData.searchCustomerName || payloadData.searchByCountryName|| payloadData.userType) {
                finalPipeline = pipeline.concat(pipeline2)
            }
            else {
                finalPipeline = pipeline2
            }

            Models.Users.aggregate(finalPipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    } else {
                        response.data = [];
                        cb(null)
                    }
                }
            })
        },

    }, function (err, result) {
        callback(err, response)
    })
};

const blockUserByAdmin = function (userData, payloadData, callback) {
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        blockAndUnblockUserInDb: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.userId
            };
            let dataToUpdate = {};
            if (payloadData.action == "BLOCK") {
                dataToUpdate.isBlocked = true
            } else {
                dataToUpdate.isBlocked = false
            }
            let option = {new: true};
            Service.UserServices.updateUser(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        }
    }, function (err, result) {
        if (payloadData.action == "BLOCK") {
            callback(err, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.BLOCKED)
        } else {
            callback(err, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT)
        }
    })
};

const getAllMaidDetails = function (userData, payloadData, callback) {
    let response = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMaidCount: function (checkForUniqueKey, cb) {
            let finalPipeline = [];
            let pipeline = [];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        $or: [
                            {firstName: {$regex: payloadData.search, $options: "si"}},
                            {lastName: {$regex: payloadData.search, $options: "si"}},
                            {fullName: {$regex: payloadData.search, $options: "si"}},
                            {nationality: {$regex: payloadData.search, $options: "si"}},
                            {makId: {$regex: payloadData.search, $options: "si"}},
                            {religion: {$regex: payloadData.search, $options: "si"}},
                            {phoneNo: {$regex: payloadData.search, $options: "si"}},
                            {nationalId: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                })
            }

            if(!payloadData.isRejected)
                pipeline.push({
                    $match: {rejectReason :{$eq:''}}
                })

            if (payloadData.searchByCountryName) {
                pipeline.push({
                    $match: {countryName: {$regex: payloadData.searchByCountryName, $options: "si"}}
                })
            } 
            if (payloadData.searchNationality) {
                pipeline.push({
                    $match: {nationality: {$regex: payloadData.searchNationality, $options: "si"}}
                })
            }
            if (payloadData.agencyId) {
                pipeline.push({
                    $match: {agencyId:mongoose.Types.ObjectId(payloadData.agencyId)}
                })
            }
           if (payloadData.experience) {
                        pipeline.push({
                            $match: {experience:payloadData.experience}
                        })
                    }

            let pipeline2 = [
                {
                    $match: {
                        isBlocked: false,
                        isDeleted: false,
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                    }
                },
                {
                    $unwind: {path: "$reviewData", preserveNullAndEmptyArrays: true}
                },
                {
                    $addFields: {
                        maidRating: "$reviewData.maidRating",
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        cleaningRating: {$sum: "$maidRating.cleaning"},
                        ironingRating: {$sum: "$maidRating.ironing"},
                        cookingRating: {$sum: "$maidRating.cooking"},
                        childCareRating: {$sum: "$maidRating.childCare"},

                        cleaningCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cleaning", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        ironingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.ironing", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        cookingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cooking", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        childCareCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.childCare", 0]},
                                    0,
                                    1
                                ]
                            }
                        }
                    }
                },
                {
                    $project: {
                        _id:0,
                        maidId: "$_id",
                        avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                        avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                        avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                        avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $project: {
                        avgCleaning:1,
                        avgIroning:1,
                        avgCooking:1,
                        avgChildCare:1,
                        _id: "$maidId._id",
                        firstName: "$maidId.firstName",
                        radius: "$maidId.radius",
                        lastName: "$maidId.lastName",
                        makId: "$maidId.makId",
                        rating: "$maidId.rating",
                        gender: "$maidId.gender",
                        email: "$maidId.email",
                        country: "$maidId.country",
                        accessToken: "$maidId.accessToken",
                        actualPrice: "$maidId.actualPrice",
                        agencyId: "$maidId.agencyId",
                        commission: "$maidId.commission",
                        countryCode: "$maidId.countryCode",
                        countryENCode: "$maidId.countryENCode",
                        countryName: "$maidId.countryName",
                        currentLocation: "$maidId.currentLocation",
                        description: "$maidId.description",
                        deviceToken: "$maidId.deviceToken",
                        deviceType: "$maidId.deviceType",
                        dob: "$maidId.dob",
                        documentPicURL: "$maidId.documentPicURL",
                        experience: "$maidId.experience",
                        isBlocked: "$maidId.isBlocked",
                        isDeleted: "$maidId.isDeleted",
                        isLogin: "$maidId.isLogin",
                        languages: "$maidId.languages",
                        makPrice: "$maidId.makPrice",
                        nationality: "$maidId.nationality",
                        nationalityFlag: "$maidId.nationalityFlag",
                        phoneNo: "$maidId.phoneNo",
                        price: "$maidId.price",
                        profilePicURL: "$maidId.profilePicURL",
                        registrationDate: "$maidId.registrationDate",
                        religion: "$maidId.religion",
                        step: "$maidId.step",
                        timeSlot: "$maidId.timeSlot",
                        timeZone: "$maidId.timeZone",
                    }
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "serviceData",
                        restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                    }
                },
                {
                    $addFields:{
                        totalService:{$size:"$serviceData"}
                    }
                },
                {
                    $project:{
                        serviceData:0,
                    }
                },
            ];
            if (payloadData.search || payloadData.searchNationality || payloadData.experience|| payloadData.searchByCountryName || payloadData.agencyId) {
                finalPipeline = pipeline.concat(pipeline2)
            }
            else {
                finalPipeline = pipeline2
            }

            Models.Maids.aggregate(finalPipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        response.count = 0;
                        cb(null)
                    }
                }
            })
        },

        listMaids: function (checkForUniqueKey, cb) {
            let finalPipeline = [];
            let pipeline = [];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        $or: [
                            {firstName: {$regex: payloadData.search, $options: "si"}},
                            {fullName: {$regex: payloadData.search, $options: "si"}},
                            {lastName: {$regex: payloadData.search, $options: "si"}},
                            {nationality: {$regex: payloadData.search, $options: "si"}},
                            {makId: {$regex: payloadData.search, $options: "si"}},
                            {religion: {$regex: payloadData.search, $options: "si"}},
                            {phoneNo: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                })
            }
            if(!payloadData.isRejected)
                pipeline.push({
                    $match: {rejectReason :{$eq:''}}
                })

            if (payloadData.searchByCountryName) {
                pipeline.push({
                    $match: {countryName: {$regex: payloadData.searchByCountryName, $options: "si"}}
                })
            }

            if (payloadData.searchNationality) {
                pipeline.push({
                    $match: {nationality: {$regex: payloadData.searchNationality, $options: "si"}}
                })
            }
            if (payloadData.agencyId) {
                pipeline.push({
                    $match: {agencyId:mongoose.Types.ObjectId(payloadData.agencyId)}
                })
            }
            if (payloadData.experience) {
                pipeline.push({
                    $match: {experience:payloadData.experience}
                })
            }
            let pipeline2 = [
                {
                    $match: {
                        isBlocked: false,
                        isDeleted: false,
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                    }
                },
                {
                    $unwind: {path: "$reviewData", preserveNullAndEmptyArrays: true}
                },
                {
                    $addFields: {
                        maidRating: "$reviewData.maidRating",
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        cleaningRating: {$sum: "$maidRating.cleaning"},
                        ironingRating: {$sum: "$maidRating.ironing"},
                        cookingRating: {$sum: "$maidRating.cooking"},
                        childCareRating: {$sum: "$maidRating.childCare"},

                        cleaningCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cleaning", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        ironingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.ironing", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        cookingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cooking", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        childCareCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.childCare", 0]},
                                    0,
                                    1
                                ]
                            }
                        }
                    }
                },
                {
                    $project: {
                        _id:0,
                        maidId: "$_id",
                        avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                        avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                        avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                        avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $project: {
                        avgCleaning:1,
                        avgIroning:1,
                        avgCooking:1,
                        avgChildCare:1,
                        _id: "$maidId._id",
                        radius: "$maidId.radius",
                        firstName: "$maidId.firstName",
                        lastName: "$maidId.lastName",
                        makId: "$maidId.makId",
                        rating: "$maidId.rating",
                        gender: "$maidId.gender",
                        acceptAgreement: "$maidId.acceptAgreement",
                        email: "$maidId.email",
                        country: "$maidId.country",
                        accessToken: "$maidId.accessToken",
                        actualPrice: "$maidId.actualPrice",
                        agencyId: "$maidId.agencyId",
                        commission: "$maidId.commission",
                        countryCode: "$maidId.countryCode",
                        countryENCode: "$maidId.countryENCode",
                        countryName: "$maidId.countryName",
                        currentLocation: "$maidId.currentLocation",
                        description: "$maidId.description",
                        deviceToken: "$maidId.deviceToken",
                        deviceType: "$maidId.deviceType",
                        dob: "$maidId.dob",
                        documentPicURL: "$maidId.documentPicURL",
                        experience: "$maidId.experience",
                        isBlocked: "$maidId.isBlocked",
                        isDeleted: "$maidId.isDeleted",
                        isLogin: "$maidId.isLogin",
                        languages: "$maidId.languages",
                        makPrice: "$maidId.makPrice",
                        nationality: "$maidId.nationality",
                        nationalityFlag: "$maidId.nationalityFlag",
                        phoneNo: "$maidId.phoneNo",
                        price: "$maidId.price",
                        profilePicURL: "$maidId.profilePicURL",
                        registrationDate: "$maidId.registrationDate",
                        religion: "$maidId.religion",
                        step: "$maidId.step",
                        nationalId: "$maidId.nationalId",
                        timeSlot: "$maidId.timeSlot",
                        timeZone: "$maidId.timeZone",
                        maidAddress: "$maidId.maidAddress",
                    }
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "serviceData",
                        restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                    }
                },
                {
                    $addFields:{
                        totalService:{$size:"$serviceData"}
                    }
                },
                {
                    $project:{
                        serviceData:0,
                    }
                },
                // {$skip: ((payloadData.pageNo - 1) * payloadData.limit)},
                // {$limit: payloadData.limit}
            ];

            if(payloadData.pageNo && payloadData.limit){
                pipeline2.push({
                    $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                });
                pipeline2.push({
                    $limit: payloadData.limit
                })
            }

            if (payloadData.search || payloadData.searchNationality ||payloadData.experience|| payloadData.searchByCountryName || payloadData.agencyId) {
                finalPipeline = pipeline.concat(pipeline2)
            }
            else {
                finalPipeline = pipeline2
            }

            Models.Maids.aggregate(finalPipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        Models.Agency.populate(result, [
                            {
                                path: 'agencyId',
                                select: '_id agencyName',
                                model: 'Agency'
                            },
                            {
                                path: 'languages',
                                select: '_id languageName',
                                model: 'Languages'
                            }
                        ], function (err1, populatedTransactions) {
                            if (err1) {
                                cb(err1)
                            }
                            else {
                                response.data = populatedTransactions;
                                cb(null)
                            }
                        })
                    } else {
                        response.data = [];
                        cb(null)
                    }
                }
            })
        },

    }, function (err, result) {
        callback(err, response)
    })
};

const listAllServiceByAdmin = function (userData, payloadData, callback) {
    let response = {};
    let maidId = [];
    let userId = [];
    let agencyId = [];

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMaidId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let criteria = {
                    $or: [
                        {firstName: {$regex: payloadData.search, $options: "si"}},
                        {lastName: {$regex: payloadData.search, $options: "si"}}
                    ],
                    agencyId: userData._id
                };
                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.MaidServices.getMaid(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            maidId = result.map(obj => {
                                return obj._id
                            });
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getUserId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let nameArray = payloadData.search.split(" ");
                let criteria = {};
                let a = [];
                if (nameArray.length) {
                    nameArray.map(obj => {
                        let objToPush = {
                            fullName: {$regex: obj, $options: "si"},
                           // phoneNo: {$regex: obj, $options: "si"}
                        };
                        a.push(objToPush)
                    })
                }
                criteria.$or = a;
                criteria.agencyId = userData._id;
                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.UserServices.getUsers(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            userId = result.map(obj => {
                                return obj._id
                            });

                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getAgencyId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let nameArray = payloadData.search.split(" ");
                let criteria = {};
                let a = [];
                if (nameArray.length) {
                    nameArray.map(obj => {
                        let objToPush = {
                            agencyName: {$regex: obj, $options: "si"}
                        };
                        a.push(objToPush)
                    })
                }
                criteria.$or = a;
                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.AgencyServices.getAgency(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            agencyId = result.map(obj => {
                                return obj._id
                            });

                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        listServices: function (getMaidId, getUserId, cb) {
            let criteria = {
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ]
            };

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    criteria.maidId = {$in: maidId};
                }
                if (userId && userId.length) {
                    criteria.userId = {$in: userId};
                }
                if (agencyId && agencyId.length) {
                    criteria.agencyId = {$in: agencyId};
                }
                if (!(maidId && maidId.length) && !(userId && userId.length) && !(agencyId && agencyId.length)) {
                    criteria.maidId = {$in: []};
                    criteria.userId = {$in: []};
                    criteria.agencyId = {$in: []};
                }
            }
            if(payloadData.userId) criteria.userId = payloadData.userId;

            if (payloadData.searchByCountryName) {
                criteria.$and.push({"address.country":{$regex: payloadData.searchByCountryName, $options: "si"}})
            }


            if (payloadData.onBasisOfDate == "ON_GOING") {
                criteria.$or = [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT}
                        ]
                    }
                ];
                criteria.startTime = {$lte: (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000};
                criteria.endTime = {$gte: (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000};
                criteria.isCompleted = false;
            }
            else if (payloadData.onBasisOfDate == "UPCOMING") {
                criteria.$or = [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                                Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]}}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                                Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]}}
                        ]
                    }
                ];
                criteria.startTime = {$gte: (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000};
                criteria.isCompleted = false;
            }
            else if (payloadData.onBasisOfDate == "COMPLETED") {
                // criteria.agencyAction = Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT;
                criteria.agencyAction = {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]};
                criteria.isCompleted = true;
            }

            let projection = {};
            let option = {
                lean: true,
            };
            if(payloadData.pageNo && payloadData.limit){
                option.skip=(((payloadData.pageNo) - 1) * payloadData.limit)
                option.limit=payloadData.limit
            }

            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo nationalId email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName nationalId makId',
                    option: {}
                },
                {
                    path: 'agencyId',
                    match: {},
                    select: '_id agencyName email phoneNo',
                    option: {},
                },
            ];
            console.log("_______________criteria fist________", criteria, userId);

            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("_______________result count "+result.length);
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    } else {
                        response.data = [];
                        cb(null)
                    }

                }
            })
        },

        listServicesCount: function (getMaidId, getUserId, cb) {
            let criteria = {
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ]
            };
            if(payloadData.userId) criteria.userId = payloadData.userId;

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    criteria.maidId = {$in: maidId};
                }
                if (userId && userId.length) {
                    criteria.userId = {$in: userId};
                }
                if (agencyId && agencyId.length) {
                    criteria.agencyId = {$in: agencyId};
                }
                if (!(maidId && maidId.length) && !(userId && userId.length) && !(agencyId && agencyId.length)) {
                    criteria.maidId = {$in: []};
                    criteria.userId = {$in: []};
                    criteria.agencyId = {$in: []};
                }
            }

            if (payloadData.searchByCountryName) {
                criteria.$and.push({"address.country":{$regex: payloadData.searchByCountryName, $options: "si"}})
            }

            if (payloadData.onBasisOfDate == "ON_GOING") {
                criteria.$or = [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT}
                        ]
                    }
                ];
                criteria.startTime = {$lte: (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000};
                criteria.endTime = {$gte: (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000};
                criteria.isCompleted = false;
            }
            else if (payloadData.onBasisOfDate == "UPCOMING") {
                criteria.$or = [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]}}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]}}
                        ]
                    }
                ];
                criteria.startTime = {$gte: (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000};
                criteria.isCompleted = false;
            }
            else if (payloadData.onBasisOfDate == "COMPLETED") {
                criteria.agencyAction = Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT;
                criteria.isCompleted = true;
            }

            let projection = {};
            let option = {
                lean: true,
                skip: ((payloadData.pageNo - 1) * payloadData.limit),
                limit: payloadData.limit
            };
            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName makId',
                    option: {}
                },
                {
                    path: 'agencyId',
                    match: {},
                    select: '_id agencyName email phoneNo',
                    option: {},
                },
            ];

            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        response.count = 0;
                        cb(null)
                    }

                }
            })
        }

    }, function (err, result) {
        callback(err, response)
    });
};

const listAllCanceledServiceAdmin = function (payloadData, userData, callback) {

    let response = {};
    let maidId = [];
    let userId = [];
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMaidId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let criteria = {
                    $or: [
                        {firstName: {$regex: payloadData.search, $options: "si"}},
                        {lastName: {$regex: payloadData.search, $options: "si"}}
                    ],
                };
                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.MaidServices.getMaid(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            maidId = result.map(obj => {
                                return mongoose.Types.ObjectId(obj._id)
                            });
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getUserId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let nameArray = payloadData.search.split(" ");
                let criteria = {};
                let a = [];
                if (nameArray.length) {
                    nameArray.map(obj => {
                        let objToPush = {
                            fullName: {$regex: obj, $options: "si"}
                        }
                        a.push(objToPush)
                    })
                }
                criteria.$or = a;
                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.UserServices.getUsers(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            userId = result.map(obj => {
                                return mongoose.Types.ObjectId(obj._id)
                            });

                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        listServices: function (getMaidId, getUserId, cb) {

            let criteria = {
                deleteAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                isCompleted: false,
                deleteFromList:false,
            };

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    criteria.maidId = {$in: maidId};
                }
                if (userId && userId.length) {
                    criteria.userId = {$in: userId};
                }
                if (!(maidId && maidId.length) && !(userId && userId.length)) {
                    criteria.maidId = {$in: []};
                    criteria.userId = {$in: []};
                }
            }

            if (payloadData.countryName) {
                criteria.$and=[{"address.country":{$regex: payloadData.countryName, $options: "si"}}]
            }
            let projection = {};
            let option = {
                lean: true,
            };
            if(payloadData.pageNo && payloadData.limit){
                option.skip=(((payloadData.pageNo) - 1) * payloadData.limit)
                option.limit=payloadData.limit
            }

            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo nationalId email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName nationalId makId',
                    option: {}
                },
                {
                    path: 'agencyId',
                    match: {},
                    select: '_id agencyName email',
                    option: {}
                }
            ];
            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    } else {
                        response.data = [];
                        cb(null)
                    }

                }
            })
        },
        listServicesCount: function (getMaidId, getUserId, cb) {

            let criteria = {
                deleteAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                isCompleted: false,
                deleteFromList:false,
            };

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    criteria.maidId = {$in: maidId};
                }
                if (userId && userId.length) {
                    criteria.userId = {$in: userId};
                }
                if (!(maidId && maidId.length) && !(userId && userId.length)) {
                    criteria.maidId = {$in: []};
                    criteria.userId = {$in: []};
                }
            }

            if (payloadData.countryName) {
                criteria.$and=[{"address.country":{$regex: payloadData.countryName, $options: "si"}}]
            }

            let projection = {};
            let option = {
                lean: true,
            };
            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName makId',
                    option: {}
                }
            ];

            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        response.count = 0;
                        cb(null)
                    }

                }
            })
        }
    }, function (err, result) {
        callback(err, response)
    });
};

const revenueReportAdmin = function (payloadData, userData, callback) {
    let response = {
        service: {
            data: [],
            count: 0
        },
        totalRevenue: 0,
    };
    let maidId = [];
    let userId = [];
    let offset = momentTz.tz(payloadData.timeZone).utcOffset() * 60 * 1000;
    // let offset = '';
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMaidId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let criteria = {
                    $or: [
                        {firstName: {$regex: payloadData.search, $options: "si"}},
                        {lastName: {$regex: payloadData.search, $options: "si"}}
                    ],
                };
                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.MaidServices.getMaid(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            maidId = result.map(obj => {
                                return mongoose.Types.ObjectId(obj._id)
                            });
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getUserId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let nameArray = payloadData.search.split(" ");
                let criteria = {};
                let a = [];
                if (nameArray.length) {
                    nameArray.map(obj => {
                        let objToPush = {
                            fullName: {$regex: obj, $options: "si"}
                        }
                        a.push(objToPush)
                    })
                }
                criteria.$or = a;

                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.UserServices.getUsers(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            userId = result.map(obj => {
                                return mongoose.Types.ObjectId(obj._id)
                            });
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getService: function (getMaidId, getUserId, cb) {
            let pipeline = [];

            if (payloadData.lat && payloadData.long) {
                let geoNear = {
                    $geoNear: {
                        near: {type: "Point", coordinates: [payloadData.long, payloadData.lat]},
                        spherical: true,
                        distanceField: "distance",
                        distanceMultiplier: 0.001,                   //convert to kilometre 6371km 0.001
                        maxDistance: 50000,
                        query: {isDeleted: false},
                    }
                };
                pipeline.push(geoNear)
            }

            let match = {
                $match: {
                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                    agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                    $and: [
                        {transactionId: {$exists: true}},
                        {transactionId: {$nin: ["", null]}}
                    ]
                }
            };
            pipeline.push(match);

            if (payloadData.year) {
                let localDate = new Date();
                let startOfYear = localDate.setFullYear(payloadData.year, 0, 1)

                let localDate1 = new Date();
                let endOfYear = localDate1.setFullYear(payloadData.year, 11, 31);
                let match = {
                    $match: {
                        workDate: {$gte: startOfYear, $lte: endOfYear},
                        agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]
                    }
                };
                pipeline.push(match)
            }

            if (payloadData.month && payloadData.year) {
                let localDate = new Date();
                let startOfMonth = localDate.setFullYear(payloadData.year, payloadData.month, 1);

                let localDate1 = new Date();
                let endOfMonth = localDate1.setFullYear(payloadData.year, payloadData.month + 1, 0);
                let match = {
                    $match: {
                        workDate: {$gte: startOfMonth, $lte: endOfMonth},
                        agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]

                    }
                };

                pipeline.push(match)
            }

            if (payloadData.startDate && payloadData.endDate) {
                let match = {
                    $match: {
                        workDate: {
                            $gte: new Date(payloadData.startDate).setHours(0, 0, 0, 0) - offset,
                            $lte: new Date(payloadData.endDate).setHours(0, 0, 0, 0) - offset
                        },
                        agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                    }
                };
                pipeline.push(match)
            }

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    pipeline.push({$match: {maidId: {$in: maidId}}})
                }
                if (userId && userId.length) {
                    pipeline.push({$match: {userId: {$in: userId}}})
                }
                if (!(maidId && maidId.length) && !(userId && userId.length)) {
                    pipeline.push({$match: {maidId: {$in: []}}})
                    pipeline.push({$match: {userId: {$in: []}}})
                }
            }

            let lookup = [
                {
                    $lookup: {
                        from: 'users',
                        localField: 'userId',
                        foreignField: '_id',
                        as: 'userId'
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$userId"
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        "_id": 1,
                        "serviceMakId": 1,
                        // "agencyId": 1,
                        "workDate": 1,
                        "startTime": 1,
                        "endTime": 1,
                        "duration": 1,
                        "amount": 1,
                        "uniquieAppKey": 1,
                        "bookingLocation": 1,
                        "bookingId":1,
                        "transactionId":1,
                        "isExtend": 1,
                        "deleteReason": 1,
                        "deleteTimeStamp": 1,
                        "deleteRequestByUser": 1,
                        "deleteAction": 1,
                        "declineReason": 1,
                        "isCompleted": 1,
                        "agencyAction": 1,
                        "address": 1,
                        "city": 1,
                        "locationName": 1,
                        "currency": 1,

                        "userId._id": 1,
                        "userId.fullName": 1,
                        "userId.phoneNo": 1,
                        "userId.email": 1,
                        "maidId._id": 1,
                        "maidId.firstName": 1,
                        "maidId.lastName": 1,
                        "maidId.makId": 1,

                        "agencyId.agencyName": 1,
                        "agencyId._id": 1,
                        "agencyId.email": 1,
                        "agencyId.countryName": 1,
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "serviceId",
                        as: "reviewData",
                        restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                    }
                },
            ];
            lookup.map(obj => {
                pipeline.push(obj)
            });


            if (payloadData.countryName) {
                pipeline.push({
                    $match: {"agencyId.countryName": {$regex: payloadData.countryName, $options: "si"}}
                })
            }

            if (payloadData.maidId) {
                let match = {
                    $match: {
                        "maidId._id": mongoose.Types.ObjectId(payloadData.maidId)
                    }
                };
                pipeline.push(match)
            }

            if (payloadData.userId) {
                let match = {
                    $match: {
                        "userId._id": mongoose.Types.ObjectId(payloadData.userId)
                    }
                };
                pipeline.push(match)
            }

            if(payloadData.pageNo && payloadData.limit){
                pipeline.push({
                    $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                });
                pipeline.push({
                    $limit: payloadData.limit
                })
            }
            if (pipeline && pipeline.length) {
                console.log("______________PIPELINE__________")
                // console.log(pipeline)
                Models.Service.aggregate(pipeline, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("_________ Total length________"+result.length)
                        if (result && result.length) {
                            response.service.data = result;
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getServiceTotalCount: function (getMaidId, getUserId, cb) {
            let pipeline = [];

            if (payloadData.lat && payloadData.long) {
                let geoNear = {
                    $geoNear: {
                        near: {type: "Point", coordinates: [payloadData.long, payloadData.lat]},
                        spherical: true,
                        distanceField: "distance",
                        distanceMultiplier: 0.001,                   //convert to kilometre 6371km 0.001
                        maxDistance: 50000,
                        query: {isDeleted: false},
                    }
                };
                pipeline.push(geoNear)
            }

            let match = {
                $match: {
                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                    agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                    $and: [
                        {transactionId: {$exists: true}},
                        {transactionId: {$nin: ["", null]}}
                    ]
                }
            };
            pipeline.push(match)


            if (payloadData.year) {
                let localDate = new Date();
                let startOfYear = localDate.setFullYear(payloadData.year, 0, 1)

                let localDate1 = new Date();
                let endOfYear = localDate1.setFullYear(payloadData.year, 11, 31);
                let match = {
                    $match: {
                        workDate: {$gte: startOfYear, $lte: endOfYear},
                        agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                    }
                };
                // console.log("_____________match",match);
                pipeline.push(match)
            }

            if (payloadData.month && payloadData.year) {
                let localDate = new Date();
                let startOfMonth = localDate.setFullYear(payloadData.year, payloadData.month, 1);

                let localDate1 = new Date();
                let endOfMonth = localDate1.setFullYear(payloadData.year, payloadData.month + 1, 0);
                let match = {
                    $match: {
                        workDate: {$gte: startOfMonth, $lte: endOfMonth},
                        agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                    }
                };

                pipeline.push(match)
            }

            if (payloadData.startDate && payloadData.endDate) {
                let match = {
                    $match: {
                        workDate: {
                            $gte: new Date(payloadData.startDate).setHours(0, 0, 0, 0),
                            $lte: new Date(payloadData.endDate).setHours(0, 0, 0, 0)
                        },
                        agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                    }
                };
                pipeline.push(match)
            }

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    pipeline.push({$match: {maidId: {$in: maidId}}})
                }
                if (userId && userId.length) {
                    pipeline.push({$match: {userId: {$in: userId}}})
                }
                if (!(maidId && maidId.length) && !(userId && userId.length)) {
                    pipeline.push({$match: {maidId: {$in: []}}})
                    pipeline.push({$match: {userId: {$in: []}}})
                }
            }
            let lookup = [
                {
                    $lookup: {
                        from: 'users',
                        localField: 'userId',
                        foreignField: '_id',
                        as: 'userId'
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$userId"
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        "_id": 1,
                        "serviceMakId": 1,
                        "workDate": 1,
                        "startTime": 1,
                        "endTime": 1,
                        "duration": 1,
                        "amount": 1,
                        "uniquieAppKey": 1,
                        "bookingLocation": 1,
                        "bookingId":1,
                        "transactionId":1,
                        "isExtend": 1,
                        "deleteReason": 1,
                        "deleteTimeStamp": 1,
                        "deleteRequestByUser": 1,
                        "deleteAction": 1,
                        "declineReason": 1,
                        "isCompleted": 1,
                        "agencyAction": 1,
                        "address": 1,
                        "city": 1,
                        "locationName": 1,

                        "userId._id": 1,
                        "userId.fullName": 1,
                        "userId.phoneNo": 1,
                        "userId.email": 1,
                        "maidId._id": 1,
                        "maidId.firstName": 1,
                        "maidId.lastName": 1,
                        "maidId.makId": 1,
                        "maidId.rating": 1,

                        "agencyId.agencyName": 1,
                        "agencyId._id": 1,
                        "agencyId.email": 1,
                        "agencyId.countryName": 1,
                    }
                }
            ];
            lookup.map(obj => {
                pipeline.push(obj)
            })

            if (payloadData.countryName) {
                pipeline.push({
                    $match: {"agencyId.countryName": {$regex: payloadData.countryName, $options: "si"}}
                })
            }

            if (payloadData.maidId) {
                let match = {
                    $match: {
                        "maidId._id": mongoose.Types.ObjectId(payloadData.maidId)
                    }
                };
                pipeline.push(match)
            }

            if (payloadData.userId) {
                let match = {
                    $match: {
                        "userId._id": mongoose.Types.ObjectId(payloadData.userId)
                    }
                };
                pipeline.push(match)
            }

            if (pipeline && pipeline.length) {
                Models.Service.aggregate(pipeline, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            response.service.count = result.length;
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getRevenue: function (getService, getServiceTotalCount, cb) {
            if (response.service.data && response.service.data.length) {
                let totalRevenue = 0;
                response.service.data.map(obj => {
                    totalRevenue = totalRevenue + obj.amount
                })
                response.totalRevenue = totalRevenue;
                cb(null)
            } else {
                cb(null)
            }
        }

    }, function (err, result) {
        callback(err, response)
    })
};

const lifeTimeSummaryAdmin = function (payloadData, userData, callback) {
    let response = {};
    let startOfMonth = moment(moment().startOf('month').format()).tz(payloadData.timeZone).unix() * 1000;
    let endOfMonth = moment(moment().endOf('month').format()).tz(payloadData.timeZone).unix() * 1000;
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getServiceDetails: function (checkForUniqueKey, cb) {

            let startThreeMonthsAgo = moment(moment(startOfMonth).subtract(3, 'months')).unix() * 1000;
            let endthreeMonthsAgo = moment(moment(endOfMonth).subtract(1, 'months')).unix() * 1000;

            let pipeline = [
                {
                    $match:{
                        deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]
                    }
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $addFields: {
                        agencyType: "$agencyId.agencyType",
                        agencyCountryName: "$agencyId.countryName",
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                    }
                },
                {
                    $group: {
                        _id: {
                            agencyCountryName: "$agencyCountryName",
                            agencyType: "$agencyType",
                            agencyId: "$agencyId",
                            agencyName: "$agencyName"

                        },
                        currentResult: {
                            $sum: {
                                $cond: {
                                    if: {$and: [{$gte: ["$workDate", startOfMonth]}, {$lte: ["$workDate", endOfMonth]}]},
                                    then: "$amount",
                                    else: 0
                                }
                            }
                        },
                        lastThreeResult: {
                            $sum: {
                                $cond: {
                                    if: {$and: [{$gte: ["$workDate", startThreeMonthsAgo]}, {$lte: ["$workDate", endthreeMonthsAgo]}]},
                                    then: "$amount",
                                    else: 0
                                }
                            }
                        },
                        all: {$sum: "$amount"}
                    }
                },
                {
                    $project: {
                        agencyCountryName: "$_id.agencyCountryName",
                        agencyType: "$_id.agencyType",
                        agencyId: "$_id.agencyId",
                        agencyName: "$_id.agencyName",
                        commission: "$_id.commission",
                        currentResult: 1,
                        lastThreeResult: 1,
                        all: 1,
                        _id: 0
                    }
                },
            ];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        agencyName: {$regex: payloadData.search, $options: "si"}
                    }
                })
            }

            let sort = [];
            if(payloadData.pageNo && payloadData.limit){
                sort.push({
                    $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                });
                sort.push({
                    $limit: payloadData.limit
                })
            }

            Models.Service.aggregate(pipeline.concat(sort), function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        if (payloadData.countryName) {
                            response.data = result.filter(obj => {
                                return (obj.agencyCountryName == payloadData.countryName)
                            });
                        } else {
                            response.data = result;
                        }
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getServiceCount: function (checkForUniqueKey, cb) {

            let startThreeMonthsAgo = moment(moment(startOfMonth).subtract(3, 'months')).unix() * 1000;
            let endthreeMonthsAgo = moment(moment(endOfMonth).subtract(1, 'months')).unix() * 1000;

            let pipeline = [
                {
                    $match:{
                        deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]
                    }
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $addFields: {
                        agencyType: "$agencyId.agencyType",
                        agencyCountryName: "$agencyId.countryName",
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                    }
                },
                {
                    $group: {
                        _id: {
                            agencyCountryName: "$agencyCountryName",
                            agencyType: "$agencyType",
                            agencyId: "$agencyId",
                            agencyName: "$agencyName"

                        },
                        currentResult: {
                            $sum: {
                                $cond: {
                                    if: {$and: [{$gte: ["$workDate", startOfMonth]}, {$lte: ["$workDate", endOfMonth]}]},
                                    then: "$amount",
                                    else: 0
                                }
                            }
                        },
                        lastThreeResult: {
                            $sum: {
                                $cond: {
                                    if: {$and: [{$gte: ["$workDate", startThreeMonthsAgo]}, {$lte: ["$workDate", endthreeMonthsAgo]}]},
                                    then: "$amount",
                                    else: 0
                                }
                            }
                        },
                        all: {$sum: "$amount"}
                    }
                },
                {
                    $project: {
                        agencyCountryName: "$_id.agencyCountryName",
                        agencyType: "$_id.agencyType",
                        agencyId: "$_id.agencyId",
                        agencyName: "$_id.agencyName",
                        currentResult: 1,
                        lastThreeResult: 1,
                        all: 1,
                        _id: 0
                    }
                },
            ];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        agencyName: {$regex: payloadData.search, $options: "si"}
                    }
                })
            }

            let sort = [];
            if(payloadData.pageNo && payloadData.limit){
                sort.push({
                    $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                });
                sort.push({
                    $limit: payloadData.limit
                })
            }

            Models.Service.aggregate(pipeline.concat(sort), function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        if (payloadData.countryName) {
                            let localResult = result.filter(obj => {
                                return (obj.agencyCountryName == payloadData.countryName)
                            });
                            response.count = localResult.length;
                        } else {
                            response.count = result.length;
                        }
                        cb(null)
                    } else {
                        response.count = 0
                        cb(null)
                    }
                }
            })
        }

    }, function (err, result) {
        callback(err, response)
    })
};

const currentMonthSummaryAdmin = function (payloadData, userData, callback) {
    let response = {};
    let boundryArray = [];
    let startOfMonth = moment(moment().startOf('month').format()).tz(payloadData.timeZone).unix() * 1000;
    let endOfMonth = moment(moment().endOf('month').format()).tz(payloadData.timeZone).unix() * 1000;
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        createBoundaryArray: function (checkForUniqueKey, cb) {
            let local = startOfMonth;
            let iteretaeTimes = parseInt(moment().daysInMonth());
            let updateLocal = function (id, callback) {
                local = local + 86400000;
                callback(null, local);
            };

            async.times(iteretaeTimes, function (n, next) {
                updateLocal(n, function (err, data) {
                    boundryArray.push(data);
                    next(err, data);
                });
            }, function (err, result) {
                cb(null)
            });
        },

        getServiceDetails: function (createBoundaryArray, cb) {
            let pipeline = [
                {
                    $match: {
                        workDate: {
                            $gte: startOfMonth, $lte: endOfMonth,
                        },
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]
                    }
                },
                {
                    $group: {
                        _id: {
                            agencyId: "$agencyId",
                        },
                        totalAmount: {$sum: "$amount"},
                    }
                },
                {
                    $project: {
                        agencyId: "$_id.agencyId",
                        totalAmount: 1,
                        _id: 0
                    }
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$agencyId",
                        connectFromField: "agencyId",
                        connectToField: "agencyId",
                        as: "serviceData",
                        restrictSearchWithMatch: {
                            workDate: {
                                $gte: startOfMonth, $lte: endOfMonth
                            }
                        }
                    }
                },
                {
                    $unwind: "$serviceData"

                },
                {
                    $project: {
                        totalAmount: 1,
                        agencyId: 1,
                        insideWorkDate: "$serviceData.workDate",
                        insideAmount: "$serviceData.amount"
                    }
                },
                {
                    $group: {
                        _id: {
                            agencyId: "$agencyId",
                            insideWorkDate: "$insideWorkDate",
                        },
                        totalInsideAmount: {$sum: "$insideAmount"},
                        totalAmount: {$avg: "$totalAmount"},
                    }
                },
                {
                    $project: {
                        agencyId: "$_id.agencyId",
                        insideWorkDate: "$_id.insideWorkDate",
                        totalAmount: 1,
                        totalInsideAmount: 1,
                        _id: 0
                    }
                },
                {
                    $group: {
                        _id: "$agencyId",
                        totalAmount: {$avg: "$totalAmount"},
                        monthData: {
                            $push: {
                                totalInsideAmount: "$totalInsideAmount",
                                insideWorkDate: "$insideWorkDate"
                            }
                        }
                    }
                },
                {
                    $project: {
                        agencyId: "$_id",
                        monthData: 1,
                        totalAmount: 1,
                        _id: 0
                    }
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        totalAmount: 1,
                        monthData: 1,
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                        agencyType: "$agencyId.agencyType",
                        countryName: "$agencyId.countryName",
                        pastTotalAmount: {
                            $filter: {
                                input: "$monthData",
                                as: "monthData",
                                cond: {$lte: ["$$monthData.insideWorkDate", (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000]}
                            }
                        },
                        fultureTotalAmount: {
                            $filter: {
                                input: "$monthData",
                                as: "monthData",
                                cond: {$gte: ["$$monthData.insideWorkDate", (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000]}
                            }
                        }
                    }
                },
                {
                    $project: {
                        totalAmount: 1,
                        monthData: 1,
                        agencyId: 1,
                        agencyName: 1,
                        agencyType: 1,
                        countryName: 1,
                        pastTotalAmount: 1,
                        fultureTotalAmount: 1,
                        pastTotal: {
                            $reduce: {
                                input: "$pastTotalAmount",
                                initialValue: {sum: 0},
                                in: {
                                    sum: {$add: ["$$value.sum", "$$this.totalInsideAmount"]},
                                }
                            }
                        },
                        futureTotal: {
                            $reduce: {
                                input: "$fultureTotalAmount",
                                initialValue: {sum: 0},
                                in: {
                                    sum: {$add: ["$$value.sum", "$$this.totalInsideAmount"]},
                                }
                            }
                        }
                    }
                },
                {
                    $project: {
                        totalAmount: 1,
                        monthData: 1,
                        agencyId: 1,
                        agencyName: 1,
                        agencyType: 1,
                        countryName: 1,
                        // pastTotalAmount:1,
                        // fultureTotalAmount:1,
                        pastTotal: "$pastTotal.sum",
                        futureTotal: "$futureTotal.sum"
                    }
                },
            ];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        agencyName: {$regex: payloadData.search, $options: "si"}
                    }
                })
            }

            let sort = [];
            if(payloadData.pageNo && payloadData.limit){
                sort.push({
                    $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                });
                sort.push({
                    $limit: payloadData.limit
                })
            }

            Models.Service.aggregate(pipeline.concat(sort), function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        if (payloadData.countryName) {
                            response.data = result.filter(obj => {
                                return (obj.countryName == payloadData.countryName)
                            });
                        } else {
                            response.data = result;
                        }
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getServiceCount: function (createBoundaryArray, cb) {
            let pipeline = [
                {
                    $match: {
                        workDate: {
                            $gte: startOfMonth, $lte: endOfMonth,
                        },
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]
                    }
                },
                {
                    $group: {
                        _id: {
                            agencyId: "$agencyId",
                        },
                        totalAmount: {$sum: "$amount"},
                    }
                },
                {
                    $project: {
                        agencyId: "$_id.agencyId",
                        totalAmount: 1,
                        _id: 0
                    }
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$agencyId",
                        connectFromField: "agencyId",
                        connectToField: "agencyId",
                        as: "serviceData",
                        restrictSearchWithMatch: {
                            workDate: {
                                $gte: startOfMonth, $lte: endOfMonth
                            }
                        }
                    }
                },
                {
                    $unwind: "$serviceData"

                },
                {
                    $project: {
                        totalAmount: 1,
                        agencyId: 1,
                        insideWorkDate: "$serviceData.workDate",
                        insideAmount: "$serviceData.amount"
                    }
                },
                {
                    $group: {
                        _id: {
                            agencyId: "$agencyId",
                            insideWorkDate: "$insideWorkDate",
                        },
                        totalInsideAmount: {$sum: "$insideAmount"},
                        totalAmount: {$avg: "$totalAmount"},
                    }
                },
                {
                    $project: {
                        agencyId: "$_id.agencyId",
                        insideWorkDate: "$_id.insideWorkDate",
                        totalAmount: 1,
                        totalInsideAmount: 1,
                        _id: 0
                    }
                },
                {
                    $group: {
                        _id: "$agencyId",
                        totalAmount: {$avg: "$totalAmount"},
                        monthData: {
                            $push: {
                                totalInsideAmount: "$totalInsideAmount",
                                insideWorkDate: "$insideWorkDate"
                            }
                        }
                    }
                },
                {
                    $project: {
                        agencyId: "$_id",
                        monthData: 1,
                        totalAmount: 1,
                        _id: 0
                    }
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        totalAmount: 1,
                        monthData: 1,
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                        agencyType: "$agencyId.agencyType",
                        countryName: "$agencyId.countryName",
                        pastTotalAmount: {
                            $filter: {
                                input: "$monthData",
                                as: "monthData",
                                cond: {$lte: ["$$monthData.insideWorkDate", (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000]}
                            }
                        },
                        fultureTotalAmount: {
                            $filter: {
                                input: "$monthData",
                                as: "monthData",
                                cond: {$gte: ["$$monthData.insideWorkDate", (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000]}
                            }
                        }
                    }
                },
                {
                    $project: {
                        totalAmount: 1,
                        monthData: 1,
                        agencyId: 1,
                        agencyName: 1,
                        agencyType: 1,
                        countryName: 1,
                        pastTotalAmount: 1,
                        fultureTotalAmount: 1,
                        pastTotal: {
                            $reduce: {
                                input: "$pastTotalAmount",
                                initialValue: {sum: 0},
                                in: {
                                    sum: {$add: ["$$value.sum", "$$this.totalInsideAmount"]},
                                }
                            }
                        },
                        futureTotal: {
                            $reduce: {
                                input: "$fultureTotalAmount",
                                initialValue: {sum: 0},
                                in: {
                                    sum: {$add: ["$$value.sum", "$$this.totalInsideAmount"]},
                                }
                            }
                        }
                    }
                },
                {
                    $project: {
                        totalAmount: 1,
                        monthData: 1,
                        agencyId: 1,
                        agencyName: 1,
                        agencyType: 1,
                        countryName: 1,
                        // pastTotalAmount:1,
                        // fultureTotalAmount:1,
                        pastTotal: "$pastTotal.sum",
                        futureTotal: "$futureTotal.sum"
                    }
                },
            ];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        agencyName: {$regex: payloadData.search, $options: "si"}
                    }
                })
            }

            Models.Service.aggregate(pipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {

                    if (result && result.length) {
                        if (payloadData.countryName) {
                            let localResult = result.filter(obj => {
                                return (obj.countryName == payloadData.countryName)
                            });
                            response.count = localResult.length;
                        }
                        else if (payloadData.countryName && payloadData.search) {
                            let localResult = result.filter(obj => {
                                return (obj.countryName == payloadData.countryName)
                            });
                            response.count = localResult.length;
                        }
                        else {
                            response.count = result.length;

                        }
                        cb(null)
                    } else {
                        response.count = 0
                        cb(null)
                    }
                }
            })
        },


    }, function (err, result) {
        callback(err, response)
    })
};

const detailedMonthlyInvoiceAdmin = function (payloadData, userData, callback) {
    let response = {};
    if(payloadData.monthFilter !=undefined){
        
    }
    else{
        payloadData.monthFilter = new Date().getMonth()+1;
    }
    let endOfMonth = new Date(new Date(new Date().setUTCDate(1)).setUTCHours(0,0,0,0)).setUTCMonth(payloadData.monthFilter +1) -1 + payloadData.timeZone;

    let startOfMonth = new Date(new Date(new Date().setUTCDate(1)).setUTCHours(0,0,0,0)).setUTCMonth(payloadData.monthFilter) + payloadData.timeZone;
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getServiceDetails: function (checkForUniqueKey, cb) {
            let finalPipeline = [];
            let countryMatch = [];
            if (payloadData.countryName) {
                countryMatch.push(
                    {
                        $match: {
                            countryName: payloadData.countryName
                        }
                    })
            }
            if (payloadData.agencyId) {
                countryMatch.push(
                    {
                        $match: {
                            _id: mongoose.Types.ObjectId(payloadData.agencyId)
                        }
                    })
            }

            let pipeline = [
                {
                    $match: {
                        isBlocked: false,
                        isDeleted: false
                    }
                },
                {
                    $graphLookup: {
                        from: "maids",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "agencyId",
                        as: "maidData",
                        restrictSearchWithMatch: {isBlocked: false, isDeleted: false}
                    }
                },
                {
                    $unwind: "$maidData"
                },
                {
                    $project: {
                        _id: 1,
                        maidId: "$maidData._id",
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        maidId: {$addToSet: "$maidId"}
                    }
                },
                {
                    $unwind: {path: "$maidId", preserveNullAndEmptyArrays: true}
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$maidId",
                        connectFromField: "maidId",
                        connectToField: "maidId",
                        as: "serviceData",
                        restrictSearchWithMatch: {
                            workDate: {
                                $gte: startOfMonth, $lte: endOfMonth
                            },
                            $and: [
                                {transactionId: {$exists: true}},
                                {transactionId: {$nin: ["", null]}}
                            ],
                            $or: [
                                {agencyAction: {$ne: Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE}},
                                {deleteAction: {$ne: Config.APP_CONSTANTS.DATABASE.ACTION.DELETED}}
                            ]
                        }
                    }
                },
                {
                    $unwind: {path: "$serviceData"}
                },
                {
                    $project: {
                        _id: 1,
                        maidId: 1,
                        duration: {serviceData: {$ifNull: ["$serviceData.duration", 0]}}
                    }
                },
                {
                    $group: {
                        _id: {
                            agencyId: "$_id",
                            maidId: "$maidId"
                        },
                        totalDuration: {
                            $sum: "$duration.serviceData"
                        }
                    }
                },
                {
                    $project: {
                        agencyId: "$_id.agencyId",
                        maidId: "$_id.maidId",
                        totalDuration: 1,
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        _id: 0,
                        totalDuration: 1,
                        maidId: "$maidId._id",
                        firstName: "$maidId.firstName",
                        lastName: "$maidId.lastName",
                        nationality: "$maidId.nationality",
                        makId: "$maidId.makId",
                        price: "$maidId.price",
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                        agencyType: "$agencyId.agencyType",
                        countryName: "$agencyId.countryName",
                    }
                },
                {
                    $addFields: {
                        revenue: {$multiply: ["$price", "$totalDuration"]}
                    }
                },
            ];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        $or: [
                            {agencyName: {$regex: payloadData.search, $options: "si"},},
                            {firstName: {$regex: payloadData.search, $options: "si"}},
                            {lastName: {$regex: payloadData.search, $options: "si"}},
                            {nationality: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                })
            }

            if (payloadData.countryName) {
                finalPipeline = countryMatch.concat(pipeline)
            }
            else {
                finalPipeline = pipeline
            }

            if(payloadData.pageNo && payloadData.limit){
                finalPipeline.push({
                    $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                });
                finalPipeline.push({
                    $limit: payloadData.limit
                })
            }

            Models.Agency.aggregate(finalPipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getServiceCount: function (checkForUniqueKey, cb) {
            let finalPipeline = [];
            let countryMatch = [];
            if (payloadData.countryName) {
                countryMatch.push(
                    {
                        $match: {
                            countryName: payloadData.countryName
                        }
                    })
            }
            if (payloadData.agencyId) {
                countryMatch.push(
                    {
                        $match: {
                            _id: mongoose.Types.ObjectId(payloadData.agencyId)
                        }
                    })
            }

            let pipeline = [
                {
                    $match: {
                        isBlocked: false,
                        isDeleted: false
                    }
                },
                {
                    $graphLookup: {
                        from: "maids",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "agencyId",
                        as: "maidData",
                        restrictSearchWithMatch: {isBlocked: false, isDeleted: false}
                    }
                },
                {
                    $unwind: "$maidData"
                },
                {
                    $project: {
                        _id: 1,
                        maidId: "$maidData._id",
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        maidId: {$addToSet: "$maidId"}
                    }
                },
                {
                    $unwind: {path: "$maidId", preserveNullAndEmptyArrays: true}
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$maidId",
                        connectFromField: "maidId",
                        connectToField: "maidId",
                        as: "serviceData",
                        restrictSearchWithMatch: {
                            workDate: {
                                $gte: startOfMonth, $lte: endOfMonth
                            },
                            $and: [
                                {transactionId: {$exists: true}},
                                {transactionId: {$nin: ["", null]}}
                            ],
                            $or: [
                                {agencyAction: {$ne: Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE}},
                                {deleteAction: {$ne: Config.APP_CONSTANTS.DATABASE.ACTION.DELETED}}
                            ]
                        }
                    }
                },
                {
                    $unwind: {path: "$serviceData"}
                },
                {
                    $project: {
                        _id: 1,
                        maidId: 1,
                        duration: {serviceData: {$ifNull: ["$serviceData.duration", 0]}},
                    }
                },
                {
                    $group: {
                        _id: {
                            agencyId: "$_id",
                            maidId: "$maidId",
                        },
                        totalDuration: {
                            $sum: "$duration.serviceData"
                        }
                    }
                },
                {
                    $project: {
                        agencyId: "$_id.agencyId",
                        maidId: "$_id.maidId",
                        totalDuration: 1,
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        _id: 0,
                        totalDuration: 1,
                        maidId: "$maidId._id",
                        firstName: "$maidId.firstName",
                        lastName: "$maidId.lastName",
                        nationality: "$maidId.nationality",
                        makId: "$maidId.makId",
                        price: "$maidId.price",
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                        agencyType: "$agencyId.agencyType",
                        countryName: "$agencyId.countryName",
                    }
                },
                {
                    $addFields: {
                        revenue: {$multiply: ["$price", "$totalDuration"]}
                    }
                },
            ];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        $or: [
                            {agencyName: {$regex: payloadData.search, $options: "si"},},
                            {firstName: {$regex: payloadData.search, $options: "si"}},
                            {lastName: {$regex: payloadData.search, $options: "si"}},
                            {nationality: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                })
            }

            if (payloadData.countryName) {
                finalPipeline = countryMatch.concat(pipeline)
            } else {
                finalPipeline = pipeline
            }

            Models.Agency.aggregate(finalPipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }

    }, function (err, result) {
        callback(err, response)
    })
};

const listAllReviewsAdmin = function (payloadData, userData, callback) {
    let response = {};
    let totalMaidRating = [];
    let totalMaidCount = 0;
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMaidRating: function (checkForUniqueKey, cb) {
            let pipeline = [
                {
                    $match: {
                        isDeleted: false,
                        isBlocked: false,
                    }
                }
            ];

            if (payloadData.countryName) {
                let match = {
                    $match: {
                        countryName: payloadData.countryName
                    }
                };
                pipeline.push(match)
            }
            if (payloadData.agencyId) {
                let match = {
                    $match: {
                        agencyId: mongoose.Types.ObjectId(payloadData.agencyId)
                    }
                };
                pipeline.push(match)
            }

            let pipeline2 = [
                // {
                //     $graphLookup: {
                //         from: "reviews",
                //         startWith: "$_id",
                //         connectFromField: "_id",
                //         connectToField: "agencyId",
                //         as: "reviewData",
                //         restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                //     }
                // },
                // {
                //     $unwind: {path: "$reviewData", preserveNullAndEmptyArrays: true}
                // },
                // {
                //     $addFields: {
                //         reviewId: {reviewData: {$ifNull: ["$reviewData._id", ""]}},
                //         maidId: {reviewData: {$ifNull: ["$reviewData.maidId", ""]}},
                //         maidRating: {
                //             reviewData: {
                //                 $ifNull: ["$reviewData.maidRating",
                //                     {
                //                         "cleaning": 0,
                //                         "ironing": 0,
                //                         "cooking": 0,
                //                         "childCare": 0
                //                     }
                //                 ]
                //             }
                //         },
                //     }
                // },
                // {
                //     $project: {
                //         _id: 0,
                //         agencyId: "$_id",
                //         reviewId: "$reviewId.reviewData",
                //         maidId: "$maidId.reviewData",
                //         maidRating: "$maidRating.reviewData"
                //     }
                // },
                // {
                //     $graphLookup: {
                //         from: "maids",
                //         startWith: "$agencyId",
                //         connectFromField: "agencyId",
                //         connectToField: "agencyId",
                //         as: "maidData",
                //         restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                //     }
                // },
                // {
                //     $unwind: "$maidData"
                // },
                // {
                //     $project: {
                //         agencyId: 1,
                //         maidId: 1,
                //         maidRating: 1,
                //         restMaid: "$maidData._id"
                //     }
                // },
                // {
                //     $group: {
                //         _id: {
                //             agencyId: "$agencyId",
                //             maidId: "$maidId",
                //         },
                //         cleaningRating: {$sum: "$maidRating.cleaning"},
                //         ironingRating: {$sum: "$maidRating.ironing"},
                //         cookingRating: {$sum: "$maidRating.cooking"},
                //         childCareRating: {$sum: "$maidRating.childCare"},
                //         cleaningCount: {
                //             $sum: {
                //                 $cond: [
                //                     {$eq: ["$maidRating.cleaning", 0]},
                //                     0,
                //                     1
                //                 ]
                //             }
                //         },
                //         ironingCount: {
                //             $sum: {
                //                 $cond: [
                //                     {$eq: ["$maidRating.ironing", 0]},
                //                     0,
                //                     1
                //                 ]
                //             }
                //         },
                //         cookingCount: {
                //             $sum: {
                //                 $cond: [
                //                     {$eq: ["$maidRating.cooking", 0]},
                //                     0,
                //                     1
                //                 ]
                //             }
                //         },
                //         childCareCount: {
                //             $sum: {
                //                 $cond: [
                //                     {$eq: ["$maidRating.childCare", 0]},
                //                     0,
                //                     1
                //                 ]
                //             }
                //         },
                //         restMaid: {$addToSet: "$restMaid"}
                //     }
                // },
                // {
                //     $project: {
                //         _id: 0,
                //         maidId: "$_id.maidId",
                //         agencyId: "$_id.agencyId",
                //         restMaid: 1,
                //         avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                //         avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                //         avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                //         avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                //     }
                // },
                // {
                //     $unwind: "$restMaid"
                // },
                // {
                //     $project: {
                //         agencyId: 1,
                //         maidId: {
                //             $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$maidId", else: "$restMaid"}
                //         },
                //         avgCleaning: {
                //             $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgCleaning", else: 0}
                //         },
                //         avgIroning: {
                //             $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgIroning", else: 0}
                //         },
                //         avgCooking: {
                //             $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgCooking", else: 0}
                //         },
                //         avgChildCare: {
                //             $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgChildCare", else: 0}
                //         }
                //     }
                // },
                // {
                //     $lookup: {
                //         from: 'agencies',
                //         localField: 'agencyId',
                //         foreignField: '_id',
                //         as: 'agencyId'
                //     }
                // },
                // {
                //     $unwind: "$agencyId"
                // },
                // {
                //     $lookup: {
                //         from: 'maids',
                //         localField: 'maidId',
                //         foreignField: '_id',
                //         as: 'maidId'
                //     }
                // },
                // {
                //     $unwind: "$maidId"
                // },
                // {
                //     $project: {
                //         _id: 0,
                //         maidId: "$maidId._id",
                //         makId: "$maidId.makId",
                //         nationality: "$maidId.nationality",
                //         firstName: "$maidId.firstName",
                //         lastName: "$maidId.lastName",
                //         agencyId: "$agencyId._id",
                //         agencyName: "$agencyId.agencyName",
                //         agencyCountryName: "$agencyId.countryName",
                //         agencyType: "$agencyId.agencyType",
                //         avgCleaning: 1,
                //         avgIroning: 1,
                //         avgCooking: 1,
                //         avgChildCare: 1,
                //     }
                // }
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                    }
                },
                {
                    $unwind: {path: "$reviewData", preserveNullAndEmptyArrays: true}
                },
                {
                    $addFields: {
                        maidRating: "$reviewData.maidRating",
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        agencyId : {$first : "$agencyId"},
                        cleaningRating: {$sum: "$maidRating.cleaning"},
                        ironingRating: {$sum: "$maidRating.ironing"},
                        cookingRating: {$sum: "$maidRating.cooking"},
                        childCareRating: {$sum: "$maidRating.childCare"},

                        cleaningCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cleaning", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        ironingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.ironing", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        cookingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cooking", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        childCareCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.childCare", 0]},
                                    0,
                                    1
                                ]
                            }
                        }
                    }
                },
                {
                    $project: {
                        _id:0,
                        maidId: "$_id",
                        agencyId: "$agencyId",
                        avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                        avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                        avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                        avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        // avgCleaning:1,
                        // avgIroning:1,
                        // avgCooking:1,
                        // avgChildCare:1,
                        // _id: "$maidId._id",
                        // firstName: "$maidId.firstName",
                        // radius: "$maidId.radius",
                        // lastName: "$maidId.lastName",
                        // makId: "$maidId.makId",
                        // rating: "$maidId.rating",
                        // gender: "$maidId.gender",
                        // email: "$maidId.email",
                        // country: "$maidId.country",
                        // accessToken: "$maidId.accessToken",
                        // actualPrice: "$maidId.actualPrice",
                        // agencyId: "$maidId.agencyId",
                        // commission: "$maidId.commission",
                        // countryCode: "$maidId.countryCode",
                        // countryENCode: "$maidId.countryENCode",
                        // countryName: "$maidId.countryName",
                        // currentLocation: "$maidId.currentLocation",
                        // description: "$maidId.description",
                        // deviceToken: "$maidId.deviceToken",
                        // deviceType: "$maidId.deviceType",
                        // dob: "$maidId.dob",
                        // documentPicURL: "$maidId.documentPicURL",
                        // experience: "$maidId.experience",
                        // isBlocked: "$maidId.isBlocked",
                        // isDeleted: "$maidId.isDeleted",
                        // isLogin: "$maidId.isLogin",
                        // languages: "$maidId.languages",
                        // makPrice: "$maidId.makPrice",
                        // nationality: "$maidId.nationality",
                        // nationalityFlag: "$maidId.nationalityFlag",
                        // phoneNo: "$maidId.phoneNo",
                        // price: "$maidId.price",
                        // profilePicURL: "$maidId.profilePicURL",
                        // registrationDate: "$maidId.registrationDate",
                        // religion: "$maidId.religion",
                        // step: "$maidId.step",
                        // timeSlot: "$maidId.timeSlot",
                        // timeZone: "$maidId.timeZone",
                        _id: 0,
                                maidId: "$maidId._id",
                                makId: "$maidId.makId",
                                nationality: "$maidId.nationality",
                                firstName: "$maidId.firstName",
                                lastName: "$maidId.lastName",
                                agencyId: "$agencyId._id",
                                agencyName: "$agencyId.agencyName",
                                agencyCountryName: "$agencyId.countryName",
                                agencyType: "$agencyId.agencyType",
                                avgCleaning: 1,
                                avgIroning: 1,
                                avgCooking: 1,
                                avgChildCare: 1,
                    }
                }
            ];


            if (payloadData.reviewFilterType && payloadData.starFilter) {
                let match = {};
                if (payloadData.reviewFilterType === 'cleaning') {
                    match.$match = {
                        avgCleaning: payloadData.starFilter
                    }
                }
                else if (payloadData.reviewFilterType === 'ironing') {
                    match.$match = {
                        avgIroning: payloadData.starFilter
                    }
                }
                else if (payloadData.reviewFilterType === 'cooking') {
                    match.$match = {
                        avgCooking: payloadData.starFilter
                    }
                }
                else if (payloadData.reviewFilterType === 'childCare') {
                    match.$match = {
                        avgChildCare: payloadData.starFilter
                    }
                }

                pipeline.push(match)
            }

            if (payloadData.search) {
                let match = {
                    $match: {
                        $or: [
                            {firstName: {$regex: payloadData.search, $options: "si"}},
                            {lastName: {$regex: payloadData.search, $options: "si"}},
                            {nationality: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                };
                pipeline2.push(match)
            }

            let sort = [
                {$skip: ((payloadData.pageNo - 1) * payloadData.limit)},
                {$limit: payloadData.limit}
            ];
            pipeline2 = pipeline2.concat(sort);

           // console.log('mainnnnnnnnnnnnn',JSON.stringify(pipeline.concat(pipeline2)))
            Models.Maids.aggregate(pipeline.concat(pipeline2), function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        totalMaidRating = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            });
        },

        getMaidCount: function (checkForUniqueKey,getMaidRating, cb) {
            let pipeline = [
                {
                    $match: {
                        isDeleted: false,
                        isBlocked: false,
                    }
                }
            ];

            if (payloadData.countryName) {
                let match = {
                    $match: {
                        countryName: payloadData.countryName
                    }
                };
                pipeline.push(match)
            }
            if (payloadData.agencyId) {
                let match = {
                    $match: {
                        agencyId: mongoose.Types.ObjectId(payloadData.agencyId)
                    }
                };
                pipeline.push(match)
            }

            let pipeline2 = [
                // {
                //     $graphLookup: {
                //         from: "reviews",
                //         startWith: "$_id",
                //         connectFromField: "_id",
                //         connectToField: "agencyId",
                //         as: "reviewData",
                //         restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                //     }
                // },
                // {
                //     $unwind: {path: "$reviewData", preserveNullAndEmptyArrays: true}
                // },
                // {
                //     $addFields: {
                //         reviewId: {reviewData: {$ifNull: ["$reviewData._id", ""]}},
                //         maidId: {reviewData: {$ifNull: ["$reviewData.maidId", ""]}},
                //         maidRating: {
                //             reviewData: {
                //                 $ifNull: ["$reviewData.maidRating",
                //                     {
                //                         "cleaning": 0,
                //                         "ironing": 0,
                //                         "cooking": 0,
                //                         "childCare": 0
                //                     }
                //                 ]
                //             }
                //         },
                //     }
                // },
                // {
                //     $project: {
                //         _id: 0,
                //         agencyId: "$_id",
                //         reviewId: "$reviewId.reviewData",
                //         maidId: "$maidId.reviewData",
                //         maidRating: "$maidRating.reviewData"
                //     }
                // },
                // {
                //     $graphLookup: {
                //         from: "maids",
                //         startWith: "$agencyId",
                //         connectFromField: "agencyId",
                //         connectToField: "agencyId",
                //         as: "maidData",
                //         restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                //     }
                // },
                // {
                //     $unwind: "$maidData"
                // },
                // {
                //     $project: {
                //         agencyId: 1,
                //         maidId: 1,
                //         maidRating: 1,
                //         restMaid: "$maidData._id"
                //     }
                // },
                // {
                //     $group: {
                //         _id: {
                //             agencyId: "$agencyId",
                //             maidId: "$maidId",
                //         },
                //         cleaningRating: {$sum: "$maidRating.cleaning"},
                //         ironingRating: {$sum: "$maidRating.ironing"},
                //         cookingRating: {$sum: "$maidRating.cooking"},
                //         childCareRating: {$sum: "$maidRating.childCare"},
                //         cleaningCount: {
                //             $sum: {
                //                 $cond: [
                //                     {$eq: ["$maidRating.cleaning", 0]},
                //                     0,
                //                     1
                //                 ]
                //             }
                //         },
                //         ironingCount: {
                //             $sum: {
                //                 $cond: [
                //                     {$eq: ["$maidRating.ironing", 0]},
                //                     0,
                //                     1
                //                 ]
                //             }
                //         },
                //         cookingCount: {
                //             $sum: {
                //                 $cond: [
                //                     {$eq: ["$maidRating.cooking", 0]},
                //                     0,
                //                     1
                //                 ]
                //             }
                //         },
                //         childCareCount: {
                //             $sum: {
                //                 $cond: [
                //                     {$eq: ["$maidRating.childCare", 0]},
                //                     0,
                //                     1
                //                 ]
                //             }
                //         },
                //         restMaid: {$addToSet: "$restMaid"}
                //     }
                // },
                // {
                //     $project: {
                //         _id: 0,
                //         maidId: "$_id.maidId",
                //         agencyId: "$_id.agencyId",
                //         restMaid: 1,
                //         avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                //         avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                //         avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                //         avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                //     }
                // },
                // {
                //     $unwind: "$restMaid"
                // },
                // {
                //     $project: {
                //         agencyId: 1,
                //         maidId: {
                //             $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$maidId", else: "$restMaid"}
                //         },
                //         avgCleaning: {
                //             $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgCleaning", else: 0}
                //         },
                //         avgIroning: {
                //             $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgIroning", else: 0}
                //         },
                //         avgCooking: {
                //             $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgCooking", else: 0}
                //         },
                //         avgChildCare: {
                //             $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgChildCare", else: 0}
                //         }
                //     }
                // },
                // {
                //     $lookup: {
                //         from: 'agencies',
                //         localField: 'agencyId',
                //         foreignField: '_id',
                //         as: 'agencyId'
                //     }
                // },
                // {
                //     $unwind: "$agencyId"
                // },
                // {
                //     $lookup: {
                //         from: 'maids',
                //         localField: 'maidId',
                //         foreignField: '_id',
                //         as: 'maidId'
                //     }
                // },
                // {
                //     $unwind: "$maidId"
                // },
                // {
                //     $project: {
                //         _id: 0,
                //         maidId: "$maidId._id",
                //         makId: "$maidId.makId",
                //         nationality: "$maidId.nationality",
                //         firstName: "$maidId.firstName",
                //         lastName: "$maidId.lastName",
                //         agencyId: "$agencyId._id",
                //         agencyName: "$agencyId.agencyName",
                //         agencyCountryName: "$agencyId.countryName",
                //         agencyType: "$agencyId.agencyType",
                //         avgCleaning: 1,
                //         avgIroning: 1,
                //         avgCooking: 1,
                //         avgChildCare: 1,
                //     }
                // }

                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                    }
                },
                {
                    $unwind: {path: "$reviewData", preserveNullAndEmptyArrays: true}
                },
                {
                    $addFields: {
                        maidRating: "$reviewData.maidRating",
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        agencyId : {$first : "$agencyId"},
                        cleaningRating: {$sum: "$maidRating.cleaning"},
                        ironingRating: {$sum: "$maidRating.ironing"},
                        cookingRating: {$sum: "$maidRating.cooking"},
                        childCareRating: {$sum: "$maidRating.childCare"},

                        cleaningCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cleaning", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        ironingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.ironing", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        cookingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cooking", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        childCareCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.childCare", 0]},
                                    0,
                                    1
                                ]
                            }
                        }
                    }
                },
                {
                    $project: {
                        _id:0,
                        maidId: "$_id",
                        agencyId: "$agencyId",
                        avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                        avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                        avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                        avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        // avgCleaning:1,
                        // avgIroning:1,
                        // avgCooking:1,
                        // avgChildCare:1,
                        // _id: "$maidId._id",
                        // firstName: "$maidId.firstName",
                        // radius: "$maidId.radius",
                        // lastName: "$maidId.lastName",
                        // makId: "$maidId.makId",
                        // rating: "$maidId.rating",
                        // gender: "$maidId.gender",
                        // email: "$maidId.email",
                        // country: "$maidId.country",
                        // accessToken: "$maidId.accessToken",
                        // actualPrice: "$maidId.actualPrice",
                        // agencyId: "$maidId.agencyId",
                        // commission: "$maidId.commission",
                        // countryCode: "$maidId.countryCode",
                        // countryENCode: "$maidId.countryENCode",
                        // countryName: "$maidId.countryName",
                        // currentLocation: "$maidId.currentLocation",
                        // description: "$maidId.description",
                        // deviceToken: "$maidId.deviceToken",
                        // deviceType: "$maidId.deviceType",
                        // dob: "$maidId.dob",
                        // documentPicURL: "$maidId.documentPicURL",
                        // experience: "$maidId.experience",
                        // isBlocked: "$maidId.isBlocked",
                        // isDeleted: "$maidId.isDeleted",
                        // isLogin: "$maidId.isLogin",
                        // languages: "$maidId.languages",
                        // makPrice: "$maidId.makPrice",
                        // nationality: "$maidId.nationality",
                        // nationalityFlag: "$maidId.nationalityFlag",
                        // phoneNo: "$maidId.phoneNo",
                        // price: "$maidId.price",
                        // profilePicURL: "$maidId.profilePicURL",
                        // registrationDate: "$maidId.registrationDate",
                        // religion: "$maidId.religion",
                        // step: "$maidId.step",
                        // timeSlot: "$maidId.timeSlot",
                        // timeZone: "$maidId.timeZone",
                        _id: 0,
                        maidId: "$maidId._id",
                        makId: "$maidId.makId",
                        nationality: "$maidId.nationality",
                        firstName: "$maidId.firstName",
                        lastName: "$maidId.lastName",
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                        agencyCountryName: "$agencyId.countryName",
                        agencyType: "$agencyId.agencyType",
                        avgCleaning: 1,
                        avgIroning: 1,
                        avgCooking: 1,
                        avgChildCare: 1,
                    }
                }
            ];


            if (payloadData.reviewFilterType && payloadData.starFilter) {
                let match = {};
                if (payloadData.reviewFilterType === 'cleaning') {
                    match.$match = {
                        avgCleaning: payloadData.starFilter
                    }
                }
                else if (payloadData.reviewFilterType === 'ironing') {
                    match.$match = {
                        avgIroning: payloadData.starFilter
                    }
                }
                else if (payloadData.reviewFilterType === 'cooking') {
                    match.$match = {
                        avgCooking: payloadData.starFilter
                    }
                }
                else if (payloadData.reviewFilterType === 'childCare') {
                    match.$match = {
                        avgChildCare: payloadData.starFilter
                    }
                }

                pipeline.push(match)
            }

            if (payloadData.search) {
                let match = {
                    $match: {
                        $or: [
                            {firstName: {$regex: payloadData.search, $options: "si"}},
                            {lastName: {$regex: payloadData.search, $options: "si"}},
                            {nationality: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                };
                pipeline2.push(match)
            }
            let sort = [
                {$skip: 0},
               // {$limit: payloadData.limit}
            ];
            pipeline2 = pipeline2.concat(sort);
           // console.log('lengthhhhhhhhhhhhhhhhh',JSON.stringify(pipeline.concat(pipeline2)))
            Models.Maids.aggregate(pipeline.concat(pipeline2), function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        totalMaidCount = result.length;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            });
        },

    }, function (err, result) {
        response.totalMaidRating = totalMaidRating;
        response.totalMaidCount = totalMaidCount;
        callback(err, response)
    });
};

const dashboardAdmin = function (payloadData, userData, callback) {
    let response = {};
    let startOfMonth = moment(moment().startOf('month').format()).tz(payloadData.timeZone).unix() * 1000;
    let endOfMonth = moment(moment().endOf('month').format()).tz(payloadData.timeZone).unix() * 1000;;
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        totalMaidCount: function (checkForUniqueKey, cb) {
            let criteria = {
                isDeleted: false,
                isBlocked: false,
            };
            let project = {};
            let option = {lean: true};
            Service.MaidServices.getMaid(criteria, project, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.maidCount = result.length;
                        cb(null)
                    } else {
                        response.maidCount = 0;
                        cb(null)
                    }
                }
            })
        },

        totalUserCount: function (checkForUniqueKey, cb) {
            let criteria = {
                isDeleted: false,
                isBlocked: false,
                isGuestFlag:false
            };
            let project = {};
            let option = {lean: true};
            Service.UserServices.getUsers(criteria, project, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.userCount = result.length;
                        cb(null)
                    } else {
                        response.userCount = 0;
                        cb(null)
                    }
                }
            })
        },
        totalGuestUserCount: function (checkForUniqueKey, cb) {
            let criteria = {
                isDeleted: false,
                isBlocked: false,
                isGuestFlag:true
            };
            let project = {};
            let option = {lean: true};
            Service.UserServices.getUsers(criteria, project, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.userGuestCount = result.length;
                        cb(null)
                    } else {
                        response.userGuestCount = 0;
                        cb(null)
                    }
                }
            })
        },
        totalAgencyCount: function (checkForUniqueKey, cb) {
            let criteria = {
                isDeleted: false,
                isBlocked: false,
            };
            let project = {};
            let option = {lean: true};
            Service.AgencyServices.getAgency(criteria, project, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.agencyCount = result.length;
                        cb(null)
                    } else {
                        response.agencyCount = 0;
                        cb(null)
                    }
                }
            })
        },

        totalToDateServiceCount: function (checkForUniqueKey, cb) {
            let criteria = {
                workDate: {$lt: new Date().setHours(23, 59, 59, 999)},
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ],
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                // isCompleted: false,
            };
            let projection = {};
            let option = {lean: true};

            Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.totalToDateService = result.length;
                        cb(null)
                    } else {
                        response.totalToDateService = 0;
                        cb(null)
                    }
                }
            })
        },

        totalUpcomingServiceCount: function (checkForUniqueKey, cb) {
            let criteria = {
                workDate: {$gt: new Date().setHours(23, 59, 59, 999)},
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ],
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                // isCompleted: false,
            };
            let projection = {};
            let option = {lean: true};

            Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.upcomingService = result.length;
                        cb(null)
                    } else {
                        response.upcomingService = 0;
                        cb(null)
                    }
                }
            })
        },

        totalToDateRevenue: function (checkForUniqueKey, cb) {
            let criteria = {
                workDate: {$lt: new Date().setHours(23, 59, 59, 999)},
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ],
                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.PENDING, Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT]}
            };
            let projection = {};
            let option = {lean: true};

            Service.ServiceServices.getService(criteria, projection, option, function (err, result) {

                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        let toDateRevenue = 0
                        result.map(obj => {
                            toDateRevenue = toDateRevenue + obj.amount;
                        });
                        response.toDateRevenue = toDateRevenue;
                        cb(null)
                    } else {
                        response.toDateRevenue = 0
                        cb(null)
                    }
                }
            })
        },

        totalUpcomingRevenue: function (checkForUniqueKey, cb) {
            let criteria = {
                workDate: {$gt: new Date().setHours(23, 59, 59, 999)},
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ],
                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.PENDING, Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT]}
            };
            let projection = {};
            let option = {lean: true};

            Service.ServiceServices.getService(criteria, projection, option, function (err, result) {

                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        let upcomingRevenue = 0
                        result.map(obj => {
                            upcomingRevenue = upcomingRevenue + obj.amount;
                        });
                        response.upcomingRevenue = upcomingRevenue;
                        cb(null)
                    } else {
                        response.upcomingRevenue = 0
                        cb(null)
                    }
                }
            })
        },

        totalCurrentMonthRevenue: function (checkForUniqueKey, cb) {
   
            let startOfMonths = moment(moment().startOf('month').format()).unix() * 1000;
            let endOfMonths = moment(moment().endOf('month').format()).unix() * 1000
            console.log("**** StartOfMonths ****"+startOfMonths);
            console.log("**** EndOfMonths ****"+endOfMonths);
            Models.Service.aggregate([  
                
                {
                    $lookup: {
                        from: 'users',
                        localField: 'userId',
                        foreignField: '_id',
                        as: 'userId'
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$userId"
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $facet: {

                        past: [                            
                            {
                                $match: {
                                    workDate: {
                                        $gte: startOfMonths,
                                        // $lte: +new Date()
                                        // $lte: moment().unix() * 1000
                                         $lte: new Date().setHours(23, 59, 59, 999)
                                    },
                                    $and: [
                                        {transactionId: {$exists: true}},
                                        {transactionId: {$nin: ["", null]}},
                                        {userId: {$nin: ["", null]}},
                                        {userId: {$exists: true}}
                                    ],
                                    isCompleted:true,
                                    agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
                                },
                                
                            },
                            {
                                $project: {
                                    pastTotal: {$sum: "$amount"},
                                }
                            }                           
                            
                        ],

                        upcoming: [
                            {
                                $match: {
                                    workDate: {
                                        $gte: +new Date(),
                                        $lte:endOfMonths
                                    },
                                    // workDate: {$gte: +new Date(), $lte: endOfMonth},
                                    $and: [
                                        {transactionId: {$exists: true}},
                                        {transactionId: {$nin: ["", null]}}
                                    ],
                                    agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}

                                }
                            },
                            {
                                $project: {
                                    upcomingTotal: {$sum: "$amount"},
                                }
                            }
                        ]
                    }
                },
                {
                    $unwind: {path: "$past", preserveNullAndEmptyArrays: true}
                },                
                {
                    $unwind: {path: "$upcoming", preserveNullAndEmptyArrays: true}
                },
                {
                    $project: {
                        past: {
                            $ifNull: ['$past.pastTotal', 0]
                        },
                        upcoming: {
                            $ifNull: ['$upcoming.upcomingTotal', 0]
                        }
                    }
                }

            ], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        console.log(result);
                        let totalPastRevenue = 0
                        result.map(obj => {
                            totalPastRevenue = totalPastRevenue + obj.past;
                        });
                         
                        let totalUpcomingRevenue = 0
                        result.map(obj => {
                            totalUpcomingRevenue = totalUpcomingRevenue + obj.upcoming;
                        });

                        response.currentMonthPastRevenue = totalPastRevenue;                        
                        response.currentMonthUpcomingRevenue = totalUpcomingRevenue;
                        response.currentMonthTotalRevenue = totalUpcomingRevenue + totalPastRevenue;
                        // response.currentMonthUpcomingRevenue = result[0].upcoming;
                        
                        cb(null)
                    } else {
                        response.currentMonthPastRevenue = 0;                        
                        response.currentMonthUpcomingRevenue = 0;
                        response.currentMonthTotalRevenue = 0 ;
                        cb(null)
                    }
                }
            })
        },


    }, function (err, result) {
        callback(err, response)
    })
};

const listAllNotificationAdmin = function (payloadData, userData, callback) {
    let respone = [];
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getNotificationData: function (checkForUniqueKey, cb) {
            let criteria = {
                $and: [
                    {adminMessage: {$exists: true}},
                    {adminMessage: {$nin: ["", null]}}
                ],
                isDeleted:{$nin:[userData._id]}
            };
            let projection = {};
            let option = {
                lean: true,
            };
            Service.NotificationServices.getNotification(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        respone = result
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        updateNotificationData: function (getNotificationData, cb) {
            let criteria = {
                receiverId:userData._id,

                /*type: {$in:[
                    Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_REJECT,
                    Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_EXTEND_CANCEL,
                    Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_DELETE,
                ]},*/
                isDeleted:{$nin:[userData._id]},
                read:{$nin:[userData._id]}
            };
            let dataToUpdate = {
                $addToSet:{
                    read: userData._id
                }
            };
            let option = {
                lean: true,
                multi: true,
                new: true
            };
            Service.NotificationServices.updateAllNotification(criteria, dataToUpdate, option, function (err, result) {
                console.log("______________err,result",err,result)
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        },

    }, function (err, result) {
        callback(err, respone)
    })
};

const getNotificationUnredCountAdmin = function (payloadData, userData, callback) {
    let respone = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getNotificationData: function (checkForUniqueKey, cb) {
            let criteria = {
                $and: [
                    {adminMessage: {$exists: true}},
                    {adminMessage: {$nin: ["", null]}}
                ],
                isDeleted:{$nin:[userData._id]},
                read: {$nin:[userData._id]},
            };
            /*let criteria = {
                type: {$in:[
                    Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_REJECT,
                    Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_EXTEND_CANCEL,
                    Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_DELETE,
                ]},
                isDeleted:{$nin:[userData._id]},
                read: {$nin:[userData._id]},
            };*/
            let projection = {};
            let option = {
                lean: true,
            };
            Service.NotificationServices.getNotification(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        respone.unreadCount = result.length;
                        cb(null)
                    } else {
                        respone.unreadCount = 0;
                        cb(null)
                    }
                }
            })
        },

    }, function (err, result) {
        respone.dateOfRegistration=userData.registrationDate;
        callback(err, respone)
    })
};

const  sendNotificationToAll = function (payloadData, userData, callback) {
    let userDeviceToken=[];
    let maidDeviceToken=[];
    let userEmail=[];
    let maidEmail=[];
    let agencyEmail=[];
    var content;
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getUserDeviceToken:function (checkForUniqueKey, cb) {
            if (payloadData.userArray && payloadData.userArray.length) {
                let criteria={
                    _id:{$in:payloadData.userArray}
                };
                let projection={};
                let option={lean:true};
                Service.UserServices.getUsers(criteria,projection,option,function (err, result) {
                    if(err){
                        cb(err)
                    }else{
                        if(result && result.length){
                            result.map(obj=> userDeviceToken.push(obj.deviceToken));
                            result.map(obj=> userEmail.push(obj.email));
                            cb(null)
                        }else{
                            cb(null)
                        }
                    }
                })
            }
            else{
                cb(null)
            }
        },

        getMaidDeviceToken:function (checkForUniqueKey, cb) {
            if (payloadData.maidArray && payloadData.maidArray.length) {
                let criteria={
                    _id:{$in:payloadData.maidArray}
                };
                let projection={};
                let option={lean:true};
                Service.MaidServices.getMaid(criteria,projection,option,function (err, result) {
                    if(err){
                        cb(err)
                    }else{
                        if(result && result.length){
                            result.map(obj=> maidDeviceToken.push(obj.deviceToken));
                            result.map(obj=> maidEmail.push(obj.email));
                            cb(null)
                        }else{
                            cb(null)
                        }
                    }
                })
            }
            else{
                cb(null)
            }
        },

        getAgencyEmail:function (checkForUniqueKey, cb) {
            if (payloadData.agencyArray && payloadData.agencyArray.length) {
                let criteria={
                    _id:{$in:payloadData.agencyArray}
                };
                let projection="email";
                Models.Agency.distinct( projection,criteria,function (err, result) {
                    if(err){
                        cb(err)
                    }else{
                        if(result && result.length){
                            agencyEmail=result;
                            cb(null)
                        }else{
                            cb(null)
                        }
                    }
                })
            }
            else{
                cb(null)
            }
        },

        createNotificationForAll:function (checkForUniqueKey, cb) {
            let allUserArray=[];
            if(payloadData.userArray && payloadData.userArray.length){
                allUserArray =  allUserArray.concat(payloadData.userArray)
            }
            if(payloadData.agencyArray && payloadData.agencyArray.length){
                allUserArray = allUserArray.concat(payloadData.agencyArray)
            }
            if(payloadData.maidArray && payloadData.maidArray.length){
                allUserArray = allUserArray.concat(payloadData.maidArray)
            }
            if(allUserArray && allUserArray.length){
                let dataToSave={
                    receiverArray:allUserArray,
                    type:Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.ALL_USER_TYPE,
                };
                if(payloadData.userArray && payloadData.userArray.length){
                    dataToSave.userMessage=payloadData.message
                }
                if(payloadData.agencyArray && payloadData.agencyArray.length){
                    dataToSave.agencyMessage=payloadData.message
                }
                if(payloadData.maidArray && payloadData.maidArray.length){
                    dataToSave.maidMessage=payloadData.message
                }
                Service.NotificationServices.createNotification(dataToSave,function (err, result) {
                    if(err){
                        cb(err)
                    }else{
                        cb(null)
                    }
                })
            }else{
                cb(null)
            }
        },

        sendPushToUsers: function (getUserDeviceToken, cb) {
            if(userDeviceToken && userDeviceToken.length){
                const serverKeyUser = 'AAAAZzfT5eE:APA91bHp1ItbBCKKyeDs2MAKdUsSsjCJuYwOpLWQLh9WabB2dwIYEVUCD3E2y3XfGv4SlZdrxeY92pQqR5zvgHlke42DrkGgoUeR4e9QMbwJx8-dHkkTrVLiib_N615VqIPUQIAF-bMu';
                let fcm = new FCM(serverKeyUser);
                let message = {
                    registration_ids: userDeviceToken,
                    notification: {
                        sound: 'default',
                        badge: 1,
                        title: "M.A.K",
                        body: payloadData.message,
                        type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.ALL_USER_TYPE,
                        typeId: "",
                    },
                    data: {
                        sound: 'default',
                        badge: 1,
                        title: "M.A.K",
                        body: payloadData.message,
                        type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.ALL_USER_TYPE,
                        typeId: "",
                    },
                    priority: 'high'
                };
                fcm.send(message, function (err, result) {
                    console.log("__________User push___________", err,result)
                });
                cb(null)
            }else{
                cb(null)
            }
        },

        sendPushToMaids:function (getMaidDeviceToken, cb) {
            if(maidDeviceToken && maidDeviceToken.length){
                const serverKeyMaid = 'AAAAul7iVhg:APA91bGEv2S9-dEyvd9D_-Us5cvr7pLcjyIAQS4rqn5f7UbRJXh17p2tBOq8T9iaeCRGe6e2SApjmwTJZjNgmf6TIGhSMyLNEUfjtPLf4ssm4whXcI_CnQvDvw15wKvminp1iymONzKn';
                let fcm1 = new FCM(serverKeyMaid);
                let message = {
                    registration_ids: maidDeviceToken,
                    notification : {
                        sound: 'default',
                        badge: 1,
                        title: "M.A.K",
                        body: payloadData.message,
                        type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.ALL_USER_TYPE,
                        typeId: "",
                    },
                    data: {
                        sound: 'default',
                        badge: 1,
                        title: "M.A.K",
                        body: payloadData.message,
                        type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.ALL_USER_TYPE,
                        typeId: "",
                    },
                    priority: 'high'
                };

                fcm1.send(message, function (err, result) {
                    console.log('maid push..........',err,result)
                });
                cb(null);

            }else{
                cb(null)
            }
        },

        sendEmail:function(cb){

            let data={};
            data.message = payloadData.message
            emailTemplateNotification(data,(err,result)=>{
                if(err){
                    cb(null)
                }
                else{
                    content=result;
                    cb(null)
                }
            })


        },
        createEmailTemplate:function (sendEmail,getUserDeviceToken, getMaidDeviceToken, getAgencyEmail, cb) {
            var subject = payloadData.subject;

            if (userEmail && userEmail.length) {
                //var content = payloadData.message;
                MailManager.sendMailBySES(userEmail, subject, content, function (err, res) {});
            }
            if(maidEmail && maidEmail.length) {
               // var content = payloadData.message;
                MailManager.sendMailBySES(maidEmail, subject, content, function (err, res) {});
            }
            if(agencyEmail && agencyEmail.length) {
                //var content = payloadData.message;
                MailManager.sendMailBySES(agencyEmail, subject, content, function (err, res) {});
            }
            cb()
        }

    },function (err, result) {
        callback(err,{})
    })
};

const emailTemplateNotification =  function(data,callback){

    let content = `
    
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style type="text/css">
        
        body,
        #bodyTable {
            height: 100%!important;
            margin: 0;
            padding: 0;
            width: 100%!important
        }

        #bodyTable {
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            min-height: 100%;
            background: #eee;
            color: #333
        }

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0
        }

        img {
            -ms-interpolation-mode: bicubic
        }

        body {
            margin: 0;
            padding: 0;
            background: #eee
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: 0;
            text-decoration: none
        }

        table {
            border-collapse: collapse!important;
            max-width: 100%!important
        }

        img {
            border: 0!important
        }

        h1 {
            font-family: Helvetica;
            font-size: 42px;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            margin: 30px auto 0
        }

        h2 {
            font-size: 32px;
            font-style: normal;
            font-weight: bold;
            margin: 50px auto 0
        }

        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 25px 0;
            letter-spacing: normal;
            text-align: left
        }

        a h3 {
            color: #444!important;
            text-decoration: none!important
        }

        .titleLink {
            text-decoration: none!important
        }

        .preheaderContent {
            color: #808080;
            font-size: 10px;
            line-height: 125%
        }

        .preheaderContent a:link,
        .preheaderContent a:visited,
        .preheaderContent a .yshortcuts {
            color: #606060;
            font-weight: normal;
            text-decoration: underline
        }

        #emailHeader,
        #tacoTip {
            color: #fff
        }

        #emailHeader {
            background-color: #fff
        }

        #content p {
            color: #4d4d4d;
            margin: 20px 70px 30px;
            font-size: 24px;
            line-height: 32px;
            text-align: center
        }

        #button {
            display: inline-block;
            margin: 10px auto;
            background: #fff;
            border-radius: 4px;
            font-weight: bold;
            font-size: 18px;
            padding: 15px 20px;
            cursor: pointer;
            color: #0079bf;
            margin-bottom: 50px
        }

        #socialIconWrap img {
            line-height: 35px!important;
            padding: 0 5px
        }

        .footerContent div {
            color: #707070;
            font-family: Arial;
            font-size: 12px;
            line-height: 125%;
            text-align: center;
            max-width: 100%!important
        }

        .footerContent div a:link,
        .footerContent div a:visited {
            color: #369;
            font-weight: normal;
            text-decoration: underline
        }

        .footerContent img {
            display: inline
        }

        #socialLinks img {
            margin: 0 2px
        }

        #utility {
            border-top: 1px solid #ddd
        }

        #utility div {
            text-align: center
        }

        #monkeyRewards img {
            max-width: 160px
        }

        #emailFooter {
            max-width: 100%!important
        }

        #footerTwitter a,
        #footerFacebook a {
            text-decoration: none!important;
            color: #fff!important;
            font-size: 14px
        }

        #emailButton {
            border-radius: 6px;
            background: #70b500!important;
            margin: 0 auto 60px;
            box-shadow: 0 4px 0 #578c00
        }

        #socialLinks a {
            width: 40px
        }

        #socialLinks #blogLink {
            width: 80px!important
        }

        .sectionWrap {
            text-align: center
        }

        #header {
            color: #fff!important
        }

    </style>
</head>

<body>
    <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px">
                                        <tbody>
                                            <tr>
                                                <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="http://52.36.127.111/mak_web/img/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120">
                                                    <h1 style="font-size: 30px;">Welcome to MAK</h1>
                                                    <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="middle" style="background:#FFFFFF;">
                                                    <p style="display:block; font-size: 20px;">Message from MAK<br><br>
                                                    ${data.message} <br>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                           
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                           
                            <tr>
                                <td align="center" valign="top">
                                    <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px">
                                        <tbody>
                                            <tr>
                                                <td class="footerContent" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0" style="width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">&nbsp;
                                                                    <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>



    `
    callback(null,content)
};

const createXlsxAdmin = function (payloadData, userData, callback) {
    let response=JSON.parse(payloadData.jsonData);
    let dataToSend=[];
    let buffers;
    let finalUrl;
    let option = {
        save: true,
        sheetName: [],
        fileName: "Current_Month_Summary_Admin" + new Date().getMilliseconds() + ".xlsx",
        path: path.resolve('.')+'/uploads/',
        defaultSheetName: "worksheet"
    };

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        createExcelFormat:function (checkForUniqueKey, cb) {
            let len = response.length;
            let count = 0;
            for (let i = 0; i < len; i++) {
                (function (i) {
                    let obj1 = {};
                    count = count + 1;
                    obj1["Agency Name"] = response[i].maidFirstName + response[i].maidLastName;
                    obj1["Country"] = response[i].maidMakId;

                    for (let j = 0; j < response[i].dailyRevenue.length; j++) {
                        let Day="Day " +j
                        obj1[Day] = response[i].dailyRevenue[j];

                    }
                    obj1["Actual Revenue"] = response[i].pastTotal;
                    obj1["Upcoming Revenue"] = response[i].futureTotal;
                    obj1["Projected Revenue"] = response[i].totalAmount;
                    dataToSend.push(obj1)
                    if (i == len - 1) {
                        cb(null)
                    }
                }(i))
            }
        },

        putData:function (createExcelFormat,cb) {
            var model = mongoXlsx.buildDynamicModel(dataToSend);

            mongoXlsx.mongoData2Xlsx(dataToSend, model, option, function (err, path) {
                cb(null, path);
            });
        },

        createBuffer:function (putData,cb) {
            dataToSend = path.resolve('.')+'/uploads/'+option.fileName;
            fs.readFile(dataToSend,function (err,fileBuffer) {
                if(err){
                    cb(err)
                }else{
                    buffers=fileBuffer;
                    cb(null)
                }
            })
        },

        uploadExcel:function (createBuffer,cb) {
            UploadManager.awsUpload(option.fileName,buffers, function (err, results) {
                if (err)
                    cb(err)
                else {
                    finalUrl=results;
                    cb(null)
                }
            })
        },

        deleteFile:function (uploadExcel,cb) {
            fsExtra.remove(dataToSend, function (err,res) {
                if(err){
                    cb(err)
                }else{
                    cb(null)
                }
            });
        },

    },function (err, result) {
        callback(err,finalUrl)
    })
};

const issueListAdmin = function (payloadData, userData, callback) {
    let response={};
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getIssueList: function (checkForUniqueKey, cb) {
            let criteria = {
                isDeleted:false,
            };
            let projection = {};
            let option = {
                lean: true,
                skip: (((payloadData.pageNo) - 1) * payloadData.limit),
                limit: payloadData.limit

            };
            let populate=[];
            if(payloadData.type == "USER"){
                criteria.userType="USER";
                populate.push({
                    path: 'userId',
                    select: '_id fullName email countryCode phoneNo',
                    model: 'Users'
                })
            }else{
                criteria.userType="MAID";
                populate.push({
                    path: 'userId',
                    select: '_id firstName lastName makId email countryCode phoneNo',
                    model: 'Maids'
                })
            }
            Service.IssueServices.getIssuePopulate(criteria, projection, option, populate, function (err, result) {
                if (err) {
                    cb(err)
                }
                else {
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    }
                    else {
                        response.data = [];
                        cb(null)
                    }
                }
            })
        },

        getIssueListCount: function (checkForUniqueKey, cb) {
            let criteria = {
                isDeleted:false,
            };
            let projection = {};
            let option = {
                lean: true,
            };
            if(payloadData.type == "USER"){
                criteria.userType="USER";
            }else{
                criteria.userType="MAID";
            }
            Service.IssueServices.getIssue(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                }
                else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    }
                    else {
                        response.count = 0;
                        cb(null)
                    }
                }
            })
        }

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, response)
        }
    })
};

const contactList = function (payloadData, userData, callback) {
    let response={};
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getContactList: function (checkForUniqueKey, cb) {
           

            let criteria = {};
            let projection = {};
            let option = {
                lean: true,
                // skip: (((payloadData.pageNo) - 1) * payloadData.limit),
                // limit: payloadData.limit,
                sort :{_id:-1}
            };
            if(payloadData.pageNo && payloadData.limit){
                option.skip = ((payloadData.pageNo - 1) * payloadData.limit);
                option.limit = payloadData.limit;
                // option.push({
                //     $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                // });
                // option.push({
                //     $limit: payloadData.limit
                // })
            } 

            Service.ContactUsServices.getContactUs(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                }
                else {
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    }
                    else {
                        response.data = [];
                        cb(null)
                    }
                }
            })
        },

        getContactListCount: function (checkForUniqueKey, cb) {
            let criteria = {};
            let projection = {};
            let option = {
                lean: true,
            };

            Service.ContactUsServices.getContactUs(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                }
                else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    }
                    else {
                        response.count = 0;
                        cb(null)
                    }
                }
            })
        }

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, response)
        }
    })
};

const changePass = function (payloadData, userData, callback) {

    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkCurrentPasswordAndUpdate: function (checkForUniqueKey, cb) {
            if (UniversalFunctions.CryptData(payloadData.oldPassword) === userData.password) {

                let criteria = {
                    _id: userData._id
                };
                let projection = {
                    password: UniversalFunctions.CryptData(payloadData.newPassword),
                };
                let option = {new: true};

                Service.AdminServices.updateAdmin(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("result***************************in *************changePassword", result);
                        callback(null, Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT)
                    }
                })
            } else {
                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASSWORD)
            }
        },

    }, function (err, result) {
        callback(err, {})
    })
};

const forgetPassword = function (payloadData, callback) {
    let userData = {};
    let passwordResetToken = "";
    let response = {};
    let content;
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        verifyEmail: function (checkForUniqueKey, cb) {
            let criteria = {
                email: payloadData.email
            };

            let projection = {};
            let option = {lean: true};
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result && result.length) {
                        userData = result[0];
                        cb(null)
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_REGISTERED)
                    }
                }
            })
        },

        generatePasswordResetToken: function (verifyEmail, cb) {
            passwordResetToken = (otpGenerator.generate(6, {specialChars: false}));
            let criteria = {
                email: payloadData.email
            };

            let projection = {
                $set: {
                    password: UniversalFunctions.CryptData(passwordResetToken),
                }
            };
            let option = {
                new: true
            };
            Service.AdminServices.updateAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    response.passwordResetToken = passwordResetToken
                    cb(null)
                }
            })
        },
        emailTemplate:function(generatePasswordResetToken,cb){
            let data={};
            data.password=passwordResetToken;

            //console.log("===========userData=======================",userData)
            data.fullName =   userData.name;

            MaidController.emailTemplateForgetPassword(data,(err,result)=>{
                if(err){
                    cb(null)
                }
                else{
                    content=result;
                    cb(null)
                }
            })
        },
        sendMail: function (emailTemplate, cb) {
            if (payloadData.email && (payloadData.email == userData.email)) {
                var subject = "M.A.K Temporary Password";

                MailManager.sendMailBySES(payloadData.email, subject, content, function (err, res) {
                });
                cb()
            }
            else {
                cb(null)
            }
        },

    }, function (err, result) {
        callback(err, response)
    })
};

module.exports = {
    adminLogin: adminLogin,
    forgetPassword: forgetPassword,
    changePass: changePass,
    adminLogout: adminLogout,
    agencySignUp: agencySignUp,
    updateAgencyProfile:updateAgencyProfile,
    addAndUpdateCommissionFeeForTheAgency: addAndUpdateCommissionFeeForTheAgency,
    getAllLanguages: getAllLanguages,
    getAllAgency: getAllAgency,
    blockAgencyByAdmin: blockAgencyByAdmin,
    getAllCustomerDetails: getAllCustomerDetails,
    blockUserByAdmin: blockUserByAdmin,
    getAllMaidDetails: getAllMaidDetails,
    listAllServiceByAdmin: listAllServiceByAdmin,
    listAllCanceledServiceAdmin:listAllCanceledServiceAdmin,
    revenueReportAdmin:revenueReportAdmin,
    lifeTimeSummaryAdmin: lifeTimeSummaryAdmin,
    currentMonthSummaryAdmin: currentMonthSummaryAdmin,
    detailedMonthlyInvoiceAdmin: detailedMonthlyInvoiceAdmin,
    listAllReviewsAdmin: listAllReviewsAdmin,
    dashboardAdmin: dashboardAdmin,
    listAllNotificationAdmin:listAllNotificationAdmin,
    getNotificationUnredCountAdmin:getNotificationUnredCountAdmin,
    sendNotificationToAll:sendNotificationToAll,
    createXlsxAdmin:createXlsxAdmin,
    issueListAdmin:issueListAdmin,
    addEditReligion:addEditReligion,
    getAllReligion:getAllReligion,
    contactList:contactList,

};