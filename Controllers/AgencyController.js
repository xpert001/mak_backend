'use strict';
const Service = require('../Services');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const async = require('async');
const TokenManager = require('../Lib/TokenManager');
const MailManager = require('../Lib/MailManager');
const NotificationManager = require('../Lib/NotificationManager');
const Config = require('../Config');
const Models = require('../Models');
const moment = require('moment');
let fs = require('fs');
let fsExtra = require('fs-extra');
let path = require('path');
const mongoose = require('mongoose');
const _ = require('lodash');
const request = require('request');
const SocketManager = require('../Lib/SocketManager');
const UploadMultipart = require('../Lib/UploadMultipart');
const UploadManager = require('../Lib/UploadManager');
const momentTz = require('moment-timezone');
let mongoXlsx = require('mongo-xlsx');
const otpGenerator = require('otp-generator');
const getCountry = require('country-currency-map').getCountry;
const MaidController = require('../Controllers/MaidController');

const addObjectsToS3 = function (payloadData, userData, callback) {
    let response = {};
    async.auto({
        uploadImageToS3: function (cb) {
            UploadMultipart.uploadFilesOnS3(payloadData.image, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    response = result;
                    cb(null)
                }
            })
        }
    }, function (err, result) {
        if (err)
            callback(err);
        else
            callback(null, {original: response.original, thumbnail: response.thumbnail})
    })
};

const agencyLogin = function (userData, callback) {
    console.log("___________payloaddata_____________", userData)
    let tokenToSend = null;
    let responseToSend = {};
    let tokenData = null;
    async.series([
        function (cb) {
            let uniquieAppKey = userData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        function (cb) {
            let getCriteria = {
                uniquieAppKey: userData.uniquieAppKey,
                email: userData.email,
                password: UniversalFunctions.CryptData(userData.password),
                isVerified: true
            };
            Service.AgencyServices.getAgency(getCriteria, {}, {}, function (err, data) {
                if (err) {
                    cb({errorMessage: 'DB Error: ' + err})
                } else {
                    if (data && data.length > 0 && data[0].email) {
                        tokenData = {
                            id: data[0]._id,
                            username: data[0].agencyName,
                            type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.AGENCY
                        };
                        cb()
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS)
                    }
                }
            });
        },

        function (cb) {
            if (tokenData && tokenData.id) {
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        tokenToSend = output && output.accessToken || null;
                        cb();
                    }
                });

            } else {
                cb()
            }

        },

        function (cb) {
            let setCriteria = {
                email: userData.email
            };
            let setQuery = {
                isLogin: true
            };
            Service.AgencyServices.updateAgency(setCriteria, setQuery, function (err, data) {
                responseToSend = data
                cb(err, data);
            });
        }
    ], function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, responseToSend)
        }
    });
};

const dashBoardData = function (payloadData, userData, callback) {

    let response = {};
    let startOfMonth = moment(moment().startOf('month').format()).tz(payloadData.timeZone).unix() * 1000;

    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getCurrentMonthRevenue: function (checkForUniqueKey, cb) {
            Models.Service.aggregate([
                {
                    $facet: {
                        past: [
                            {
                                $match: {
                                    agencyId: mongoose.Types.ObjectId(userData._id),
                                    workDate: {
                                        $gte: startOfMonth, $lte: +new Date()
                                    },
                                    $and: [
                                        {transactionId: {$exists: true}},
                                        {transactionId: {$nin: ["", null]}}
                                    ],
                                    agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
                                }
                            },
                            {
                                $project: {
                                    pastTotal: {$sum: "$amount"},
                                }
                            }
                        ],

                        upcoming: [
                            {
                                $match: {
                                    agencyId: mongoose.Types.ObjectId(userData._id),
                                    workDate: {$gte: +new Date()},
                                    $and: [
                                        {transactionId: {$exists: true}},
                                        {transactionId: {$nin: ["", null]}}
                                    ],
                                    agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
                                }
                            },
                            {
                                $project: {
                                    upcomingTotal: {$sum: "$amount"},
                                }
                            }
                        ]
                    }
                },
                {
                    $unwind: {path: "$past", preserveNullAndEmptyArrays: true}
                },
                {
                    $unwind: {path: "$upcoming", preserveNullAndEmptyArrays: true}
                },
                {
                    $project: {
                        past: {
                            $ifNull: ['$past.pastTotal', 0]
                        },
                        upcoming: {
                            $ifNull: ['$upcoming.upcomingTotal', 0]
                        }
                    }
                }

            ], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.currentMonthPastRevenue = result[0].past;
                        response.currentMonthUpcomingRevenue = result[0].upcoming;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        totalRevenue: function (checkForUniqueKey, cb) {
            Models.Service.aggregate([
                {
                    $facet: {
                        past: [
                            {
                                $match: {
                                    agencyId: mongoose.Types.ObjectId(userData._id),
                                    workDate: {$lte: +new Date()},
                                    $and: [
                                        {transactionId: {$exists: true}},
                                        {transactionId: {$nin: ["", null]}}
                                    ],
                                    agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
                                }
                            },
                            {
                                $project: {
                                    pastTotal: {$sum: "$amount"},
                                }
                            }
                        ],

                        upcoming: [
                            {
                                $match: {
                                    agencyId: mongoose.Types.ObjectId(userData._id),
                                    workDate: {$gte: +new Date()},
                                    $and: [
                                        {transactionId: {$exists: true}},
                                        {transactionId: {$nin: ["", null]}}
                                    ],
                                    agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
                                }
                            },
                            {
                                $project: {
                                    upcomingTotal: {$sum: "$amount"},
                                }
                            }
                        ]
                    }
                },
                {
                    $unwind: {path: "$past", preserveNullAndEmptyArrays: true}
                },
                {
                    $unwind: {path: "$upcoming", preserveNullAndEmptyArrays: true}
                },
                {
                    $project: {
                        past: {
                            $ifNull: ['$past.pastTotal', 0]
                        },
                        upcoming: {
                            $ifNull: ['$upcoming.upcomingTotal', 0]
                        }
                    }
                }

            ], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.totalPastRevenue = result[0].past;
                        response.totalUpcomingRevenue = result[0].upcoming;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        totalbooking: function (checkForUniqueKey, cb) {
            Models.Service.aggregate([
                {
                    $facet: {
                        past: [
                            {
                                $match: {
                                    agencyId: mongoose.Types.ObjectId(userData._id),
                                    workDate: {$lte: +new Date()},
                                    $and: [
                                        {transactionId: {$exists: true}},
                                        {transactionId: {$nin: ["", null]}}
                                    ],
                                    agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
                                }
                            }
                        ],

                        upcoming: [
                            {
                                $match: {
                                    agencyId: mongoose.Types.ObjectId(userData._id),
                                    workDate: {$gte: +new Date()},
                                    $and: [
                                        {transactionId: {$exists: true}},
                                        {transactionId: {$nin: ["", null]}}
                                    ],
                                    agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
                                }
                            }
                        ]
                    }
                }
            ], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.totalPastBooking = result[0].past.length || 0;
                        response.totalUpcomingBooking = result[0].upcoming.length || 0;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        totalHoursBokked: function (checkForUniqueKey, cb) {
            Models.Service.aggregate([
                {
                    $facet: {
                        past: [
                            {
                                $match: {
                                    agencyId: mongoose.Types.ObjectId(userData._id),
                                    workDate: {$lte: +new Date()},
                                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                                    $and: [
                                        {transactionId: {$exists: true}},
                                        {transactionId: {$nin: ["", null]}}
                                    ],
                                }
                            },
                            {
                                $group: {
                                    _id: null,
                                    pastHours: {$sum: "$duration"}
                                }
                            }
                        ],
                        upcoming: [
                            {
                                $match: {
                                    agencyId: mongoose.Types.ObjectId(userData._id),
                                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                                    workDate: {$gte: +new Date()},
                                    $and: [
                                        {transactionId: {$exists: true}},
                                        {transactionId: {$nin: ["", null]}}
                                    ]
                                }
                            },
                            {
                                $group: {
                                    _id: null,
                                    upcomingHours: {$sum: "$duration"}
                                }
                            }
                        ]
                    }
                },
                {
                    $unwind: {path: "$past", preserveNullAndEmptyArrays: true}
                },
                {
                    $unwind: {path: "$upcoming", preserveNullAndEmptyArrays: true}
                },
                {
                    $project: {
                        past: {
                            $ifNull: ['$past.pastHours', 0]
                        },
                        upcoming: {
                            $ifNull: ['$upcoming.upcomingHours', 0]
                        }
                    }
                }
            ], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.pastHours = result[0].past;
                        response.upcomingHours = result[0].upcoming;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        totalMaidsCount: function (checkForUniqueKey, cb) {
            let criteria = {
                agencyId: userData._id,
                isVerified : true,
                rejectReason :{$eq:''},
                isDeleted: false,
                isBlocked: false,
            };
            Service.MaidServices.count(criteria, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    response.totalMaids = result;
                    cb(null)
                }
            })
        },

        totalAverageRating: function (checkForUniqueKey, cb) {
            Models.Maids.aggregate([
                {
                    $match: {
                        isDeleted: false,
                        isBlocked: false,
                        agencyId: mongoose.Types.ObjectId(userData._id),
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                    }
                },
                {
                    $unwind: {path: "$reviewData", preserveNullAndEmptyArrays: true}
                },
                {
                    $addFields: {
                        maidRating: {
                            reviewData: {
                                $ifNull: ["$reviewData.maidRating",
                                    {
                                        "cleaning": 0,
                                        "ironing": 0,
                                        "cooking": 0,
                                        "childCare": 0
                                    }
                                ]
                            }
                        },
                    }
                },
                {
                    $project: {
                        _id: 0,
                        maidRating: "$maidRating.reviewData"
                    }
                },
                {
                    $group: {
                        _id: "",
                        cleaningRating: {$sum: "$maidRating.cleaning"},
                        ironingRating: {$sum: "$maidRating.ironing"},
                        cookingRating: {$sum: "$maidRating.cooking"},
                        childCareRating: {$sum: "$maidRating.childCare"},
                        cleaningCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cleaning", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        ironingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.ironing", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        cookingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cooking", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        childCareCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.childCare", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                    }
                },
                {
                    $project: {
                        _id: 0,
                        avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                        avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                        avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                        avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                    }
                },

            ], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.averageMaidRating = {
                            "avgCleaning": result[0].avgCleaning,
                            "avgIroning": result[0].avgIroning,
                            "avgCooking": result[0].avgCooking,
                            "avgChildCare": result[0].avgChildCare,
                        }
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }
    }, function (err, result) {
        callback(err, response)
    })
};

const getAgencyProfile = function (payloadData, userData, callback) {
    let response = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getAgencyProfileData: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: userData._id
            };
            let dataToUpdate = {
                password: 0,
                __v: 0,
                uniquieAppKey: 0,
            };
            Service.AgencyServices.getAgency(criteria, dataToUpdate, {lean: true}, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    response = result;
                    cb(null);
                }
            })
        },
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, response);
        }
    })
};

const addMaid = function (payloadData, userData, callback) {
    let response = {};
    let makId = null;
    let criteria = {},content;

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkDuplicateMaid: function (checkForUniqueKey, cb) {
            criteria.email = payloadData.email;
            criteria.isDeleted = false;

            let projection = {};
            let options = {
                lean: true
            };
            console.log("check maid", criteria);
            Service.MaidServices.getMaid(criteria, projection, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_EXIST);
                    } else {
                        cb(null)
                    }
                }
            });
        },

        validateCountryCode: function (checkDuplicateMaid, cb) {

            if (payloadData.countryCode) {
                if (payloadData.countryCode.lastIndexOf('+') == 0) {
                    if (!isFinite(payloadData.countryCode.substr(1))) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                    }

                    // else if (!payloadData.phoneNo || payloadData.phoneNo == '') {
                    //     cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_MISSING);
                    // } 
                    else {
                        cb(null);
                    }
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
            } else if (payloadData.phoneNo) {
                if (!payloadData.countryCode) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.COUNTRY_CODE_MISSING);
                } else if (payloadData.countryCode && payloadData.countryCode.lastIndexOf('+') == 0) {
                    if (!isFinite(payloadData.countryCode.substr(1))) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                    } else {
                        cb(null);
                    }
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
            } else {
                cb(null);
            }
        },

        checkPhoneNumberAlreadyExist: function (validateCountryCode, cb) {
            console.log("********  checkPhoneNumberAlreadyExist ****")
            let criteria1 = {};
            criteria1.countryCode = payloadData.countryCode;
            criteria1.phoneNo = payloadData.phoneNo;
            criteria1.isDeleted = false;
            if(payloadData.phoneNo.trim() !== '') {
                let projection = {};
                let options = {
                    lean: true
                };
                console.log("check maid", criteria1);
                Service.MaidServices.getMaid(criteria1, projection, options, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        if (data && data.length) {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_ALREADY_EXIST);
                        } else {
                            cb(null)
                        }
                    }
                });
            } else {
                cb(null)
            }
            
        },

        createMakId: function (checkPhoneNumberAlreadyExist, cb) {
            console.log("********  createMakId ****")
            Service.MaidServices.getMaid({}, {}, {}, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    makId = "MAID_" + (result.length + 40);
                    cb(null)
                }
            })
        },

        createNewMaid: function (createMakId, cb) {
            console.log("********  createNewMaid  ****")
            console.log("__makiD____", makId);
           // let localCurency = getCountry(payloadData.countryName);
           let dataToInsert = {
                firstName: payloadData.firstName,
                lastName: payloadData.lastName,
                fullName : payloadData.firstName + ' ' + payloadData.lastName,
               // religion: payloadData.religion,
               // gender: payloadData.gender,
                email: payloadData.email,
                // password: UniversalFunctions.CryptData(payloadData.password),
               // nationality: payloadData.nationality,
               // nationalId: payloadData.nationalId,
               // documentPicURL: payloadData.documentPicURL,
                countryENCode: payloadData.countryENCode,
               // countryName: payloadData.countryName,
                countryCode: payloadData.countryCode,
               // dob: payloadData.dob,
                phoneNo: payloadData.phoneNo,
                makId: makId,
                agencyId: userData._id,
                commission: userData.commission,
                uniquieAppKey: UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME),
                step: 0,
                timeZone: payloadData.timeZone,
                isVerified: true,
                acceptAgreement: true,

            };
            console.log("***** password  **********"+payloadData.password)
            if(payloadData.password != '' && payloadData.password != undefined){
                
                dataToInsert.password = UniversalFunctions.CryptData(payloadData.password);
            }
            
            if(payloadData.countryName) {
                dataToInsert.countryName = payloadData.countryName;
                dataToInsert.currency= getCountry(payloadData.countryName);
            }

            Service.MaidServices.createMaid(dataToInsert, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result) {
                        response = result;
                        cb(null)
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                    }
                }
            })
        },
        createEmailTemplate:function(createNewMaid,cb){
            let data =  {
                fullName: payloadData.firstName+' '+payloadData.lastName,
                email : payloadData.email,
                password : payloadData.password,
            };
            if(userData.agencyType !== 'NORMAL'){
                if(userData.agencyType === Config.APP_CONSTANTS.DATABASE.AGENCY_TYPE.MAK_REGISTERED_BAHRAIN )
                    data.agency = 'Bahrain'
                else if(userData.agencyType === Config.APP_CONSTANTS.DATABASE.AGENCY_TYPE.MAK_REGISTERED_UAE)
                    data.agency = 'UAE'
                maidSignupTemplate(data,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        content=result;
                        cb(null)
                    }
                })
            }
            else {
                maidSignupTemplateNormal(data,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        content=result;
                        cb(null)
                    }
                })
            }

        },
        sendMail: function (createEmailTemplate, cb) {

            let subject = "Welcome to MAK!!";
            if(payloadData.email !== '' && payloadData.email !== undefined){
                MailManager.sendMailBySES(payloadData.email, subject, content, function (err, res) {
                });
                cb()
            } else {
                cb()
            }
           
        },
    }, function (err, result) {
        console.log("********  final result  ****")
        callback(err, response)
    })
};

function maidSignupTemplate(data,callback) {
    let result =`<head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width"> <title></title> <style type="text/css"> body, #bodyTable { height: 100%!important; margin: 0; padding: 0; width: 100%!important } #bodyTable { font-family: Helvetica Neue, Helvetica, Arial, sans-serif; min-height: 100%; background: #eee; color: #333 } body, table, td, p, a, li, blockquote { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100% } table, td { mso-table-lspace: 0; mso-table-rspace: 0 } img { -ms-interpolation-mode: bicubic } body { margin: 0; padding: 0; background: #eee } img { border: 0; height: auto; line-height: 100%; outline: 0; text-decoration: none } table { border-collapse: collapse!important; max-width: 100%!important } img { border: 0!important } h1 { font-family: Helvetica; font-size: 42px; font-style: normal; font-weight: bold; text-align: center; margin: 30px auto 0 } h2 { font-size: 32px; font-style: normal; font-weight: bold; margin: 50px auto 0 } h3 { font-size: 20px; font-weight: bold; margin: 25px 0; letter-spacing: normal; text-align: left } a h3 { color: #444!important; text-decoration: none!important } .titleLink { text-decoration: none!important } .preheaderContent { color: #808080; font-size: 10px; line-height: 125% } .preheaderContent a:link, .preheaderContent a:visited, .preheaderContent a .yshortcuts { color: #606060; font-weight: normal; text-decoration: underline } #emailHeader, #tacoTip { color: #fff } #emailHeader { background-color: #fff } #content p { color: #4d4d4d; margin: 20px 70px 30px; font-size: 24px; line-height: 32px; text-align: center } #button { display: inline-block; margin: 10px auto; background: #fff; border-radius: 4px; font-weight: bold; font-size: 18px; padding: 15px 20px; cursor: pointer; color: #0079bf; margin-bottom: 50px } #socialIconWrap img { line-height: 35px!important; padding: 0 5px } .footerContent div { color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center; max-width: 100%!important } .footerContent div a:link, .footerContent div a:visited { color: #369; font-weight: normal; text-decoration: underline } .footerContent img { display: inline } #socialLinks img { margin: 0 2px } #utility { border-top: 1px solid #ddd } #utility div { text-align: center } #monkeyRewards img { max-width: 160px } #emailFooter { max-width: 100%!important } #footerTwitter a, #footerFacebook a { text-decoration: none!important; color: #fff!important; font-size: 14px } #emailButton { border-radius: 6px; background: #70b500!important; margin: 0 auto 60px; box-shadow: 0 4px 0 #578c00 } #socialLinks a { width: 40px } #socialLinks #blogLink { width: 80px!important } .sectionWrap { text-align: center } #header { color: #fff!important } </style></head><body> <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%"> <tbody> <tr> <td align="center" valign="top"> <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px"> <tbody> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%"> <tbody> <tr> <td> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td> </tr> </tbody> </table> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px"> <tbody> <tr> <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="http://52.36.127.111/mak_web/img/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120"> <h1 style="font-size: 30px;">Welcome to MAK</h1> <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p> </td> </tr> <tr> <td align="left" style="background:#FFFFFF;"> <br> Dear <b>${data.fullName}</b>! <br><br> Congratulations!! You are successfully registered with MAK.  <br><br></td></tr> <tr><td align="left" style="background:#FFFFFF;"> You are currently a legal resident in the Kingdom of ${data.agency} and in possession of a valid FlexPermit, issued by the Labour Market Regulatory Authority of the Kingdom of ${data.agency} (“LMRA”) or a holder of a Residency Permit and CPR - identifying you as a Domestic Employee - plus a Non-Objection Certificate issued by your sponsor/employer allowing you to engage in part-time work outside your regular working hours.<br><br>Please contact us at +973 39414244 should any of the aforementioned not apply to you. Please note that in that case you may not be able to register on <a href="https://mak.today">MAK.TODAY</a> as a MAK Registered Maid.<br><br> </td> </tr> <tr><td align="left" style="background:#FFFFFF;">Please use the below details to log in to the MAK Helper app:<br><br> <b>Email : ${data.email}</b> .<br> <b>Password : ${data.password}</b> . </p> </td> </tr> <tr> <td align="left">Please download our MAK Helper app using the below link:<br><br> </td> </tr><tr> <td align="center" bgcolor="#61BD4F" border="0" cellpadding="0" cellspacing="0" class="emailButton" id="button" style=" border-radius:6px; display:inline-block; margin:0px auto 60px; padding: 10px 14px;"> <div><span style="font-size:20px"><a href="$\{data.link}" style="color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:500; letter-spacing: 0.4px; line-height:48px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;font-size:18px !important"><img src="https://s3-us-west-2.amazonaws.com/mak-s3/29793763_343336722842176_8545984603976892416_n.png"></a></span>&nbsp;&nbsp;&nbsp;<span style="font-size:20px"><a href="$\{data.link}" style="color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:500; letter-spacing: 0.4px; line-height:48px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;font-size:18px !important"><img src="https://s3-us-west-2.amazonaws.com/mak-s3/30124862_343336729508842_3973615652479959040_n.png"</a></span>&nbsp;  </div> </td> </tr> <tr> <td align="left">Sincerely,<br>MAK Team<br><br></td></tr> </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px"> <tbody> <tr> <td class="footerContent" valign="top"> <table border="0" cellpadding="10" cellspacing="0" style="width:100%"> <tbody> <tr> <td valign="top">&nbsp; <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table></body>`
    callback(null,result)
}

function maidSignupTemplateNormal(data,callback) {
    let result =`<head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width"> <title></title> <style type="text/css"> body, #bodyTable { height: 100%!important; margin: 0; padding: 0; width: 100%!important } #bodyTable { font-family: Helvetica Neue, Helvetica, Arial, sans-serif; min-height: 100%; background: #eee; color: #333 } body, table, td, p, a, li, blockquote { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100% } table, td { mso-table-lspace: 0; mso-table-rspace: 0 } img { -ms-interpolation-mode: bicubic } body { margin: 0; padding: 0; background: #eee } img { border: 0; height: auto; line-height: 100%; outline: 0; text-decoration: none } table { border-collapse: collapse!important; max-width: 100%!important } img { border: 0!important } h1 { font-family: Helvetica; font-size: 42px; font-style: normal; font-weight: bold; text-align: center; margin: 30px auto 0 } h2 { font-size: 32px; font-style: normal; font-weight: bold; margin: 50px auto 0 } h3 { font-size: 20px; font-weight: bold; margin: 25px 0; letter-spacing: normal; text-align: left } a h3 { color: #444!important; text-decoration: none!important } .titleLink { text-decoration: none!important } .preheaderContent { color: #808080; font-size: 10px; line-height: 125% } .preheaderContent a:link, .preheaderContent a:visited, .preheaderContent a .yshortcuts { color: #606060; font-weight: normal; text-decoration: underline } #emailHeader, #tacoTip { color: #fff } #emailHeader { background-color: #fff } #content p { color: #4d4d4d; margin: 20px 70px 30px; font-size: 24px; line-height: 32px; text-align: center } #button { display: inline-block; margin: 10px auto; background: #fff; border-radius: 4px; font-weight: bold; font-size: 18px; padding: 15px 20px; cursor: pointer; color: #0079bf; margin-bottom: 50px } #socialIconWrap img { line-height: 35px!important; padding: 0 5px } .footerContent div { color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center; max-width: 100%!important } .footerContent div a:link, .footerContent div a:visited { color: #369; font-weight: normal; text-decoration: underline } .footerContent img { display: inline } #socialLinks img { margin: 0 2px } #utility { border-top: 1px solid #ddd } #utility div { text-align: center } #monkeyRewards img { max-width: 160px } #emailFooter { max-width: 100%!important } #footerTwitter a, #footerFacebook a { text-decoration: none!important; color: #fff!important; font-size: 14px } #emailButton { border-radius: 6px; background: #70b500!important; margin: 0 auto 60px; box-shadow: 0 4px 0 #578c00 } #socialLinks a { width: 40px } #socialLinks #blogLink { width: 80px!important } .sectionWrap { text-align: center } #header { color: #fff!important } </style></head><body> <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%"> <tbody> <tr> <td align="center" valign="top"> <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px"> <tbody> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%"> <tbody> <tr> <td> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td> </tr> </tbody> </table> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px"> <tbody> <tr> <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="http://52.36.127.111/mak_web/img/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120"> <h1 style="font-size: 30px;">Welcome to MAK</h1> <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p> </td> </tr> <tr> <td align="left" style="background:#FFFFFF;"> <br> Dear <b>${data.fullName}</b>! <br><br> Congratulations!! You are registered with MAK.  <br><br></td></tr> <tr><td align="left" style="background:#FFFFFF;"><br>Please contact us at +973 39414244 should any of the aforementioned not apply to you. Please note that in that case you may not be able to register on <a href="https://mak.today">MAK.TODAY</a> as a MAK Registered Maid.<br><br> </td> </tr> <tr><td align="left" style="background:#FFFFFF;"></p> </td> </tr> <tr> <td align="left">Please download our MAK Helper app using the below link:<br><br> </td> </tr><tr> <td align="center" bgcolor="#61BD4F" border="0" cellpadding="0" cellspacing="0" class="emailButton" id="button" style=" border-radius:6px; display:inline-block; margin:0px auto 60px; padding: 10px 14px;"> <div><span style="font-size:20px"><a href="${data.link}" style="color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:500; letter-spacing: 0.4px; line-height:48px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;font-size:18px !important"><img src="https://s3-us-west-2.amazonaws.com/mak-s3/29793763_343336722842176_8545984603976892416_n.png"></a></span>&nbsp;&nbsp;&nbsp;<span style="font-size:20px"><a href="${data.link}" style="color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:500; letter-spacing: 0.4px; line-height:48px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;font-size:18px !important"><img src="https://s3-us-west-2.amazonaws.com/mak-s3/30124862_343336729508842_3973615652479959040_n.png"</a></span>&nbsp;  </div> </td> </tr> <tr> <td align="left">Sincerely,<br>MAK Team<br><br></td></tr> </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px"> <tbody> <tr> <td class="footerContent" valign="top"> <table border="0" cellpadding="10" cellspacing="0" style="width:100%"> <tbody> <tr> <td valign="top">&nbsp; <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table></body>`
    callback(null,result)
}

const updateMaidProfile = function (payloadData, userData, callback) {
    let response = {};
    let slotsArray = [];
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        checkNationalId :(checkForUniqueKey,cb)=>{
            if(payloadData.step ===1){
                let criteria = {
                    nationalId: payloadData.nationalId,
                    isDeleted: false,
                };
                Service.MaidServices.getMaid(criteria, {}, {lean: true}, function (err, result) {
                    if (err) {cb(err)}
                    else {
                        if (result.length) {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NATIONAL_ID_EXIST)
                        } else {
                            cb()
                        }
                    }
                })
            }
            else cb()
        },
        checkDuplicateMaid: function (checkNationalId, cb) {
            let criteria = {};
            criteria.email = payloadData.email;
            criteria.isDeleted = false;
            criteria._id ={ $ne: payloadData.maidId };

            let projection = {};
            let options = {
                lean: true
            };
            console.log("check maid", criteria,userData);
            Service.MaidServices.getMaid(criteria, projection, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_EXIST);
                    } else {
                        cb(null)
                    }
                }
            });
        },


        checkPhoneNumberAlreadyExist: function (checkDuplicateMaid, cb) {
            let criteria1 = {};
            criteria1.countryCode = payloadData.countryCode;
            criteria1.phoneNo = payloadData.phoneNo;
            criteria1.isDeleted = false;
            criteria1._id = { $ne: payloadData.maidId };

            let projection = {};
            let options = {
                lean: true
            };
            console.log("check maid", criteria1);
            Service.MaidServices.getMaid(criteria1, projection, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_ALREADY_EXIST);
                    } else {
                        cb(null)
                    }
                }
            });
        },

        checkMaidAvailableForUpdate: function (checkPhoneNumberAlreadyExist, cb) {
            let criteria = {
                _id: payloadData.maidId,
                isDeleted: false,
            };

            Service.MaidServices.getMaid(criteria, {}, {lean: true}, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if (result && result.length) {
                        cb(null)
                    } else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DATA_NOT_FOUND)
                    }
                }
            })
        },

        updateMaidInfo: function (checkMaidAvailableForUpdate, cb) {
            let criteria = {
                _id: payloadData.maidId,
                isDeleted: false
            };
            if(payloadData.languages)
                payloadData.languages.map((obj)=>{
                    return mongoose.Types.ObjectId(obj)
                });

            let dataToUpdate = {},lang =[];
            if (payloadData.step === 1) {

                if (payloadData.dob) {
                    dataToUpdate.dob = payloadData.dob;
                }
                if (payloadData.gender) {
                    dataToUpdate.gender = payloadData.gender;
                }

                if (payloadData.nationality) {
                    dataToUpdate.nationality = payloadData.nationality;
                }

                if (payloadData.material) {
                    dataToUpdate.material = payloadData.material;
                }

                if (payloadData.religion) {
                    dataToUpdate.religion = payloadData.religion;
                }

                if (payloadData.nationalId) {
                    dataToUpdate.nationalId = payloadData.nationalId;
                }
                if (payloadData.documentPicURL) {
                    dataToUpdate.documentPicURL = payloadData.documentPicURL;
                }

                dataToUpdate.profilePicURL = payloadData.profilePicURL;
                dataToUpdate.experience = payloadData.experience;
                dataToUpdate.dateJoinedAgency = payloadData.dateJoinedAgency || 0;
                dataToUpdate.description = payloadData.description;
                dataToUpdate.languages = payloadData.languages;
                dataToUpdate.price = payloadData.price;
                // dataToUpdate.currency = payloadData.currency;
                dataToUpdate.actualPrice = (payloadData.price) + (payloadData.price) * (userData.commission / 100);
                dataToUpdate.makPrice = (payloadData.price) * (userData.commission / 100);
                dataToUpdate.step = 1;
                dataToUpdate.maidAppStep = 1;
            }

            else if (payloadData.step === 2) {
                if (payloadData.timeSlot) {
                    dataToUpdate.$set = {
                        timeSlot: payloadData.timeSlot
                    }
                }
                if (payloadData.material) {
                    dataToUpdate.material = payloadData.material;
                }
                dataToUpdate.step = 2;
                dataToUpdate.maidAppStep = 2;
            }
            else if (payloadData.step === 3) {
                let curr = getCountry(payloadData.maidAddress.countryName)
                dataToUpdate.currentLocation = [payloadData.long, payloadData.lat];
                dataToUpdate.locationName = payloadData.locationName;
                dataToUpdate.currency = curr.currency;
                dataToUpdate.maidAddress = payloadData.maidAddress;
                dataToUpdate.countryName = payloadData.maidAddress.countryName;
                dataToUpdate.step = 3;
                dataToUpdate.maidAppStep = 3;
                if (payloadData.material) {
                    dataToUpdate.material = payloadData.material;
                }
            }
            else if (!payloadData.step) {

                if (payloadData.timeSlot) {
                    dataToUpdate.$set = {
                        timeSlot: payloadData.timeSlot
                    }
                }

                if (payloadData.firstName) {
                    dataToUpdate.firstName = payloadData.firstName;
                }

                if (payloadData.lastName) {
                    dataToUpdate.lastName = payloadData.lastName;
                }
                if(payloadData.firstName && payloadData.lastName){
                    dataToUpdate.fullName = payloadData.firstName + ' ' + payloadData.lastName;
                }

                if (payloadData.material) {
                    dataToUpdate.material = payloadData.material;
                }
                if (payloadData.email) {
                    dataToUpdate.email = payloadData.email;
                }

                if (payloadData.countryENCode && payloadData.countryCode && payloadData.phoneNo) {
                    dataToUpdate.countryENCode = payloadData.countryENCode;
                    dataToUpdate.countryCode = payloadData.countryCode;
                    dataToUpdate.phoneNo = payloadData.phoneNo;
                }

                if (payloadData.profilePicURL) {
                    dataToUpdate.profilePicURL = payloadData.profilePicURL;
                }

                if (payloadData.dob) {
                    dataToUpdate.dob = payloadData.dob;
                }
                if (payloadData.gender) {
                    dataToUpdate.gender = payloadData.gender;
                }

                if (payloadData.nationality) {
                    dataToUpdate.nationality = payloadData.nationality;
                }

                if (payloadData.religion) {
                    dataToUpdate.religion = payloadData.religion;
                }

                if (payloadData.nationalId) {
                    dataToUpdate.nationalId = payloadData.nationalId;
                }

                if (payloadData.documentPicURL) {
                    dataToUpdate.documentPicURL = payloadData.documentPicURL;
                }

                if (payloadData.experience) {
                    dataToUpdate.experience = payloadData.experience;
                }

                if (payloadData.languages) {
                    dataToUpdate.languages = payloadData.languages;
                }

                if (payloadData.description) {
                    dataToUpdate.description = payloadData.description;
                }

                if (payloadData.price) {
                    dataToUpdate.price = payloadData.price;
                    dataToUpdate.actualPrice = (payloadData.price) + (payloadData.price) * (userData.commission / 100);
                    dataToUpdate.makPrice = (payloadData.price) * (userData.commission / 100);
                }



                if (payloadData.locationName && payloadData.lat && payloadData.long && payloadData.maidAddress) {

                    dataToUpdate.currentLocation = [payloadData.long, payloadData.lat];
                    dataToUpdate.locationName = payloadData.locationName;
                    dataToUpdate.maidAddress = payloadData.maidAddress;

                }
            }

            else {
                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
            }

            if(payloadData.radius){
                dataToUpdate.radius= payloadData.radius
            }
            let option = {new: true};
            Service.MaidServices.updateMaid(criteria, dataToUpdate, option, function (err, data) {
                if (err) {
                    cb(err)
                }
                else {
                    if (data && data._id) {
                        response = data;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

    }, function (err, result) {
        callback(err, response)
    })
};

const listAllUnVerifiedMaids = function (payloadData, userData, callback) {
    let response = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        listMaids: function (checkForUniqueKey, cb) {
            let criteria = {
                agencyId: userData._id,
                isDeleted: false,
                isVerified: false,
                rejectReason :{$eq:''}
              /*  $and: [
                    {rejectReason: {$exists: false}},
                    {rejectReason: {$in: ["", null]}}
                ]*/
            };

            if (payloadData.searchMaidName) {
                criteria.$or = [
                    {firstName: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {lastName: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {phoneNo: {$regex: payloadData.searchMaidName, $options: "si"}}
                ]
            }
            if (payloadData.maidId) {
                criteria._id = payloadData.maidId
            }

            let projection = {};
            let option = {
                lean: true,
                skip: ((payloadData.pageNo - 1) * payloadData.limit),
                sort:{_id:-1},
                limit: payloadData.limit
            };
            let populate = [
                {
                    path: 'agencyId',
                    select: '_id agencyName',
                    model: 'Agency'
                },
                {
                    path: 'languages',
                    select: '_id languageName',
                    model: 'Languages'
                },
            ];

            Service.MaidServices.getMaidPopulate(criteria, projection, option, populate, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        if (payloadData.maidId) {
                            if (result[0].step == 3 && result[0].maidAppStep == 2) {
                                result[0].step = 2;
                                response.data = result;
                            } else {
                                response.data = result;
                            }
                        } else {
                            response.data = result;
                        }
                        cb(null)
                    } else {
                        response.data = [];
                        cb(null)
                    }
                }
            })
        },

        listMaidsCount: function (checkForUniqueKey, cb) {
            let criteria = {
                agencyId: userData._id,
                isDeleted: false,
                isVerified: false,
                rejectReason :{$eq:''}
                /*$and: [
                    {rejectReason: {$exists: false}},
                    {rejectReason: {$in: ["", null]}}
                ]*/
            };


            if (payloadData.searchMaidName) {
                criteria.$or = [
                    {firstName: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {lastName: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {phoneNo: {$regex: payloadData.searchMaidName, $options: "si"}}
                ]
            }
            if (payloadData.maidId) {
                criteria._id = payloadData.maidId
            }

            let projection = {};
            let option = {lean: true};

            Service.MaidServices.getMaid(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        response.count = 0;
                        cb(null)
                    }
                }
            })
        },

    }, function (err, result) {
        callback(err, response)
    });
};

const verifyMaid = function (payloadData, userData, callback) {
    console.log("________________payload", payloadData);
    let maidData,content ;
    async.auto({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        updateMaidInfo:['checkForUniqueKey', function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.maidId,
                isDeleted: false
            }
            let dataToUpdate = {};

            if (payloadData.reject) {
                dataToUpdate.rejectReason = payloadData.rejectReason
            } else {
                dataToUpdate.isVerified = true,
                    dataToUpdate.rejectReason =''
            }

            let option = {new: true};
            Service.MaidServices.updateMaid(criteria, dataToUpdate, option, function (err, data) {
                if (err) {
                    cb(err)
                }
                else {
                    if (data && data._id) {
                        maidData = data;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }],
        updateJoinDate :['updateMaidInfo',(err,cb)=>{
            if(maidData.dateJoinedAgency === 0){
                let criteria = {
                    _id: payloadData.maidId,
                    isDeleted: false
                }
                let dataToUpdate = {};
                Service.MaidServices.updateMaid(criteria,{dateJoinedAgency:+new Date()}, {new:true}, function (err, data) {
                    if (err) {cb(err)
                    }
                    else {
                        cb()
                    }
                })
            }
            else cb()
        }],
        createEmailTemplate:['updateMaidInfo',function(err,cb){
                let data =  {
                    fullName: maidData.firstName+' '+maidData.lastName,
                    email: maidData.email,
                    // link : "http://52.36.127.111/MAK/AgencyPanel/#/maidAgreement?email=" + payloadData.email
                };
                if(maidData.passwordResetToken === '' && !payloadData.reject ){
                    data.password = '**********';
                    emailTemplate(data,(err,result)=>{   //not send the password
                        if(err){
                            cb(err)
                        }
                        else{
                            content=result;
                            cb(null)
                        }
                    })
                }
                else if(!payloadData.reject){
                    data.password = maidData.passwordResetToken;
                    emailTemplate(data,(err,result)=>{
                        if(err){             //send the password
                            cb(err)
                        }
                        else{
                            content=result;
                            cb(null)
                        }
                    })
                }
                else {
                    rejectTemp(data,(err,result)=>{
                        if(err){cb(err)
                        }
                        else{
                            content=result;
                            cb(null)
                        }
                    })
                }
        }],
        sendMail: ['createEmailTemplate',function (updateMaidInfo, cb) {
                let subject = "";
                if (payloadData.reject) {
                    subject = "AGENCY REJECTED YOUR REGISTRATION REQUEST";
                } else {
                    subject = "AGENCY ACCEPTED YOUR REGISTRATION REQUEST";
                }

                MailManager.sendMailBySES(maidData.email, subject, content, function (err, res) {
                });
                cb()
        }]

    }, function (err, result) {
        callback(err, {})
    })
};

const bookingCancel =  function(data,callback){

    let content = `
 <head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width"> <title></title> <style type="text/css"> body, #bodyTable { height: 100%!important; margin: 0; padding: 0; width: 100%!important } #bodyTable { font-family: Helvetica Neue, Helvetica, Arial, sans-serif; min-height: 100%; background: #eee; color: #333 } body, table, td, p, a, li, blockquote { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100% } table, td { mso-table-lspace: 0; mso-table-rspace: 0 } img { -ms-interpolation-mode: bicubic } body { margin: 0; padding: 0; background: #eee } img { border: 0; height: auto; line-height: 100%; outline: 0; text-decoration: none } table { border-collapse: collapse!important; max-width: 100%!important } img { border: 0!important } h1 { font-family: Helvetica; font-size: 42px; font-style: normal; font-weight: bold; text-align: center; margin: 30px auto 0 } h2 { font-size: 32px; font-style: normal; font-weight: bold; margin: 50px auto 0 } h3 { font-size: 20px; font-weight: bold; margin: 25px 0; letter-spacing: normal; text-align: left } a h3 { color: #444!important; text-decoration: none!important } .titleLink { text-decoration: none!important } .preheaderContent { color: #808080; font-size: 10px; line-height: 125% } .preheaderContent a:link, .preheaderContent a:visited, .preheaderContent a .yshortcuts { color: #606060; font-weight: normal; text-decoration: underline } #emailHeader, #tacoTip { color: #fff } #emailHeader { background-color: #fff } #content p { color: #4d4d4d; margin: 20px 70px 30px; font-size: 24px; line-height: 32px; text-align: left } #button { display: inline-block; margin: 10px auto; background: #fff; border-radius: 4px; font-weight: bold; font-size: 18px; padding: 15px 20px; cursor: pointer; color: #0079bf; margin-bottom: 50px } #socialIconWrap img { line-height: 35px!important; padding: 0 5px } .footerContent div { color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center; max-width: 100%!important } .footerContent div a:link, .footerContent div a:visited { color: #369; font-weight: normal; text-decoration: underline } .footerContent img { display: inline } #socialLinks img { margin: 0 2px } #utility { border-top: 1px solid #ddd } #utility div { text-align: center } #monkeyRewards img { max-width: 160px } #emailFooter { max-width: 100%!important } #footerTwitter a, #footerFacebook a { text-decoration: none!important; color: #fff!important; font-size: 14px } #emailButton { border-radius: 6px; background: #70b500!important; margin: 0 auto 60px; box-shadow: 0 4px 0 #578c00 } #socialLinks a { width: 40px } #socialLinks #blogLink { width: 80px!important } .sectionWrap { text-align: center } #header { color: #fff!important } </style></head><body> <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%"> <tbody> <tr> <td align="center" valign="top"> <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px"> <tbody> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%"> <tbody> <tr> <td> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td> </tr> </tbody> </table> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px"> <tbody> <tr> <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="https://s3-us-west-2.amazonaws.com/mak-s3/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120"> <h1 style="font-size: 30px;"></h1> <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p> </td> </tr> 

	<tr> <td align="left" style="background:#FFFFFF;"> <p>Dear ${data.userName}, <br>Unfortunately the maid / agency had to cancel your job as no replacement can be found.   

As per our agreed Terms & Conditions, you will receive a full refund.</p> </td> </tr> 


	<tr> 
		 <td style="text-align: left; color:#000; font-weight: bold; padding: 0 10px">

             <table style="width: 100%; border: solid 1px #ccc; margin-top: 20px; text-align: center;">
             <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Number</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.bookingNumber}</td>
             	</tr>
             	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Maid Name</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.maidName}</td>
             	</tr>
             	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Maid ID</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.maidId}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Agency Name</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.agencyName}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Time</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.bookingTime}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Date</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.bookingDate}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Duration</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.duration}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Villa Number</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.villa}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Block Number</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.block}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Road Number/Name</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.road}</td>
             	</tr>
               <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Additional Details</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.details}</td>
             	</tr>
               <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">City</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.city}</td>
             	</tr>
                 <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Rate/Hour</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.rate}</td>
             	</tr>
                 <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Total Amount</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.amount}</td>
             	</tr>
             </table>

		 </td>
	</tr>


	
	<tr>  </div> </td> </tr> <tr> <tr> <td align="left" style="background:#FFFFFF;"> <p>Please contact us if you have any questions.<br>
Sincerely,<br>MAK Team </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px"> <tbody> <tr> <td class="footerContent" valign="top"> <table border="0" cellpadding="10" cellspacing="0" style="width:100%"> <tbody> <tr> <td valign="top">&nbsp; <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table></body>

    `
    callback(null,content)
};

const emailTemplate =  function(data,callback){

    let content = `
    <head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width"> <title></title> <style type="text/css"> body, #bodyTable { height: 100%!important; margin: 0; padding: 0; width: 100%!important } #bodyTable { font-family: Helvetica Neue, Helvetica, Arial, sans-serif; min-height: 100%; background: #eee; color: #333 } body, table, td, p, a, li, blockquote { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100% } table, td { mso-table-lspace: 0; mso-table-rspace: 0 } img { -ms-interpolation-mode: bicubic } body { margin: 0; padding: 0; background: #eee } img { border: 0; height: auto; line-height: 100%; outline: 0; text-decoration: none } table { border-collapse: collapse!important; max-width: 100%!important } img { border: 0!important } h1 { font-family: Helvetica; font-size: 42px; font-style: normal; font-weight: bold; text-align: center; margin: 30px auto 0 } h2 { font-size: 32px; font-style: normal; font-weight: bold; margin: 50px auto 0 } h3 { font-size: 20px; font-weight: bold; margin: 25px 0; letter-spacing: normal; text-align: left } a h3 { color: #444!important; text-decoration: none!important } .titleLink { text-decoration: none!important } .preheaderContent { color: #808080; font-size: 10px; line-height: 125% } .preheaderContent a:link, .preheaderContent a:visited, .preheaderContent a .yshortcuts { color: #606060; font-weight: normal; text-decoration: underline } #emailHeader, #tacoTip { color: #fff } #emailHeader { background-color: #fff } #content p { color: #4d4d4d; margin: 20px 70px 30px; font-size: 24px; line-height: 32px; text-align: center } #button { display: inline-block; margin: 10px auto; background: #fff; border-radius: 4px; font-weight: bold; font-size: 18px; padding: 15px 20px; cursor: pointer; color: #0079bf; margin-bottom: 50px } #socialIconWrap img { line-height: 35px!important; padding: 0 5px } .footerContent div { color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center; max-width: 100%!important } .footerContent div a:link, .footerContent div a:visited { color: #369; font-weight: normal; text-decoration: underline } .footerContent img { display: inline } #socialLinks img { margin: 0 2px } #utility { border-top: 1px solid #ddd } #utility div { text-align: center } #monkeyRewards img { max-width: 160px } #emailFooter { max-width: 100%!important } #footerTwitter a, #footerFacebook a { text-decoration: none!important; color: #fff!important; font-size: 14px } #emailButton { border-radius: 6px; background: #70b500!important; margin: 0 auto 60px; box-shadow: 0 4px 0 #578c00 } #socialLinks a { width: 40px } #socialLinks #blogLink { width: 80px!important } .sectionWrap { text-align: center } #header { color: #fff!important } </style></head><body> <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%"> <tbody> <tr> <td align="center" valign="top"> <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px"> <tbody> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%"> <tbody> <tr> <td> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td> </tr> </tbody> </table> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px"> <tbody> <tr> <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="http://52.36.127.111/mak_web/img/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120"> <h1 style="font-size: 30px;">Welcome to MAK</h1> <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p> </td> </tr> <tr> <td align="left" style="background:#FFFFFF;"> <br> Dear <b>${data.fullName}</b>! <br><br> Thank you for MAKing it! Your verification process with MAK is completed! <br><br></td></tr> <tr><td align="left" style="background:#FFFFFF;"> You are currently a legal resident in the Kingdom of Bahrain and in possession of a valid FlexPermit, issued by the Labour Market Regulatory Authority of the Kingdom of Bahrain (“LMRA”) or a holder of a Residency Permit and CPR - identifying you as a Domestic Employee - plus a Non-Objection Certificate issued by your sponsor/employer allowing you to engage in part-time work outside your regular working hours.<br><br>Please contact us at +973 39414244 should any of the aforementioned not apply to you. Please note that in that case you may not be able to register on <a href="https://mak.today">MAK.TODAY</a> as a MAK Registered Maid.<br><br> </td> </tr> <tr><td align="left" style="background:#FFFFFF;">Please use the below details to log in to the MAK Helper app:<br><br> <b>Email : ${data.email}</b> .<br> <b>Password : ${data.password}</b> . </p> </td> </tr> <tr> <td align="left">Please download our MAK Helper app using the below link:<br><br> </td> </tr><tr> <td align="center" bgcolor="#61BD4F" border="0" cellpadding="0" cellspacing="0" class="emailButton" id="button" style=" border-radius:6px; display:inline-block; margin:0px auto 60px; padding: 10px 14px;"> <div><span style="font-size:20px"><a href="${data.link}" style="color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:500; letter-spacing: 0.4px; line-height:48px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;font-size:18px !important"><img src="https://s3-us-west-2.amazonaws.com/mak-s3/29793763_343336722842176_8545984603976892416_n.png"></a></span>&nbsp;&nbsp;&nbsp;<span style="font-size:20px"><a href="${data.link}" style="color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:500; letter-spacing: 0.4px; line-height:48px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;font-size:18px !important"><img src="https://s3-us-west-2.amazonaws.com/mak-s3/30124862_343336729508842_3973615652479959040_n.png"</a></span>&nbsp;  </div> </td> </tr> <tr> <td align="left">Sincerely,<br>MAK Team<br><br></td></tr> </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px"> <tbody> <tr> <td class="footerContent" valign="top"> <table border="0" cellpadding="10" cellspacing="0" style="width:100%"> <tbody> <tr> <td valign="top">&nbsp; <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table></body>
    `
    callback(null,content)
};

const rejectTemp =  function(data,callback){

    let content = `
    
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style type="text/css">
        
        body,
        #bodyTable {
            height: 100%!important;
            margin: 0;
            padding: 0;
            width: 100%!important
        }

        #bodyTable {
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            min-height: 100%;
            background: #eee;
            color: #333
        }

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0
        }

        img {
            -ms-interpolation-mode: bicubic
        }

        body {
            margin: 0;
            padding: 0;
            background: #eee
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: 0;
            text-decoration: none
        }

        table {
            border-collapse: collapse!important;
            max-width: 100%!important
        }

        img {
            border: 0!important
        }

        h1 {
            font-family: Helvetica;
            font-size: 42px;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            margin: 30px auto 0
        }

        h2 {
            font-size: 32px;
            font-style: normal;
            font-weight: bold;
            margin: 50px auto 0
        }

        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 25px 0;
            letter-spacing: normal;
            text-align: left
        }

        a h3 {
            color: #444!important;
            text-decoration: none!important
        }

        .titleLink {
            text-decoration: none!important
        }

        .preheaderContent {
            color: #808080;
            font-size: 10px;
            line-height: 125%
        }

        .preheaderContent a:link,
        .preheaderContent a:visited,
        .preheaderContent a .yshortcuts {
            color: #606060;
            font-weight: normal;
            text-decoration: underline
        }

        #emailHeader,
        #tacoTip {
            color: #fff
        }

        #emailHeader {
            background-color: #fff
        }

        #content p {
            color: #4d4d4d;
            margin: 20px 70px 30px;
            font-size: 24px;
            line-height: 32px;
            text-align: center
        }

        #button {
            display: inline-block;
            margin: 10px auto;
            background: #fff;
            border-radius: 4px;
            font-weight: bold;
            font-size: 18px;
            padding: 15px 20px;
            cursor: pointer;
            color: #0079bf;
            margin-bottom: 50px
        }

        #socialIconWrap img {
            line-height: 35px!important;
            padding: 0 5px
        }

        .footerContent div {
            color: #707070;
            font-family: Arial;
            font-size: 12px;
            line-height: 125%;
            text-align: center;
            max-width: 100%!important
        }

        .footerContent div a:link,
        .footerContent div a:visited {
            color: #369;
            font-weight: normal;
            text-decoration: underline
        }

        .footerContent img {
            display: inline
        }

        #socialLinks img {
            margin: 0 2px
        }

        #utility {
            border-top: 1px solid #ddd
        }

        #utility div {
            text-align: center
        }

        #monkeyRewards img {
            max-width: 160px
        }

        #emailFooter {
            max-width: 100%!important
        }

        #footerTwitter a,
        #footerFacebook a {
            text-decoration: none!important;
            color: #fff!important;
            font-size: 14px
        }

        #emailButton {
            border-radius: 6px;
            background: #70b500!important;
            margin: 0 auto 60px;
            box-shadow: 0 4px 0 #578c00
        }

        #socialLinks a {
            width: 40px
        }

        #socialLinks #blogLink {
            width: 80px!important
        }

        .sectionWrap {
            text-align: center
        }

        #header {
            color: #fff!important
        }

    </style>
</head>

<body>
    <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px">
                                        <tbody>
                                            <tr>
                                                <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="http://52.36.127.111/mak_web/img/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120">
                                                    <h1 style="font-size: 30px;">Welcome to MAK</h1>
                                                    <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="background:#FFFFFF;">
                                                    <br>
                                                    Dear <b>${data.fullName}</b><br><br>
                                                 Unfortunately your application for a MAK Registered Maid was rejected on this occasion.  Please contact us for further information on reasons for rejection.<br><br>
           </p>
                                                </td>
                                            </tr>
                                           
                                            <tr>
                                            <td align="left">

Sincerely,<br>
MAK Team<br><br>
                                              </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px">
                                        <tbody>
                                            <tr>
                                                <td class="footerContent" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0" style="width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">&nbsp;
                                                                    <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
    `
    callback(null,content)
};

const listAllMaids = function (payloadData, userData, callback) {
    let response = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        listMaids: function (checkForUniqueKey, cb) {
            let pipeline = [];
            let match = {
                agencyId: mongoose.Types.ObjectId(userData._id),
                //acceptAgreement: true,
                isDeleted: false
            };
            if (payloadData.maidId) {
                match._id = mongoose.Types.ObjectId(payloadData.maidId)
            }
            if (payloadData.rejectMaids) {
                match.rejectReason ={$ne:''};
                match.isVerified ={$in:[true,false]}
            }
             else {
                match.rejectReason ={$eq:''};
                match.isVerified = true
            }

            if (payloadData.nationality) {
                match.nationality = {$in: payloadData.nationality}
            }

            if (payloadData.languages) {
                let language = payloadData.languages.map(obj => {
                    return mongoose.Types.ObjectId(obj)
                })
                match.languages = {$in: language}
            }
            if (payloadData.gender) {
                match.gender = {$in: payloadData.gender}
            }
            if (payloadData.searchMaidName) {
                match.$or = [
                    {firstName: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {fullName: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {lastName: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {nationalId: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {phoneNo: {$regex: payloadData.searchMaidName, $options: "si"}}
                ]
            }
            pipeline.push({$match: match});
            let pipeline2 = [
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                    }
                },
                {
                    $unwind: {path: "$reviewData", preserveNullAndEmptyArrays: true}
                },
                {
                    $addFields: {
                        maidRating: "$reviewData.maidRating",
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        cleaningRating: {$sum: "$maidRating.cleaning"},
                        ironingRating: {$sum: "$maidRating.ironing"},
                        cookingRating: {$sum: "$maidRating.cooking"},
                        childCareRating: {$sum: "$maidRating.childCare"},

                        cleaningCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cleaning", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        ironingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.ironing", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        cookingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cooking", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        childCareCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.childCare", 0]},
                                    0,
                                    1
                                ]
                            }
                        }
                    }
                },
                {
                    $project: {
                        maidId: "$_id",
                        avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                        avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                        avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                        avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: '_id',
                        foreignField: '_id',
                        as: 'maidData'
                    }
                },
                {
                    $unwind: "$maidData"
                },
                {
                    $project: {
                        _id: 1,
                        avgCleaning: 1,
                        avgIroning: 1,
                        avgCooking: 1,
                        avgChildCare: 1,
                        gender: "$maidData.gender",
                        email: "$maidData.email",
                        radius: "$maidData.radius",
                        material: "$maidData.material",
                        country: "$maidData.country",
                        countryCode: "$maidData.countryCode",
                        phoneNo: "$maidData.phoneNo",
                        makId: "$maidData.makId",
                        locationName: "$maidData.locationName",
                        languages: "$maidData.languages",
                        price: "$maidData.price",
                        actualPrice: "$maidData.actualPrice",
                        makPrice: "$maidData.makPrice",
                        experience: "$maidData.experience",
                        nationality: "$maidData.nationality",
                        dob: "$maidData.dob",
                        description: "$maidData.description",
                        lastName: "$maidData.lastName",
                        firstName: "$maidData.firstName",
                        feedBack: "$maidData.feedBack",
                        currentLocation: "$maidData.currentLocation",
                        documentPicURL: "$maidData.documentPicURL",
                        profilePicURL: "$maidData.profilePicURL",
                        religion: "$maidData.religion",
                        agencyId: "$maidData.agencyId",
                        timeSlot: "$maidData.timeSlot",
                        isBlocked: "$maidData.isBlocked",
                        isDeleted: "$maidData.isDeleted",
                        isVerified: "$maidData.isVerified",
                        timeZone: "$maidData.timeZone",
                        maidAddress: "$maidData.maidAddress",
                        countryName: "$maidData.countryName",
                        commission: "$maidData.commission",
                        countryENCode: "$maidData.countryENCode",
                        nationalId: "$maidData.nationalId",
                        step: "$maidData.step",
                        dateJoinedAgency: "$maidData.dateJoinedAgency",
                        registrationDate: "$maidData.registrationDate",
                        maidAppStep: "$maidData.maidAppStep",
                        acceptAgreement: "$maidData.acceptAgreement",
                    }
                },
                {
                    $lookup: {
                        from: "languages",
                        localField: "languages",
                        foreignField: "_id",
                        as: "languages"
                    }
                },
                {
                    $sort: {
                        _id: -1
                    }
                },
                // {$skip: ((payloadData.pageNo - 1) * payloadData.limit)},
                // {$limit: payloadData.limit}
            ];
            if (payloadData.pageNo && payloadData.limit) {
                pipeline2.push({
                    $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                });
                pipeline2.push({
                    $limit: payloadData.limit
                })
            }
            console.log('33333333333333333',pipeline[0].$match.rejectReason)
            Models.Maids.aggregate(pipeline.concat(pipeline2), function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        if (payloadData.maidId) {
                            if (result[0].step == 3 && result[0].maidAppStep == 2) {
                                result[0].step = 2;
                                response.data = result;
                            } else {
                                response.data = result;
                            }
                        } else {
                            response.data = result;
                        }
                        // response.data = result;
                        cb(null)
                    } else {
                        response.data = [];
                        cb(null)
                    }
                }
            })
        },
        listMaidsCount: function (checkForUniqueKey, cb) {
            let pipeline = [];
            let match = {
                agencyId: mongoose.Types.ObjectId(userData._id),
                isVerified: true,
                //acceptAgreement: true,
                isDeleted: false
            };
            if (payloadData.maidId) {
                match._id = mongoose.Types.ObjectId(payloadData.maidId)
            }
            if (payloadData.nationality) {
                match.nationality = {$in: payloadData.nationality}
            }

            if (payloadData.rejectMaids) {
                match.rejectReason ={$ne:''};
                match.isVerified ={$in:[true,false]}
            }
            else {
                match.rejectReason ={$eq:''};
                match.isVerified = true
            }

            if (payloadData.languages) {
                let language = payloadData.languages.map(obj => {
                    return mongoose.Types.ObjectId(obj)
                })
                match.languages = {$in: language}
            }
            if (payloadData.gender) {
                match.gender = {$in: payloadData.gender}
            }
            if (payloadData.searchMaidName) {
                match.$or = [
                    {firstName: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {fullName: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {lastName: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {nationalId : {$regex: payloadData.searchMaidName, $options: "si"}},
                    {phoneNo: {$regex: payloadData.searchMaidName, $options: "si"}}
                ]
            }
            pipeline.push({$match: match});
            let pipeline2 = [
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                    }
                },
                {
                    $unwind: {path: "$reviewData", preserveNullAndEmptyArrays: true}
                },
                {
                    $addFields: {
                        maidRating: "$reviewData.maidRating",
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        cleaningRating: {$sum: "$maidRating.cleaning"},
                        ironingRating: {$sum: "$maidRating.ironing"},
                        cookingRating: {$sum: "$maidRating.cooking"},
                        childCareRating: {$sum: "$maidRating.childCare"},

                        cleaningCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cleaning", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        ironingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.ironing", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        cookingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cooking", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        childCareCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.childCare", 0]},
                                    0,
                                    1
                                ]
                            }
                        }
                    }
                },
                {
                    $project: {
                        maidId: "$_id",
                        avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                        avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                        avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                        avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: '_id',
                        foreignField: '_id',
                        as: 'maidData'
                    }
                },
                {
                    $unwind: "$maidData"
                },
                {
                    $project: {
                        _id: 1,
                        avgCleaning: 1,
                        avgIroning: 1,
                        avgCooking: 1,
                        avgChildCare: 1,
                        gender: "$maidData.gender",
                        email: "$maidData.email",
                        country: "$maidData.country",
                        material: "$maidData.material",
                        countryCode: "$maidData.countryCode",
                        phoneNo: "$maidData.phoneNo",
                        makId: "$maidData.makId",
                        locationName: "$maidData.locationName",
                        languages: "$maidData.languages",
                        price: "$maidData.price",
                        actualPrice: "$maidData.actualPrice",
                        makPrice: "$maidData.makPrice",
                        experience: "$maidData.experience",
                        nationality: "$maidData.nationality",
                        dob: "$maidData.dob",
                        description: "$maidData.description",
                        lastName: "$maidData.lastName",
                        firstName: "$maidData.firstName",
                        feedBack: "$maidData.feedBack",
                        currentLocation: "$maidData.currentLocation",
                        documentPicURL: "$maidData.documentPicURL",
                        profilePicURL: "$maidData.profilePicURL",
                        religion: "$maidData.religion",
                        agencyId: "$maidData.agencyId",
                        nationalId: "$maidData.nationalId"
                    }
                },
                {
                    $lookup: {
                        from: "languages",
                        localField: "languages",
                        foreignField: "_id",
                        as: "languages"
                    }
                },
                {
                    $sort: {
                        experience: -1
                    }
                }
            ];
            Models.Maids.aggregate(pipeline.concat(pipeline2), function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        response.count = 0;
                        cb(null)
                    }
                }
            })
        }
    }, function (err, result) {
        callback(err, response)
    });
};

const listAllUsers = function (payloadData, userData, callback) {
    let response = {
        data: [],
        count: 0
    };
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        listusers: function (checkForUniqueKey, cb) {

            let option = {
                lean: true
            };
            if (payloadData.pageNo && payloadData.limit) {
                option.skip = ((payloadData.pageNo - 1) * payloadData.limit)
                option.limit = payloadData.limit
            }

            Service.UserServices.getUsers({
                isDeleted: false,
                isBlocked: false
            }, {fullName: 1}, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    } else {
                        response.data = [];
                        cb(null)
                    }
                }
            })
        },

        listUsersCount: function (listusers, cb) {
            Service.UserServices.getUsers({
                isDeleted: false,
                isBlocked: false
            }, {fullName: 1}, {lean: true,}, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        response.count = 0;
                        cb(null)
                    }
                }
            })
        }

    }, function (err, result) {
        callback(err, response)
    });
};

const listAllService = function (payloadData, userData, callback) {

    let response = {};
    let maidId = [];
    let userId = [];
    let offset = momentTz.tz(payloadData.timeZone).utcOffset() * 60 * 1000;

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMaidId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let criteria = {
                    $or: [
                        {firstName: {$regex: payloadData.search, $options: "si"}},
                        {lastName: {$regex: payloadData.search, $options: "si"}}
                    ],
                    agencyId: userData._id
                };
                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.MaidServices.getMaid(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            maidId = result.map(obj => {
                                return obj._id
                            });
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getUserId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let criteria = {};
                criteria.$or = [{fullName: new RegExp(payloadData.search,'i')},
                    {phoneNo: new RegExp(payloadData.search,'i')},
                    {email: new RegExp(payloadData.search,'i')},
                ];
                let projection = {__v: 0, password: 0};
                Service.UserServices.getUsers(criteria, projection, {lean: true,}, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            userId = result.map(obj => {
                                return obj._id
                            });
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        listServices: function (getMaidId, getUserId, cb) {
            let criteria = {
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                agencyId: userData._id,
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ]
            };

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    criteria.maidId = {$in: maidId};
                }
                if (userId && userId.length) {
                    criteria.userId = {$in: userId};
                }
                if (!(maidId && maidId.length) && !(userId && userId.length)) {
                    criteria.maidId = {$in: []};
                    criteria.userId = {$in: []};
                }
            }

            if (payloadData.onBasisOfDate === "ON_GOING") {
                criteria.$or = [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT}
                        ]
                    }
                ];
                criteria.startTime = {$lte: +new Date()};
                criteria.endTime = {$gte: +new Date()};
                criteria.isCompleted = false;
            }
            else if (payloadData.onBasisOfDate === "UPCOMING") {
                criteria.$or = [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]}}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]}}
                        ]
                    }
                ];
                criteria.startTime = {$gte: +new Date()};
                criteria.isCompleted = false;
            }
            else if (payloadData.onBasisOfDate === "COMPLETED") {
                criteria.agencyAction = Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT;
                criteria.isCompleted = true;
            }

            let projection = {};
            let option = {
                lean: true,
                sort  :{_id:-1}
            };
            if (payloadData.pageNo && payloadData.limit) {
                option.skip = ((payloadData.pageNo - 1) * payloadData.limit)
                option.limit = payloadData.limit
            }
            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName makId',
                    option: {}
                }
            ];
            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    } else {
                        response.data = [];
                        cb(null)
                    }

                }
            })
        },

        listServicesCount: function (getMaidId, getUserId, cb) {
            let criteria = {
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                agencyId: userData._id,
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ]
            };

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    criteria.maidId = {$in: maidId};
                }
                if (userId && userId.length) {
                    criteria.userId = {$in: userId};
                }
                if (!(maidId && maidId.length) && !(userId && userId.length)) {
                    criteria.maidId = {$in: []};
                    criteria.userId = {$in: []};
                }
            }

            if (payloadData.onBasisOfDate == "ON_GOING") {
                criteria.$or = [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT}
                        ]
                    }
                ];
                criteria.startTime = {$lte: +new Date()};
                criteria.endTime = {$gte: +new Date()};
                criteria.isCompleted = false;
            }
            else if (payloadData.onBasisOfDate == "UPCOMING") {
                criteria.$or = [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]}}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]}}
                        ]
                    }
                ];
                criteria.startTime = {$gte: +new Date()};
                criteria.isCompleted = false;
            }
            else if (payloadData.onBasisOfDate == "COMPLETED") {
                criteria.agencyAction = Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT;
                criteria.isCompleted = true;
            }

            let projection = {};
            let option = {
                lean: true,
            };
            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName makId',
                    option: {}
                }
            ];
            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        response.count = 0;
                        cb(null)
                    }

                }
            })
        }

    }, function (err, result) {
        callback(err, response)
    });
};

const approveService = function (payloadData, userData, callback) {

    let userId = "";
    let maidId = "";
    let notificationDataUser = {};
    let notificationDataMaid = {};
    let bookingNumber;
    let bookingData;
    let maidData;
    let usersData;


    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        approveByAgency: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.servciceId,
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
            };

            let dataToUpdate = {
                agencyAction: payloadData.agencyAction
            };

            if (payloadData.agencyAction == Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE) {
                if (payloadData.declineReason) {
                    dataToUpdate.declineReason = payloadData.declineReason
                } else {
                    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.REASON_REQUIRED)
                }
            }

            Service.ServiceServices.updateService(criteria, dataToUpdate, {new: true}, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result) {
                        bookingData = result
                        userId = result.userId;
                        maidId = result.maidId;
                        bookingNumber = result.bookingId;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getUsersData: function (approveByAgency, cb) {
            notificationDataUser.requestId = payloadData.servciceId;
            notificationDataUser.senderId = userData._id;
            notificationDataUser.agencyName = userData.agencyName;
            notificationDataUser.senderProfilePicURL = userData.profilePicURL;

            let criteria = {
                _id: userId,
                isDeleted: false,
            };
            let projection = {};
            let option = {lean: true};
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {

                    if (result && result.length) {
                        usersData = result[0];
                        notificationDataUser.receiverId = result[0]._id;
                        notificationDataUser.fullName = result[0].fullName;
                        notificationDataUser.profilePicURL = result[0].profilePicURL;
                        notificationDataUser.deviceToken = result[0].deviceToken;
                        cb()
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getMaidsData: function (approveByAgency, cb) {

            notificationDataMaid.requestId = payloadData.servciceId;
            notificationDataMaid.senderId = userData._id;
            notificationDataMaid.agencyName = userData.agencyName;
            notificationDataMaid.senderProfilePicURL = userData.profilePicURL;

            let criteria = {
                _id: maidId,
                isDeleted: false,
            };
            let projection = {};
            let option = {lean: true};
            let populate=[{
                path : 'agencyId',
                select : 'agencyName'
            }]
            Service.MaidServices.getMaidPopulate(criteria, projection, option,populate, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        maidData = result[0]
                        notificationDataMaid.receiverId = result[0]._id;
                        notificationDataMaid.fullName = result[0].firstName + " " + result[0].lastName;
                        notificationDataMaid.profilePicURL = result[0].profilePicURL;
                        notificationDataMaid.deviceToken = result[0].deviceToken;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },
        sendPushToUser: function (getUsersData,getMaidsData, cb) {
            notificationDataUser.title = 'M.A.K.';
            notificationDataUser.bookingNumber = bookingNumber;
            notificationDataUser.maidId = maidId;
            if (payloadData.agencyAction === Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT) {
                notificationDataUser.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_ACCEPT;
                notificationDataUser.body = userData.agencyName + " accepted your service request";
            } else {
                notificationDataUser.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_REJECT;
                notificationDataUser.body = userData.agencyName + " agency canceled your service with booking number : "+bookingNumber;
            }

            sendNotification(notificationDataUser, function (err, result) {
                if (err) {
                    console.log("1____err_____________SRVICE_ACCEPT_________push")
                    cb(err)
                } else {
                    cb(null)
                }
            })
        },

        sendPushToMaid: function (getUsersData, getMaidsData, cb) {
            if (payloadData.agencyAction === Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT) {
                notificationDataMaid.title = 'M.A.K.';
                notificationDataMaid.bookingNumber = bookingNumber;
                notificationDataMaid.maidId = maidId;
                notificationDataMaid.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_ACCEPT;

                if (payloadData.agencyAction === Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT) {
                    notificationDataMaid.body = " You have a new service request from " + notificationDataUser.fullName+' booking number '+bookingNumber;
                } else {
                    notificationDataUser.body = userData.agencyName + " agency canceled your service with booking number : "+bookingNumber;
                }
                sendNotificationMaid(notificationDataMaid, function (err, result) {
                    if (err) {
                        console.log("1____err______maid_______SRVICE_ACCEPT_________push")
                        cb(err)
                    } else {
                        cb(null)
                    }
                })
            } else {
                cb(null)
            }

        },

        createNotificationForAdmin: function (approveByAgency, cb) {
            if (payloadData.agencyAction === 'DECLINE') {
                let dataToSet = {
                    senderId: userId,
                    receiverId: userData._id,
                    maidId: maidId,
                    type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_REJECT,
                    serviceId: payloadData.servciceId,
                    bookingId: bookingNumber,
                    adminMessage: "A service request has been rejected by " + userData.agencyName,
                };
                Service.NotificationServices.createNotification(dataToSet, function (err, result) {
                    if (err) {
                        callback(err)
                    } else {
                        cb(null)
                    }
                })
            } else {
                cb(null)
            }
        }

    }, function (err, result) {
        callback(err, result)
    })
};

const deleteService = function (payloadData, userData, callback) {

    let notificationData = {};
    let response;
    let maidId = "";
    let userId = "";
    let bookingNumber;
    notificationData.requestId = payloadData.servciceId;
    notificationData.senderId = userData._id;
    notificationData.agencyName = userData.agencyName;
    notificationData.senderProfilePicURL = userData.profilePicURL;
    let notificationDataMaid = {};

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        deleteByAgency: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.servciceId,
                agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                deleteAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
            };

            let dataToUpdate = {
                deleteAction: Config.APP_CONSTANTS.DATABASE.ACTION.DELETED,
                deleteReason: payloadData.deleteReason,
                deleteTimeStamp: Date.now()
            };

            Service.ServiceServices.updateService(criteria, dataToUpdate, {new: true}, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    response = result;
                    bookingNumber = result.bookingId;
                    maidId = result.maidId;
                    userId = result.userId;
                    cb(null)
                }
            })
        },
        refundAmount: function (deleteByAgency, cb) {
                var formData = {
                    merchant_email: 'KAM@MAK.TODAY',
                    //secret_key: Config.APP_CONSTANTS.SERVER.SECRET_KEY_PAYTAB,
                    refund_amount : response.actualPrice - (response.actualPrice * 3.33/100),
                    refund_reason : ' agency cancelled the service',
                    transaction_id : response.transactionId,
                };
                if(response.cardType && response.cardType ===1)
                    formData.secret_key = Config.APP_CONSTANTS.SERVER.SECRET_KEY_PAYTAB_DEBIT
                else  formData.secret_key = Config.APP_CONSTANTS.SERVER.SECRET_KEY_PAYTAB

                request.post({
                        url: 'https://www.paytabs.com/apiv2/refund_process',
                        form: formData
                    },
                    function (err, httpResponse, body) {
                        let saveData ={

                            responseCode : body.response_code,
                            userId : response.userId,
                            serviceId : response._id,
                            transactionId : response.transactionId,
                            refundAmount : formData.refund_amount,
                            actualPrice : response.actualPrice,
                            result : body.result,
                            createdOn : new Date(),
                        }
                        Service.RefundServices.createRefund(saveData,function (err, result) {
                            cb()
                        })
                    });
        },

        getReceiversData: function (deleteByAgency, cb) {
            let criteria = {
                _id: userId,
                isDeleted: false,
                isDeactivated: false,
            };
            let projection = {};
            let option = {lean: true};
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        notificationData.receiverId = result[0]._id;
                        notificationData.fullName = result[0].fullName;
                        notificationData.email = result[0].email;
                        notificationData.profilePicURL = result[0].profilePicURL;
                        notificationData.deviceToken = result[0].deviceToken;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getMaidsData: function (deleteByAgency, cb) {
            notificationDataMaid.requestId = payloadData.servciceId;
            notificationDataMaid.senderId = userData._id;
            notificationDataMaid.agencyName = userData.agencyName;
            notificationDataMaid.senderProfilePicURL = userData.profilePicURL;

            let criteria = {
                _id: maidId,
                isDeleted: false,
            };
            let projection = {};
            let option = {lean: true};
            let populate=[{
                path : 'agencyId',
                select : 'agencyName'
            }];
            Service.MaidServices.getMaidPopulate(criteria, projection, option,populate, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        notificationDataMaid.receiverId = result[0]._id;
                        notificationDataMaid.fullName = result[0].firstName + " " + result[0].lastName;
                        notificationDataMaid.profilePicURL = result[0].profilePicURL;
                        notificationDataMaid.deviceToken = result[0].deviceToken;
                        notificationDataMaid.agencyName = result[0].agencyId.agencyName;
                        notificationDataMaid.makId = result[0].makId;
                        notificationDataMaid.actualPrice = result[0].actualPrice;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })

        },

        senMail :(deleteByAgency,getReceiversData,getMaidsData,cb)=>{
                let data ={
                    userName : notificationData.fullName,
                    bookingNumber : response.bookingId,
                    maidName : notificationDataMaid.fullName,
                    maidId : notificationDataMaid.makId,
                    agencyName : notificationDataMaid.agencyName,
                    bookingTime : moment.tz(response.startTime, response.timeZone).format("hh:mm a"),
                    bookingDate : moment.tz(response.startTime,response.timeZone).format("LL"),
                    duration :response.duration,
                    villa : response.address.villaName,
                    block : response.address.buildingName,
                    road : response.address.streetName,
                    details : response.address.moreDetailedaddress || 'NA',
                    city : response.address.city,
                    rate : notificationDataMaid.actualPrice,
                    amount :  response.amount,

                };
                bookingCancel(data,(err,res)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        let content=res;
                        let subject = "MAK Booking Cancelled!";
                        MailManager.sendMailBySES(notificationData.email, subject, content, function (err, res) {
                        });
                        cb(null)
                    }
                })
        },
        sendPushToUser: function (getReceiversData, cb) {
            notificationData.title = 'M.A.K.';
            notificationData.bookingNumber = bookingNumber;
            notificationData.maidId = maidId;
            notificationData.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_DELETE;
            notificationData.body = userData.agencyName + " deleted your service request with booking number : "+bookingNumber;


            sendNotification(notificationData, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        },

        sendPushToMaid: function (getReceiversData, getMaidsData, cb) {
            notificationDataMaid.title = 'M.A.K.';
            notificationDataMaid.bookingNumber = bookingNumber;
            notificationDataMaid.maidId = maidId;
            notificationDataMaid.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_ACCEPT;
            notificationDataMaid.body = " Your service has been cancelled by agency with booking number : "+bookingNumber;
            //notificationDataMaid.body = " You have a new service request from " + notificationData.fullName;

            sendNotificationMaid(notificationDataMaid, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })

        },

        createNotificationForAdmin: function (getReceiversData, getMaidsData, cb) {
            let dataToSet = {
                senderId: userId,
                receiverId: userData._id,
                type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_DELETE,
                serviceId: payloadData.servciceId,
                adminMessage: userData.agencyName + " has deleted a service request of " + notificationData.fullName,
                bookingId: bookingNumber,
                maidId: maidId
            };
            Service.NotificationServices.createNotification(dataToSet, function (err, result) {
                if (err) {
                    callback(err)
                } else {
                    cb(null)
                }
            })
        }

    }, function (err, result) {
        callback(err, result)
    })
};

const refundPayment = function (payloadData, userData, callback) {

    async.autoInject({

        returnPayment: function (cb) {
            stripe.refunds.create({
                charge: "ch_ATbgYw7MpO4hlv",
            }, function (err, refund) {
                console.log("__________err, refund____________", err, refund)
                cb(null)
            });
        },

        returnPayment1: function (cb) {
            stripe.refunds.create({
                charge: "ch_ATbgYw7MpO4hlv",
                amount: 1000,
            }, function (err, refund) {
                cb(null)
            });

        }

    }, function (err, result) {
        callback(err, result)
    })
};

const listAllCanceledService = function (payloadData, userData, callback) {
    console.log("______________payloadData", payloadData);

    let response = {};
    let maidId = [];
    let userId = [];
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMaidId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let criteria = {
                    $or: [
                        {firstName: {$regex: payloadData.search, $options: "si"}},
                        {lastName: {$regex: payloadData.search, $options: "si"}}
                    ],
                    agencyId: userData._id
                };
                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.MaidServices.getMaid(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            maidId = result.map(obj => {
                                return mongoose.Types.ObjectId(obj._id)
                            });
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getUserId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let nameArray = payloadData.search.split(" ");
                let criteria = {};
                let a = [];
                if (nameArray.length) {
                    nameArray.map(obj => {
                        let objToPush = {
                            fullName: {$regex: obj, $options: "si"}
                        }
                        a.push(objToPush)
                    })
                }
                criteria.$or = a;
                criteria.agencyId = userData._id;
                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.UserServices.getUsers(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            userId = result.map(obj => {
                                return mongoose.Types.ObjectId(obj._id)
                            });

                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        listServices: function (getMaidId, getUserId, cb) {

            let criteria = {
                deleteAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                    Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                isCompleted: false,
                deleteFromList:false,
                agencyId: userData._id
            };

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    criteria.maidId = {$in: maidId};
                }
                if (userId && userId.length) {
                    criteria.userId = {$in: userId};
                }
                if (!(maidId && maidId.length) && !(userId && userId.length)) {
                    criteria.maidId = {$in: []};
                    criteria.userId = {$in: []};
                }
            }

            let projection = {};
            let option = {
                lean: true,
            };
            if (payloadData.pageNo && payloadData.limit) {
                option.skip = ((payloadData.pageNo - 1) * payloadData.limit)
                option.limit = payloadData.limit
            }
            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName makId',
                    option: {}
                }
            ];
            console.log("_______________criteria", criteria);

            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("_______________result");
                    if (result && result.length) {
                        // response.count = result.length;
                        response.data = result;
                        cb(null)
                    } else {
                        //response.count = 0;
                        response.data = [];
                        cb(null)
                    }

                }
            })
        },

        listServicesCount: function (getMaidId, getUserId, cb) {

            let criteria = {
                deleteAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                isCompleted: false,
                deleteFromList:false,
                agencyId: userData._id
            };

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    criteria.maidId = {$in: maidId};
                }
                if (userId && userId.length) {
                    criteria.userId = {$in: userId};
                }
                if (!(maidId && maidId.length) && !(userId && userId.length)) {
                    criteria.maidId = {$in: []};
                    criteria.userId = {$in: []};
                }
            }

            let projection = {};
            let option = {
                lean: true,
            };
            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName makId',
                    option: {}
                }
            ];
            console.log("_______________criteria", criteria);

            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("_______________result");
                    if (result && result.length) {
                        response.count = result.length;
                        //response.data = result;
                        cb(null)
                    } else {
                        response.count = 0;
                        //  response.data = [];
                        cb(null)
                    }

                }
            })
        }

    }, function (err, result) {
        callback(err, response)
    });
};

const getCalender = function (payloadData, userData, callback) {
    let response = [];
    let offset = momentTz.tz(payloadData.timeZone).utcOffset() * 60 * 1000;

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        listServices: function (checkForUniqueKey, cb) {
            let criteria = {
                $or: [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]}}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]}}
                        ]
                    }
                ],

                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.PENDING, Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT]},
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                startTime: {$gte: new Date().setHours(0, 0, 0, 0) - offset},
                isCompleted: false,
                agencyId: userData._id,
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ]
            };

            if (payloadData.maidId) {
                criteria.maidId = payloadData.maidId;
            }
            let projection = {_v: 0};
            let option = {lean: true};
            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName makId countryENCode countryCode phoneNo',
                    option: {}
                }
            ];
            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

    }, function (err, result) {
        callback(err, response)
    })
};

const listAllExtendedService = function (payloadData, userData, callback) {
    console.log("______________payloadData", payloadData);

    let response = {};
    let maidId = [];
    let userId = [];

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMaidId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let criteria = {
                    $or: [
                        {firstName: {$regex: payloadData.search, $options: "si"}},
                        {lastName: {$regex: payloadData.search, $options: "si"}},
                        {fullName: {$regex: payloadData.search, $options: "si"}},
                    ],
                    agencyId: userData._id
                };
                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.MaidServices.getMaid(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            maidId = result.map(obj => {
                                return mongoose.Types.ObjectId(obj._id)
                            });
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getUserId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let nameArray = payloadData.search.split(" ");
                let criteria = {};
                let a = [];
                if (nameArray.length) {
                    nameArray.map(obj => {
                        let objToPush = {
                            fullName: {$regex: obj, $options: "si"}
                        };
                        a.push(objToPush)
                    })
                }
                criteria.$or = a;
               // criteria.agencyId = userData._id
                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.UserServices.getUsers(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            userId = result.map(obj => {
                                return mongoose.Types.ObjectId(obj._id)
                            });

                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        listServices: function (getMaidId, getUserId, cb) {
            let criteria = {
                agencyAction: {$ne: Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE},
                "isExtend.requested": true,
                "isExtend.agencyAction": Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                $and: [
                    {"isExtend.transactionId": {$exists: true}},
                    {"isExtend.transactionId": {$nin: ["", null]}}
                ],
                agencyId: userData._id
            };

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    criteria.maidId = {$in: maidId};
                }
                if (userId && userId.length) {
                    criteria.userId = {$in: userId};
                }
                if (!(maidId && maidId.length) && !(userId && userId.length)) {
                    criteria.maidId = {$in: []};
                    criteria.userId = {$in: []};
                }
            }

            let projection = {};
            let option = {
                lean: true,
            };
            if (payloadData.pageNo && payloadData.limit) {
                option.skip = ((payloadData.pageNo - 1) * payloadData.limit)
                option.limit = payloadData.limit
            }
            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName makId',
                    option: {}
                }
            ];
            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("_______________result", result);
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    } else {
                        response.data = [];
                        cb(null)
                    }

                }
            })
        },

        listServicesCount: function (getMaidId, getUserId, cb) {
            let criteria = {
                agencyAction: {$ne: Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE},
                "isExtend.requested": true,
                "isExtend.agencyAction": Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                $and: [
                    {"isExtend.transactionId": {$exists: true}},
                    {"isExtend.transactionId": {$nin: ["", null]}}
                ],
                agencyId: userData._id
            };

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    criteria.maidId = {$in: maidId};
                }
                if (userId && userId.length) {
                    criteria.userId = {$in: userId};
                }
                if (!(maidId && maidId.length) && !(userId && userId.length)) {
                    criteria.maidId = {$in: []};
                    criteria.userId = {$in: []};
                }
            }

            let projection = {};
            let option = {
                lean: true
            };
            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName makId',
                    option: {}
                }
            ];
            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("_______________result", result);
                    if (result && result.length) {
                        response.count = result.length;
                        //  response.data = result;
                        cb(null)
                    } else {
                        response.count = 0;
                        // response.data = [];
                        cb(null)
                    }

                }
            })
        }

    }, function (err, result) {
        callback(err, response)
    });
};

const approveExtendedService = function (payloadData, userData, callback) {

    let workDate = "";
    let startTime = "";
    let endTime = "";
    let durationToUpdate = 0;
    let amount = 0;
    let commission = 0;
    let userId = "";
    let maidId = "";
    let notificationDataUser = {
        requestId: payloadData.serviceId
    };
    let notificationDataMaid = {
        requestId: payloadData.serviceId
    };
    let bookingNumber;

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getServiceData: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.serviceId,
                $and: [
                    {"isExtend.transactionId": {$exists: true}},
                    {"isExtend.transactionId": {$nin: ["", null]}}
                ],
            };
            let projection = {};
            let option = {
                lean: true
            };
            Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("_______________result", result);
                    if (result && result.length) {
                        bookingNumber = result[0].bookingId;
                        maidId = result[0].maidId;
                        userId = result[0].userId;
                        workDate = result[0].workDate;
                        startTime = result[0].startTime;
                        endTime = startTime + ((result[0].isExtend.extendDuration + result[0].duration) * 60 * 60 * 1000);
                        durationToUpdate = result[0].isExtend.extendDuration + result[0].duration;
                        amount = result[0].isExtend.extendAmount + result[0].amount;
                        commission = result[0].commission;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        checkMaidAvailability: function (getServiceData, cb) {
            let criteria = {
                _id: {$nin: [payloadData.serviceId]},
                workDate: {$eq: workDate},
                $and: [
                    {"isExtend.requested": true},
                    {"isExtend.agencyAction": "ACCEPT"},
                    {"isExtend.transactionId": {$exists: true}},
                    {"isExtend.transactionId": {$nin: ["", null]}}
                ],
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
            };


            let projection = {};
            let option = {
                lean: true
            };
            Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        let localArray = result.filter(obj => {
                            return (obj.startTime <= endTime && startTime <= obj.bufferEndTime)
                        });
                        if (localArray && localArray.length) {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_BUSY);
                        } else {
                            cb(null)
                        }
                    } else {
                        cb(null)
                    }
                }
            })
        },

        approveByAgency: function (checkMaidAvailability, cb) {
            let criteria = {
                _id: payloadData.serviceId,
                "isExtend.requested": true,
                "isExtend.agencyAction": Config.APP_CONSTANTS.DATABASE.ACTION.PENDING,
            };

            let dataToUpdate = {
                "isExtend.agencyAction": payloadData.agencyAction,
            };

            if (payloadData.agencyAction == Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE) {
                if (payloadData.declineReason) {
                    dataToUpdate.declineReason = payloadData.declineReason
                } else {
                    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.REASON_REQUIRED)
                }
            }

            if (payloadData.agencyAction == Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT) {

                dataToUpdate.agencyAction = payloadData.agencyAction;
                dataToUpdate.endTime = endTime;
                dataToUpdate.bufferEndTime = parseInt(endTime) + (1 * 60 * 60 * 1000);
                dataToUpdate.duration = durationToUpdate;
                dataToUpdate.amount = amount;
                dataToUpdate.commissionToBePaid = ((amount * commission) / 100);
            }
            console.log("__________dataToUpdate__________", dataToUpdate);

            Service.ServiceServices.updateService(criteria, dataToUpdate, {new: true}, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    // console.log("____________________", result);
                    if (result) {
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getUsersData: function (approveByAgency, cb) {
            // notificationDataUser.requestId = payloadData.servciceId;
            notificationDataUser.senderId = userData._id;
            notificationDataUser.agencyName = userData.agencyName;
            notificationDataUser.senderProfilePicURL = userData.profilePicURL;

            let criteria = {
                _id: userId,
                isDeleted: false,
                isDeactivated: false,
            };
            let projection = {};
            let option = {lean: true};
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        notificationDataUser.receiverId = result[0]._id;
                        notificationDataUser.fullName = result[0].fullName;
                        notificationDataUser.profilePicURL = result[0].profilePicURL;
                        notificationDataUser.deviceToken = result[0].deviceToken;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getMaidsData: function (getUsersData, cb) {
            if (payloadData.agencyAction == Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT) {
                // notificationDataMaid.requestId = payloadData.servciceId;
                notificationDataMaid.senderId = userData._id;
                notificationDataMaid.agencyName = userData.agencyName;
                notificationDataMaid.senderProfilePicURL = userData.profilePicURL;

                let criteria = {
                    _id: maidId,
                    isDeleted: false,
                    isBlocked: false,
                };
                let projection = {};
                let option = {lean: true};
                Service.MaidServices.getMaid(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            notificationDataMaid.receiverId = result[0]._id;
                            notificationDataMaid.fullName = result[0].firstName + " " + result[0].lastName;
                            notificationDataMaid.profilePicURL = result[0].profilePicURL;
                            notificationDataMaid.deviceToken = result[0].deviceToken;
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        sendPushToUser: function (getMaidsData, cb) {
            notificationDataUser.title = 'M.A.K.';
            notificationDataUser.bookingNumber = bookingNumber;
            notificationDataUser.maidId = maidId;
            if (payloadData.agencyAction == Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT) {
                notificationDataUser.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_EXTEND_ACCEPT;
                notificationDataUser.body = userData.agencyName + " accepted your request for extending the service.";
            } else {
                notificationDataUser.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_EXTEND_CANCEL;
                notificationDataUser.body = userData.agencyName + " rejected your request for extending the service.";
            }

            sendNotification(notificationDataUser, function (err, result) {
                if (err) {
                    console.log("1____err_____________SRVICE_ACCEPT_________push")
                    cb(err)
                } else {
                    console.log("2_____result_____________SRVICE_ACCEPT_________push")
                    cb(null)
                }
            })
        },

        sendPushToMaid: function (sendPushToUser, cb) {
            if (payloadData.agencyAction == Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT) {
                notificationDataMaid.title = 'M.A.K.';
                notificationDataMaid.bookingNumber = bookingNumber;
                notificationDataMaid.maidId = maidId;
                notificationDataMaid.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_EXTEND_ACCEPT;
                notificationDataMaid.body = " You have a new service extension request from " + notificationDataUser.fullName;

                sendNotificationMaid(notificationDataMaid, function (err, result) {
                    if (err) {
                        console.log("1____err______maid_______SRVICE_ACCEPT_________push")
                        cb(err)
                    } else {
                        console.log("2_____result_____maid________SRVICE_ACCEPT_________push")
                        cb(null)
                    }
                })
            } else {
                cb(null)
            }

        },

        createNotificationForAdmin: function (sendPushToMaid, cb) {
            if (payloadData.agencyAction == 'DECLINE') {
                let dataToSet = {
                    senderId: userId,
                    receiverId: "" /*userData._id*/,
                    maidId: maidId,
                    type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_REJECT,
                    serviceId: payloadData.servciceId,
                    bookingId: bookingNumber,
                    adminMessage: "A service extension request has been rejected by " + userData.agencyName,
                };
                Service.NotificationServices.createNotification(dataToSet, function (err, result) {
                    if (err) {
                        callback(err)
                    } else {
                        cb(null)
                    }
                })
            } else {
                cb(null)
            }
        }


    }, function (err, result) {
        callback(err, result)
    })
};

const revenueReport = function (payloadData, userData, callback) {
    let response = {
        service: {
            data: [],
            count: 0
        },
        totalRevenue: 0,
    };
    let maidId = [];
    let userId = [];
    let offset = momentTz.tz(payloadData.timeZone).utcOffset() * 60 * 1000;

    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMaidId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let criteria = {
                    $or: [
                        {firstName: {$regex: payloadData.search, $options: "si"}},
                        {lastName: {$regex: payloadData.search, $options: "si"}},
                        {fullName: {$regex: payloadData.search, $options: "si"}},
                    ],
                    agencyId: userData._id
                };
                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.MaidServices.getMaid(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            maidId = result.map(obj => {
                                return mongoose.Types.ObjectId(obj._id)
                            });
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getUserId: function (checkForUniqueKey, cb) {
            if (payloadData.search) {
                let nameArray = payloadData.search.split(" ");
                let criteria = {};
                let a = [];
                if (nameArray.length) {
                    nameArray.map(obj => {
                        let objToPush = {
                            fullName: {$regex: obj, $options: "si"}
                        }
                        a.push(objToPush)
                    })
                }
                criteria.$or = a;
                criteria.agencyId = userData._id;

                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.UserServices.getUsers(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            userId = result.map(obj => {
                                return mongoose.Types.ObjectId(obj._id)
                            });

                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getService: function (getMaidId, getUserId, cb) {
            let pipeline = [];

            if (payloadData.lat && payloadData.long) {
                let geoNear = {
                    $geoNear: {
                        near: {type: "Point", coordinates: [payloadData.long, payloadData.lat]},
                        spherical: true,
                        distanceField: "distance",
                        distanceMultiplier: 0.001,                   //convert to kilometre 6371km 0.001
                        maxDistance: 50000,
                        query: {isDeleted: false},
                    }
                };
                pipeline.push(geoNear)
            }

            let match = {
                $match: {
                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                    agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                    agencyId: userData._id,
                    $and: [
                        {transactionId: {$exists: true}},
                        {transactionId: {$nin: ["", null]}}
                    ]
                }
            };
            pipeline.push(match);

            if (payloadData.year) {
                let localDate = new Date();
                let startOfYear = localDate.setFullYear(payloadData.year, 0, 1)

                let localDate1 = new Date();
                let endOfYear = localDate1.setFullYear(payloadData.year, 11, 31);
                let match = {
                    $match: {
                        workDate: {$gte: startOfYear, $lte: endOfYear},
                        agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                        agencyId: userData._id,
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]
                    }
                };
                pipeline.push(match)
            }

            if (payloadData.month && payloadData.year) {
                let localDate = new Date();
                let startOfMonth = localDate.setFullYear(payloadData.year, payloadData.month, 1);

                let localDate1 = new Date();
                let endOfMonth = localDate1.setFullYear(payloadData.year, payloadData.month + 1, 0);
                let match = {
                    $match: {
                        workDate: {$gte: startOfMonth, $lte: endOfMonth},
                        agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                        agencyId: userData._id

                    }
                };

                pipeline.push(match)
            }

            if (payloadData.startDate && payloadData.endDate) {
                let match = {
                    $match: {
                        workDate: {
                            $gte: new Date(payloadData.startDate).setHours(0, 0, 0, 0) - offset,
                            $lte: new Date(payloadData.endDate).setHours(0, 0, 0, 0) - offset
                        },
                        agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                        agencyId: userData._id
                    }
                };
                console.log("_____________match", match);

                pipeline.push(match)
            }

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    pipeline.push({$match: {maidId: {$in: maidId}}})
                }
                if (userId && userId.length) {
                    pipeline.push({$match: {userId: {$in: userId}}})
                }
                if (!(maidId && maidId.length) && !(userId && userId.length)) {
                    pipeline.push({$match: {maidId: {$in: []}}})
                    pipeline.push({$match: {userId: {$in: []}}})
                }
            }

            let lookup = [
                {
                    $lookup: {
                        from: 'users',
                        localField: 'userId',
                        foreignField: '_id',
                        as: 'userId'
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$userId"
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        "_id": 1,
                        "serviceMakId": 1,
                        // "agencyId": 1,
                        "workDate": 1,
                        "startTime": 1,
                        "endTime": 1,
                        "duration": 1,
                        "amount": 1,
                        "uniquieAppKey": 1,
                        "bookingLocation": 1,
                        "bookingId":1,
                        "transactionId":1,
                        "isExtend": 1,
                        "deleteReason": 1,
                        "deleteTimeStamp": 1,
                        "deleteRequestByUser": 1,
                        "deleteAction": 1,
                        "declineReason": 1,
                        "isCompleted": 1,
                        "agencyAction": 1,
                        "address": 1,
                        "city": 1,
                        "locationName": 1,
                        "currency": 1,

                        "userId._id": 1,
                        "userId.fullName": 1,
                        "userId.phoneNo": 1,
                        "userId.email": 1,
                        "maidId._id": 1,
                        "maidId.firstName": 1,
                        "maidId.lastName": 1,
                        "maidId.makId": 1,
                        "maidId.rating": 1,

                        "agencyId.agencyName": 1,
                        "agencyId._id": 1,
                        "agencyId.email": 1,
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "serviceId",
                        as: "reviewData",
                        restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                    }
                },
            ];
            lookup.map(obj => {
                pipeline.push(obj)
            });

            if (payloadData.maidId) {
                let match = {
                    $match: {
                        "maidId._id": mongoose.Types.ObjectId(payloadData.maidId)
                    }
                };
                pipeline.push(match)
            }

            if (payloadData.userId) {
                console.log("8888888888888888888", payloadData.userId)
                let match = {
                    $match: {
                        "userId._id": mongoose.Types.ObjectId(payloadData.userId)
                    }
                };
                pipeline.push(match)
            }

            if (payloadData.pageNo && payloadData.limit) {
                pipeline.push({
                    $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                });
                pipeline.push({
                    $limit: payloadData.limit
                })
            }


            console.log("____________pipeline", pipeline);

            if (pipeline && pipeline.length) {
                Models.Service.aggregate(pipeline, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            response.service.data = result;
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getServiceTotalCount: function (getMaidId, getUserId, cb) {
            let pipeline = [];

            if (payloadData.lat && payloadData.long) {
                let geoNear = {
                    $geoNear: {
                        near: {type: "Point", coordinates: [payloadData.long, payloadData.lat]},
                        spherical: true,
                        distanceField: "distance",
                        distanceMultiplier: 0.001,                   //convert to kilometre 6371km 0.001
                        maxDistance: 50000,
                        query: {isDeleted: false},
                    }
                };
                pipeline.push(geoNear)
            }

            let match = {
                $match: {
                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                    agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                    agencyId: userData._id,
                    $and: [
                        {transactionId: {$exists: true}},
                        {transactionId: {$nin: ["", null]}}
                    ]
                }
            };
            pipeline.push(match)


            if (payloadData.year) {
                let localDate = new Date();
                let startOfYear = localDate.setFullYear(payloadData.year, 0, 1)

                let localDate1 = new Date();
                let endOfYear = localDate1.setFullYear(payloadData.year, 11, 31);
                let match = {
                    $match: {
                        workDate: {$gte: startOfYear, $lte: endOfYear},
                        agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                        agencyId: userData._id
                    }
                };
                // console.log("_____________match",match);
                pipeline.push(match)
            }

            if (payloadData.month && payloadData.year) {
                let localDate = new Date();
                let startOfMonth = localDate.setFullYear(payloadData.year, payloadData.month, 1);

                let localDate1 = new Date();
                let endOfMonth = localDate1.setFullYear(payloadData.year, payloadData.month + 1, 0);
                let match = {
                    $match: {
                        workDate: {$gte: startOfMonth, $lte: endOfMonth},
                        agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                        agencyId: userData._id
                    }
                };
                // console.log("_____________match",match);

                pipeline.push(match)
            }

            if (payloadData.startDate && payloadData.endDate) {
                let match = {
                    $match: {
                        workDate: {
                            $gte: new Date(payloadData.startDate).setHours(0, 0, 0, 0),
                            $lte: new Date(payloadData.endDate).setHours(0, 0, 0, 0)
                        },
                        agencyAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE]},
                        agencyId: userData._id
                    }
                };
                console.log("_____________match", match);

                pipeline.push(match)
            }

            if (payloadData.search) {
                if (maidId && maidId.length) {
                    pipeline.push({$match: {maidId: {$in: maidId}}})
                }
                if (userId && userId.length) {
                    pipeline.push({$match: {userId: {$in: userId}}})
                }
                if (!(maidId && maidId.length) && !(userId && userId.length)) {
                    pipeline.push({$match: {maidId: {$in: []}}})
                    pipeline.push({$match: {userId: {$in: []}}})
                }
            }


            let lookup = [
                {
                    $lookup: {
                        from: 'users',
                        localField: 'userId',
                        foreignField: '_id',
                        as: 'userId'
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$userId"
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $project: {
                        "_id": 1,
                        "serviceMakId": 1,
                        "agencyId": 1,
                        "workDate": 1,
                        "startTime": 1,
                        "endTime": 1,
                        "duration": 1,
                        "amount": 1,
                        "uniquieAppKey": 1,
                        "bookingLocation": 1,
                        "bookingId":1,
                        "transactionId":1,
                        "isExtend": 1,
                        "deleteReason": 1,
                        "deleteTimeStamp": 1,
                        "deleteRequestByUser": 1,
                        "deleteAction": 1,
                        "declineReason": 1,
                        "isCompleted": 1,
                        "agencyAction": 1,
                        "address": 1,
                        "city": 1,
                        "locationName": 1,

                        "userId._id": 1,
                        "userId.fullName": 1,
                        "userId.phoneNo": 1,
                        "userId.email": 1,
                        "maidId._id": 1,
                        "maidId.firstName": 1,
                        "maidId.lastName": 1,
                        "maidId.makId": 1,
                        "maidId.rating": 1
                    }
                }
            ];
            lookup.map(obj => {
                pipeline.push(obj)
            })


            if (payloadData.maidId) {
                let match = {
                    $match: {
                        "maidId._id": mongoose.Types.ObjectId(payloadData.maidId)
                    }
                };
                pipeline.push(match)
            }

            if (payloadData.userId) {
                console.log("8888888888888888888", payloadData.userId)
                let match = {
                    $match: {
                        "userId._id": mongoose.Types.ObjectId(payloadData.userId)
                    }
                };
                pipeline.push(match)
            }

            if (pipeline && pipeline.length) {
                Models.Service.aggregate(pipeline, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            response.service.count = result.length;
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getRevenue: function (getService, getServiceTotalCount, cb) {
            if (response.service.data && response.service.data.length) {
                let totalRevenue = 0;
                response.service.data.map(obj => {
                    totalRevenue = totalRevenue + obj.amount
                })
                response.totalRevenue = totalRevenue;
                cb(null)
            } else {
                cb(null)
            }
        }

    }, function (err, result) {
        callback(err, response)
    })
};

const lifeTimeSummary = function (payloadData, userData, callback) {
    let response = {};
    let startOfMonth = moment(moment().startOf('month').format()).tz(payloadData.timeZone).unix() * 1000;
    let endOfMonth = moment(moment().endOf('month').format()).tz(payloadData.timeZone).unix() * 1000;
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getServiceDetails: function (checkForUniqueKey, cb) {

            let startThreeMonthsAgo = moment(moment(startOfMonth).subtract(3, 'months')).unix() * 1000;
            let endthreeMonthsAgo = moment(moment(endOfMonth).subtract(1, 'months')).unix() * 1000;

             //console.log("sdfffffffffffffffffff",startOfMonth, endOfMonth,startThreeMonthsAgo,endthreeMonthsAgo);

            let pipeline = [
                {
                    $match: {
                        agencyId: mongoose.Types.ObjectId(userData._id),
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]
                    }
                },
                {

                    $addFields: { totalAmount:
                        { $add: ["$isExtend.extendAmount" ,"$amount"] }
                    }
                },
                {
                    $group: {
                        _id: {
                            maidId: "$maidId",
                        },
                        currentResult: {
                            $sum: {
                                $cond: {
                                    if: {$and: [{$gte: ["$workDate", startOfMonth]}, {$lte: ["$workDate", endOfMonth]}]},
                                    then: "$totalAmount",
                                    else: 0
                                }
                            }
                        },
                        lastThreeResult: {
                            $sum: {
                                $cond: {
                                    if: {$and: [{$gte: ["$workDate", startThreeMonthsAgo]}, {$lte: ["$workDate", endthreeMonthsAgo]}]},
                                    then: "$totalAmount",
                                    else: 0
                                }
                            }
                        },
                        all: {$sum: "$totalAmount"}
                    }
                },
                {
                    $project: {
                        maidId: "$_id.maidId",
                        currentResult: "$currentResult",
                        lastThreeResult: "$lastThreeResult",
                        all: 1,
                        _id: 0
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $project: {
                        currentResult: 1,
                        lastThreeResult: 1,
                        all: 1,
                        maidFirstName: "$maidId.firstName",
                        maidLastName: "$maidId.lastName",
                        maidNationality: "$maidId.nationality",
                        maidMakId: "$maidId.makId",
                        maidId: "$maidId._id",
                        maidDateJoinedAgency: "$maidId.dateJoinedAgency",
                    }
                },
            ];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        $or: [
                            {maidFirstName: {$regex: payloadData.search, $options: "si"}},
                            {maidLastName: {$regex: payloadData.search, $options: "si"}},
                            {maidNationality: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                })
            }

            let sort = [];
            if (payloadData.pageNo && payloadData.limit) {
                sort.push({
                    $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                });
                sort.push({
                    $limit: payloadData.limit
                })
            }

            Models.Service.aggregate(pipeline.concat(sort), function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },
        getServiceCount: function (checkForUniqueKey, cb) {

            let startThreeMonthsAgo = moment(moment(startOfMonth).subtract(3, 'months')).unix() * 1000;
            let endthreeMonthsAgo = moment(moment(endOfMonth).subtract(1, 'months')).unix() * 1000;
            let pipeline = [
                {
                    $match: {
                        agencyId: mongoose.Types.ObjectId(userData._id),
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]
                    }
                },
                {
                    $group: {
                        _id: {
                            maidId: "$maidId",
                        },

                        currentResult: {
                            $sum:{
                                $cond: {
                                    if: {$and: [{$gte: ["$workDate", startOfMonth]}, {$lte: ["$workDate", endOfMonth]}]},
                                    then: "$amount",
                                    else: 0
                                }
                            }
                        },
                        lastThreeResult: {
                            $sum:{
                                $cond: {
                                    if: {$and: [{$gte: ["$workDate", startThreeMonthsAgo]}, {$lte: ["$workDate", endthreeMonthsAgo]}]},
                                    then: "$amount",
                                    else: 0
                                }
                            }
                        },
                        all: {$sum:"$amount"}
                    }
                },
                {
                    $project: {
                        maidId: "$_id.maidId",
                        currentResult: "$currentResult",
                        lastThreeResult: "$lastThreeResult",
                        all: 1,
                        _id: 0
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $project: {
                        currentResult: 1,
                        lastThreeResult: 1,
                        all: 1,
                        maidFirstName: "$maidId.firstName",
                        maidLastName: "$maidId.lastName",
                        maidNationality: "$maidId.nationality",
                        maidMakId: "$maidId.makId",
                        maidId: "$maidId._id",
                        maidDateJoinedAgency: "$maidId.dateJoinedAgency",
                    }
                },
            ];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        $or: [
                            {maidFirstName: {$regex: payloadData.search, $options: "si"}},
                            {maidLastName: {$regex: payloadData.search, $options: "si"}},
                            {maidNationality: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                })
            }


            Models.Service.aggregate(pipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },


    }, function (err, result) {
        callback(err, response)
    })
};

const currentMonthSummary = function (payloadData, userData, callback) {
    let response = {
        data: [],
        count: 0,
    };
    let startOfMonth = moment(moment().startOf('month').format()).tz(payloadData.timeZone).unix() * 1000;
    let endOfMonth = moment(moment().endOf('month').format()).tz(payloadData.timeZone).unix() * 1000;

    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getServiceDetails: function (checkForUniqueKey, cb) {
            let pipeline = [
                {
                    $match: {
                        agencyId: mongoose.Types.ObjectId(userData._id),
                        workDate: {
                            $gte: startOfMonth, $lte: endOfMonth
                        },
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]
                    }
                },
                {

                    $addFields: { totalAmount:
                        { $add: ["$isExtend.extendAmount" ,"$amount"] }
                    }
                },
                {
                    $group: {
                        _id: {
                            maidId: "$maidId",
                        },
                        totalAmount: {$sum: "$totalAmount"},
                    }
                },
                {
                    $project: {
                        maidId: "$_id.maidId",
                        totalAmount: 1,
                        _id: 0
                    }
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$maidId",
                        connectFromField: "maidId",
                        connectToField: "maidId",
                        as: "serviceData",
                        restrictSearchWithMatch: {}
                    }
                },
                {
                    $unwind: "$serviceData"

                },
                {
                    $project: {
                        maidId: 1,
                        totalAmount: 1,
                        insideWorkDate: "$serviceData.workDate",
                        insideAmount: "$serviceData.amount"
                    }
                },
                {
                    $group: {
                        _id: {
                            maidId: "$maidId",
                            insideWorkDate: "$insideWorkDate",
                        },
                        totalInsideAmount: {$sum: "$insideAmount"},
                        totalAmount: {$avg: "$totalAmount"},
                    }
                },
                {
                    $project: {
                        maidId: "$_id.maidId",
                        insideWorkDate: "$_id.insideWorkDate",
                        totalAmount: 1,
                        totalInsideAmount: 1,
                        _id: 0
                    }
                },
                {
                    $group: {
                        _id: "$maidId",
                        totalAmount: {$avg: "$totalAmount"},
                        monthData: {
                            $push: {
                                totalInsideAmount: "$totalInsideAmount",
                                insideWorkDate: "$insideWorkDate"
                            }
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: '_id',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $project: {
                        _id: 0,
                        totalAmount: 1,
                        monthData: 1,
                        maidFirstName: "$maidId.firstName",
                        maidLastName: "$maidId.lastName",
                        maidNationality: "$maidId.nationality",
                        maidMakId: "$maidId.makId",
                        maidId: "$maidId._id",
                        pastTotalAmount: {
                            $filter: {
                                input: "$monthData",
                                as: "monthData",
                                cond: {$lte: ["$$monthData.insideWorkDate", (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000]}
                            }
                        },
                        fultureTotalAmount: {
                            $filter: {
                                input: "$monthData",
                                as: "monthData",
                                cond: {$gte: ["$$monthData.insideWorkDate", (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000]}
                            }
                        }
                    }
                },
                {
                    $project: {
                        _id: 0,
                        totalAmount: 1,
                        monthData: 1,
                        maidFirstName: 1,
                        maidLastName: 1,
                        maidNationality: 1,
                        maidMakId: 1,
                        maidId: 1,
                        pastTotal: {
                            $reduce: {
                                input: "$pastTotalAmount",
                                initialValue: {sum: 0},
                                in: {
                                    sum: {$add: ["$$value.sum", "$$this.totalInsideAmount"]},
                                }
                            }
                        },
                        futureTotal: {
                            $reduce: {
                                input: "$fultureTotalAmount",
                                initialValue: {sum: 0},
                                in: {
                                    sum: {$add: ["$$value.sum", "$$this.totalInsideAmount"]},
                                }
                            }
                        }
                    }
                },
                {
                    $project: {
                        _id: 0,
                        totalAmount: 1,
                        monthData: 1,
                        maidFirstName: 1,
                        maidLastName: 1,
                        maidNationality: 1,
                        maidMakId: 1,
                        maidId: 1,
                        pastTotal: "$pastTotal.sum",
                        futureTotal: "$futureTotal.sum"
                    }
                },
            ];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        $or: [
                            {maidFirstName: {$regex: payloadData.search, $options: "si"}},
                            {maidLastName: {$regex: payloadData.search, $options: "si"}},
                            {maidNationality: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                })
            }

            let sort = [];
            if (payloadData.pageNo && payloadData.limit) {
                sort.push({
                    $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                });
                sort.push({
                    $limit: payloadData.limit
                })
            }

            Models.Service.aggregate(pipeline.concat(sort), function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.data = result;
                        // serviceData=result
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getServiceCount: function (checkForUniqueKey, cb) {
            let pipeline = [
                {
                    $match: {
                        agencyId: mongoose.Types.ObjectId(userData._id),
                        workDate: {
                            $gte: startOfMonth, $lte: endOfMonth
                        },
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]
                    }
                },
                {
                    $group: {
                        _id: {
                            maidId: "$maidId",
                        },
                        totalAmount: {$sum: "$amount"},
                    }
                },
                {
                    $project: {
                        maidId: "$_id.maidId",
                        totalAmount: 1,
                        _id: 0
                    }
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$maidId",
                        connectFromField: "maidId",
                        connectToField: "maidId",
                        as: "serviceData",
                        restrictSearchWithMatch: {}
                    }
                },
                {
                    $unwind: "$serviceData"

                },
                {
                    $project: {
                        maidId: 1,
                        totalAmount: 1,
                        insideWorkDate: "$serviceData.workDate",
                        insideAmount: "$serviceData.amount"
                    }
                },
                {
                    $group: {
                        _id: {
                            maidId: "$maidId",
                            insideWorkDate: "$insideWorkDate",
                        },
                        totalInsideAmount: {$sum: "$insideAmount"},
                        totalAmount: {$avg: "$totalAmount"},


                    }
                },
                {
                    $project: {
                        maidId: "$_id.maidId",
                        insideWorkDate: "$_id.insideWorkDate",
                        totalAmount: 1,
                        totalInsideAmount: 1,
                        _id: 0
                    }
                },
                {
                    $group: {
                        _id: "$maidId",
                        totalAmount: {$avg: "$totalAmount"},
                        monthData: {
                            $push: {
                                totalInsideAmount: "$totalInsideAmount",
                                insideWorkDate: "$insideWorkDate"
                            }
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: '_id',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $project: {
                        _id: 0,
                        totalAmount: 1,
                        monthData: 1,
                        maidFirstName: "$maidId.firstName",
                        maidLastName: "$maidId.lastName",
                        maidNationality: "$maidId.nationality",
                        maidMakId: "$maidId.makId",
                        maidId: "$maidId._id",
                        pastTotalAmount: {
                            $filter: {
                                input: "$monthData",
                                as: "monthData",
                                cond: {$lte: ["$$monthData.insideWorkDate", (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000]}
                            }
                        },
                        fultureTotalAmount: {
                            $filter: {
                                input: "$monthData",
                                as: "monthData",
                                cond: {$gte: ["$$monthData.insideWorkDate", (moment(parseInt(Date.now())).tz(payloadData.timeZone).unix()) * 1000]}
                            }
                        }
                    }
                },
                {
                    $project: {
                        _id: 0,
                         totalAmount: 1,
                        monthData: 1,
                        maidFirstName: 1,
                        maidLastName: 1,
                        maidNationality: 1,
                        maidMakId: 1,
                        maidId: 1,
                        pastTotal: {
                            $reduce: {
                                input: "$pastTotalAmount",
                                initialValue: {sum: 0},
                                in: {
                                    sum: {$add: ["$$value.sum", "$$this.totalInsideAmount"]},
                                }
                            }
                        },
                        futureTotal: {
                            $reduce: {
                                input: "$fultureTotalAmount",
                                initialValue: {sum: 0},
                                in: {
                                    sum: {$add: ["$$value.sum", "$$this.totalInsideAmount"]},
                                }
                            }
                        }
                    }
                },
                {
                    $project: {
                        _id: 0,
                        totalAmount: 1,
                        monthData: 1,
                        maidFirstName: 1,
                        maidLastName: 1,
                        maidNationality: 1,
                        maidMakId: 1,
                        maidId: 1,
                        pastTotal: "$pastTotal.sum",
                        futureTotal: "$futureTotal.sum"
                    }
                },
            ];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        $or: [
                            {maidFirstName: {$regex: payloadData.search, $options: "si"}},
                            {maidLastName: {$regex: payloadData.search, $options: "si"}},
                            {maidNationality: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                })
            }


            Models.Service.aggregate(pipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

    }, function (err, result) {
        callback(err, response)
    })
};

const detailedMonthlyInvoice = function (payloadData, userData, callback) {
    let response = {};
    if(payloadData.monthFilter !=undefined){

    }
    else{
        payloadData.monthFilter = new Date().getMonth();
    }
    let endOfMonth = new Date(new Date(new Date().setUTCDate(1)).setUTCHours(0,0,0,0)).setUTCMonth(payloadData.monthFilter +1) -1 + payloadData.timeZone;

    let startOfMonth = new Date(new Date(new Date().setUTCDate(1)).setUTCHours(0,0,0,0)).setUTCMonth(payloadData.monthFilter) + payloadData.timeZone;

    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getServiceDetails: function (checkForUniqueKey, cb) {
            let pipeline = [
                {
                    $match: {
                        agencyId: mongoose.Types.ObjectId(userData._id),
                        workDate: {
                            $gte: startOfMonth, $lte: endOfMonth
                        },
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]
                    }
                },
                {
                    $group: {
                        _id: "$maidId",
                        totalDuration: {$sum: "$duration"},

                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: '_id',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $project: {
                        _id: 1,
                        maidFirstName: "$maidId.firstName",
                        maidLastName: "$maidId.lastName",
                        maidNationality: "$maidId.nationality",
                        maidMakId: "$maidId.makId",
                        maidId: "$maidId._id",
                        maidPrice: "$maidId.actualPrice",
                        agencyId: "$maidId.agencyId",
                        duration: 1,
                        totalDuration: 1,
                        gross: {$multiply: ["$totalDuration", "$maidId.actualPrice"]},
                    }
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        _id: 1,
                        maidFirstName: 1,
                        maidLastName: 1,
                        maidNationality: 1,
                        maidMakId: 1,
                        maidId: 1,
                        maidPrice: 1,
                        duration: 1,
                        totalDuration: 1,
                        agencyId: "$agencyId._id",
                        commission: "$agencyId.commission",
                        gross: 1,
                    }
                },
                {
                    $addFields: {
                        netCommission: {
                            $multiply: [{$divide: ["$commission", 100]}, "$gross"]

                        }
                    }
                },
                {
                    $addFields: {
                        netAmount: {
                            $subtract: ["$gross", "$netCommission"]

                        }
                    }
                },
            ];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        $or: [
                            {maidFirstName: {$regex: payloadData.search, $options: "si"}},
                            {maidLastName: {$regex: payloadData.search, $options: "si"}},
                            {maidNationality: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                })
            }

            let sort = [];
            if (payloadData.pageNo && payloadData.limit) {
                sort.push({
                    $skip: ((payloadData.pageNo - 1) * payloadData.limit)
                });
                sort.push({
                    $limit: payloadData.limit
                })
            }


            Models.Service.aggregate(pipeline.concat(sort), function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getServiceCount: function (checkForUniqueKey, cb) {
            let pipeline = [
                {
                    $match: {
                        agencyId: mongoose.Types.ObjectId(userData._id),
                        workDate: {
                            $gte: startOfMonth, $lte: endOfMonth
                        },
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ]
                    }
                },
                {
                    $group: {
                        _id: "$maidId",
                        totalDuration: {$sum: "$duration"},

                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: '_id',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $project: {
                        _id: 1,
                        maidFirstName: "$maidId.firstName",
                        maidLastName: "$maidId.lastName",
                        maidNationality: "$maidId.nationality",
                        maidMakId: "$maidId.makId",
                        maidId: "$maidId._id",
                        maidPrice: "$maidId.price",
                        agencyId: "$maidId.agencyId",
                        duration: 1,
                        totalDuration: 1,
                        gross: {$multiply: ["$totalDuration", "$maidId.price"]},
                    }
                },
                {
                    $lookup: {
                        from: 'agencies',
                        localField: 'agencyId',
                        foreignField: '_id',
                        as: 'agencyId'
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        _id: 1,
                        maidFirstName: 1,
                        maidLastName: 1,
                        maidNationality: 1,
                        maidMakId: 1,
                        maidId: 1,
                        maidPrice: 1,
                        duration: 1,
                        totalDuration: 1,
                        agencyId: "$agencyId._id",
                        commission: "$agencyId.commission",
                        gross: 1,
                    }
                },
                {
                    $addFields: {
                        netCommission: {
                            $multiply: [{$divide: ["$commission", 100]}, "$gross"]

                        }
                    }
                },
                {
                    $addFields: {
                        netAmount: {
                            $subtract: ["$gross", "$netCommission"]

                        }
                    }
                },
            ];

            if (payloadData.search) {
                pipeline.push({
                    $match: {
                        $or: [
                            {maidFirstName: {$regex: payloadData.search, $options: "si"}},
                            {maidLastName: {$regex: payloadData.search, $options: "si"}},
                            {maidNationality: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                })
            }


            Models.Service.aggregate(pipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }

    }, function (err, result) {
        callback(err, response)
    })
};

const listAllReviewsAgency = function (payloadData, userData, callback) {
    console.log("______________payloadData", payloadData);

    let response = {};
    let totalMaidRating = [];
    let averageRating = [];
    let totalMaidCount = 0;

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMaidRating: function (checkForUniqueKey, cb) {
            let pipeline = [
                {
                    $match: {
                        isBlocked: false,
                        isDeleted: false,
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidData'
                    }
                },
                {
                    $unwind: "$maidData"
                },
                {
                    $project: {
                        serviceId: 1,
                        maidId: 1,
                        agencyId: "$maidData.agencyId",
                        maidRating: 1,
                        feedBack: 1,
                        restMaid: "$maidData._id"
                    }
                },
                {
                    $match: {
                        agencyId: mongoose.Types.ObjectId(userData._id)
                    }
                },
                {
                    $group: {
                        _id: "$maidId",
                        cleaningRating: {$sum: "$maidRating.cleaning"},
                        ironingRating: {$sum: "$maidRating.ironing"},
                        cookingRating: {$sum: "$maidRating.cooking"},
                        childCareRating: {$sum: "$maidRating.childCare"},
                        cleaningCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cleaning", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        ironingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.ironing", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        cookingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cooking", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        childCareCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.childCare", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        restMaid: {$addToSet: "$restMaid"}

                    }
                },
                {
                    $project: {
                        maidId: "$_id",
                        restMaid: 1,
                        avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                        avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                        avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                        avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                    }
                },
                {
                    $unwind: "$restMaid"
                },
                {
                    $project: {
                        maidId: {
                            $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$maidId", else: "$restMaid"}
                        },
                        avgCleaning: {
                            $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgCleaning", else: 0}
                        },
                        avgIroning: {
                            $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgIroning", else: 0}
                        },
                        avgCooking: {
                            $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgCooking", else: 0}
                        },
                        avgChildCare: {
                            $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgChildCare", else: 0}
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $project: {
                        _id: 0,
                        maidId: "$maidId._id",
                        makId: "$maidId.makId",
                        nationality: "$maidId.nationality",
                        firstName: "$maidId.firstName",
                        lastName: "$maidId.lastName",
                        avgCleaning: 1,
                        avgIroning: 1,
                        avgCooking: 1,
                        avgChildCare: 1,
                    }
                }
            ];

            if (payloadData.reviewFilterType && payloadData.starFilter) {
                let match = {};
                if (payloadData.reviewFilterType == 'cleaning') {
                    match.$match = {
                        avgCleaning: payloadData.starFilter
                    }
                }
                else if (payloadData.reviewFilterType == 'ironing') {
                    match.$match = {
                        avgIroning: payloadData.starFilter
                    }
                }
                else if (payloadData.reviewFilterType == 'cooking') {
                    match.$match = {
                        avgCooking: payloadData.starFilter
                    }
                }
                else if (payloadData.reviewFilterType == 'childCare') {
                    match.$match = {
                        avgChildCare: payloadData.starFilter
                    }
                }
                pipeline.push(match)
            }

            if (payloadData.search) {
                let match = {
                    $match: {
                        $or: [
                            {firstName: {$regex: payloadData.search, $options: "si"}},
                            {lastName: {$regex: payloadData.search, $options: "si"}},
                            {nationality: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                };
                pipeline.push(match)
            }
            let sort = [
                {$skip: ((payloadData.pageNo - 1) * payloadData.limit)},
                {$limit: payloadData.limit}
            ];
            pipeline = pipeline.concat(sort);

            Models.Review.aggregate(pipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        totalMaidRating = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            });
        },

        getMaidCount: function (checkForUniqueKey, cb) {
            let pipeline = [
                {
                    $match: {
                        isBlocked: false,
                        isDeleted: false,
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidData'
                    }
                },
                {
                    $unwind: "$maidData"
                },
                {
                    $project: {
                        serviceId: 1,
                        maidId: 1,
                        agencyId: "$maidData.agencyId",
                        maidRating: 1,
                        feedBack: 1,
                        restMaid: "$maidData._id"
                    }
                },
                {
                    $match: {
                        agencyId: mongoose.Types.ObjectId(userData._id)
                    }
                },
                {
                    $group: {
                        _id: "$maidId",
                        cleaningRating: {$sum: "$maidRating.cleaning"},
                        ironingRating: {$sum: "$maidRating.ironing"},
                        cookingRating: {$sum: "$maidRating.cooking"},
                        childCareRating: {$sum: "$maidRating.childCare"},
                        cleaningCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cleaning", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        ironingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.ironing", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        cookingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cooking", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        childCareCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.childCare", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        restMaid: {$addToSet: "$restMaid"}

                    }
                },
                {
                    $project: {
                        maidId: "$_id",
                        restMaid: 1,
                        avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                        avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                        avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                        avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                    }
                },
                {
                    $unwind: "$restMaid"
                },
                {
                    $project: {
                        maidId: {
                            $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$maidId", else: "$restMaid"}
                        },
                        avgCleaning: {
                            $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgCleaning", else: 0}
                        },
                        avgIroning: {
                            $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgIroning", else: 0}
                        },
                        avgCooking: {
                            $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgCooking", else: 0}
                        },
                        avgChildCare: {
                            $cond: {if: {$eq: ["$maidId", "$restMaid"]}, then: "$avgChildCare", else: 0}
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidId'
                    }
                },
                {
                    $unwind: "$maidId"
                },
                {
                    $project: {
                        _id: 0,
                        maidId: "$maidId._id",
                        makId: "$maidId.makId",
                        nationality: "$maidId.nationality",
                        firstName: "$maidId.firstName",
                        lastName: "$maidId.lastName",
                        avgCleaning: 1,
                        avgIroning: 1,
                        avgCooking: 1,
                        avgChildCare: 1,
                    }
                }
            ];

            if (payloadData.reviewFilterType && payloadData.starFilter) {
                let match = {};
                if (payloadData.reviewFilterType == 'cleaning') {
                    match.$match = {
                        avgCleaning: payloadData.starFilter
                    }
                }
                else if (payloadData.reviewFilterType == 'ironing') {
                    match.$match = {
                        avgIroning: payloadData.starFilter
                    }
                }
                else if (payloadData.reviewFilterType == 'cooking') {
                    match.$match = {
                        avgCooking: payloadData.starFilter
                    }
                }
                else if (payloadData.reviewFilterType == 'childCare') {
                    match.$match = {
                        avgChildCare: payloadData.starFilter
                    }
                }
                pipeline.push(match)
            }

            if (payloadData.search) {
                let match = {
                    $match: {
                        $or: [
                            {firstName: {$regex: payloadData.search, $options: "si"}},
                            {lastName: {$regex: payloadData.search, $options: "si"}},
                            {nationality: {$regex: payloadData.search, $options: "si"}},
                        ]
                    }
                };
                pipeline.push(match)
            }

            Models.Review.aggregate(pipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        totalMaidCount = result.length;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            });
        },

        getAvergaeRating: function (checkForUniqueKey, cb) {
            let pipeline = [
                {
                    $match: {
                        isBlocked: false,
                        isDeleted: false,
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: 'maidId',
                        foreignField: '_id',
                        as: 'maidData'
                    }
                },
                {
                    $unwind: "$maidData"
                },
                {
                    $project: {
                        serviceId: 1,
                        maidId: 1,
                        agencyId: "$maidData.agencyId",
                        maidRating: 1,
                        feedBack: 1,
                        restMaid: "$maidData._id"
                    }
                },
                {
                    $match: {
                        agencyId: mongoose.Types.ObjectId(userData._id)
                    }
                },
                {
                    $group: {
                        _id: "",
                        cleaningRating: {$sum: "$maidRating.cleaning"},
                        ironingRating: {$sum: "$maidRating.ironing"},
                        cookingRating: {$sum: "$maidRating.cooking"},
                        childCareRating: {$sum: "$maidRating.childCare"},
                        cleaningCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cleaning", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        ironingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.ironing", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        cookingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cooking", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        childCareCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.childCare", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        // restMaid: {$addToSet: "$restMaid"}
                    }
                },
                {
                    $project: {
                        _id: 0,
                        avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                        avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                        avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                        avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                    }
                }
            ];

            Models.Review.aggregate(pipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        averageRating = result[0];
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            });
        }

    }, function (err, result) {
        response.totalMaidRating = totalMaidRating;
        response.totalMaidCount = totalMaidCount;
        response.averageRating = averageRating;
        callback(err, response)
    });
};

const changePasswordByAgency = function (payloadData, userData, callback) {
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkCurrentPasswordAndUpdate: function (checkForUniqueKey, cb) {
            if (UniversalFunctions.CryptData(payloadData.oldPassword) === userData.password) {

                let criteria = {
                    _id: userData._id,
                    isDeleted: false,
                    isBlocked: false,
                };
                let projection = {
                    password: UniversalFunctions.CryptData(payloadData.newPassword),
                };
                let option = {new: true};

                Service.AgencyServices.updateAgency(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        callback(null, Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT)
                    }
                })
            } else {
                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASSWORD)
            }
        },

    }, function (err, result) {
        callback(err, {})
    })
};

const forgetPasswordByAgency = function (payloadData, callback) {
    let userData = {};
    let passwordResetToken = "";
    let response = {};
    var content;
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        verifyEmail: function (checkForUniqueKey, cb) {
            let criteria = {
                email: payloadData.email,
                isDeleted: false,
                isBlocked: false,
            };

            let projection = {};
            let option = {lean: true};
            Service.AgencyServices.getAgency(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result && result.length) {
                        userData = result[0];
                        cb(null)
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_REGISTERED)
                    }
                }
            })
        },

        generatePasswordResetToken: function (verifyEmail, cb) {
            passwordResetToken = (otpGenerator.generate(6, {specialChars: false}));
            let criteria = {
                email: payloadData.email,
                isDeleted: false,
                isDeactivated: false,
            };

            let projection = {
                $set: {
                    password: UniversalFunctions.CryptData(passwordResetToken.toLowerCase()),
                }
            };
            let option = {
                new: true
            };
            Service.AgencyServices.updateAgency(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                  //  response.passwordResetToken = passwordResetToken.toLowerCase();
                    cb(null)
                }
            })
        },
        emailTemplate:function(generatePasswordResetToken,cb){
            let data={};
            data.password = passwordResetToken.toLowerCase();
            data.fullName =   userData.agencyName;

            MaidController.emailTemplateForgetPassword(data,(err,result)=>{
                if(err){
                    cb(null)
                }
                else{
                    content=result;
                    cb(null)
                }
            })
        },
        sendMail: function (emailTemplate, cb) {
            if (payloadData.email && (payloadData.email === userData.email)) {
                var subject = "M.A.K Temporary Password";
            
                MailManager.sendMailBySES(payloadData.email, subject, content, function (err, res) {
                });
                cb()
            }
            else {
                cb()
            }
        },

    }, function (err, result) {
        callback(err, response)
    })
};

const deleteMaidByAgency = function (payloadData, callback) {
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        deleteANdUndeleteMaidInDb: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.maidId
            };
            let dataToUpdate = {};
            if (payloadData.action == "DELETE") {
                dataToUpdate.isDeleted = true
            } else {
                dataToUpdate.isDeleted = false
            }
            let option = {new: true};
            Service.MaidServices.updateMaid(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        }

    }, function (err, result) {
        if (payloadData.action === "DELETE") {
            callback(err, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DELETED)
        } else {
            callback(err, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT)
        }
    })
};

const blockMaidByAgency = function (payloadData, callback) {
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        blockAndUnblockMaidInDb: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.maidId
            };
            let dataToUpdate = {};
            if (payloadData.action == "BLOCK") {
                dataToUpdate.isBlocked = true
            } else {
                dataToUpdate.isBlocked = false
            }
            let option = {new: true};
            Service.MaidServices.updateMaid(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        }

    }, function (err, result) {
        if (payloadData.action == "BLOCK") {
            callback(err, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.BLOCKED)
        } else {
            callback(err, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT)
        }
    })
};

const sendNotification = function (notificationData, callback) {

    async.auto({
        sendPushFinally: function (cb) {
            if (!(notificationData.deviceToken == null)) {
                if (notificationData.deviceToken.length > 15) {
                    async.parallel([
                        function (cb) {
                            NotificationManager.sendPushUser(notificationData, function (err, result) {
                                if (err) {
                                    cb(null)
                                } else {
                                    cb(null)
                                }
                            })
                        },

                        function (cb) {
                            let dataToSet = {
                                senderId: notificationData.senderId,
                                receiverId: notificationData.receiverId,
                                maidId: notificationData.maidId,
                                type: notificationData.type,
                                serviceId: notificationData.requestId,
                                bookingId: notificationData.bookingNumber,
                                userMessage: notificationData.body,
                            };

                            Service.NotificationServices.createNotification(dataToSet, function (err, result) {
                                if (err) {
                                    callback(err)
                                } else {
                                    cb(null)
                                }
                            })
                        },
                    ], function (err, result) {
                        callback(err, result)
                    })
                } else {
                    callback()
                }
            } else {
                callback()
            }
        }
    }, function (err, res) {
        callback()
    })
};

const sendNotificationMaid = function (notificationData, callback) {

    async.auto({
        sendPushFinally: function (cb) {
            async.parallel([
                function (cb) {
                    if (!(notificationData.deviceToken == null)) {
                        if (notificationData.deviceToken.length > 15) {
                            NotificationManager.sendPushMaid(notificationData, function (err, result) {
                                if (err) {
                                    cb(null)
                                } else {
                                    cb(null)
                                }
                            })
                        } else {
                            cb(null)
                        }
                    } else {
                        cb(null)
                    }

                },

                function (cb) {
                    let dataToSet = {
                        senderId: notificationData.senderId,
                        receiverId: notificationData.maidId,
                        maidId: notificationData.maidId,
                        type: notificationData.type,
                        serviceId: notificationData.requestId,
                        bookingId: notificationData.bookingNumber,
                        maidMessage: notificationData.body,
                        fullName: notificationData.fullName,
                    };

                    Service.NotificationServices.createNotification(dataToSet, function (err, result) {
                        if (err) {
                            callback(err)
                        } else {
                            cb(null)
                        }
                    })
                },
            ], function (err, result) {
                callback(err, result)
            })

        }
    }, function (err, res) {
        callback()
    })
};

const listAllNotificationAgency = function (payloadData, userData, callback) {
    let respone = [];
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getNotificationData: function (checkForUniqueKey, cb) {
            let criteria = {
                $or: [
                    {receiverId: userData._id,},
                    {receiverArray: userData._id}
                ],
                $and: [
                    {agencyMessage: {$exists: true}},
                    {agencyMessage: {$nin: ["", null]}}
                ],
                isDeleted: {$nin: [userData._id]}
            };
            if (payloadData.type == "NEW_REGISTRATION") {
                criteria.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.NEW_MAID
            }
            else if (payloadData.type == "JOB") {
                criteria.type = {$nin: [Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.NEW_MAID,]}
            }

            let projection = {};
            let option = {
                lean: true,
                sort: {timeStamp: -1},
            };
            Service.NotificationServices.getNotification(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        Models.Users.populate(result, [{
                            path: 'senderId',
                            select: '_id fullName profilePicURL',
                            model: 'Users'
                        }], function (err, populatedTransactions) {
                            if (err) {
                                cb(err)
                            }
                            else {
                                respone = populatedTransactions;
                                cb(null)
                            }
                        })
                    } else {
                        cb(null)
                    }
                }
            })
        },

        updateNotificationData: function (getNotificationData, cb) {
            let criteria = {
                $or: [
                    {receiverId: userData._id,},
                    {receiverArray: userData._id}
                ],
                isDeleted: {$nin: [userData._id]},
                read: {$nin: [userData._id]}
            };
            if (payloadData.type == "NEW_REGISTRATION") {
                criteria.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.NEW_MAID
            }
            else if (payloadData.type == "JOB") {
                criteria.type = {$nin: [Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.NEW_MAID,]}
            }
            let dataToUpdate = {
                $addToSet: {
                    read: userData._id
                }
            };
            let option = {
                lean: true,
                multi: true,
                new: true
            };
            Service.NotificationServices.updateAllNotification(criteria, dataToUpdate, option, function (err, result) {
                console.log("______________err,result", err, result)
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        },

    }, function (err, result) {
        callback(err, respone)
    })
};

const clearNotificationAgency = function (payloadData, userData, callback) {
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        updateNotificationData: function (checkForUniqueKey, cb) {
            let criteria = {
                receiverId: userData._id
            };
            if (payloadData.type == "NEW_REGISTRATION") {
                criteria.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.NEW_MAID
            }
            else if (payloadData.type == "JOB") {
                criteria.type = {$nin: [Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.NEW_MAID]}
            }

            let dataToUpdate = {
                $addToSet: {
                    isDeleted: userData._id
                }
            };
            let option = {
                lean: true,
                multi: true,
                new: true
            };
            Service.NotificationServices.updateAllNotification(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        },

    }, function (err, result) {
        callback(err, {})
    })
};

const getNotificationUnredCount = function (payloadData, userData, callback) {
    let respone = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getNotificationData: function (checkForUniqueKey, cb) {
            let criteria = {
                $or: [
                    {receiverId: userData._id,},
                    {receiverArray: userData._id}
                ],
                isDeleted: {$nin: [userData._id]},
                read: {$nin: [userData._id]},
            };
            let projection = {};
            let option = {
                lean: true,
            };
            Service.NotificationServices.getNotification(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        respone.unreadCount = result.length;
                        respone.commission = userData.commission || process.env.commission;
                        cb(null)
                    } else {
                        respone.unreadCount = 0;
                        respone.commission = userData.commission || process.env.commission;
                        cb(null)
                    }
                }
            })
        },

    }, function (err, result) {
        respone.dateOfRegistration = userData.registrationDate;
        callback(err, respone)
    })
};

const actionPerformOnNotification = function (payloadData, userData, callback) {
    let respone = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        actionPerformTrue: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.notificationId
            };
            let dataToUpdate = {
                $addToSet: {
                    actionPerformed: userData._id
                }
            };
            let option = {
                new: true
            };
            Service.NotificationServices.updateNotification(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        },
    }, function (err, result) {
        callback(err, respone)
    })
};

const contactUs = function (payloadData, userData, callback) {
    let response = {},content;
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        addcontactUs: function (checkForUniqueKey, cb) {
            let dataToInsert = {};
            dataToInsert = {
                name: payloadData.name,
                email: payloadData.email,
                comment: payloadData.comment,
                createdOn : +new Date(),
                uniquieAppKey: UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME),
            };
            if (payloadData.phoneNumber) {
                dataToInsert.phoneNumber = payloadData.phoneNumber
            }
            Service.ContactUsServices.createContactUs(dataToInsert, function (err, result) {
                if (err) {
                    console.log("__________FDS", err);
                    cb(err);
                } else {
                    if (result) {
                        response = result;
                        cb(null)
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                    }
                }
            })
        },
        createEmailTemplate:function(addcontactUs,cb){

            let data =  {
                fullName: payloadData.name,
            }

            if(payloadData.type === 'USER'){
                contactUsUser(data,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        content=result;
                        cb(null)
                    }
                })
            }
            else {
                contactUsTemp(data,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        content=result;
                        cb(null)
                    }
                })
            }


        },
        sendMail: function (createEmailTemplate, cb) {

            let subject = "Thank you for your interest in MAK!";

            MailManager.sendMailBySES(payloadData.email, subject, content, function (err, res) {
            });
            cb()
        }
    }, function (err, result) {
        callback(err, response)
    })
};

const contactUsUser =  function(data,callback){

    let content = `
      <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style type="text/css">
        
        body,
        #bodyTable {
            height: 100%!important;
            margin: 0;
            padding: 0;
            width: 100%!important
        }

        #bodyTable {
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            min-height: 100%;
            background: #eee;
            color: #333
        }

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0
        }

        img {
            -ms-interpolation-mode: bicubic
        }

        body {
            margin: 0;
            padding: 0;
            background: #eee
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: 0;
            text-decoration: none
        }

        table {
            border-collapse: collapse!important;
            max-width: 100%!important
        }

        img {
            border: 0!important
        }

        h1 {
            font-family: Helvetica;
            font-size: 42px;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            margin: 30px auto 0
        }

        h2 {
            font-size: 32px;
            font-style: normal;
            font-weight: bold;
            margin: 50px auto 0
        }

        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 25px 0;
            letter-spacing: normal;
            text-align: left
        }

        a h3 {
            color: #444!important;
            text-decoration: none!important
        }

        .titleLink {
            text-decoration: none!important
        }

        .preheaderContent {
            color: #808080;
            font-size: 10px;
            line-height: 125%
        }

        .preheaderContent a:link,
        .preheaderContent a:visited,
        .preheaderContent a .yshortcuts {
            color: #606060;
            font-weight: normal;
            text-decoration: underline
        }

        #emailHeader,
        #tacoTip {
            color: #fff
        }

        #emailHeader {
            background-color: #fff
        }

        #content p {
            color: #4d4d4d;
            margin: 20px 70px 30px;
            font-size: 24px;
            line-height: 32px;
            text-align: center
        }

        #button {
            display: inline-block;
            margin: 10px auto;
            background: #fff;
            border-radius: 4px;
            font-weight: bold;
            font-size: 18px;
            padding: 15px 20px;
            cursor: pointer;
            color: #0079bf;
            margin-bottom: 50px
        }

        #socialIconWrap img {
            line-height: 35px!important;
            padding: 0 5px
        }

        .footerContent div {
            color: #707070;
            font-family: Arial;
            font-size: 12px;
            line-height: 125%;
            text-align: center;
            max-width: 100%!important
        }

        .footerContent div a:link,
        .footerContent div a:visited {
            color: #369;
            font-weight: normal;
            text-decoration: underline
        }

        .footerContent img {
            display: inline
        }

        #socialLinks img {
            margin: 0 2px
        }

        #utility {
            border-top: 1px solid #ddd
        }

        #utility div {
            text-align: center
        }

        #monkeyRewards img {
            max-width: 160px
        }

        #emailFooter {
            max-width: 100%!important
        }

        #footerTwitter a,
        #footerFacebook a {
            text-decoration: none!important;
            color: #fff!important;
            font-size: 14px
        }

        #emailButton {
            border-radius: 6px;
            background: #70b500!important;
            margin: 0 auto 60px;
            box-shadow: 0 4px 0 #578c00
        }

        #socialLinks a {
            width: 40px
        }

        #socialLinks #blogLink {
            width: 80px!important
        }

        .sectionWrap {
            text-align: center
        }

        #header {
            color: #fff!important
        }

    </style>
</head>

<body>
    <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px">
                                        <tbody>
                                            <tr>
                                                <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="http://52.36.127.111/mak_web/img/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120">
                                                    <h1 style="font-size: 30px;">Welcome to MAK</h1>
                                                    <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="background:#FFFFFF;">
                                                    <br>
                                                    Dear <b>${data.fullName}</b><br><br>
                                                  Thank you for your interest in MAK. A member of the MAK team will get in touch with you shortly.<br><br>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                           <td align="left">

Sincerely,<br>
MAK Team<br><br>
                                              </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px">
                                        <tbody>
                                            <tr>
                                                <td class="footerContent" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0" style="width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">&nbsp;
                                                                    <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
  
    `
    callback(null,content)
}

const contactUsTemp =  function(data,callback){

    let content = `
      <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style type="text/css">
        
        body,
        #bodyTable {
            height: 100%!important;
            margin: 0;
            padding: 0;
            width: 100%!important
        }

        #bodyTable {
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            min-height: 100%;
            background: #eee;
            color: #333
        }

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0
        }

        img {
            -ms-interpolation-mode: bicubic
        }

        body {
            margin: 0;
            padding: 0;
            background: #eee
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: 0;
            text-decoration: none
        }

        table {
            border-collapse: collapse!important;
            max-width: 100%!important
        }

        img {
            border: 0!important
        }

        h1 {
            font-family: Helvetica;
            font-size: 42px;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            margin: 30px auto 0
        }

        h2 {
            font-size: 32px;
            font-style: normal;
            font-weight: bold;
            margin: 50px auto 0
        }

        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 25px 0;
            letter-spacing: normal;
            text-align: left
        }

        a h3 {
            color: #444!important;
            text-decoration: none!important
        }

        .titleLink {
            text-decoration: none!important
        }

        .preheaderContent {
            color: #808080;
            font-size: 10px;
            line-height: 125%
        }

        .preheaderContent a:link,
        .preheaderContent a:visited,
        .preheaderContent a .yshortcuts {
            color: #606060;
            font-weight: normal;
            text-decoration: underline
        }

        #emailHeader,
        #tacoTip {
            color: #fff
        }

        #emailHeader {
            background-color: #fff
        }

        #content p {
            color: #4d4d4d;
            margin: 20px 70px 30px;
            font-size: 24px;
            line-height: 32px;
            text-align: center
        }

        #button {
            display: inline-block;
            margin: 10px auto;
            background: #fff;
            border-radius: 4px;
            font-weight: bold;
            font-size: 18px;
            padding: 15px 20px;
            cursor: pointer;
            color: #0079bf;
            margin-bottom: 50px
        }

        #socialIconWrap img {
            line-height: 35px!important;
            padding: 0 5px
        }

        .footerContent div {
            color: #707070;
            font-family: Arial;
            font-size: 12px;
            line-height: 125%;
            text-align: center;
            max-width: 100%!important
        }

        .footerContent div a:link,
        .footerContent div a:visited {
            color: #369;
            font-weight: normal;
            text-decoration: underline
        }

        .footerContent img {
            display: inline
        }

        #socialLinks img {
            margin: 0 2px
        }

        #utility {
            border-top: 1px solid #ddd
        }

        #utility div {
            text-align: center
        }

        #monkeyRewards img {
            max-width: 160px
        }

        #emailFooter {
            max-width: 100%!important
        }

        #footerTwitter a,
        #footerFacebook a {
            text-decoration: none!important;
            color: #fff!important;
            font-size: 14px
        }

        #emailButton {
            border-radius: 6px;
            background: #70b500!important;
            margin: 0 auto 60px;
            box-shadow: 0 4px 0 #578c00
        }

        #socialLinks a {
            width: 40px
        }

        #socialLinks #blogLink {
            width: 80px!important
        }

        .sectionWrap {
            text-align: center
        }

        #header {
            color: #fff!important
        }

    </style>
</head>

<body>
    <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px">
                                        <tbody>
                                            <tr>
                                                <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="http://52.36.127.111/mak_web/img/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120">
                                                    <h1 style="font-size: 30px;">Welcome to MAK</h1>
                                                    <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="background:#FFFFFF;">
                                                    <br>
                                                    Dear <b>${data.fullName}</b><br><br>
                                                  Thank you for your interest in MAK. A member of the MAK team will get in touch with you shortly.<br><br>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>

                                            </tr>
                                            <tr>
                                           <td align="left">

Sincerely,<br>
MAK Team<br><br>
                                              </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px">
                                        <tbody>
                                            <tr>
                                                <td class="footerContent" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0" style="width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">&nbsp;
                                                                    <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
  
    `
    callback(null,content)
}

const issueList = function (payloadData, userData, callback) {
    let response = {};
    let maidId = [];
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMaids: function (checkForUniqueKey, cb) {
            Models.Maids.distinct("_id", {agencyId: userData._id}, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        maidId = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getIssueList: function (getMaids, cb) {
            if (maidId && maidId.length) {
                let criteria = {
                    isDeleted: false,
                    userType: "MAID",
                    userId: {$in: maidId},
                };
                let projection = {};
                let option = {
                    lean: true,
                    skip: ((payloadData.pageNo - 1) * payloadData.limit),
                    limit: payloadData.limit,
                    sort :{_id:-1}
                };
                let populate = [{
                    path: 'userId',
                    select: '_id firstName lastName makId email countryCode phoneNo',
                    model: 'Maids'
                }];

                Service.IssueServices.getIssuePopulate(criteria, projection, option, populate, function (err, result) {
                    if (err) {
                        cb(err)
                    }
                    else {
                        if (result && result.length) {
                            response.data = result;
                            cb(null)
                        }
                        else {
                            response.data = [];
                            cb(null)
                        }
                    }
                })
            } else {
                response.data = [];
                cb(null)

            }
        },

        getIssueListCount: function (getMaids, cb) {
            if (maidId && maidId.length) {
                let criteria = {
                    isDeleted: false,
                    userType: "MAID",
                    userId: {$in: maidId},
                };
                let projection = {};
                let option = {
                    lean: true
                };


                Service.IssueServices.getIssue(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    }
                    else {
                        if (result && result.length) {
                            response.count = result.length;
                            cb(null)
                        }
                        else {
                            response.count = 0;
                            cb(null)
                        }
                    }
                })
            } else {
                response.count = 0;
                cb(null)
            }
        }

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, response)
        }
    })
};

const countryList = function (queryData, callback) {
    let userData;
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = queryData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        getAdvs: function (checkForUniqueKey, cb) {
            let criteria = {};
            let projection = {};
            let option = {
                lean: true
            };
            Service.CountryServices.getCountry(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                }
                else {
                    if (result && result.length) {
                        userData = result;
                        cb(null)
                    }
                    else {
                        cb(null)
                    }
                }
            })
        }
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, userData)
        }
    })
};

const createXlsx = function (payloadData, userData, callback) {
    let response = JSON.parse(payloadData.jsonData);
    let dataToSend = [];
    let buffers;
    let finalUrl;
    let option = {
        save: true,
        sheetName: [],
        fileName: "Current_Month_Summary" + new Date().getMilliseconds() + ".xlsx",
        path: path.resolve('.') + '/uploads/',
        defaultSheetName: "worksheet"
    };
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        createExcelFormat: function (checkForUniqueKey, cb) {
            let len = response.length;
            let count = 0;
            for (let i = 0; i < len; i++) {
                (function (i) {
                    let obj1 = {};
                    count = count + 1;
                    obj1["Maid Name"] = response[i].maidFirstName + response[i].maidLastName;
                    obj1["MAID_ID"] = response[i].maidMakId;
                    obj1["Nationality"] = response[i].maidNationality;
                    obj1["Actual Revenue"] = response[i].pastTotal;
                    obj1["Upcoming Revenue"] = response[i].futureTotal;
                    obj1["Projected Revenue"] = response[i].totalAmount;

                    for (let j = 0; j < response[i].dailyRevenue.length; j++) {
                        let Day = "Day " + j
                        obj1[Day] = response[i].dailyRevenue[j];

                    }
                    dataToSend.push(obj1)
                    if (i == len - 1) {
                        cb(null)
                    }
                }(i))
            }
        },

        putData: function (createExcelFormat, cb) {
            var model = mongoXlsx.buildDynamicModel(dataToSend);

            mongoXlsx.mongoData2Xlsx(dataToSend, model, option, function (err, path) {
                cb(null, path);
            });
        },

        createBuffer: function (putData, cb) {
            dataToSend = path.resolve('.') + '/uploads/' + option.fileName;
            fs.readFile(dataToSend, function (err, fileBuffer) {
                if (err) {
                    cb(err)
                } else {
                    buffers = fileBuffer;
                    cb(null)
                }
            })
        },

        uploadExcel: function (createBuffer, cb) {
            UploadManager.awsUpload(option.fileName, buffers, function (err, results) {
                if (err) cb(err);
                else {
                    finalUrl = results;
                    cb(null)
                }
            })
        },

        deleteFile: function (uploadExcel, cb) {
            fsExtra.remove(dataToSend, function (err, res) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            });
        },

    }, function (err, result) {
        callback(err, finalUrl)
    })
};

const listAllAgency = function (payloadData, userData, callback) {

    let response = [];
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        listAgency: function (checkForUniqueKey, cb) {
			// console.log("****** ####3 userData #########33"+JSON.stringify(userData));
            let criteria = {
                // _id:{$ne:userData._id},
                agencyType:{$nin:[Config.APP_CONSTANTS.DATABASE.AGENCY_TYPE.NORMAL]}
            };
            let projection = {
                agencyName:1,
                countryName:1,
            };
            let option = {lean: true};
            Service.AgencyServices.getAgency(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }

    }, function (err, result) {
        callback(err, response)
    });
};

const switchMAKAgency = function (payloadData, userdata, callback) {
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        updateAgencyId: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.maidId
            };
            let dataToUpdate = {
                agencyId: payloadData.agencyId
            };
            let option = {new: true};
            Service.MaidServices.updateMaid(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        }
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {})
        }
    })
};

const deleteFromList = function (payloadData, userdata, callback) {
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        updateAgencyId: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.serviceId
            };
            let dataToUpdate = {
                deleteFromList: true
            };
            let option = {new: true};
            Service.ServiceServices.updateService(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        }
    }, function (err, result) {
        if (err) {callback(err)}
        else {callback(null, {})
        }
    })
};


module.exports = {
    addObjectsToS3: addObjectsToS3,
    agencyLogin: agencyLogin,
    dashBoardData: dashBoardData,
    getAgencyProfile: getAgencyProfile,
    addMaid: addMaid,
    updateMaidProfile: updateMaidProfile,
    listAllUnVerifiedMaids: listAllUnVerifiedMaids,
    verifyMaid: verifyMaid,
    listAllMaids: listAllMaids,
    listAllUsers: listAllUsers,
    listAllService: listAllService,
    approveService: approveService,
    deleteService: deleteService,
    listAllCanceledService: listAllCanceledService,
    getCalender: getCalender,
    listAllExtendedService: listAllExtendedService,
    approveExtendedService: approveExtendedService,
    revenueReport: revenueReport,
    lifeTimeSummary: lifeTimeSummary,
    currentMonthSummary: currentMonthSummary,
    detailedMonthlyInvoice: detailedMonthlyInvoice,
    listAllReviewsAgency: listAllReviewsAgency,

    changePasswordByAgency: changePasswordByAgency,
    forgetPasswordByAgency: forgetPasswordByAgency,

    deleteMaidByAgency: deleteMaidByAgency,
    blockMaidByAgency: blockMaidByAgency,
    listAllNotificationAgency: listAllNotificationAgency,
    clearNotificationAgency: clearNotificationAgency,
    getNotificationUnredCount: getNotificationUnredCount,
    actionPerformOnNotification: actionPerformOnNotification,

    contactUs: contactUs,
    issueList: issueList,
    countryList: countryList,

    createXlsx: createXlsx,

    listAllAgency: listAllAgency,
    switchMAKAgency: switchMAKAgency,
    deleteFromList: deleteFromList,

    sendNotification: sendNotification,
    sendNotificationMaid: sendNotificationMaid,
};