'use strict';
const Service = require('../Services');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const async = require('async');
const TokenManager = require('../Lib/TokenManager');
const SMSManager = require('../Lib/SMSManager');
const MailManager = require('../Lib/MailManager');
const NotificationManager = require('../Lib/NotificationManager');
const Config = require('../Config');
const Models = require('../Models');
const moment = require('moment');
const mongoose = require('mongoose');
const _ = require('lodash');
const SocketManager = require('../Lib/SocketManager');
const otpGenerator = require('otp-generator');
const momentTz = require('moment-timezone');
const geolib = require('geolib');
const getCountry = require('country-currency-map').getCountry;


const maidSignup1 = function (payloadData, callback) {
    let response = {};
    let makId = null;
    let criteria = {};
    let commission = 0;
    let content;
    let content1;
    let passwordResetToken = (otpGenerator.generate(6, {specialChars: false}));
    
    let agencyEmail = "";

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkDuplicateMaid: function (checkForUniqueKey, cb) {
            criteria.email = payloadData.email;
            criteria.isDeleted =false;

            let projection = {};
            let options = {
                lean: true
            };
            Service.MaidServices.getMaid(criteria, projection, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_EXIST);
                    } else {
                        cb(null)
                    }
                }
            });
        },
        checkPhoneNumberAlreadyExist: function (checkDuplicateMaid, cb) {
            let criteria1 = {};
            console.log("I am here");
            console.log(payloadData.phoneNo, "asdasd");
            if(payloadData.phoneNo.trim() !== '') {
                criteria1.countryCode = payloadData.countryCode;
                criteria1.phoneNo = payloadData.phoneNo;
                criteria1.isDeleted = false;

                Service.MaidServices.getMaid(criteria1, {}, { lean: true}, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        if (data && data.length) {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_ALREADY_EXIST);
                        } else {
                            cb(null)
                        }
                    }
                });
            } else {
                cb(null);
            }
            
        },
        validateCountryCode: function (checkPhoneNumberAlreadyExist, cb) {

            if (payloadData.countryCode) {
                if (payloadData.countryCode.lastIndexOf('+') === 0) {
                    if (!isFinite(payloadData.countryCode.substr(1))) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                    }
                    else if (!payloadData.phoneNo || payloadData.phoneNo == '') {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_MISSING);
                    } else {
                        cb(null);
                    }
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
            } else if (payloadData.phoneNo) {
                if (!payloadData.countryCode) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.COUNTRY_CODE_MISSING);
                } else if (payloadData.countryCode && payloadData.countryCode.lastIndexOf('+') == 0) {
                    if (!isFinite(payloadData.countryCode.substr(1))) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                    } else {
                        cb(null);
                    }
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
            } else {
                cb(null);
            }
        },

        createMakId: function (validateCountryCode, cb) {
            Service.MaidServices.getMaid({}, {}, {}, function (err, result) {
                if (err) {
					console.log("error in create mak id "+err);
                    cb(err)
                } else {
                    makId = "MAID_" + (result.length + 40);
                    console.log("*********** create mak id ***********"+makId);
                    console.log("*********** create mak Length ***********"+result.length);
                    cb(null)
                }
            })
        },

        getAgencyData: function (createMakId, cb) {
            let criteria = {
                _id: payloadData.agencyId
            };
            let project = {};
            let option = {lean: true};
            Service.AgencyServices.getAgency(criteria, project, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        commission = result[0].commission;
                        agencyEmail = result[0].email;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        createNewMaid: function (getAgencyData, cb) {

            let dataToInsert = {
                email: payloadData.email,
                firstName: payloadData.firstName,
                lastName: payloadData.lastName,
                countryENCode: payloadData.countryENCode,
                countryCode: payloadData.countryCode,
                phoneNo: payloadData.phoneNo,
                fullName : payloadData.firstName + ' ' + payloadData.lastName,
                makId: makId,
                agencyId: payloadData.agencyId,
                commission: commission,
                uniquieAppKey: UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME),
                step: 0,
                timeZone: payloadData.timeZone,
                isVerified: false,
            };
            if(payloadData.password){
                dataToInsert.password=UniversalFunctions.CryptData(payloadData.password)
            }else{
                dataToInsert.password= UniversalFunctions.CryptData(passwordResetToken.toLowerCase())
                dataToInsert.passwordResetToken = passwordResetToken.toLowerCase()
            }
                

            if (payloadData.deviceToken) {
                dataToInsert.deviceToken = payloadData.deviceToken
            }
            if (payloadData.deviceType) {
                dataToInsert.deviceType = payloadData.deviceType
            }

            Service.MaidServices.createMaid(dataToInsert, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result) {
                        response = result;
                        cb(null)
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                    }
                }
            })
        },

        generateTokenAndUpdate: function (createNewMaid, cb) {
            let tokenData = {
                id: response._id,
                type: 'MAID'
            };
            TokenManager.setToken(tokenData, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    response.accessToken = data.accessToken;
                    cb(null);
                }
            })
        },

        populateAgencyId: function (generateTokenAndUpdate, cb) {
            Models.Agency.populate(response, [
                {
                    path: 'agencyId',
                    select: '_id agencyName',
                    model: 'Agency'
                }
            ], function (err, populatedTransactions) {
                if (err) {
                    cb(err)
                }
                else {
                    response = populatedTransactions;
                    cb(null)
                }
            })
        },

        createEmailTemplate:function(cb){
            let data =  {
                firstName: payloadData.firstName,
                lastName: payloadData.lastName,
                link : "http://mak.today/MAK/AgencyPanel/#/maidAgreement?email=" + payloadData.email
            }
            emailTemplate(data,(err,result)=>{
                if(err){
                    cb(err)
                }
                else{
                    content=result;
                    cb(null)
                }
            })
        },
        sendMail: function (createEmailTemplate, cb) {

            let subject = "Agreement for M.A.K ";

            MailManager.sendMailBySES(payloadData.email, subject, content, function (err, res) {
            });
            cb()
        },
    }, function (err, result) {
        callback(err, response)
    })
};

const updateMaidProfileInApp = function (payloadData, userData, callback) {
    let response = {};
    let commission = 0;
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        checkNationalId :(checkForUniqueKey,cb)=>{
            if(payloadData.step ===1){
                let criteria = {
                    nationalId: payloadData.nationalId,
                    isDeleted: false,
                };
                Service.MaidServices.getMaid(criteria, {}, {lean: true}, function (err, result) {
                    if (err) {cb(err)}
                    else {
                        if (result.length) {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NATIONAL_ID_EXIST)
                        } else {
                          cb()
                        }
                    }
                })
            }
            else cb()
        },
        checkMaidAvailableForUpdate: function (checkNationalId, cb) {
            let criteria = {
                _id: payloadData.maidId,
                isDeleted: false
            };

            Service.MaidServices.getMaid(criteria, {}, {lean: true}, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if (result && result.length) {
                        commission = result[0].commission;
                        cb(null)
                    } else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DATA_NOT_FOUND)
                    }
                }
            })
        },
        updateMaidInfo: function (checkMaidAvailableForUpdate, cb) {
            let criteria = {
                _id: payloadData.maidId,
                isDeleted: false
            };

            let dataToUpdate = {};
            if(payloadData.languages)
                payloadData.languages.map((obj)=>{
                 return mongoose.Types.ObjectId(obj)
            });

            if (payloadData.step === 1) {
                dataToUpdate.profilePicURL = payloadData.profilePicURL;
                dataToUpdate.dob = payloadData.dob;
                dataToUpdate.gender = payloadData.gender;
                dataToUpdate.nationality = payloadData.nationality;
                dataToUpdate.religion = payloadData.religion;
                dataToUpdate.nationalId = payloadData.nationalId;
                dataToUpdate.documentPicURL = payloadData.documentPicURL;
                dataToUpdate.experience = parseInt(payloadData.experience);
               // dataToUpdate.languages = payloadData.languages;
                dataToUpdate.description = payloadData.description;
                dataToUpdate.$addToSet ={languages :{$each:payloadData.languages}}
                dataToUpdate.price = payloadData.price;
                // dataToUpdate.currency = payloadData.currency;
                dataToUpdate.actualPrice = (payloadData.price) + (payloadData.price) * (commission / 100);
                dataToUpdate.makPrice = (payloadData.price) * (commission / 100);
                dataToUpdate.step = 1;
                dataToUpdate.maidAppStep = 1;
            }

            else if (payloadData.step === 2) {
                let localCurency = getCountry(payloadData.maidAddress.countryName);

                if (localCurency && localCurency.currency) {
                    dataToUpdate.currency = localCurency.currency;
                }
                dataToUpdate.currentLocation = [payloadData.long, payloadData.lat];
                dataToUpdate.locationName = payloadData.locationName;
                dataToUpdate.radius = payloadData.radius;

                dataToUpdate.countryName = payloadData.maidAddress.countryName;
                dataToUpdate.maidAddress = payloadData.maidAddress;
                dataToUpdate.step = 2;
                dataToUpdate.maidAppStep = 2;
            }

            else if (payloadData.step === 3) {
                if (payloadData.timeSlot) {
                    dataToUpdate.$set = {
                        timeSlot: payloadData.timeSlot
                    }
                }
                dataToUpdate.step = 3;
                dataToUpdate.maidAppStep = 3;
            }


            else if (!payloadData.step) {

                if (payloadData.timeSlot) {
                    dataToUpdate.$set = {
                        timeSlot: payloadData.timeSlot
                    }
                }

                if (payloadData.firstName) {
                    dataToUpdate.firstName = payloadData.firstName;
                }

                if (payloadData.lastName) {
                    dataToUpdate.lastName = payloadData.lastName;
                }

                if(payloadData.firstName && payloadData.lastName){
                    dataToUpdate.fullName = payloadData.firstName + ' ' + payloadData.lastName;
                }

                if (payloadData.email) {
                    dataToUpdate.email = payloadData.email;
                }

                if (payloadData.countryENCode && payloadData.countryCode && payloadData.phoneNo) {
                    dataToUpdate.countryENCode = payloadData.countryENCode;
                    dataToUpdate.countryCode = payloadData.countryCode;
                    dataToUpdate.phoneNo = payloadData.phoneNo;
                }

                if (payloadData.profilePicURL) {
                    dataToUpdate.profilePicURL = payloadData.profilePicURL;
                }

                if (payloadData.dob) {
                    dataToUpdate.dob = payloadData.dob;
                }

                if (payloadData.gender) {
                    dataToUpdate.gender = payloadData.gender;
                }

                if (payloadData.nationality) {
                    dataToUpdate.nationality = payloadData.nationality;
                }

                if (payloadData.religion) {
                    dataToUpdate.religion = payloadData.religion;
                }

                if (payloadData.nationalId) {
                    dataToUpdate.nationalId = payloadData.nationalId;
                }

                if (payloadData.documentPicURL) {
                    dataToUpdate.documentPicURL = payloadData.documentPicURL;
                }

                if (payloadData.experience) {
                    dataToUpdate.experience = parseInt(payloadData.experience);
                }

                if (payloadData.languages) {
                    dataToUpdate.$addToSet ={languages :{$each:payloadData.languages}}
                    //dataToUpdate.languages = payloadData.languages;
                }

                if (payloadData.description) {
                    dataToUpdate.description = payloadData.description;
                }

                if (payloadData.price) {
                    dataToUpdate.price = payloadData.price;
                    dataToUpdate.actualPrice = (payloadData.price) + (payloadData.price) * (commission / 100);
                    dataToUpdate.makPrice = (payloadData.price) * (commission / 100);
                }

            }

            else {
                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
            }

            if (payloadData.deviceToken) {
                dataToUpdate.deviceToken = payloadData.deviceToken
            }
            if (payloadData.deviceType) {
                dataToUpdate.deviceType = payloadData.deviceType
            }

            let option = {new: true};
            Service.MaidServices.updateMaid(criteria, dataToUpdate, option, function (err, data) {
                if (err) {
                    cb(err)
                }
                else {
                    if (data && data._id) {
                        Models.Agency.populate(data, [
                            {
                                path: 'agencyId',
                                select: '_id agencyName',
                                model: 'Agency'
                            },
                            {
                                path: 'languages',
                                select: '_id languageName',
                                model: 'Languages'
                            }
                        ], function (err, populatedTransactions) {
                            if (err) {
                                cb(err)
                            }
                            else {
                                if (populatedTransactions.experience === 2) {
                                    populatedTransactions.experience = 'Up to 2 years';
                                }
                                else if (populatedTransactions.experience === 5) {
                                    populatedTransactions.experience = '3 to 5 years';
                                }
                                else if (populatedTransactions.experience === 6) {
                                    populatedTransactions.experience = 'Above 5 years';
                                }
                                response = populatedTransactions;
                                cb(null)
                            }
                        })
                    } else {
                        cb(null)
                    }
                }
            })
        },

    }, function (err, result) {
        callback(err, response)
    })
};

const acceptAgreements = function (payloadData, callback) {
    let maidData = {};
    let content;
    let agencyEmail=""
    async.autoInject({
        updateMaidInfo: function (cb) {
            let criteria = {
                email: payloadData.email,
                //isVerified: false,
                isDeleted: false
            };

            let dataToUpdate = {};

            if(payloadData.action === "ACCEPT"){
                dataToUpdate.acceptAgreement=true;
            }

            if(payloadData.action === "REJECT"){
                dataToUpdate.acceptAgreement=false;
                dataToUpdate.rejectReasonForAggrementByMaid=payloadData.rejectReasonForAggrementByMaid;
            }


            let option = {new: true};
            Service.MaidServices.updateMaid(criteria, dataToUpdate, option, function (err, data) {
                if (err) {
                    cb(err)
                }
                else {
                    if (data && data._id) {
                        maidData = data;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getAgencyData: function (updateMaidInfo, cb) {
            let criteria = {
                _id: maidData.agencyId
            };
            let project = {};
            let option = {lean: true};
            Service.AgencyServices.getAgency(criteria, project, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        agencyEmail = result[0].email
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        createNotificationForAgency: function (getAgencyData, cb) {
            let dataToSet = {
                senderId: maidData._id,
                receiverId: maidData.agencyId,
                maidId: maidData._id,
                type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.NEW_MAID,
                serviceId: maidData._id,
                bookingId: 0,
                agencyMessage: maidData.firstName + " has registered in your agency with maid registration ID : "+maidData.makId
            };
            Service.NotificationServices.createNotification(dataToSet, function (err, result) {
                if (err) {
                    callback(err)
                } else {
                    cb(null)
                }
            })
        },

        emailTemplate:function(createNotificationForAgency,cb){
            let data={};
            data.password=maidData.makId;
            data.fullName =  maidData.firstName + " " + maidData.lastName;

            emailTemplateNewRegistration(data,(err,result)=>{
                if(err){
                    cb(null)
                }
                else{
                    content=result;
                    cb(null)
                }
            })

        },

        sendMailToAgency: function (emailTemplate, cb) {
            let subject = "New Maid Registration";
           // let content = "Congrats, a new maid has registered with your agency, with maid registration ID : "+maidData.makId;

            MailManager.sendMailBySES(agencyEmail, subject, content, function (err, res) {
            });
            cb()
        }

    }, function (err, result) {
        callback(err, {})
    })
};

const updateDeviceTokenMaid = function (payloadData, userData, callback) {
    let responseObject = {};
    let dataToUpdate = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        updateUser: function (checkForUniqueKey, cb) {
            if (payloadData.deviceToken) {
                dataToUpdate.deviceToken = payloadData.deviceToken
            } else if (payloadData.deviceType) {
                dataToUpdate.deviceType = payloadData.deviceType

            } else if (payloadData.deviceToken && payloadData.deviceType) {
                dataToUpdate.deviceToken = payloadData.deviceToken
                dataToUpdate.deviceType = payloadData.deviceType
            }
            let criteria = {
                _id: userData._id,
                isDeleted: false,
            };
            let option = {new: true};
            Service.MaidServices.updateMaid(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    responseObject = result;
                    cb(null)
                }
            })
        }
    }, function (err, result) {
        callback(err, responseObject)
    })
};

const changeLanguageMaid = function (payloadData, userData, callback) {
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        setLanguageList: function (checkForUniqueKey, cb) {

            let criteria = {
                _id: userData._id,
            };

            let dataToUpdate = {
                $set: {
                    appLanguage: payloadData.language
                }
            };
            let option = {
                lean: true,
                new: true
            };
            Service.MaidServices.updateMaid(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        },

    }, function (err, result) {
        callback(err, {})
    });
};

const maidLogin = function (payloadData, callback) {

    let responseObject = {};
    let userData = {};
    let accessTokenUpdated = null;
    let password = UniversalFunctions.CryptData(payloadData.password);

    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkUserExist: function (checkForUniqueKey, cb) {
            let criteria = {
                email: payloadData.email,
            };
            let projections = {};
            let options = {
                lean: true
            };
            Service.MaidServices.getMaid(criteria, projections, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        if (data[0].rejectReason!=='') {
                            if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.REJECT)
                            else  callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.REJECT)
                            // callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.BLOCK_USER)
                        }
                        else if (data[0].isBlocked) {
                            if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.BLOCK_USER)
                            else  callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.BLOCK_USER_AR)
                           // callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.BLOCK_USER)
                        }
                        else if (data[0].isDeleted) {
                            if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_DELETED);
                            else    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_DELETED_AR);
                        }
                        else if (!data[0].isVerified) {
                            if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_VERIFIED);
                            else  callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_VERIFIED_AR);
                        }
                        else if (data[0].isVerified && !data[0].acceptAgreement && (data[0].step === 3) && (data[0].maidAppStep == 3)) {

                            if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.AGGREMENT_NOT_ACCEPTED);
                            else  callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.AGGREMENT_NOT_ACCEPTED_AR);
                        }
                        else if (!data[0].isBlocked && !data[0].isDeleted) {
                            if (data[0].password === password || data[0].passwordResetToken === password) {
                                userData = data;
                                cb(null)
                            }
                            else {
                                if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_PASSWORD);
                                else  callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_PASSWORD_AR);
                            }
                        }
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_REGISTERED);
                    }
                }
            })
        },
        generateTokenAndUpdate: function (checkUserExist, cb) {
            let tokenData = {
                id: userData[0]._id,
                type: 'MAID'
            };
            TokenManager.setToken(tokenData, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    accessTokenUpdated = data.accessToken;
                    cb(null);
                }
            })
        },
        updateUserForToken: function (generateTokenAndUpdate, cb) {
            let criteria = {
                _id: userData[0]._id,
                isDeleted: false,
            };
            let setQuery = {
                isLogin: true,
                timeZone: payloadData.timeZone,
            };
            if (payloadData.deviceToken) {
                setQuery.deviceToken = payloadData.deviceToken
            }
            if (payloadData.deviceType) {
                setQuery.deviceType = payloadData.deviceType
            }
            Service.MaidServices.updateMaid(criteria, setQuery, {new: true, lean: true}, function (err, data) {
                if (err) {
                    cb(err)
                } else {
                    if (data && data._id) {
                        Models.Agency.populate(data, [
                            {
                                path: 'agencyId',
                                select: '_id agencyName',
                                model: 'Agency'
                            },
                            {
                                path: 'languages',
                                select: '_id languageName',
                                model: 'Languages'
                            }
                        ], function (err, populatedTransactions) {
                            if (err) {
                                cb(err)
                            }
                            else {
                                if (populatedTransactions.experience == 2) {
                                    populatedTransactions.experience = 'Up to 2 years';
                                }
                                else if (populatedTransactions.experience == 5) {
                                    populatedTransactions.experience = '3 to 5 years';
                                }
                                else if (populatedTransactions.experience == 6) {
                                    populatedTransactions.experience = 'Above 5 years';
                                }
                                if (populatedTransactions.step == 3 && populatedTransactions.maidAppStep == 2) {
                                    populatedTransactions.step = 2;
                                    responseObject = populatedTransactions;
                                } else {
                                    responseObject = populatedTransactions;
                                }
                                cb(null)
                            }
                        })
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            });
        }

    }, function (err, result) {
        callback(err, responseObject)
    })
};

const listMakAgencyForMaid = function (payloadData, callback) {
    let response = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getAgencyData: function (checkForUniqueKey, cb) {
            let criteria = {
                agencyType: {$nin: [Config.APP_CONSTANTS.DATABASE.AGENCY_TYPE.NORMAL]}
            };
            let project = {};
            let option = {lean: true};
            Service.AgencyServices.getAgency(criteria, project, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },
    }, function (err, result) {
        callback(err, response)
    })
};

const listAllServiceMaid = function (payloadData, userData, callback) {

    let response = {
        data: [],
        review: []
    };
    let offset = momentTz.tz(payloadData.timeZone).utcOffset() * 60 * 1000;

    let onGoingServiceUserId = "";
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        listServices: function (checkForUniqueKey, cb) {
            let criteria = {
                deleteAction: {$ne: Config.APP_CONSTANTS.DATABASE.ACTION.DELETED},
                deleteRequestByUser: false,
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ],
                maidId: userData._id,
            };
            let option = {
                lean: true,
                skip: (((payloadData.pageNo) - 1) * payloadData.limit),
                limit: payloadData.limit,
            };

            if (payloadData.onBasisOfDate === "ON_GOING") {
                criteria.$or = [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT}
                        ]
                    }
                ];
                criteria.startTime = {$lte: +new Date()};
                criteria.endTime = {$gte: +new Date()};
                criteria.isCompleted = false;
                option.sort = {startTime:1}
            }
            else if (payloadData.onBasisOfDate === "UPCOMING") {
                criteria.$or = [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT]}}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT]}}
                        ]
                    }
                ];
                //criteria.workDate = parseInt(payloadData.workDate);
                criteria.startTime = {$gte: +new Date()};
                criteria.date = moment.tz(parseInt(payloadData.workDate),payloadData.timeZone).get('date');
                criteria.isCompleted = false;
                option.sort = {startTime:1}

            }
            else if (payloadData.onBasisOfDate === "COMPLETED") {
                criteria.agencyAction = Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT;
                criteria.isCompleted = true;
                option.sort = {startTime:-1}
            }

            let projection = {};

            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName makId currency',
                    option: {}
                }
            ];
            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        if (payloadData.onBasisOfDate === "ON_GOING") {
                            onGoingServiceUserId = result[0].userId._id;
                            calculateDistance(result, userData, function (err1, result1) {
                                response.data = result1;
                                cb(null)
                            })
                        }
                        if (payloadData.onBasisOfDate === "UPCOMING") {

                            async.each(result, function (obj, cb1) {
                                obj.bookingTime = moment.tz(obj.startTime,obj.timeZone).format("hh:mm a")
                                listCummulativeReviewOfUsers(obj.userId._id, function (e, r) {
                                    if (r && r.length) {
                                        obj.userReviewData = r[0];
                                        cb1(null)
                                    } else {
                                        obj.userReviewData = {
                                            avgUserRating: 0,
                                            userId: {},
                                            reviewData: []
                                        };
                                        cb1(null)
                                    }
                                })
                            }, function (e1, r1) {
                                response.data = result;
                                cb(null)
                            })
                        }
                        if (payloadData.onBasisOfDate === "COMPLETED") {
                            calculateDistance(result, userData, function (err1, result1) {
                                response.data = result1;
                                cb(null)
                            })
                        }
                    } else {
                        response.data = [];
                        cb(null)
                    }

                }
            })
        },

        getUsersRating: function (listServices, cb) {
            if (payloadData.onBasisOfDate === "ON_GOING" && onGoingServiceUserId) {
                Models.Review.aggregate([
                    {
                        $match: {
                            userId: mongoose.Types.ObjectId(onGoingServiceUserId),
                            isDeleted: false,
                            isBlocked: false,
                            reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.MAID
                        }
                    },
                    {
                        $project: {
                            _id: 1,
                            userId: 1,
                            maidId: 1,
                            serviceId: 1,
                            reviewByType: 1,
                            userRating: 1,
                            feedBack: 1,
                            createdOn: 1,
                            timeStamp: 1,
                        }
                    },
                    {$skip: ((payloadData.pageNo - 1) * payloadData.limit)},
                    {$limit: payloadData.limit}
                ], function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            Models.Maids.populate(result, [{
                                path: 'maidId',
                                select: '_id firstName lastName',
                                model: 'Maids'
                            },
                                {
                                    path: 'userId',
                                    select: '_id fullName',
                                    model: 'Users'
                                }
                            ], function (err, populatedTransactions) {
                                if (err) {
                                    cb(err)
                                }
                                else {
                                    response.review = populatedTransactions;
                                    cb(null)
                                }
                            })
                        } else {
                            cb(null)
                        }
                    }
                });
            } else {
                response.review = [];
                cb(null)
            }
        },
    }, function (err, result) {
        callback(err, response)
    });
};

const calculateDistance = function (payloadData, userData, callback) {
    if (payloadData && payloadData.length) {
        for (let i = 0; i < payloadData.length; i++) {
            (function (i) {
                payloadData[i].bookingTime = moment.tz(payloadData[i].startTime,payloadData[i].timeZone).format("hh:mm a")
                let distance = geolib.getDistance(
                    {latitude: userData.currentLocation[1], longitude: userData.currentLocation[0]},
                    {latitude: payloadData[i].bookingLocation[1], longitude: payloadData[i].bookingLocation[0]}
                );
                payloadData[i].distance = Math.round((distance / (1000 * 1.60934)) * 100) / 100;        ///in miles

                if (i == payloadData.length - 1) {
                    callback(null, payloadData)
                }
            }(i))
        }
    } else {
        callback(null, [])
    }
};

const listAllReviewsMaid = function (payloadData, userData, callback) {

    let response = [];

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMaidRating: function (checkForUniqueKey, cb) {
            Models.Review.aggregate([
                {
                    $match: {
                        maidId: mongoose.Types.ObjectId(userData._id),
                        isDeleted: false,
                        isBlocked: false,
                        reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER
                    }
                },
                {$skip: ((payloadData.pageNo - 1) * payloadData.limit)},
                {$limit: payloadData.limit}
            ], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        Models.Users.populate(result, [
                            {
                                path: 'userId',
                                select: '_id fullName',
                                model: 'Users'
                            },
                            {
                                path: 'maidId',
                                select: '_id firstName lastName',
                                model: 'Maids'
                            },
                        ], function (err, populatedTransactions) {
                            if (err) {
                                cb(err)
                            }
                            else {
                                response = populatedTransactions;
                                cb(null)
                            }
                        })
                    } else {
                        cb(null)
                    }
                }
            });
        },
    }, function (err, result) {
        callback(err, response)
    });
};

const wallet = function (payloadData, userData, callback) {

    let response = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        listAllCompletedServices: function (checkForUniqueKey, cb) {
            let pipeline = [];

            if (payloadData.dayFilter === "today") {
                let match = {
                    $match: {
                        maidId: mongoose.Types.ObjectId(userData._id),
                        $and: [
                            {transactionId: {$exists: true}},
                            {transactionId: {$nin: ["", null]}}
                        ],
                        workDate: {
                            $gte: parseInt(new Date().setHours(0, 0, 0, 0)),
                            $lte: (parseInt(new Date().setHours(0, 0, 0, 0)) + (24 * 60 * 60 * 1000))
                        },
                        agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                        deleteAction: {$ne: Config.APP_CONSTANTS.DATABASE.ACTION.DELETED},
                        isCompleted: true,
                    }
                };
                pipeline.push(match)
            }
            else if (payloadData.dayFilter === "weekly") {
                let curr = new Date;
                let first = curr.getDate() - curr.getDay();
                let last = first + 6;
                let fweekDay = new Date(curr.setDate(first + 1)).setUTCHours(0, 0, 0, 0);
                let lweekDay = new Date(curr.setDate(last + 1)).setUTCHours(0, 0, 0, 0);
                let match = {
                    $match: {
                        maidId: mongoose.Types.ObjectId(userData._id),
                        workDate: {
                            $gte: fweekDay, $lte: lweekDay
                        },
                        agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                        deleteAction: {$ne: Config.APP_CONSTANTS.DATABASE.ACTION.DELETED},
                        isCompleted: true,
                    }
                };
                pipeline.push(match)
            }
            else if (payloadData.dayFilter === "monthly") {
                let startOfMonth = moment(moment().startOf('month').format()).tz(payloadData.timeZone).unix() * 1000;
                let endOfMonth = moment(moment().endOf('month').format()).tz(payloadData.timeZone).unix() * 1000;
                // console.log("____________abc___________",startOfMonth,endOfMonth)

                let match = {
                    $match: {
                        maidId: mongoose.Types.ObjectId(userData._id),
                        workDate: {
                            $gte: startOfMonth, $lte: endOfMonth
                        },
                        agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                        deleteAction: {$ne: Config.APP_CONSTANTS.DATABASE.ACTION.DELETED},
                        isCompleted: true,
                    }
                };
                pipeline.push(match)
            }
            if(payloadData.pageNo) pipeline.push({$skip:(payloadData.pageNo -1) * 10})
            if(payloadData.limit) pipeline.push({$limit:payloadData.limit})
            let pipeline2 = [

                {
                    $group: {
                        _id: {
                            maidId: "$maidId",
                        },
                        totalDuration: {$sum: "$duration"},
                        totalAmount: {$sum: {$multiply:['$price','$duration']}},
                        serviceData: {$push: "$$ROOT"}
                    }
                },
                {
                    $project: {
                        totalDuration: 1,
                        totalAmount: 1,
                        serviceData: 1,
                        _id: 0
                    }
                },
            ];
            console.log("___________pipeline.concat(pipeline2)_________", pipeline.concat(pipeline2))

            Models.Service.aggregate(pipeline.concat(pipeline2), function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        Models.Users.populate(result[0].serviceData, [{
                            path: 'userId',
                            select: '_id fullName',
                            model: 'Users'
                        }], function (err, populatedTransactions) {
                            if (err) {
                                cb(err)
                            }
                            else {
                                response = result[0];
                                console.log("____2_______today__________")

                                cb(null)
                            }
                        })
                    } else {
                        response.totalDuration = 0;
                        response.totalAmount = 0;
                        response.serviceData = [];
                        cb(null)
                    }
                }
            })
        }

    }, function (err, result) {
        console.log("__3_________today__________")

        callback(err, response)
    })
};

const addReviewForUser = function (payloadData, userData, callback) {

    let userId = "";
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        getForDuplicateReview: function (checkForUniqueKey, cb) {
            let criteria = {
                serviceId: payloadData.serviceId,
                reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.MAID,
                maidId: userData._id,
                isDeleted: false
            };
            let projection = {};
            let option = {
                lean: true
            };

            Service.ReviewServices.getReview(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        if(userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_CANNOT_REVIEW_AGAIN)
                        else  callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_CANNOT_REVIEW_AGAIN_AR)
                    }
                    else {
                        cb(null)
                    }
                }
            })
        },
        getServiceDetails: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.serviceId,
                maidId: userData._id
            };
            let projection = {};
            let option = {
                lean: true
            };

            Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        if (result[0].maidId.toString() == userData._id.toString()) {
                            userId = result[0].userId;
                            cb(null)
                        } else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
                        }
                    }
                    else {
                        cb(null)
                    }
                }
            })
        },
        createReviewByUser: function (getServiceDetails, cb) {
            let dataToSave = {
                uniquieAppKey: UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME),
                maidId: userData._id,
                userId: userId,
                serviceId: payloadData.serviceId,
                userRating: payloadData.userRating || 0,
                feedBack: payloadData.feedBack || "",
                reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.MAID,
                createdOn: +new Date()
            };
            Service.ReviewServices.createReview(dataToSave, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        },
        updateNotification:function (createReviewByUser,cb) {
            let criteria = {
                serviceId: payloadData.serviceId,
                type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_COMPLETE,
                receiverId: userData._id
            };
            let dataToUp = {
                maidReviewSubmitted: true
            };
            Service.NotificationServices.updateNotification(criteria, dataToUp, {new: true}, (err) => {
                if (err) {
                    cb(err)
                } else cb()
            });
        }
    }, function (err, result) {
        callback(err, {})
    })
};

const listCummulativeReviewOfUsers = function (userId, callback) {
    let response = [];
    async.autoInject({
        getUsersReviews: function (cb) {
            Models.Review.aggregate([
                {
                    $match: {
                        userId: mongoose.Types.ObjectId(userId),
                        isDeleted: false,
                        isBlocked: false,
                        reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.MAID
                    }
                },
                {
                    $group: {
                        _id: "$userId",
                        userRating: {$sum: "$userRating"},
                        userRatingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$userRating", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        reviewData: {$push: "$$ROOT"}
                    }
                },
                {
                    $project: {
                        _id: 0,
                        userId: "$_id",
                        avgUserRating: {$cond: [{$eq: ["$userRatingCount", 0]}, 0, {$divide: ["$userRating", "$userRatingCount"]}]},
                        reviewData: 1,
                    }
                },

            ], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        Models.Maids.populate(result, [{
                            path: 'reviewData.maidId',
                            select: '_id firstName lastName',
                            model: 'Maids'
                        },
                            {
                                path: 'reviewData.userId',
                                select: '_id fullName',
                                model: 'Users'
                            }
                        ], function (err, populatedTransactions) {
                            if (err) {cb(err)
                            }
                            else {
                                response = populatedTransactions;
                                cb(null)
                            }
                        })
                    } else {
                        cb(null)
                    }
                }
            });
        }
    }, function (err, result) {
        callback(err, response)
    })
};

const startService = function (payloadData, userData, callback) {
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        updateServiceForStartTime: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.serviceId,
                maidId: userData._id,
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ]
            };

            let dataToUpdate = {
                serviceStartByMaid: +new Date(),
                isStart: true
            };

            Service.ServiceServices.updateService(criteria, dataToUpdate, {new: true}, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result) {
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }
    }, function (err, result) {
        callback(err, {})
    })
};

const createChat = function (payloadData, userData, callback) {
    let response = [];
    let notificationData = {};

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        createChatInDb: function (checkForUniqueKey, cb) {

            var dataToSave = {};
            dataToSave.senderType = payloadData.senderType;
            dataToSave.senderId = userData._id;
            dataToSave.receiverId = payloadData.receiverId;
            dataToSave.serviceId = payloadData.serviceId;
            dataToSave.conversationId = [userData._id, payloadData.receiverId].sort().join('.');
            dataToSave.messageType = payloadData.messageType;
            dataToSave.timeStamp = new Date();
            dataToSave.isRead = [userData._id];

            if (payloadData.lat && payloadData.lng && payloadData.messageType == "LOCATION") {
                dataToSave.location = [payloadData.lng, payloadData.lat];
                dataToSave.locationName = payloadData.locationName || "";
            }
            if (payloadData.message && payloadData.messageType == "MESSAGE") {
                dataToSave.message = payloadData.message;
            }

            Service.ChatServices.createChat(dataToSave, function (err, res) {
                if (err) {
                    cb(err)
                } else {
                    response = res;
                    cb(null)
                }
            })
        },

        getRecieverData: function (createChatInDb, cb) {
            if (payloadData.senderType == "USER") {
                notificationData.fullName = userData.fullName;

                Service.MaidServices.getMaid({_id: payloadData.receiverId}, {}, {lean: true}, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            notificationData.receiverName = result[0].firstName;
                            notificationData.deviceToken = result[0].deviceToken;
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })

            } else if (payloadData.senderType == "MAID") {
                notificationData.fullName = userData.firstName + " " + userData.lastName;

                Service.UserServices.getUsers({_id: payloadData.receiverId}, {}, {lean: true}, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            notificationData.receiverName = result[0].fullName;
                            notificationData.deviceToken = result[0].deviceToken;
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },
        sendPushForChat: function (getRecieverData, cb) {
            notificationData.title = 'M.A.K.';
            if (payloadData.senderType === "USER") {
                notificationData.body = userData.fullName + " sent you message.";
                notificationData.senderType = "USER";
            }
            if (payloadData.senderType === "MAID") {
                notificationData.body = userData.firstName + " sent you message.";
                notificationData.senderType = "MAID";
            }
            notificationData.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.MESSAGE;
            notificationData.serviceId = payloadData.serviceId;
            notificationData.senderId = userData._id;
            notificationData.receiverId = payloadData.receiverId;
            if (payloadData.message) {
                notificationData.message = payloadData.message;
            } else {
                notificationData.message = "";
            }
            if (payloadData.lat && payloadData.lng) {
                notificationData.location = [payloadData.lng, payloadData.lat];
            } else {
                notificationData.location = [];
            }

            sendNotificationForChat(notificationData, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        }

    }, function (err, result) {
        callback(err, response)
    })
};

const getAllChat = function (payloadData, userData, callback) {
    let chatData = [];
    let response = [];

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getChat: function (checkForUniqueKey, cb) {
            if(userData._id){
                Models.Chat.aggregate([
                    {
                        $match: {
                            $or: [{senderId: userData._id.toString()}, {receiverId: userData._id.toString()}],
                            isExpired: false
                        }
                    },
                    {
                        $group: {
                            _id: '$conversationId',
                            senderId: {$last: '$senderId'},
                            receiverId: {$last: '$receiverId'},
                            serviceId: {$last: '$serviceId'},
                            senderType: {$last: '$senderType'},
                            messageType: {$last: '$messageType'},
                            message: {$last: '$message'},
                            location: {$last: '$location'},
                            locationName: {$last: '$locationName'},
                            timeStamp: {$last: '$timeStamp'},
                        }
                    },
                    {$sort :{timeStamp:-1}}
                ], function (err, res) {
                    chatData = res;
                    cb(null)
                })
            }
            else {
                cb(null)
            }

        },

        addUnreadCount: function (getChat, cb) {
            if(chatData.length){
                async.each(chatData, function (obj, cb1) {
                    let criteria = {
                        receiverId: userData._id,
                        senderId: obj.senderId,
                        isRead: {$nin: [userData._id]},
                        isExpired: false
                    };

                    let option = {lean: true};

                    Service.ChatServices.getChat(criteria, {}, option, function (err, result) {
                        if (err) {
                            cb1(err)
                        } else {
                            if (result && result.length) {
                                obj.unreadCount = result.length;
                                cb1(null)
                            } else {
                                obj.unreadCount = 0;
                                cb1(null)
                            }
                        }
                    })

                }, function (err, result) {
                    cb(null)
                })
            }
            else cb()

        },

        getOtherUserData: function (addUnreadCount, cb) {
            if(chatData.length){
                async.each(chatData, function (obj, cb1) {
                    let populateArray = [];
                    if (obj.senderId == userData._id && payloadData.userType == "MAID") {
                        populateArray.push({
                            path: 'receiverId',
                            select: '_id fullName',
                            model: 'Users'
                        })
                    }
                    else if (obj.receiverId == userData._id && payloadData.userType == "MAID") {
                        populateArray.push({
                            path: 'senderId',
                            select: '_id fullName',
                            model: 'Users'
                        })
                    }
                    else if (obj.senderId == userData._id && payloadData.userType == "USER") {
                        populateArray.push({
                            path: 'receiverId',
                            select: '_id firstName lastName',
                            model: 'Maids'
                        })
                    }
                    else if (obj.receiverId == userData._id && payloadData.userType == "USER") {
                        populateArray.push({
                            path: 'senderId',
                            select: '_id firstName lastName',
                            model: 'Maids'
                        })
                    }
                    Models.Users.populate(obj, populateArray, function (err, populatedTransactions) {
                        if (err) {
                            cb(err)
                        }
                        else {
                            if (obj.senderId == userData._id && payloadData.userType == "MAID") {
                                populatedTransactions.userData = populatedTransactions.receiverId
                            }
                            else if (obj.receiverId == userData._id && payloadData.userType == "MAID") {
                                populatedTransactions.userData = populatedTransactions.senderId
                            }
                            else if (obj.senderId == userData._id && payloadData.userType == "USER") {
                                populatedTransactions.userData = populatedTransactions.receiverId
                            }
                            else if (obj.receiverId == userData._id && payloadData.userType == "USER") {
                                populatedTransactions.userData = populatedTransactions.senderId
                            }
                            response.push(populatedTransactions);
                            cb1(null)
                        }
                    })
                    /* let populateArray=[];
                     if((obj.senderType == "USER" && payloadData.userType == "USER") || (obj.senderType == "USER" && payloadData.userType == "MAID")){
                     populateArray=populateArray.concat([
                     {
                     path: 'senderId',
                     select: '_id fullName',
                     model: 'Users'
                     },
                     {
                     path: 'receiverId',
                     select: '_id firstName lastName',
                     model: 'Maids'
                     }
                     ])
                     }
                     else if((obj.senderType == "MAID" && payloadData.userType == "MAID") || (obj.senderType == "MAID" && payloadData.userType == "USER")){
                     populateArray=populateArray.concat([
                     {
                     path: 'receiverId',
                     select: '_id fullName',
                     model: 'Users'
                     },
                     {
                     path: 'senderId',
                     select: '_id firstName lastName',
                     model: 'Maids'
                     }
                     ])
                     }
                     Models.Agency.populate(obj, populateArray , function (err, populatedTransactions) {
                     if (err) {
                     cb(err)
                     }
                     else {
                     response.push(populatedTransactions);
                     cb1(null)
                     }
                     })*/
                }, function (err, result) {
                    cb(null)
                })
            }
            else cb()
        }

    }, function (err, result) {
        callback(err, chatData)
    })
};

const getChatHistory = function (payloadData, userData, callback) {
    let responseToSend = {};
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getChatData: function (checkForUniqueKey, cb) {
            let criteria = {
                senderId: {$in: [userData._id, payloadData.userId]},
                receiverId: {$in: [userData._id, payloadData.userId]},
                isDeleted: {$nin: [userData._id]},
                isExpired: false
            };
            /*if(payloadData.lastMessageCount == 0){

             }else{
             criteria.isRead= {$nin: [userData._id]}
             }*/

            let project = {
                __v: 0
            };

            let option = {
                lean: true,
                sort: {timeStamp: 1},
            }
            Service.ChatServices.getChat(criteria, project, option, function (err, res) {
                if (err) {
                    cb(err)
                } else {
                    // console.log("____________chateData_____________",res)
                    if (res && res.length) {

                        res.map(obj => {
                            obj.timeStamp = obj.timeStamp.getTime()
                        })
                        responseToSend.totalMsgCount = res.length;
                        responseToSend.latestMessage = res.slice(payloadData.lastMessageCount);
                        cb(null)
                    } else {
                        responseToSend.totalMsgCount = 0;
                        responseToSend.latestMessage = []
                        cb(null)
                    }
                }
            })
        },

        updateReadStatus: function (getChatData, cb) {
            let criteria = {
                senderId: {$in: [payloadData.userId]},
                receiverId: {$in: [userData._id]},
                // isExpired:false
                // isDeleted: {$nin: [userData._id]}
            };
            Service.ChatServices.updateAllChat(criteria, {$addToSet: {isRead: userData._id}}, {
                lean: true,
                new: true,
                multi: true
            }, function (err, res) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        }
    }, function (err, result) {
        callback(err, responseToSend)
    })
};

const listAllNotificationsMaids = function (payloadData, userData, callback) {
    let response = [];

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getNotification: function (checkForUniqueKey, cb) {

            let criteria = {
                $or: [
                    {receiverId: userData._id,},
                    {receiverArray: userData._id}
                ],
                isDeleted: {$nin: [userData._id]},
            };
            let projection = {};

            let option = {
                lean: true,
                sort: {'timeStamp': -1},
            };

            Service.NotificationServices.getNotification(criteria, projection, option, function (err, data) {
                if (err) {
                    cb(err)
                } else {
                    Models.Agency.populate(data, [
                        {
                            path: 'senderId',
                            select: '_id agencyName',
                            model: 'Agency'
                        },

                    ], function (err, populatedTransactions) {
                        if (err) {
                            cb(err)
                        }
                        else {
                            response = populatedTransactions;
                            cb(null)
                        }
                    })
                }
            })
        },
        updateNotificationToRead: function (getNotification, cb) {
            let criteria = {
                $or: [
                    {receiverId: userData._id,},
                    {receiverArray: userData._id}
                ],
                read: {$nin: [userData._id]}
            };
            let dataToSet = {
                $addToSet: {
                    read: userData._id
                }
            };
            let option = {
                new: true,
                multi: true
            };
            Service.NotificationServices.updateAllNotification(criteria, dataToSet, option, function (err, data) {
                if (err) {
                    callback(err)
                } else {
                    cb(null);
                }
            })
        },
    }, function (err, result) {
        callback(err, response)
    })
};

const sendNotificationForChat = function (notificationData, callback) {

    async.auto({
        sendPushFinally: function (cb) {

            if (!(notificationData.deviceToken == null)) {
                if (notificationData.deviceToken.length > 15) {
                    async.parallel([
                        function (cb) {
                            NotificationManager.sendPushInChat(notificationData, function (err, result) {
                                if (err) {
                                    cb(null)
                                } else {
                                    cb(null)
                                }
                            })
                        }
                    ], function (err, result) {
                        callback(err, result)
                    })
                } else {
                    callback()
                }
            } else {
                callback()
            }
        }
    }, function (err, res) {
        callback()
    })
};

const changePasswordMaid = function (payloadData, userData, callback) {
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkCurrentPasswordAndUpdate: function (checkForUniqueKey, cb) {
            if (UniversalFunctions.CryptData(payloadData.oldPassword) === userData.password) {
                let criteria = {
                    _id: userData._id,
                    isDeleted: false,
                    isBlocked: false,
                };
                let projection = {
                    password: UniversalFunctions.CryptData(payloadData.newPassword),
                };
                let option = {new: true};

                Service.MaidServices.updateMaid(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        callback(null, Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT)
                    }
                })
            } else {
                if(userData.appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASSWORD)
                else callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASSWORD_AR)}
        },

    }, function (err, result) {
        callback(err, {})
    })
};

const forgetPasswordMaid = function (payloadData, callback) {
    let userData = {};
    let passwordResetToken = "";
    let content;
    let response = {};
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        verifyEmail: function (checkForUniqueKey, cb) {
            let criteria = {
                email: payloadData.email
            };

            let projection = {};
            let option = {lean: true};
            Service.MaidServices.getMaid(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result && result.length) {
                        userData = result[0];
                        cb(null)
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_REGISTERED)
                    }
                }
            })
        },

        generatePasswordResetToken: function (verifyEmail, cb) {
            passwordResetToken = (otpGenerator.generate(6, {specialChars: false}));
            let criteria = {
                email: payloadData.email,
                isDeleted: false,
                //isDeactivated: false,
            };

            let projection = {
                $set: {
                    password: UniversalFunctions.CryptData(passwordResetToken.toLowerCase()),
                }
            };
            let option = {
                new: true
            };
            Service.MaidServices.updateMaid(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    response.passwordResetToken = passwordResetToken.toLowerCase();
                    cb(null)
                }
            })
        },
        
        emailTemplate:function(generatePasswordResetToken,cb){
            let data={};
            data.password = passwordResetToken.toLowerCase();
            data.fullName =  userData.firstName + " " + userData.lastName;
            
            emailTemplateForgetPassword(data,(err,result)=>{
                if(err){
                    cb(null)
                }
                else{
                    content=result;
                    cb(null)
                }
            })
            
        },

        sendMail: function (generatePasswordResetToken, cb) {
            if (payloadData.email && (payloadData.email == userData.email)) {
                var subject = "M.A.K Temporary Password";
                MailManager.sendMailBySES(payloadData.email, subject, content, function (err, res) {
                });
                cb()
            }
            else {
                cb(null)
            }
        },

    }, function (err, result) {
        callback(err, response)
    })
};

const emailTemplateNewRegistration =  function(data,callback){

    let content = `

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style type="text/css">
        
        body,
        #bodyTable {
            height: 100%!important;
            margin: 0;
            padding: 0;
            width: 100%!important
        }

        #bodyTable {
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            min-height: 100%;
            background: #eee;
            color: #333
        }

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0
        }

        img {
            -ms-interpolation-mode: bicubic
        }

        body {
            margin: 0;
            padding: 0;
            background: #eee
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: 0;
            text-decoration: none
        }

        table {
            border-collapse: collapse!important;
            max-width: 100%!important
        }

        img {
            border: 0!important
        }

        h1 {
            font-family: Helvetica;
            font-size: 42px;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            margin: 30px auto 0
        }

        h2 {
            font-size: 32px;
            font-style: normal;
            font-weight: bold;
            margin: 50px auto 0
        }

        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 25px 0;
            letter-spacing: normal;
            text-align: left
        }

        a h3 {
            color: #444!important;
            text-decoration: none!important
        }

        .titleLink {
            text-decoration: none!important
        }

        .preheaderContent {
            color: #808080;
            font-size: 10px;
            line-height: 125%
        }

        .preheaderContent a:link,
        .preheaderContent a:visited,
        .preheaderContent a .yshortcuts {
            color: #606060;
            font-weight: normal;
            text-decoration: underline
        }

        #emailHeader,
        #tacoTip {
            color: #fff
        }

        #emailHeader {
            background-color: #fff
        }

        #content p {
            color: #4d4d4d;
            margin: 20px 70px 30px;
            font-size: 24px;
            line-height: 32px;
            text-align: center
        }

        #button {
            display: inline-block;
            margin: 10px auto;
            background: #fff;
            border-radius: 4px;
            font-weight: bold;
            font-size: 18px;
            padding: 15px 20px;
            cursor: pointer;
            color: #0079bf;
            margin-bottom: 50px
        }

        #socialIconWrap img {
            line-height: 35px!important;
            padding: 0 5px
        }

        .footerContent div {
            color: #707070;
            font-family: Arial;
            font-size: 12px;
            line-height: 125%;
            text-align: center;
            max-width: 100%!important
        }

        .footerContent div a:link,
        .footerContent div a:visited {
            color: #369;
            font-weight: normal;
            text-decoration: underline
        }

        .footerContent img {
            display: inline
        }

        #socialLinks img {
            margin: 0 2px
        }

        #utility {
            border-top: 1px solid #ddd
        }

        #utility div {
            text-align: center
        }

        #monkeyRewards img {
            max-width: 160px
        }

        #emailFooter {
            max-width: 100%!important
        }

        #footerTwitter a,
        #footerFacebook a {
            text-decoration: none!important;
            color: #fff!important;
            font-size: 14px
        }

        #emailButton {
            border-radius: 6px;
            background: #70b500!important;
            margin: 0 auto 60px;
            box-shadow: 0 4px 0 #578c00
        }

        #socialLinks a {
            width: 40px
        }

        #socialLinks #blogLink {
            width: 80px!important
        }

        .sectionWrap {
            text-align: center
        }

        #header {
            color: #fff!important
        }

    </style>
</head>

<body>
    <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px">
                                        <tbody>
                                            <tr>
                                                <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="https://s3-us-west-2.amazonaws.com/mak-s3/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120">
                                                    <h1 style="font-size: 30px;">Welcome to MAK</h1>
                                                    <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="middle" style="background:#FFFFFF;">
                                                    <br>
                                                    Welcome <b>${data.fullName}</b>! <br>Congrats, a new maid has registered with your agency, with maid registration ID : <b>${data.password}</b> .
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                           
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px">
                                        <tbody>
                                            <tr>
                                                <td class="footerContent" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0" style="width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">&nbsp;
                                                                    <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>


    `
    callback(null,content)
};

const emailTemplateForgetPassword =  function(data,callback){

    let content = `
  
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style type="text/css">
        
        body,
        #bodyTable {
            height: 100%!important;
            margin: 0;
            padding: 0;
            width: 100%!important
        }

        #bodyTable {
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            min-height: 100%;
            background: #eee;
            color: #333
        }

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0
        }

        img {
            -ms-interpolation-mode: bicubic
        }

        body {
            margin: 0;
            padding: 0;
            background: #eee
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: 0;
            text-decoration: none
        }

        table {
            border-collapse: collapse!important;
            max-width: 100%!important
        }

        img {
            border: 0!important
        }

        h1 {
            font-family: Helvetica;
            font-size: 42px;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            margin: 30px auto 0
        }

        h2 {
            font-size: 32px;
            font-style: normal;
            font-weight: bold;
            margin: 50px auto 0
        }

        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 25px 0;
            letter-spacing: normal;
            text-align: left
        }

        a h3 {
            color: #444!important;
            text-decoration: none!important
        }

        .titleLink {
            text-decoration: none!important
        }

        .preheaderContent {
            color: #808080;
            font-size: 10px;
            line-height: 125%
        }

        .preheaderContent a:link,
        .preheaderContent a:visited,
        .preheaderContent a .yshortcuts {
            color: #606060;
            font-weight: normal;
            text-decoration: underline
        }

        #emailHeader,
        #tacoTip {
            color: #fff
        }

        #emailHeader {
            background-color: #fff
        }

        #content p {
            color: #4d4d4d;
            margin: 20px 70px 30px;
            font-size: 24px;
            line-height: 32px;
            text-align: center
        }

        #button {
            display: inline-block;
            margin: 10px auto;
            background: #fff;
            border-radius: 4px;
            font-weight: bold;
            font-size: 18px;
            padding: 15px 20px;
            cursor: pointer;
            color: #0079bf;
            margin-bottom: 50px
        }

        #socialIconWrap img {
            line-height: 35px!important;
            padding: 0 5px
        }

        .footerContent div {
            color: #707070;
            font-family: Arial;
            font-size: 12px;
            line-height: 125%;
            text-align: center;
            max-width: 100%!important
        }

        .footerContent div a:link,
        .footerContent div a:visited {
            color: #369;
            font-weight: normal;
            text-decoration: underline
        }

        .footerContent img {
            display: inline
        }

        #socialLinks img {
            margin: 0 2px
        }

        #utility {
            border-top: 1px solid #ddd
        }

        #utility div {
            text-align: center
        }

        #monkeyRewards img {
            max-width: 160px
        }

        #emailFooter {
            max-width: 100%!important
        }

        #footerTwitter a,
        #footerFacebook a {
            text-decoration: none!important;
            color: #fff!important;
            font-size: 14px
        }

        #emailButton {
            border-radius: 6px;
            background: #70b500!important;
            margin: 0 auto 60px;
            box-shadow: 0 4px 0 #578c00
        }

        #socialLinks a {
            width: 40px
        }

        #socialLinks #blogLink {
            width: 80px!important
        }

        .sectionWrap {
            text-align: center
        }

        #header {
            color: #fff!important
        }

    </style>
</head>

<body>
    <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px">
                                        <tbody>
                                            <tr>
                                                <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="https://s3-us-west-2.amazonaws.com/mak-s3/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120">
                                                    <h1 style="font-size: 30px;">Welcome to MAK</h1>
                                                    <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="middle" style="background:#FFFFFF;">
                                                    <br>
                                                    Welcome <b>${data.fullName}</b>! <br>Your temporary password is <b>${data.password}</b>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                           
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px">
                                        <tbody>
                                            <tr>
                                                <td class="footerContent" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0" style="width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">&nbsp;
                                                                    <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>


    `
    callback(null,content)
};

const emailTemplate =  function(data,callback){

    let content = `
    
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style type="text/css">
        
        body,
        #bodyTable {
            height: 100%!important;
            margin: 0;
            padding: 0;
            width: 100%!important
        }

        #bodyTable {
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            min-height: 100%;
            background: #eee;
            color: #333
        }

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0
        }

        img {
            -ms-interpolation-mode: bicubic
        }

        body {
            margin: 0;
            padding: 0;
            background: #eee
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: 0;
            text-decoration: none
        }

        table {
            border-collapse: collapse!important;
            max-width: 100%!important
        }

        img {
            border: 0!important
        }

        h1 {
            font-family: Helvetica;
            font-size: 42px;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            margin: 30px auto 0
        }

        h2 {
            font-size: 32px;
            font-style: normal;
            font-weight: bold;
            margin: 50px auto 0
        }

        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 25px 0;
            letter-spacing: normal;
            text-align: left
        }

        a h3 {
            color: #444!important;
            text-decoration: none!important
        }

        .titleLink {
            text-decoration: none!important
        }

        .preheaderContent {
            color: #808080;
            font-size: 10px;
            line-height: 125%
        }

        .preheaderContent a:link,
        .preheaderContent a:visited,
        .preheaderContent a .yshortcuts {
            color: #606060;
            font-weight: normal;
            text-decoration: underline
        }

        #emailHeader,
        #tacoTip {
            color: #fff
        }

        #emailHeader {
            background-color: #fff
        }

        #content p {
            color: #4d4d4d;
            margin: 20px 70px 30px;
            font-size: 24px;
            line-height: 32px;
            text-align: left
        }

        #button {
            display: inline-block;
            margin: 10px auto;
            background: #fff;
            border-radius: 4px;
            font-weight: bold;
            font-size: 18px;
            padding: 15px 20px;
            cursor: pointer;
            color: #0079bf;
            margin-bottom: 50px
        }

        #socialIconWrap img {
            line-height: 35px!important;
            padding: 0 5px
        }

        .footerContent div {
            color: #707070;
            font-family: Arial;
            font-size: 12px;
            line-height: 125%;
            text-align: center;
            max-width: 100%!important
        }

        .footerContent div a:link,
        .footerContent div a:visited {
            color: #369;
            font-weight: normal;
            text-decoration: underline
        }

        .footerContent img {
            display: inline
        }

        #socialLinks img {
            margin: 0 2px
        }

        #utility {
            border-top: 1px solid #ddd
        }

        #utility div {
            text-align: center
        }

        #monkeyRewards img {
            max-width: 160px
        }

        #emailFooter {
            max-width: 100%!important
        }

        #footerTwitter a,
        #footerFacebook a {
            text-decoration: none!important;
            color: #fff!important;
            font-size: 14px
        }

        #emailButton {
            border-radius: 6px;
            background: #70b500!important;
            margin: 0 auto 60px;
            box-shadow: 0 4px 0 #578c00
        }

        #socialLinks a {
            width: 40px
        }

        #socialLinks #blogLink {
            width: 80px!important
        }

        .sectionWrap {
            text-align: center
        }

        #header {
            color: #fff!important
        }

    </style>
</head>

<body>
    <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px">
                                        <tbody>
                                            <tr>
                                                <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="https://s3-us-west-2.amazonaws.com/mak-s3/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120">
                                                    <h1 style="font-size: 30px;">Welcome to MAK</h1>
                                                    <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="background:#FFFFFF;">
                                                    <br><br><p>
                                                    Dear <b>${data.firstName} ${data.lastName} </b>
                                                  <br>
                                                 
                                                  <br>Thank you for registering with MAK. Please verify your email address below.
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" bgcolor="#61BD4F" border="0" cellpadding="0" cellspacing="0" class="emailButton" id="button" style=" border-radius:6px;  display:inline-block;  margin:0px auto 60px; padding: 10px 14px;">
                                                    <div><span style="font-size:20px"><a href="${data.link}" style="background-color:#61BD4F;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:500; letter-spacing: 0.4px;  line-height:48px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;font-size:18px !important">Verify</a></span>&nbsp;&nbsp;&nbsp;
                                                        <!--[if mso]></center></v:rect><![endif]-->
                                                    </div>
                                                </td>
                                          </tr>
                                          <tr>
                                              <td align="left" style="background:#FFFFFF;"><p>Once the verification is fully completed, you will receive a confirmation email from the Agency along with the login details to log into the MAK Helper app.<br><br>

Please download our MAK Helper app using the below link.<br>
</p></td>
                                            </tr>
                                            <tr>
                                            <td align="center" bgcolor="#61BD4F" border="0" cellpadding="0" cellspacing="0" class="emailButton" id="button" style=" border-radius:6px;  display:inline-block;  margin:0px auto 60px; padding: 10px 14px;">
                                                    <div><span style="font-size:20px"><a href="${data.link}" style="color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:500; letter-spacing: 0.4px;  line-height:48px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;font-size:18px !important"><img src="https://s3-us-west-2.amazonaws.com/mak-s3/29793763_343336722842176_8545984603976892416_n.png"></a></span>&nbsp;&nbsp;&nbsp;<span style="font-size:20px"><a href="$\\\\{data.link}" style="color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:500; letter-spacing: 0.4px;  line-height:48px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;font-size:18px !important"><img src="https://s3-us-west-2.amazonaws.com/mak-s3/30124862_343336729508842_3973615652479959040_n.png"</a></span>&nbsp;
                                                        <!--[if mso]></center></v:rect><![endif]-->
                                                    </div>
                                                </td>
                                            </tr>
                                           <tr>
                                              <td align="left" style="background:#FFFFFF;">
                                                <p>Sincerely,<br>MAK Team
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px">
                                        <tbody>
                                            <tr>
                                                <td class="footerContent" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0" style="width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">&nbsp;
                                                                    <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

    `
    callback(null,content)
};

const emailTemplateIssueRaised =  function(data,callback){

    let content = `  
    
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style type="text/css">
        
        body,
        #bodyTable {
            height: 100%!important;
            margin: 0;
            padding: 0;
            width: 100%!important
        }

        #bodyTable {
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            min-height: 100%;
            background: #eee;
            color: #333
        }

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0
        }

        img {
            -ms-interpolation-mode: bicubic
        }

        body {
            margin: 0;
            padding: 0;
            background: #eee
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: 0;
            text-decoration: none
        }

        table {
            border-collapse: collapse!important;
            max-width: 100%!important
        }

        img {
            border: 0!important
        }

        h1 {
            font-family: Helvetica;
            font-size: 42px;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            margin: 30px auto 0
        }

        h2 {
            font-size: 32px;
            font-style: normal;
            font-weight: bold;
            margin: 50px auto 0
        }

        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 25px 0;
            letter-spacing: normal;
            text-align: left
        }

        a h3 {
            color: #444!important;
            text-decoration: none!important
        }

        .titleLink {
            text-decoration: none!important
        }

        .preheaderContent {
            color: #808080;
            font-size: 10px;
            line-height: 125%
        }

        .preheaderContent a:link,
        .preheaderContent a:visited,
        .preheaderContent a .yshortcuts {
            color: #606060;
            font-weight: normal;
            text-decoration: underline
        }

        #emailHeader,
        #tacoTip {
            color: #fff
        }

        #emailHeader {
            background-color: #fff
        }

        #content p {
            color: #4d4d4d;
            margin: 20px 70px 30px;
            font-size: 24px;
            line-height: 32px;
            text-align: center
        }

        #button {
            display: inline-block;
            margin: 10px auto;
            background: #fff;
            border-radius: 4px;
            font-weight: bold;
            font-size: 18px;
            padding: 15px 20px;
            cursor: pointer;
            color: #0079bf;
            margin-bottom: 50px
        }

        #socialIconWrap img {
            line-height: 35px!important;
            padding: 0 5px
        }

        .footerContent div {
            color: #707070;
            font-family: Arial;
            font-size: 12px;
            line-height: 125%;
            text-align: center;
            max-width: 100%!important
        }

        .footerContent div a:link,
        .footerContent div a:visited {
            color: #369;
            font-weight: normal;
            text-decoration: underline
        }

        .footerContent img {
            display: inline
        }

        #socialLinks img {
            margin: 0 2px
        }

        #utility {
            border-top: 1px solid #ddd
        }

        #utility div {
            text-align: center
        }

        #monkeyRewards img {
            max-width: 160px
        }

        #emailFooter {
            max-width: 100%!important
        }

        #footerTwitter a,
        #footerFacebook a {
            text-decoration: none!important;
            color: #fff!important;
            font-size: 14px
        }

        #emailButton {
            border-radius: 6px;
            background: #70b500!important;
            margin: 0 auto 60px;
            box-shadow: 0 4px 0 #578c00
        }

        #socialLinks a {
            width: 40px
        }

        #socialLinks #blogLink {
            width: 80px!important
        }

        .sectionWrap {
            text-align: center
        }

        #header {
            color: #fff!important
        }

    </style>
</head>

<body>
    <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px">
                                        <tbody>
                                            <tr>
                                                <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="http://52.36.127.111/mak_web/img/ic_logo_big.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120">
                                                    <h1 style="font-size: 30px;">Welcome to MAK</h1>
                                                    <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="background:#FFFFFF;">
                                                    <br>
                                                    Dear <b>${data.fullName}</b> <br>
                                                  <br>
                                                  Your ticket number for the issue raised  :${data.password}.<br><br>
                                                  
                                         We will get back to you shortly.<br><br>
                                                  
Yours Sincerely <br>
MAK Team.
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                           
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="bottom" style="text-align:center!important;background-repeat:no-repeat;background-size:600px 50px; background-position: center center;"><a href=""><img height="50" id="logo" src="http://52.36.127.111/mak_web/img/bannerLogo.png" style="margin:20px auto; opacity: 0.4;" width="172"></a></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px">
                                        <tbody>
                                            <tr>
                                                <td class="footerContent" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0" style="width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">&nbsp;
                                                                    <div mc:edit="std_footer"><em>Copyright © 2018 MAK, All rights reserved.</em></div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>


    `
    callback(null,content)
};

const maidLogout = function (payloadData, userData, callback) {
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        logout: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: userData._id,
                isDeleted: false,
            };
            let projection = {
                $unset: {
                    accessToken: 1,
                    deviceToken: 1,
                },
                appLanguage :   Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN
            };
            let option = {lean: true, new: true};
            Service.MaidServices.updateMaid(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    callback(null, Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.LOGOUT)
                }
            })
        }
    }, function (err, result) {
        callback(err, result)
    })
};

const raiseAnIssueMaid = function (payloadData, userData, callback) {
    let count;
    let content;
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        checkTicketNo:function (cb) {
            let criteria = {
                isDeleted:false
            };
            let projection = {};
            let option = {
                lean: true
            };
                criteria.userType="MAID";
            Service.IssueServices.getIssue(criteria,{},{},(err,result)=>{
                if(err){
                    cb(err)
                }
                else{
                    count= result.length+1;
                    cb(null);
                }
            })
        },
        createReviewUser: function (checkTicketNo, cb) {

            let dataToInsert = {
                ticketNo:count,
                userType: 'MAID',
                userId: userData._id,
                issueDescription: payloadData.issue,
                uniquieAppKey: UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)
            };

            Service.IssueServices.createIssue(dataToInsert, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result) {
                        cb(null)
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                    }
                }
            })
        },
        emailTemplate:function(createReviewUser,cb){
            let data={};
            data.password =  "'" + payloadData.issue + "'" + " is Ticket-" +count
            data.fullName =   userData.firstName + " " +userData.lastName;
         
            emailTemplateIssueRaised(data,(err,result)=>{
                if(err){
                    cb(null)
                }
                else{
                    content=result;
                    cb(null)
                }
            })
        },
        sendPass: function (emailTemplate, cb) {
                
                let subject = "Ticket number for issue";
                MailManager.sendMailBySES(userData.email, subject, content, function (err, res) {
                  //  console.log("============erre=====================",err,res)
                });
                cb()
        }
        
    }, function (err, result) {
        callback(err, {})
    })
};


module.exports = {

    maidSignup1: maidSignup1,
    updateMaidProfileInApp: updateMaidProfileInApp,
    acceptAgreements: acceptAgreements,
    updateDeviceTokenMaid: updateDeviceTokenMaid,
    changeLanguageMaid: changeLanguageMaid,
    maidLogin: maidLogin,
    listMakAgencyForMaid: listMakAgencyForMaid,
    listAllServiceMaid: listAllServiceMaid,
    listAllReviewsMaid: listAllReviewsMaid,
    wallet: wallet,
    addReviewForUser: addReviewForUser,
    startService: startService,
    createChat: createChat,
    getAllChat: getAllChat,
    getChatHistory: getChatHistory,
    listAllNotificationsMaids: listAllNotificationsMaids,
    changePasswordMaid: changePasswordMaid,
    forgetPasswordMaid: forgetPasswordMaid,
    maidLogout: maidLogout,
    raiseAnIssueMaid: raiseAnIssueMaid,
    emailTemplateForgetPassword:emailTemplateForgetPassword

};