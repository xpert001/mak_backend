'use strict';
const Service = require('../Services');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const async = require('async');
const TokenManager = require('../Lib/TokenManager');
const SMSManager = require('../Lib/SMSManager');
const MailManager = require('../Lib/MailManager');
const NotificationManager = require('../Lib/NotificationManager');
const Config = require('../Config');
const Models = require('../Models');
const moment = require('moment');
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');
const _ = require('lodash');
const SocketManager = require('../Lib/SocketManager');
const MaidController = require('../Controllers/MaidController');
const randomstring = require("randomstring");
const request = require('request');
const otpGenerator = require('otp-generator');
const momentTz = require('moment-timezone');
const stripe = require('stripe')('sk_test_JLqqLJ4TtGG4g2wbd4qKBYVd');
const getCountry = require('country-currency-map').getCountry;
const googleTranslate = require('google-translate')("AIzaSyDqUpdEETnj0g6rU3NyQ6FQRY3PE1sZlEA");
const rp = require('request-promise');

var crypto = require('crypto');
const signUp1 = function (payloadData, callback) {
    var dataToInsert = {};
    var userData = {};
    var responseObject = {};
    let doNotDoSignup = false;
    let customerStripeId = "";

  //  console.log("==================payloadData================", payloadData);

    async.series([
        function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        function (cb) {
            // validate email
            if (!UniversalFunctions.verifyEmailFormat(payloadData.email)) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
            } else {
                cb(null)
            }
        },

        function (cb) {
            //checkEmail in db
            let criteria = {
                email: payloadData.email,
                isDeleted: false,
                isDeactivated: false
            };

            let projection = {};
            let options = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        if (data[0].isGuestFlag) {
                            userData = data[0];
                            doNotDoSignup = true;
                            cb(null)
                        } else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_EXIST);
                        }
                    } else {
                        dataToInsert.email = payloadData.email;
                        cb(null)
                    }
                }
            });
        },
        function (cb) {
                    //checkEmail in db
                let criteria = {
                    email: payloadData.email,
                    isDeleted: false,
                };

                let projection = {};
                let options = {
                    lean: true
                };
                Service.MaidServices.getMaid(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        if (data && data.length) {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MEMEBR_EMAIL_EXIST);
                        }
                        else cb()
                    }
                });
        },

        function (cb) {
            if (!doNotDoSignup) {
                let firstName = payloadData.fullName.split(" ");
                stripe.customers.create({
                    description: "customer_" + firstName[0],
                    email: payloadData.email
                }, function (err, result) {
                    if (err) {
                        console.log("error", err);
                        cb(err);
                    }
                    else {
                        customerStripeId = result.id;
                        cb(null);
                    }
                });
            } else {
                cb(null)
            }
        },

        function (cb) {
            if (doNotDoSignup == false) {
                // insert user in db
                dataToInsert.fullName = payloadData.fullName;
                // dataToInsert.fullNameAr = languageConvertor(payloadData.fullName);
                dataToInsert.customerStripeId = customerStripeId;
                dataToInsert.password = UniversalFunctions.CryptData(payloadData.password);
                dataToInsert.uniquieAppKey = UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME);

                if (payloadData.deviceToken) {
                    dataToInsert.deviceToken = payloadData.deviceToken;
                }
                if (payloadData.deviceType) {
                    dataToInsert.deviceType = payloadData.deviceType;
                }

                dataToInsert.isGuestFlag = false;
                dataToInsert.actualSignupDone = true;


                Service.UserServices.createUser(dataToInsert, function (err, result) {
                    console.log("==================err,result===============", err);

                    if (err) {
                        cb(err);
                    } else {
                        if (result) {
                            userData = result;
                            responseObject.fullName = result.fullName;
                            responseObject.email = result.email;
                            responseObject._id = result._id;
                            responseObject.profileComplete = result.profileComplete;
                            responseObject.isGuestFlag = result.isGuestFlag;
                            responseObject.firstBookingDone = result.firstBookingDone;
                            responseObject.actualSignupDone = result.actualSignupDone;
                            cb(null)
                        } else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                        }
                    }
                })
            }
            else {
                // update user in db

                let criteria = {
                    _id: userData._id
                };
                let dataToUpdate = {};

                dataToUpdate.fullName = payloadData.fullName;
                // dataToInsert.fullNameAr = languageConvertor(payloadData.fullName);
                dataToUpdate.password = UniversalFunctions.CryptData(payloadData.password);
                dataToUpdate.uniquieAppKey = UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME);

                if (payloadData.deviceToken) {
                    dataToUpdate.deviceToken = payloadData.deviceToken
                }
                if (payloadData.deviceType) {
                    dataToUpdate.deviceType = payloadData.deviceType
                }

                dataToUpdate.isGuestFlag = false;
                dataToUpdate.actualSignupDone = true;


                Service.UserServices.updateUser(criteria, dataToUpdate, {new: true}, function (err, result) {
                    if (err) {
                        cb(err);
                    } else {
                        if (result) {
                            userData = result;
                            console.log("==================userData*****else*****result================", userData);
                            responseObject.fullName = result.fullName;
                            responseObject.email = result.email;
                            responseObject._id = result._id;
                            responseObject.profileComplete = result.profileComplete;
                            responseObject.isGuestFlag = result.isGuestFlag;
                            responseObject.firstBookingDone = result.firstBookingDone;
                            responseObject.actualSignupDone = result.actualSignupDone;
                            cb(null)
                        } else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                        }
                    }
                })
            }
        },

        function (cb) {
            // set access token
            if (userData) {
                let tokenData = {
                    id: userData._id,
                    type: 'USER'
                };
                TokenManager.setToken(tokenData, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        if (data) {
                            responseObject.accessToken = data.accessToken;
                            cb(null);
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

    ], function (err, result) {
        callback(err, responseObject)
    })
};

const signUp2 = function (payloadData, userData, callback) {

    let responseObject = {};
    console.log("==================payloadData================", payloadData);
    async.series([
        function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        function (cb) {
            // validate country code
            if (payloadData.countryCode) {
                if (payloadData.countryCode.lastIndexOf('+') == 0) {
                    if (!isFinite(payloadData.countryCode.substr(1))) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                    }
                    else if (!payloadData.phoneNo || payloadData.phoneNo == '') {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_MISSING);
                    } else {
                        cb(null);
                    }
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
            } else if (payloadData.phoneNo) {
                if (!payloadData.countryCode) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.COUNTRY_CODE_MISSING);
                } else if (payloadData.countryCode && payloadData.countryCode.lastIndexOf('+') == 0) {
                    if (!isFinite(payloadData.countryCode.substr(1))) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                    } else {
                        cb(null);
                    }
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
            } else {
                cb(null);
            }
        },

        function (cb) {
            // check phone number in db
            let criteria = {
                    phoneNo: payloadData.phoneNo,
                    isDeleted: false,
                    isDeactivated: false
                };
            let projection = {};
            let options = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_ALREADY_EXIST);
                    } else {
                        cb(null)
                    }
                }
            });
        },

        function (cb) {
            let criteria = {
                _id: userData._id,
                isDeleted: false,
                isDeactivated: false
            };

            let billingAddress = {
                billingName: userData.fullName || "",
                streetName: payloadData.streetName || "",
                buildingName: payloadData.buildingName || "",
                villaName: payloadData.villaName || "",
                city: payloadData.city || "",
                country: payloadData.country || "",
                countryENCode: payloadData.countryENCode,
                countryCode: payloadData.countryCode,
                phoneNo: payloadData.phoneNo,
                moreDetailedaddress: payloadData.moreDetailedaddress || ""
            };

            let usersAddress = {
                streetName: payloadData.streetName || "",
                buildingName: payloadData.buildingName || "",
                villaName: payloadData.villaName || "",
                city: payloadData.city || "",
                country: payloadData.country || "",
                moreDetailedaddress: payloadData.moreDetailedaddress || ""
            };

            let dataToUpdate = {
                nationalId: payloadData.nationalId,
                billingAddress: billingAddress,
                usersAddress: usersAddress,
                countryENCode: payloadData.countryENCode,
                countryCode: payloadData.countryCode,
                phoneNo: payloadData.phoneNo,
                profileComplete: true,
                isGuestFlag: false,
                actualSignupDone: true,
                timeZone: payloadData.timeZone || "",
            };

            if (payloadData.profilePicURL) {
                dataToUpdate.profilePicURL = payloadData.profilePicURL
            }
            if (payloadData.documentPicURL) {
                dataToUpdate.documentPicURL = payloadData.documentPicURL
            }
            if (payloadData.deviceToken) {
                dataToUpdate.deviceToken = payloadData.deviceToken
            }
            if (payloadData.deviceType) {
                dataToUpdate.deviceType = payloadData.deviceType
            }

            let option = {new: true};
            Service.UserServices.updateUser(criteria, dataToUpdate, option, function (err, data) {
                console.log("____________update user result_____________",err)
                if (err) {
                    cb(err)
                } else {
                    if (data) {
                        var result = data;
                        cb(null)
                    }
                    else {
                        cb();
                    }
                }
            })
        },

        function (cb) {
            let criteria = {
                _id: userData._id,
                isDeleted: false,
                isDeactivated: false,
            };

            let projection = {
                _v: 0,
                password: 0,
            };
            Service.UserServices.getUsers(criteria, projection, {lean: true}, function (err, data) {
                if (err) {
                    cb(err)
                } else {
                    if (data && data.length) {
                        responseObject = data[0];
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }

    ], function (err, result) {

        if (err) {
            if (err.code == 11000 && (err.message.indexOf('nationalId_1') > -1)) {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NATIONAL_ID_EXIST, {})
            } else {
                if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN) {
                    callback(err, responseObject)
                } else {
                  //  responseObject.fullName = responseObject.fullNameAr;
                    callback(err, responseObject)
                }
            }
        } else {
            let data =  {
                fullName: userData.fullName,
               // lastName: payloadData.lastName,
                link : "http://mak.today/#/userVerification/"+userData._id
            }
           // console.log('3333333333333333333',responseObject)
            let email ;
            if(payloadData.email && payloadData.email!=='')
                email = payloadData.email
            else email = responseObject.email
            verifyTemplate(data,(err,result)=>{
                let subject = "M.A.K ";

                MailManager.sendMailBySES(email, subject, result, function (err, res) {
                });
            })

            if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN) {
                callback(err, responseObject)
            } else {
              //  responseObject.fullName = responseObject.fullNameAr;
                callback(err, responseObject)
            }
        }
    })

};

const verifyTemplate =  function(data,callback){

    let content = `
    
      <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style type="text/css">
        
        body,
        #bodyTable {
            height: 100%!important;
            margin: 0;
            padding: 0;
            width: 100%!important
        }

        #bodyTable {
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            min-height: 100%;
            background: #eee;
            color: #333
        }

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0
        }

        img {
            -ms-interpolation-mode: bicubic
        }

        body {
            margin: 0;
            padding: 0;
            background: #eee
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: 0;
            text-decoration: none
        }

        table {
            border-collapse: collapse!important;
            max-width: 100%!important
        }

        img {
            border: 0!important
        }

        h1 {
            font-family: Helvetica;
            font-size: 42px;
            font-style: normal;
            font-weight: bold;
            text-align: center;
            margin: 30px auto 0
        }

        h2 {
            font-size: 32px;
            font-style: normal;
            font-weight: bold;
            margin: 50px auto 0
        }

        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 25px 0;
            letter-spacing: normal;
            text-align: left
        }

        a h3 {
            color: #444!important;
            text-decoration: none!important
        }

        .titleLink {
            text-decoration: none!important
        }

        .preheaderContent {
            color: #808080;
            font-size: 10px;
            line-height: 125%
        }

        .preheaderContent a:link,
        .preheaderContent a:visited,
        .preheaderContent a .yshortcuts {
            color: #606060;
            font-weight: normal;
            text-decoration: underline
        }

        #emailHeader,
        #tacoTip {
            color: #fff
        }

        #emailHeader {
            background-color: #fff
        }

        #content p {
            color: #4d4d4d;
            margin: 20px 70px 30px;
            font-size: 24px;
            line-height: 32px;
            text-align: left
        }

        #button {
            display: inline-block;
            margin: 10px auto;
            background: #fff;
            border-radius: 4px;
            font-weight: bold;
            font-size: 18px;
            padding: 15px 20px;
            cursor: pointer;
            color: #0079bf;
            margin-bottom: 50px
        }

        #socialIconWrap img {
            line-height: 35px!important;
            padding: 0 5px
        }

        .footerContent div {
            color: #707070;
            font-family: Arial;
            font-size: 12px;
            line-height: 125%;
            text-align: center;
            max-width: 100%!important
        }

        .footerContent div a:link,
        .footerContent div a:visited {
            color: #369;
            font-weight: normal;
            text-decoration: underline
        }

        .footerContent img {
            display: inline
        }

        #socialLinks img {
            margin: 0 2px
        }

        #utility {
            border-top: 1px solid #ddd
        }

        #utility div {
            text-align: center
        }

        #monkeyRewards img {
            max-width: 160px
        }

        #emailFooter {
            max-width: 100%!important
        }

        #footerTwitter a,
        #footerFacebook a {
            text-decoration: none!important;
            color: #fff!important;
            font-size: 14px
        }

        #emailButton {
            border-radius: 6px;
            background: #70b500!important;
            margin: 0 auto 60px;
            box-shadow: 0 4px 0 #578c00
        }

        #socialLinks a {
            width: 40px
        }

        #socialLinks #blogLink {
            width: 80px!important
        }

        .sectionWrap {
            text-align: center
        }

        #header {
            color: #fff!important
        }

    </style>
</head>

<body>
    <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="left" border="0" cellspacing="0" style="width:50%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px">
                                        <tbody>
                                            <tr>
                                                <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="https://s3-us-west-2.amazonaws.com/mak-s3/makLogo.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120">
                                                    <h1 style="font-size: 30px;">Welcome to MAK</h1>
                                                    <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="background:#FFFFFF;">
                                                    <br><br><p>
                                                    Dear <b>${data.fullName} </b>
                                                  <br>
                                                 
                                                  <br>Thank you for registering with MAK. Please verify your email address below.
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" bgcolor="#61BD4F" border="0" cellpadding="0" cellspacing="0" class="emailButton" id="button" style=" border-radius:6px;  display:inline-block;  margin:0px auto 60px; padding: 10px 14px;">
                                                    <div><span style="font-size:20px"><a href="${data.link}" style="background-color:#61BD4F;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:500; letter-spacing: 0.4px;  line-height:48px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;font-size:18px !important">Verify</a></span>&nbsp;&nbsp;&nbsp;
                                                        <!--[if mso]></center></v:rect><![endif]-->
                                                    </div>
                                                </td>
                                          </tr>
                                          <tr>
                                              <td align="center" style="background:#FFFFFF;">
</p></td>
                                            </tr>
                                            <tr>
                                            <td align="center" bgcolor="#61BD4F" border="0" cellpadding="0" cellspacing="0" class="emailButton" id="button" style=" border-radius:6px;  display:inline-block;  margin:0px auto 60px; padding: 10px 14px;">
                                                    <div><span style="font-size:20px"><a href="${data.link}" style="color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:500; letter-spacing: 0.4px;  line-height:48px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;font-size:18px !important"><img src="https://s3-us-west-2.amazonaws.com/mak-s3/29793763_343336722842176_8545984603976892416_n.png"></a></span>&nbsp;&nbsp;&nbsp;<span style="font-size:20px"><a href="$\\{data.link}" style="color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:500; letter-spacing: 0.4px;  line-height:48px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;font-size:18px !important"><img src="https://s3-us-west-2.amazonaws.com/mak-s3/30124862_343336729508842_3973615652479959040_n.png"</a></span>&nbsp;
                                                        <!--[if mso]></center></v:rect><![endif]-->
                                                    </div>
                                                </td>
                                            </tr>
                                           <tr>
                                              <td align="left" style="background:#FFFFFF;">
                                                <p>Sincerely,<br>MAK Team
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px">
                                        <tbody>
                                            <tr>
                                                <td class="footerContent" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0" style="width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">&nbsp;
                                                                    <div mc:edit="std_footer"><em>Copyright � 2018 MAK, All rights reserved.</em></div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

    `
    callback(null,content)
};

const updateDeviceToken = function (payloadData, userData, callback) {
    let responseObject = {};
    let dataToUpdate = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        updateUser: function (checkForUniqueKey, cb) {
            if (payloadData.deviceToken) {
                dataToUpdate.deviceToken = payloadData.deviceToken
            } else if (payloadData.deviceType) {
                dataToUpdate.deviceType = payloadData.deviceType

            } else if (payloadData.deviceToken && payloadData.deviceType) {
                dataToUpdate.deviceToken = payloadData.deviceToken
                dataToUpdate.deviceType = payloadData.deviceType
            }
            let criteria = {
                _id: userData._id,
                isDeleted: false,
                isDeactivated: false
            };

            let option = {new: true};


            if ((payloadData.deviceToken || payloadData.deviceType) && dataToUpdate) {

                Service.UserServices.updateUser(criteria, dataToUpdate, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        responseObject = result;
                        cb(null)
                    }
                })
            } else {
                cb(null)
            }
        }

    }, function (err, result) {
        if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN) {
            callback(err, responseObject)
        } else {
            //responseObject.fullName = responseObject.fullNameAr;
            callback(err, responseObject)
        }
    })
};

const verifyUser = function (payloadData, callback) {

    async.autoInject({
        check :(cb)=>{
            let criteria = {
                _id: payloadData.userId,
            };

            let option = {lean: true};
            Service.UserServices.getUsers(criteria,{}, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if(result.length && result[0].isVerified)
                    {
                        if(result[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ALREADY_VERIFY);
                        else  cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ALREADY_VERIFY_AR);
                    }
                    else cb()
                }
            })
        },
        updateUser: function (check,cb) {
            let criteria = {
                _id: payloadData.userId,
                isVerified: false,
            };

            let option = {new: true};
            Service.UserServices.updateUser(criteria,{isVerified:true}, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb()
                    }
                })
        }

    }, function (err, result) {
        if (err) {
            callback(err)
        } else {  //responseObject.fullName = responseObject.fullNameAr;
            callback(null)
        }
    })
};

const addBillingInfo = function (payloadData, userData, callback) {
    let insertUser = false;
    let updateUser = false;
    let userDetails = {};
    let responseObject = {};
    let customerStripeId = "";
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        validateEmail: function (checkForUniqueKey, cb) {
            if (payloadData.isGuestFlag) {
                // validate email
                if (!UniversalFunctions.verifyEmailFormat(payloadData.email)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                } else {
                    cb(null)
                }
            } else {
                cb(null)
            }
        },

        checkMail :(validateEmail,cb)=>{

            //checkEmail in db
            if(!userData.actualSignupDone){
                let criteria = {
                    email: payloadData.email,
                    isDeleted: false,
                };

                let projection = {};
                let options = {
                    lean: true
                };
                Service.MaidServices.getMaid(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        if (data && data.length) {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MEMEBR_EMAIL_EXIST);
                        }
                        else cb()
                    }
                });
            }else cb()
        },

        checkEmailInDb: function (validateEmail, cb) {
            if (payloadData.isGuestFlag) {

                let criteria = {
                    email: payloadData.email,
                    isDeleted: false,
                    isDeactivated: false
                };

                let projection = {};
                let options = {
                    lean: true
                };
                Service.UserServices.getUsers(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        if (data && data.length && data[0].firstBookingDone) {
                            return callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_EXIST)
                            // if (data[0].firstBookingDone) {
                            //     callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.FIRST_BOOKING_DONE);
                            //
                            // } else {
                            //     updateUser = true;
                            //     userDetails = data[0];
                            //     cb(null)
                            // }
                        } else {
                            insertUser = true;
                            cb(null)
                        }
                    }
                });
            }
            else {
                if (userData && userData._id) {

                    let criteria = {
                        _id: userData._id,
                        isDeleted: false,
                        isDeactivated: false
                    };

                    let billingAddress = {
                        billingName: payloadData.fullName,
                        email: payloadData.email,
                        streetName: payloadData.streetName,
                        buildingName: payloadData.buildingName,
                        villaName: payloadData.villaName,
                        city: payloadData.city,
                        country: payloadData.country,
                        countryENCode: payloadData.countryENCode,
                        countryCode: payloadData.countryCode,
                        phoneNo: payloadData.phoneNo,
                        nationalId: payloadData.nationalId,
                        timeZone: payloadData.timeZone,
                    };
                    let dataToSet = {
                        $set: {
                            billingAddress: billingAddress
                        }
                    };
                    Service.UserServices.updateUser(criteria, dataToSet, {new: true}, function (err, result) {
                        if (err) {
                            cb(err)
                        } else {
                            if (result) {
                                // insertUser = true;
                                responseObject = result;
                                cb(null)
                            } else {
                                cb(null)
                            }
                        }
                    })
                } else {
                    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
                }
            }
        },

        createStripeCustomerId: function (checkEmailInDb, cb) {

            if (insertUser) {

                let firstName = payloadData.fullName.split(" ");
                stripe.customers.create({
                    description: "customer_" + firstName[0],
                    email: payloadData.email
                }, function (err, result) {
                    if (err) {
                        console.log("error", err);
                        cb(err);
                    }
                    else {
                        customerStripeId = result.id;
                        cb(null);
                    }
                });
            } else {
                cb(null)
            }
        },

        insertUserFunction: function (createStripeCustomerId, cb) {
            if (insertUser) {
                let objectToSave = {};
                let billingAddress = {
                    billingName: payloadData.fullName,
                    email: payloadData.email,
                    line1: payloadData.line1,
                    city: payloadData.city,
                    state: payloadData.state,
                    country: payloadData.country,
                    countryENCode: payloadData.countryENCode,
                    countryCode: payloadData.countryCode,
                    phoneNo: payloadData.phoneNo,
                    nationalId: payloadData.nationalId,
                    moreDetailedaddress: payloadData.moreDetailedaddress || ""
                };

                let usersAddress = {
                    streetName: payloadData.streetName,
                    buildingName: payloadData.buildingName,
                    villaName: payloadData.villaName,
                    city: payloadData.city,
                    country: payloadData.country,
                    moreDetailedaddress: payloadData.moreDetailedaddress || ""
                };

                objectToSave.uniquieAppKey = UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME);
                objectToSave.billingAddress = billingAddress;
                objectToSave.usersAddress = usersAddress;
                objectToSave.email = payloadData.email;
                objectToSave.nationalId = payloadData.nationalId;
                objectToSave.fullName = payloadData.fullName;
                // objectToSave.fullName = languageConvertor(payloadData.fullName);
                objectToSave.countryENCode = payloadData.countryENCode;
                objectToSave.countryCode = payloadData.countryCode;
                objectToSave.phoneNo = payloadData.phoneNo;
                objectToSave.isGuestFlag = true;
                objectToSave.timeZone = payloadData.timeZone;
                objectToSave.customerStripeId = customerStripeId;


                if (payloadData.deviceToken) {
                    objectToSave.deviceToken = payloadData.deviceToken;
                }
                if (payloadData.deviceType) {
                    objectToSave.deviceType = payloadData.deviceType;
                }

                Service.UserServices.createUser(objectToSave, function (err, result) {
                    if (err) {
                        cb(err);
                    } else {
                        if (result) {
                            responseObject = result;
                            cb(null)
                        } else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                        }
                    }
                })
            }
            else {
                cb()
            }
        },

        updateUserFunction: function (createStripeCustomerId, cb) {
            if (updateUser) {
                let criteria = {
                    _id: userDetails._id,
                    isDeleted: false,
                    isDeactivated: false
                };
                let billingAddress = {
                    billingName: payloadData.fullName,
                    email: payloadData.email,
                    line1: payloadData.line1,
                    city: payloadData.city,
                    state: payloadData.state,
                    country: payloadData.country,
                    countryENCode: payloadData.countryENCode,
                    countryCode: payloadData.countryCode,
                    phoneNo: payloadData.phoneNo,
                    nationalId: payloadData.nationalId,
                    timeZone: payloadData.timeZone,
                };
                let dataToSet = {
                    billingAddress: billingAddress,
                };
                if (payloadData.moreDetailedaddress) {
                    dataToSet.moreDetailedaddress = payloadData.moreDetailedaddress
                }
                Service.UserServices.updateUser(criteria, dataToSet, {new: true}, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result) {
                            responseObject = result;
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        createAccessToken: function (insertUserFunction, updateUserFunction, cb) {
            // set access token
            if (insertUser) {
                let tokenData = {
                    id: responseObject._id,
                    type: 'USER'
                };
                TokenManager.setToken(tokenData, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        if (data) {
                            responseObject.accessToken = data.accessToken;
                            cb(null);
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

    }, function (err, result) {
        if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN) {
            callback(err, responseObject)
        } else {
          //  responseObject.fullName = responseObject.fullNameAr;
            callback(err, responseObject)
        }
    })
};

const emailLogin = function (payloadData, callback) {
    let responseObject = {};
    let userData = {};
    let accessTokenUpdated = null;
    let password = UniversalFunctions.CryptData(payloadData.password);

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkUserExist: function (checkForUniqueKey, cb) {
            let criteria = {
                email: payloadData.email,
            };
            let projections = {};
            let options = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projections, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        /*if (data[0].profileComplete=== false && data[0].isVerified===false) {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.VERIFY_USER);
                        }
                        else */if (data[0].profileComplete && data[0].isVerified === false) {
                            if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.VERIFY_USER);
                            else  callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.VERIFY_USER_AR);
                        }
                        else if (data[0].isBlocked) {
                            if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.BLOCK_USER)
                            else  callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.BLOCK_USER_AR)
                        }
                        else if (data[0].isDeleted) {
                            if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_DELETED);
                            else    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_DELETED_AR);
                        }
                        else if (!data[0].isBlocked && !data[0].isDeleted) {
                            if (data[0].password == password) {
                                userData = data;
                                cb(null)
                            }
                            else {
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_PASSWORD)
                            }
                        }
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_REGISTERED);
                    }
                }
            })
        },

        generateTokenAndUpdate: function (checkUserExist, cb) {
            let tokenData = {
                id: userData[0]._id,
                type: 'USER'
            };
            TokenManager.setToken(tokenData, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    accessTokenUpdated = data.accessToken;
                    cb(null);
                }
            })
        },

        updateUserForToken: function (generateTokenAndUpdate, cb) {
            let criteria = {
                _id: userData[0]._id,
                isDeleted: false,
            };
            let setQuery = {
                isLogin: true,
                isDeactivated: false,
                timeZone: payloadData.timeZone,
            };
            if (payloadData.deviceToken) {
                setQuery.deviceToken = payloadData.deviceToken
            }
            if (payloadData.deviceType) {
                setQuery.deviceType = payloadData.deviceType
            }
            Service.UserServices.updateUser(criteria, setQuery, {new: true}, function (err, data) {
                if (err) {
                    cb(err)
                } else {
                    if (data && data._id) {
                        var result = data;
                        cb(null);
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            });
        },

        createResponse: function (updateUserForToken, cb) {
            let criteria = {
                _id: userData[0]._id,
                isDeleted: false,
                isDeactivated: false,
            };

            let projection = {
                _v: 0,
                password: 0,
            };
            Service.UserServices.getUsers(criteria, projection, {lean: true}, function (err, data) {
                if (err) {
                    cb(err)
                } else {
                    if (data && data.length) {
                        responseObject = data[0];
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }

    }, function (err, result) {
        if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN) {
            callback(err, responseObject)
        } else {
           // responseObject.fullName = responseObject.fullNameAr;
            callback(err, responseObject)
        }
    })
};

const facebookLogin = function (payloadData, callback) {
    let response = {};
    let userData = {};
    let keyExist = false;
    let updateUser = false;
    let insertUser = false;
    let emailExist = false;
    let isGuestFlagFacebook = false;
    let isGuestFlagGmail = false;
    let customerStripeId = "";

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkKey: function (checkForUniqueKey, cb) {
            var criteria = {
                facebookId: payloadData.facebookId,
                isDeleted: false,
                isDeactivated: false,
            };

            Service.UserServices.getUsers(criteria, {}, {lean: true}, function (err, data) {
                if (err) {
                    cb(err)
                } else {
                    if (data && data.length) {
                        if (data[0].profileComplete && data[0].isVerified === false) {
                            if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.VERIFY_USER);
                            else  callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.VERIFY_USER);
                        }
                        else if (data[0].isBlocked) {
                            if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.BLOCK_USER);
                            else  callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.BLOCK_USER_AR)
                        }
                        else if (data[0].isDeleted) {
                            if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_DELETED);
                            else    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_DELETED_AR);
                        }
                        else {
                            userData = data[0];
                            isGuestFlagFacebook = data[0].isGuestFlag;
                            keyExist = true;
                            console.log("facebook_________keyExist : true_______________", isGuestFlagFacebook);
                            cb(null)
                        }

                    } else {
                        console.log("facebook_________keyExist : false______________");
                        keyExist = false;
                        cb(null)
                    }
                }
            })
        },

        checkEmailExist: function (checkKey, cb) {
            if (!keyExist && payloadData.email || payloadData.email != '') {
                var criteria = {
                    email: payloadData.email,
                    isDeleted: false,
                    isDeactivated: false,
                };

                Service.UserServices.getUsers(criteria, {}, {lean: true}, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        if (data && data.length) {
                            userData = data[0];
                            isGuestFlagGmail = data[0].isGuestFlag;
                            updateUser = true;
                            insertUser = false;
                            emailExist = true;
                            console.log("_email Exist:true_____updateUser : true_________", isGuestFlagGmail);
                            cb(null)
                        } else {
                            console.log("__email/phone Exist:true______updateUser : false_________");
                            emailExist = false;
                            updateUser = false;
                            insertUser = true;
                            cb(null)
                        }
                    }
                })
            } else {
                console.log("_email Exist:false_____updateUser : true_________", userData);
                updateUser = true;
                insertUser = false;
                cb(null)
            }
        },

        createStripeCustomerId: function (checkEmailExist, cb) {
            if (insertUser) {
                let firstName = payloadData.fullName.split(" ");
                stripe.customers.create({
                    description: "customer_" + firstName[0],
                    email: payloadData.email
                }, function (err, result) {
                    if (err) {
                        console.log("error", err);
                        cb(err);
                    }
                    else {
                        customerStripeId = result.id;
                        cb(null);
                    }
                });
            } else {
                cb(null)
            }
        },

        action: function (createStripeCustomerId, cb) {
            console.log("ddddddddddddddddddddddddddddd",
                isGuestFlagFacebook, isGuestFlagGmail, updateUser, insertUser)

            if (updateUser && (isGuestFlagGmail || isGuestFlagFacebook)) {
                console.log("____________1____________updateUser");
                var criteria = {
                    _id: userData._id,
                    isDeleted: false,
                    isDeactivated: false
                };
                var dataToSet = {
                    $set: {
                        facebookId: payloadData.facebookId,
                    },
                    timeZone: payloadData.timeZone || "",
                    isGuestFlag: false,
                    actualSignupDone: false
                };
                if (payloadData.fullName) {
                    dataToSet.fullName = payloadData.fullName;
                    // dataToSet.fullNameAr = languageConvertor(payloadData.fullName);
                }
                if (payloadData.email || payloadData.email != '') {
                    dataToSet.email = payloadData.email;
                }
                if (payloadData.profilePicURL) {
                    dataToSet.profilePicURL = payloadData.profilePicURL;
                }
                if (payloadData.deviceToken) {
                    dataToSet.deviceToken = payloadData.deviceToken;
                }
                if (payloadData.deviceType) {
                    dataToSet.deviceType = payloadData.deviceType;
                }

                Service.UserServices.updateUser(criteria, dataToSet, {new: true}, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        userData = data;
                        cb(null)
                    }
                })
            }
            else if (updateUser && (!isGuestFlagGmail || !isGuestFlagFacebook)) {
                console.log("____________2____________updateUser");
                var criteria = {
                    _id: userData._id,
                    isDeleted: false,
                    isDeactivated: false
                };
                var dataToSet = {
                    $set: {
                        facebookId: payloadData.facebookId,
                    },
                    timeZone: payloadData.timeZone || "",

                };
                if (payloadData.fullName) {
                    dataToSet.fullName = payloadData.fullName;
                    // dataToSet.fullNameAr = languageConvertor(payloadData.fullName);
                }
                if (payloadData.email || payloadData.email != '') {
                    dataToSet.email = payloadData.email;
                }
                if (payloadData.profilePicURL) {
                    dataToSet.profilePicURL = payloadData.profilePicURL;
                }
                if (payloadData.deviceToken) {
                    dataToSet.deviceToken = payloadData.deviceToken;
                }
                if (payloadData.deviceType) {
                    dataToSet.deviceType = payloadData.deviceType;
                }

                Service.UserServices.updateUser(criteria, dataToSet, {new: true}, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        userData = data;
                        cb(null)
                    }
                })
            }
            else if (insertUser) {
                console.log("_________3_______________insertUser");
                let objectToSave = {
                    facebookId: payloadData.facebookId,
                    faceBookLogin: true,
                    isLogin: true,
                    firstTimeLogin: true,
                    uniquieAppKey: UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME),
                    timeZone: payloadData.timeZone || "",
                    customerStripeId: customerStripeId,
                };
                if (payloadData.fullName) {
                    objectToSave.fullName = payloadData.fullName;
                    // objectToSave.fullName = languageConvertor(payloadData.fullName);
                }
                if (payloadData.email || payloadData.email != '') {
                    objectToSave.email = payloadData.email;
                }
                if (payloadData.profilePicURL) {
                    objectToSave.profilePicURL = payloadData.profilePicURL;
                }
                if (payloadData.deviceToken) {
                    objectToSave.deviceToken = payloadData.deviceToken;
                }
                if (payloadData.deviceType) {
                    objectToSave.deviceType = payloadData.deviceType;
                }
                Service.UserServices.createUser(objectToSave, function (err, result) {
                    if (err) {
                        cb(err);
                    } else {
                        if (result) {
                            console.log("social signUp____________________")
                            userData = result;
                            cb(null)
                        } else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                        }
                    }
                })
            }
            else {
                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
            }
        },

        generateTokenAndUpdate: function (action, cb) {
            if (insertUser || updateUser) {
                let tokenData = {
                    id: userData._id,
                    type: 'USER'
                };
                TokenManager.setToken(tokenData, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        cb(null);
                    }
                })
            } else {
                cb(null)
            }
        },

        createResponse: function (generateTokenAndUpdate, cb) {
            let criteria = {
                _id: userData._id,
                isDeleted: false,
                isDeactivated: false,
            };

            let projection = {
                _v: 0,
                password: 0,
            };
            Service.UserServices.getUsers(criteria, projection, {lean: true}, function (err, data) {
                if (err) {
                    cb(err)
                } else {
                    if (data && data.length) {
                        response = data[0];
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }

    }, function (err, result) {
        if (userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN) {
            callback(err, response)
        } else {
            callback(err, response)
        }
    })
};

const updateUserProfile = function (payloadData, userData, callback) {

    let responseObject = {};
    console.log("==================payloadData================", payloadData);
    async.series([
        function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        function (cb) {
            // validate country code
            if (payloadData.countryCode) {
                if (payloadData.countryCode.lastIndexOf('+') === 0) {
                    if (!isFinite(payloadData.countryCode.substr(1))) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                    }
                    else if (!payloadData.phoneNo || payloadData.phoneNo === '') {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_MISSING);
                    } else {
                        cb(null);
                    }
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
            } else if (payloadData.phoneNo) {
                if (!payloadData.countryCode) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.COUNTRY_CODE_MISSING);
                } else if (payloadData.countryCode && payloadData.countryCode.lastIndexOf('+') === 0) {
                    if (!isFinite(payloadData.countryCode.substr(1))) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                    } else {
                        cb(null);
                    }
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
            } else {
                cb(null);
            }
        },

        function (cb) {
            // check phone number in db
            let criteria =
                {
                    isDeleted: false,
                    isDeactivated: false,
                    _id:{$ne:userData._id}
                };
            let projection = {};
            let options = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        if (payloadData.phoneNo) {
                            if (data[0].phoneNo === payloadData.phoneNo) {
                                if(data[0].appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_ALREADY_EXIST);
                                else    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_ALREADY_EXIST_AR);
                            }
                            else {
                                cb(null)
                            }
                        }
                        else cb(null)
                    } else cb(null)
                }
            });
        },

        function (cb) {
            let criteria = {
                _id: userData._id,
                isDeleted: false,
                isDeactivated: false
            };

            let billingAddress = {};
            let usersAddress = {};
            if (payloadData.city) {
                billingAddress.city = payloadData.city;
                usersAddress.city = payloadData.city
            }
            if (payloadData.country) {
                billingAddress.country = payloadData.country;
                usersAddress.country = payloadData.country
            }
            if (payloadData.streetName) {
                billingAddress.streetName = payloadData.streetName;
                usersAddress.streetName = payloadData.streetName
            }
            if (payloadData.buildingName) {
                billingAddress.buildingName = payloadData.buildingName;
                usersAddress.buildingName = payloadData.buildingName
            }
            if (payloadData.villaName) {
                billingAddress.villaName = payloadData.villaName;
                usersAddress.villaName = payloadData.villaName
            }
            if (payloadData.moreDetailedaddress) {
                billingAddress.moreDetailedaddress = payloadData.moreDetailedaddress;
                usersAddress.moreDetailedaddress = payloadData.moreDetailedaddress
            }
            if (payloadData.countryENCode) {
                billingAddress.countryENCode = payloadData.countryENCode
            }
            if (payloadData.countryCode) {
                billingAddress.countryCode = payloadData.countryCode
            }
            if (payloadData.phoneNo) {
                billingAddress.phoneNo = payloadData.phoneNo
            }

            if (payloadData.fullName) {
                billingAddress.billingName = payloadData.fullName
            }

            let dataToUpdate = {
                billingAddress: billingAddress,
                usersAddress: usersAddress,
                profileComplete: true,
                timeZone: payloadData.timeZone || "",
            };
            if (payloadData.fullName) {
                dataToUpdate.fullName = payloadData.fullName
                // dataToUpdate.fullNameAr = languageConvertor(payloadData.fullName)
            }
            if (payloadData.timeZone) {
                dataToUpdate.timeZone = payloadData.timeZone
            }
            if (payloadData.phoneNo) {
                dataToUpdate.countryCode = payloadData.countryCode
                dataToUpdate.countryENCode = payloadData.countryENCode
                dataToUpdate.phoneNo = payloadData.phoneNo
            }

            let option = {new: true};
            Service.UserServices.updateUser(criteria, dataToUpdate, option, function (err, data) {
                if (err) {
                    cb(err)
                } else {
                    if (data) {
                        var result = data;
                        console.log("==================result*****signUp2*****result================", result);
                        responseObject._id = result._id;
                        responseObject.fullName = result.fullName;
                        responseObject.billingAddress = result.billingAddress;
                        responseObject.usersAddress = result.usersAddress;
                        responseObject.email = result.email;
                        responseObject.nationalId = result.nationalId;
                        responseObject.countryENCode = result.countryENCode;
                        responseObject.countryCode = result.countryCode;
                        responseObject.facebookId = result.facebookId;
                        responseObject.phoneNo = result.phoneNo;
                        responseObject.accessToken = result.accessToken;
                        responseObject.multipleAddress = result.multipleAddress;
                        responseObject.documentPicURL = result.documentPicURL || {original: "", thumbnail: ""};
                        if (result.profilePicURL) {
                            responseObject.profilePicURL = result.profilePicURL;
                        }

                        if (result.deviceType) {
                            responseObject.deviceType = result.deviceType;
                        }
                        if (result.deviceToken) {
                            responseObject.deviceToken = result.deviceToken;
                        }
                        responseObject.profileComplete = result.profileComplete;
                        responseObject.isGuestFlag = result.isGuestFlag;
                        responseObject.firstBookingDone = result.firstBookingDone;
                        responseObject.actualSignupDone = result.actualSignupDone;
                        cb(null)
                    }
                    else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                    }
                }
            })
        }

    ], function (err, result) {

        if (err) {
            if (err.code == 11000 && (err.message.indexOf('nationalId_1') > -1)) {
                if(userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NATIONAL_ID_EXIST, {})
                else callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NATIONAL_ID_EXIST_AR, {})
            } else {
                if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN) {
                    callback(err, responseObject)
                } else {
                   // responseObject.fullName = responseObject.fullNameAr;
                    callback(err, responseObject)
                }
            }
        } else {
            if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN) {
                callback(err, responseObject)
            } else {
                //responseObject.fullName = responseObject.fullNameAr;
                callback(err, responseObject)
            }
        }
    })
};

const saveAddress = function (payloadData, userData, callback) {

    let responseObject = {};
    let flag = 0;
    console.log("==================payloadData================", payloadData);
    async.series([
        function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        function (cb) {
            Models.Users.find(
                {_id: userData._id},
                {
                    multipleAddress: {
                        $elemMatch: {
                            streetName: payloadData.streetName,
                            buildingName: payloadData.buildingName,
                            villaName: payloadData.villaName,
                            city: payloadData.city,
                            country: payloadData.country,
                        }
                    }
                }, function (err, result) {
                    if (err) {
                        cb(err)
                    }
                    else {
                        console.log("result *******************", result, result.length);
                        if (result[0].multipleAddress) {
                            if (result[0].multipleAddress.length) {
                                // callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_DATA)
                                callback(null, userData)
                            }
                            else {
                                flag = 1;
                                cb(null)
                            }
                        }
                        else {
                            flag = 1;
                            cb(null)
                        }
                    }
                })
        },

        function (cb) {
            if (flag == 1) {
                let criteria = {
                    _id: userData._id,
                    isDeleted: false,
                    isDeactivated: false
                };
                let usersAddress = {
                    streetName: payloadData.streetName,
                    buildingName: payloadData.buildingName,
                    villaName: payloadData.villaName,
                    city: payloadData.city,
                    country: payloadData.country,
                    lat: payloadData.lat,
                    long: payloadData.long,
                    moreDetailedaddress: payloadData.moreDetailedaddress || ""
                };

                let dataToUpdate = {
                    $addToSet: {
                        multipleAddress: usersAddress
                    },
                    usersAddress: usersAddress
                };
                let option = {new: true};
                Service.UserServices.updateUser(criteria, dataToUpdate, option, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("data *********************************", data);
                        if (data) {
                            responseObject = data;
                            cb(null)
                        }
                        else {
                            cb(null);
                        }
                    }
                })
            }
            else {
                cb(null)
            }

        },
    ], function (err, result) {

        if (err) {
            if (err.code == 11000 && (err.message.indexOf('nationalId_1') > -1)) {
                if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NATIONAL_ID_EXIST, {})
                else callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NATIONAL_ID_EXIST_AR, {})
            } else {
                if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN) {
                    callback(err, responseObject)
                } else {
                    //responseObject.fullName = responseObject.fullNameAr;
                    callback(err, responseObject)
                }
            }
        } else {
            if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN) {
                callback(err, responseObject)
            } else {
                //responseObject.fullName = responseObject.fullNameAr;
                callback(err, responseObject)
            }
        }
    })

};

const deleteAddress = function (payloadData, userData, callback) {

    let responseObject = {};
    console.log("==================payloadData================", payloadData);
    async.series([
            function (cb) {
                let uniquieAppKey = payloadData.uniquieAppKey;
                if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                    cb(null)
                } else {
                    callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
                }
            },

            function (cb) {
                let criteria = {
                    _id: userData._id
                };

                var setQuery = {
                    $pull: {multipleAddress: {_id: payloadData.id}}
                };

                var options = {new: true};
                Service.UserServices.updateUser(criteria, setQuery, options, function (err, data) {
                    if (err) {
                        cb(err)
                    }
                    else {
                        responseObject = data
                        cb(null);
                    }
                })
            },
        ],
        function (err, result) {
            if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN) {
                callback(err, responseObject)
            } else {
               // responseObject.fullName = responseObject.fullNameAr;
                callback(err, responseObject)
            }
        })
};

const getAllAddress = function (payloadData, userData, callback) {
    let responseObject = {};
    async.series([
        function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        function (cb) {
            let criteria = {
                _id: userData._id,
            };
            let projection = {};
            let option = {new: true};
            Service.UserServices.getUsers(criteria, projection, option, function (err, data) {
                if (err) {
                    cb(err)
                } else {
                    if (data) {
                        responseObject = data[0];
                        cb(null)
                    }
                    else {
                        cb(null);
                    }
                }
            })
        }
    ], function (err, result) {
        if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN) {
            callback(err, responseObject)
        } else {
            //responseObject.fullName = responseObject.fullNameAr;
            callback(err, responseObject)
        }
    })
};

const userLogout = function (payloadData, userData, callback) {
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        logout: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: userData._id,
                isDeleted: false,
                isDeactivated: false
            };
            let projection = {
                $unset: {
                    accessToken: 1,
                    deviceToken: 1,
                },
            };
            let option = {lean: true, new: true};
            Service.UserServices.updateUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    callback(null, Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.LOGOUT)
                }
            })
        }
    }, function (err, result) {
        callback(err, result)
    })
};

const listAllLanguagesUser = function (payloadData, userData, callback) {

    let response = [];
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getLanguageList: function (checkForUniqueKey, cb) {

            let criteria = {
                isDeleted: false
            };

            let projection = {
                __v: 0,
                uniquieAppKey: 0,
            };
            let option = {
                lean: true,
                sort :{languageName:1}
                // skip: ((payloadData.pageNo - 1) * payloadData.limit),
                // limit: payloadData.limit
            };

            Service.LanguageServices.getLanguage(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }
    }, function (err, result) {
        callback(err, response)
    });
};

const changeLanguageUser = function (payloadData, userData, callback) {
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getLanguageList: function (checkForUniqueKey, cb) {

            let criteria = {
                _id: userData._id,
            };

            let dataToUpdate = {
                $set: {
                    appLanguage: payloadData.language
                }
            };
            let option = {
                lean: true,
                new: true
            };

            Service.UserServices.updateUser(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        },


    }, function (err, result) {
        callback(err, {})
    });
};

const listAllNationalityUser = function (payloadData, userData, callback) {

    let response = [];
    let data = [];
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getList: function (checkForUniqueKey, cb) {

            let criteria = {
                isDeleted: false,
                step: 3,
                nationality :{$ne:''},
                rejectReason : ''
            };

            let projection = {
                __v: 0,
                uniquieAppKey: 0,
            };
            let option = {
                lean: true,
                sort :{nationality:1}
                // skip: ((payloadData.pageNo - 1) * payloadData.limit),
                // limit: payloadData.limit
            };

            Service.MaidServices.getMaid(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response = result.map(obj => {
                            if (obj.nationality && obj.nationality !== null && obj.nationality!==undefined) {
                                return obj.nationality
                            }
                        });
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

    }, function (err, result) {
        data = response.filter(function (item, pos) {
            return response.indexOf(item) === pos;
        });
        callback(err, data)
    });
};

const listAllReligion = function (payloadData, userData, callback) {

    let response = [];
    let data = [];
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getLanguageList: function (checkForUniqueKey, cb) {

            /*let criteria = {
                isDeleted: false,
                step: 3,
                religion :{$ne:''}
            };

            let projection = {
                __v: 0,
                uniquieAppKey: 0,
            };
            let option = {
                lean: true,
                // skip: ((payloadData.pageNo - 1) * payloadData.limit),
                // limit: payloadData.limit
            };

            Service.MaidServices.getMaid(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response = result.map(obj => {
                            if (obj.religion && obj.religion !== null && obj.religion!==undefined && obj.religion!=='') {
                                return obj.religion
                            }
                        });
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },*/
            data = [
                'Buddhism',
                'Christian',
                'Gnosticism',
                'Hinduism',
                'Irreligious and atheist',
                'Islam',
                'Judaism',
                'Jainism',
                'Muslim',
                'Sikhism',
            ];
            cb()
        }

    }, function (err, result) {
        callback(err, data)
    });
};

const listAllAgencyUser = function (payloadData, userData, callback) {

    let response = [];
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        listAgency: function (checkForUniqueKey, cb) {

            let pipeline = [
                {
                    $match: {
                        isDeleted: false,
                        isBlocked: false,
                        countryName: {$regex: payloadData.country, $options: 'i'},
                    }
                },
                {
                    $graphLookup: {
                        from: "maids",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "agencyId",
                        as: "maidData",
                        restrictSearchWithMatch: {isDeleted: false, isBlocked: false}
                    }
                },
                {
                    $project: {
                        maidCount: {$size: "$maidData"},
                        maidData: 1,
                        profilePicURL: 1,
                        agencyName: 1,
                        registrationDate: 1

                    }
                },
                {
                    $unwind: "$maidData"
                },
                {
                    $group: {
                        _id: "$_id",
                        rating: {$avg: "$maidData.rating"},
                        profilePicURL: {$first: '$profilePicURL'},
                        agencyName: {$first: '$agencyName'},
                        maidCount: {$first: '$maidCount'},
                        registrationDate: {$first: '$registrationDate'},

                    }
                },
                {
                    $sort: {
                        registrationDate: 1
                    }
                },
                {$skip: ((payloadData.pageNo - 1) * payloadData.limit)},
                {$limit: payloadData.limit}
            ];

            if (payloadData.searchAgencyName) {

                var match = {
                    $match: {
                        agencyName: {$regex: payloadData.searchAgencyName, $options: "i"}
                    }
                };
                pipeline.push(match)
            }
            Models.Agency.aggregate(pipeline, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }

    }, function (err, result) {
        callback(err, response)
    });
};

const listAllMaidsUser = function (payloadData, userData, callback) {

    let offset = moment.tz(userData.timeZone).utcOffset();
    offset = offset * 60000;
    let response = {
        requestedMaids: [],
        suggestedMaids: [],
        otherMaids: [],
    };
    let languages = [];
    let days = [];
    let daysGreater24 = [];
    let key = "";
    let keyGreater24 = "";
    let maidIdToIgnore = [];
    let startTime = payloadData.startTime;

    let count = 0;
    let validAgency = [];
    let makAgency = [];
    let inValidMaids = [];
    let hourToadd =0
    async.autoInject({

        checkForUniqueKey: function (cb) {

            let time1 = moment.tz(payloadData.startTime,payloadData.deviceTimeZone).get('hour');
            let time2 =  moment.tz(payloadData.timeZone).get('hour');

            let minutes =  moment.tz(payloadData.timeZone).get('minutes');
            if(minutes > 0) hourToadd = 3;
            else  hourToadd = 2;
            console.log('$$$$$$$$$$$$$$$$$$$$$',minutes,time1, time2,time1 >= time2 + hourToadd, time1 + payloadData.duration > 23);

            let date1 = moment.tz(payloadData.startTime,payloadData.deviceTimeZone);
            let date2 = moment.tz(payloadData.startTime,payloadData.timeZone);

            console.log('dayyyyyyyyyyyyyyyyyyy',date2 > new Date() ,
                new Date() , date2);

            if(payloadData.timeZone === payloadData.deviceTimeZone){
                console.log('in first ifffffffffffffffff', payloadData.workDate > +new Date())
                if(payloadData.workDate > +new Date()){
                    if( time1 + payloadData.duration > 23){
                        if(!userData._id || userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK);
                        else callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK_AR);
                    } else cb()
                }
                else if(time1 >= time2 + hourToadd){
                        if( time1 + payloadData.duration > 23){
                            if(!userData._id || userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK);
                            else callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK_AR);
                        }
                        else {
                            console.log('herrrrrrrrrrrrrrrrrrr')
                            cb()
                        }
                }
                else {
                    if(!userData._id || userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TIME_ERROR);
                    else  callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TIME_ERROR_AR);
                }
            }
            else if(payloadData.workDate > new Date()){
                console.log('he111111111111111')
                if( time1 + payloadData.duration > 23){
                    if(!userData._id || userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK);
                    else callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK_AR);
                }
                else cb()
            }
            else {
                if(time1 >= time2 + hourToadd) {
                    if( time1 + payloadData.duration > 23){
                        if(!userData._id || userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK);
                        else callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK_AR);
                    }
                    else cb()
                }
                else {
                    if(!userData._id || userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TIME_ERROR);
                    else  callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TIME_ERROR_AR);
                }
            }
        },

        updateUsersLocation: function (checkForUniqueKey, cb) {
            if (userData && userData._id) {
                let criteria = {
                    _id: userData._id
                };
                let dataToUpdate = {
                    currentLocation: [payloadData.long, payloadData.lat],
                    timeZone : payloadData.deviceTimeZone
                };
                if (payloadData.locationName) {
                    dataToUpdate.locationName = payloadData.locationName;
                }
                let option = {new: true};
                Service.UserServices.updateUser(criteria, dataToUpdate, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null)
                    }
                })
            } else {
                cb(null)
            }
        },

        getValidAgency: function (updateUsersLocation, cb) {
            let criteria = {
                isDeleted: false,
                isBlocked: false
            };
            if (payloadData.agencyId && payloadData.agencyId.length) {
                let payloadAgencyId = payloadData.agencyId.map(obj => {
                    return mongoose.Types.ObjectId(obj)
                })
                criteria._id = {$in: payloadAgencyId}
            }
            //criteria.countryName= {$regex: payloadData.country, $options: 'i'};
          //  console.log('@@@@@@@@@@@@@@@@@',criteria)
            Models.Agency.aggregate([
                {
                    $geoNear: {
                        near: {type: "Point", coordinates: [payloadData.long, payloadData.lat]},
                        spherical: true,
                        distanceField: "distance",
                        distanceMultiplier: 0.000621371,                   //convert to miles  3963.2miles 0.000621371
                        maxDistance: 100000000,                            //in metres
                        query: criteria,
                    }

                },
               /* {
                    $addFields: {
                        distanceDifference: {$subtract: ["$distance", "$radius"]}
                    }
                },
                {
                    $match: {
                        distanceDifference: {$lt: 0}
                    }
                }*/
            ], function (err, result) {
              //  console.log("validAgency__666", result)
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        validAgency = result.map(obj => {
                            return obj._id
                        });
                      //  console.log("validAgency_id", validAgency)

                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getMakAgencyId: function (updateUsersLocation, cb) {
            Service.AgencyServices.getAgency({agencyType: {$nin: [Config.APP_CONSTANTS.DATABASE.AGENCY_TYPE.NORMAL]}}, {}, {lean: true}, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        makAgency = result.map(obj => {
                            return mongoose.Types.ObjectId(obj._id)
                        });
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getValidMaids: function (getMakAgencyId, cb) {
            let criteria = {
                agencyId: {$in: makAgency},
                isDeleted: false,
                isBlocked: false,
                isVerified: true,
                acceptAgreement:true
            };

            Models.Maids.aggregate([
                {
                    $geoNear: {
                        near: {type: "Point", coordinates: [payloadData.long, payloadData.lat]},
                        spherical: true,
                        distanceField: "distance",
                        distanceMultiplier: 0.000621371,                   //convert to miles  3963.2miles 0.000621371
                        maxDistance: 8046720,                            //in metres
                        query: criteria
                    }
                },
                {
                    $addFields: {
                        distanceDifference: {$subtract: ["$distance", "$radius"]}
                    }
                },
                {
                    $match: {
                        distanceDifference: {$gt: 0}
                    }
                }
            ], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        inValidMaids = result.map(obj => { // out of radius
                         //   console.log('444444444444444',JSON.stringify(obj))
                            return mongoose.Types.ObjectId(obj._id)
                        });
                    console.log("____________________in validMaids_________________", inValidMaids)

                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getAvailableSlots: function (updateUsersLocation, cb) {

            if (payloadData.workDate && payloadData.duration && payloadData.startTime) {
                let workDate = payloadData.workDate;
                let endTime = +moment.tz(payloadData.hour, payloadData.timeZone).toDate() + ((payloadData.duration) * 60 * 60 * 1000);
                let criteria = {

                    transactionId : {$ne : ''},
                    agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                        Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                    date:  moment.tz(payloadData.hour,payloadData.timeZone).get('date'),
                };

                let projection = {};
                let option = {
                    lean: true
                };
                console.log("___endTime and Start time__________", endTime, startTime);

                Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {

                        if (result && result.length) {
                            let localArray = result.filter(obj => {
                                console.log('maiddddddddddddddd',obj.maidId)
                                console.log("============maidIdToIgnore=",obj.startTime , endTime,obj.startTime <= endTime)
                                console.log("============maidIdToIgnore 111",endTime , obj.bufferEndTime, endTime< obj.bufferEndTime)
                                return (obj.startTime <= endTime && payloadData.hour < obj.bufferEndTime)
                            });
                            localArray.map(obj => {
                                maidIdToIgnore.push(obj.maidId);
                            });
                        console.log("============maidIdToIgnore=============",maidIdToIgnore)
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        listMaids: function (getMakAgencyId, getValidMaids, getAvailableSlots, cb) {

            let geoNear = {
                near: {type: "Point", coordinates: [payloadData.long, payloadData.lat]},
                spherical: true,
                distanceField: "distance",
                //distanceMultiplier: 0.001,                   //convert to kilometre 6371km 0.001
                distanceMultiplier: 0.000621371,                   //convert to miles  3963.2miles 0.000621371
                maxDistance: 8046720,
                query: {
                    isDeleted: false,
                    isBlocked: false,
                    isVerified: true,
                    acceptAgreement: true,
                    countryName: {$regex: payloadData.country, $options: 'i'},
                    agencyId: {$in: validAgency},
                },
            };

            let criteria = {
                isDeleted: false,
                isBlocked: false,
                isVerified: true,
                acceptAgreement: true,
                countryName: {$regex: payloadData.country, $options: 'i'},
                agencyId: {$in: validAgency},
            };

            if (maidIdToIgnore && maidIdToIgnore.length) {

                let combineArr = inValidMaids.concat(maidIdToIgnore)
                geoNear.query._id = {$nin: combineArr}
            }
            else {
                if (inValidMaids && inValidMaids.length) {
                    geoNear.query._id = {$nin: inValidMaids}
                } else {

                }
            }

            let startHour = moment.tz(payloadData.hour,payloadData.timeZone).get('hour');

            console.log('startHourrrrrrrr', startHour)

            for (let i = startHour; i < (startHour + 1 + payloadData.duration); i++) {
                (function (i) {
                 //   console.log("_i_", i);
                    if (i > 24) {
                        daysGreater24.push(i - 24)
                    } else {
                        days.push(i)
                    }
                }(i))
            }
            let criteriaDay = moment(parseInt(payloadData.hour)).tz(payloadData.timeZone).day();

            console.log('criteriaDay........', criteriaDay,days)

            key = 'timeSlot.' + criteriaDay;
            keyGreater24 = 'timeSlot.' + (criteriaDay + 1);

            console.log('key...........', key);

            console.log('keyGreater24', keyGreater24,'daysGreater24',daysGreater24);

            geoNear.query[key] = {$all: days};
            if (daysGreater24 && daysGreater24.length) {
                geoNear.query[keyGreater24] = {$all: daysGreater24};
            }

          //  console.log('criteria.............', geoNear.query);

            if (payloadData.nationality) {
                geoNear.query.nationality = {$in: payloadData.nationality}
            }
            if (payloadData.religion) {
                geoNear.query.religion = {$in: payloadData.religion}
            }
            if (payloadData.languages) {
                async.each(payloadData.languages, function (file, cb) {
                    languages.push(mongoose.Types.ObjectId(file));
                    cb(null)
                }, function (err) {
                    geoNear.query.languages = {$in: languages}
                })
            }
            if (payloadData.gender) {
                if (payloadData.gender === "MALE") {
                    geoNear.query.gender = Config.APP_CONSTANTS.DATABASE.GENDER.MALE
                }
                else if (payloadData.gender === "FEMALE") {
                    geoNear.query.gender = Config.APP_CONSTANTS.DATABASE.GENDER.FEMALE
                }
                else if (payloadData.gender === "BOTH") {
                    geoNear.query.gender = {$in: [Config.APP_CONSTANTS.DATABASE.GENDER.MALE, Config.APP_CONSTANTS.DATABASE.GENDER.FEMALE]}
                }
            }
            if (payloadData.searchMaidName) {

                geoNear.query.$or = [
                    {fullName: new RegExp(payloadData.searchMaidName,'i')},
                    {firstName: new RegExp(payloadData.searchMaidName,'i')}
                ]
            }

          //  console.log("___criteria____geoNear______", JSON.stringify(geoNear));

            Models.Maids.aggregate([
                {
                    $geoNear: geoNear
                },
               /* {
                    $match: criteria
                },*/
                {$skip: ((payloadData.pageNo - 1) * payloadData.limit)},
                {$limit: payloadData.limit},
                {
                    $lookup: {
                        from: "languages",
                        localField: "languages",
                        foreignField: "_id",
                        as: "languages"
                    }
                },
                {
                    $lookup: {
                        from: "agencies",
                        localField: "agencyId",
                        foreignField: "_id",
                        as: "agencyId"
                    }
                },
                {
                    $unwind:{
                        path: '$agencyId',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $project: {
                        _id: 1,
                        gender: 1,
                        email: 1,
                        country: 1,
                        countryCode: 1,
                        phoneNo: 1,
                        makId: 1,
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                        agencyImage: "$agencyId.profilePicURL",
                        locationName: 1,
                        languages: 1,
                        price: 1,
                        timeSlot: 1,
                        actualPrice: 1,
                        makPrice: 1,
                        experience: 1,
                        nationality: 1,
                        dob: 1,
                        description: 1,
                        lastName: 1,
                        firstName: 1,
                        feedBack: 1,
                        currentLocation: 1,
                        documentPicURL: 1,
                        profilePicURL: 1,
                        distance: 1,
                        religion: 1,
                        currency: 1,
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {
                            isDeleted: false,
                            isBlocked: false,
                            reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER
                        }
                    }
                },
                {$sort: { 'reviewData.createdOn': -1 } },

            ], function (err, result) {
               //console.log('ffffffffffffffffff',err,result.length)
                if (result && result.length) {
                    let localFavourites;
                    if (userData.favouriteMaidId && userData.favouriteMaidId.length) {
                        localFavourites = userData.favouriteMaidId.map(obj => {
                            return JSON.stringify(obj)
                        });
                    } else {
                        localFavourites = []
                    }

                    for (let i = 0; i < result.length; i++) {
                        result[i].count = count;
                        if (localFavourites && localFavourites.length) {
                            let flag = _.indexOf(localFavourites, JSON.stringify(result[i]._id));
                            if (flag >= 0) {
                                result[i].isFavourite = true;
                            } else {
                                result[i].isFavourite = false;
                            }
                        } else {
                            result[i].isFavourite = false;
                        }


                        if (result[i].experience == 2) {
                            result[i].experience1 = result[i].experience;
                            result[i].experience = 'Up to 2 years';
                        }
                        if (result[i].experience == 5) {
                            result[i].experience1 = result[i].experience;
                            result[i].experience = '3 to 5 years';
                        }
                        if (result[i].experience == 6) {
                            result[i].experience1 = result[i].experience;
                            result[i].experience = 'Above 5 years';
                        }
                        result[i].distance = result[i].distance.toFixed(1);

                        let reduceResult = {cleaning: 0, ironing: 0, cooking: 0, childCare: 0};
                        if (result[i].reviewData.length > 0) {
                            let countCleaning = 0;
                            let countIroning = 0;
                            let countCooking = 0;
                            let countChildCare = 0;
                            reduceResult = result[i].reviewData.reduce((prev, curr) => {
                                prev.cleaning += curr.maidRating.cleaning;
                                prev.ironing += curr.maidRating.ironing;
                                prev.cooking += curr.maidRating.cooking;
                                prev.childCare += curr.maidRating.childCare;

                                if (curr.maidRating.cleaning > 0) {
                                    countCleaning = countCleaning + 1;
                                }
                                if (curr.maidRating.ironing > 0) {
                                    countIroning = countIroning + 1;
                                }
                                if (curr.maidRating.cooking > 0) {
                                    countCooking = countCooking + 1;
                                }
                                if (curr.maidRating.childCare > 0) {
                                    countChildCare = countChildCare + 1;
                                }
                                return prev
                            }, reduceResult);
                            result[i].avgCleaning = (reduceResult.cleaning / countCleaning);
                            result[i].avgIroning = (reduceResult.ironing / countIroning);
                            result[i].avgCooking = (reduceResult.cooking / countCooking);
                            result[i].avgChildCare = (reduceResult.childCare / countChildCare);

                        }
                        else {
                            result[i].avgCleaning = 0;
                            result[i].avgIroning = 0;
                            result[i].avgCooking = 0;
                            result[i].avgChildCare = 0;
                        }
                    }

                    if (payloadData.sort === 'RATING') {
                        result = _.sortBy(result, ['avgCleaning', 'avgIroning', 'avgCooking', 'avgChildCare']);
                        response.requestedMaids = _.reverse(result)
                    }
                    else if (payloadData.sort === 'EXPERIENCE') {
                        result = _.sortBy(result, ['experience1']);
                        response.requestedMaids = _.reverse(result)
                    }
                    else if (payloadData.sort === 'DISTANCE') {
                        result = _.sortBy(result, ['distance']);
                        response.requestedMaids = _.reverse(result)
                    }
                    else if (payloadData.sort === 'PRICE_LOW') {
                        result = _.sortBy(result, ['price']);
                        response.requestedMaids = result
                    }
                    else if (payloadData.sort === 'PRICE_HIGH') {
                        result = _.sortBy(result, ['price']);
                        response.requestedMaids = _.reverse(result)
                    }
                    else {
                        result = _.sortBy(result, ['distance']);
                        response.requestedMaids = _.reverse(result)
                    }
                    cb()
                } else {
                    cb(null)
                }
            })
        },

        listSuggestedMaids: function (listMaids, cb) {
            if(!payloadData.searchMaidName){
                let geoNear = {
                    near: {type: "Point", coordinates: [payloadData.long, payloadData.lat]},
                    spherical: true,
                    distanceField: "distance",
                    //distanceMultiplier: 0.001,                   //convert to kilometre 6371km 0.001
                    distanceMultiplier: 0.000621371,                   //convert to miles  3963.2miles 0.000621371
                    maxDistance : 8046720,
                    query: {
                        isDeleted: false,
                        isBlocked: false,
                        acceptAgreement:true,isVerified: true,
                        countryName: {$regex: payloadData.country, $options: 'i'},
                        agencyId: {$in: validAgency},
                    },
                };
                let criteria = {
                    isDeleted: false,
                    isBlocked: false,
                    acceptAgreement:true,isVerified: true,
                    countryName: {$regex: payloadData.country, $options: 'i'},
                    agencyId: {$in: validAgency},
                };
                if (payloadData.nationality) {
                    geoNear.query.nationality = {$in: payloadData.nationality}
                }
                /* if (payloadData.searchMaidName) {
                     geoNear.query.$or = [
                         {firstName: {$regex: payloadData.searchMaidName, $options: "si"}},
                         {lastName: {$regex: payloadData.searchMaidName, $options: "si"}}
                     ]
                 }*/
                if (maidIdToIgnore && maidIdToIgnore.length) {
                    let combineArr = inValidMaids.concat(maidIdToIgnore)
                    geoNear.query._id = {$nin: combineArr}
                }
                else {
                    geoNear.query._id = {$nin: inValidMaids}
                }

                geoNear.query[key] = {$all: days};

                //  console.log("=============criteria[key]==========",inValidMaids,maidIdToIgnore)
                if (daysGreater24 && daysGreater24.length) {
                    geoNear.query[keyGreater24] = {$all: daysGreater24};
                }

                //console.log('sugesssssssssssted........',geoNear.query)
                Models.Maids.aggregate([
                    {
                        $geoNear: geoNear
                    },
                    /*  {
                          $match: criteria
                      },*/
                    {
                        $sort: {
                            distance: -1,
                        }
                    },
                    {
                        $lookup: {
                            from: "languages",
                            localField: "languages",
                            foreignField: "_id",
                            as: "languages"
                        }
                    },
                    {
                        $lookup: {
                            from: "agencies",
                            localField: "agencyId",
                            foreignField: "_id",
                            as: "agencyId"
                        }
                    },
                    {
                        $unwind: "$agencyId"
                    },
                    {
                        $project: {
                            _id: 1,
                            gender: 1,
                            email: 1,
                            country: 1,
                            countryCode: 1,
                            phoneNo: 1,
                            makId: 1,
                            agencyId: "$agencyId._id",
                            agencyName: "$agencyId.agencyName",
                            agencyImage: "$agencyId.profilePicURL",
                            locationName: 1,
                            rating: 1,
                            timeSlot: 1,
                            languages: 1,
                            price: 1,
                            actualPrice: 1,
                            makPrice: 1,
                            experience: 1,
                            nationality: 1,
                            dob: 1,
                            description: 1,
                            lastName: 1,
                            firstName: 1,
                            feedBack: 1,
                            currentLocation: 1,
                            documentPicURL: 1,
                            profilePicURL: 1,
                            distance: 1,
                            religion: 1,
                            currency: 1
                        }
                    },
                    {
                        $graphLookup: {
                            from: "reviews",
                            startWith: "$_id",
                            connectFromField: "_id",
                            connectToField: "maidId",
                            as: "reviewData",
                            restrictSearchWithMatch: {
                                isDeleted: false,
                                isBlocked: false,
                                reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER
                            }
                        }
                    },
                    {$sort: { 'reviewData.createdOn': -1 } },
                ], function (err, result) {

                   // console.log("=========result============",result,err)
                    if (result && result.length) {
                        for (let i = 0; i < result.length; i++) {
                            if (userData.favouriteMaidId && userData.favouriteMaidId.length) {
                                if (userData.favouriteMaidId.indexOf(result[i]._id)) {
                                    result[i].isFavourite = true;
                                } else {
                                    result[i].isFavourite = false;
                                }
                            }
                            else {
                                result[i].isFavourite = false;
                            }

                            if (result[i].experience === 2) {
                                result[i].experience = 'Up to 2 years';
                            }
                            if (result[i].experience === 5) {
                                result[i].experience = '3 to 5 years';
                            }
                            if (result[i].experience === 6) {
                                result[i].experience = 'Above 5 years';
                            }
                            result[i].distance = result[i].distance.toFixed(1);

                            let reduceResult = {cleaning: 0, ironing: 0, cooking: 0, childCare: 0};
                            if (result[i].reviewData.length > 0) {
                                let countCleaning = 0;
                                let countIroning = 0;
                                let countCooking = 0;
                                let countChildCare = 0;
                                reduceResult = result[i].reviewData.reduce((prev, curr) => {
                                    prev.cleaning += curr.maidRating.cleaning;
                                    prev.ironing += curr.maidRating.ironing;
                                    prev.cooking += curr.maidRating.cooking;
                                    prev.childCare += curr.maidRating.childCare;

                                    if (curr.maidRating.cleaning > 0) {
                                        countCleaning = countCleaning + 1;
                                    }
                                    if (curr.maidRating.ironing > 0) {
                                        countIroning = countIroning + 1;
                                    }
                                    if (curr.maidRating.cooking > 0) {
                                        countCooking = countCooking + 1;
                                    }
                                    if (curr.maidRating.childCare > 0) {
                                        countChildCare = countChildCare + 1;
                                    }
                                    return prev
                                }, reduceResult);
                                result[i].avgCleaning = (reduceResult.cleaning / countCleaning);
                                result[i].avgIroning = (reduceResult.ironing / countIroning);
                                result[i].avgCooking = (reduceResult.cooking / countCooking);
                                result[i].avgChildCare = (reduceResult.childCare / countChildCare);

                            }
                            else {
                                result[i].avgCleaning = 0;
                                result[i].avgIroning = 0;
                                result[i].avgCooking = 0;
                                result[i].avgChildCare = 0;
                            }
                        }
                        result = _.sortBy(result, ['avgCleaning', 'avgIroning', 'avgCooking', 'avgChildCare']);

                        //console.log("=====================aaaaaaaaaaaaaaaaaaa==================",result)
                        //response.suggestedMaids.push(result);
                       response.suggestedMaids.push(result[result.length - 1]);
                        //console.log("  response.requestedMaids "+ JSON.stringify(response.requestedMaids));
                        response.requestedMaids = response.requestedMaids.filter(obj => {
                            //console.log("___________in filter________________", (JSON.stringify(obj._id) !== JSON.stringify(result[result.length - 1]._id)))
                            return (JSON.stringify(obj._id) !== JSON.stringify(result[result.length - 1]._id))
                        });
                        cb(null)
                    } else {
                        cb(null)
                    }
                })
            }
           else {
                response.suggestedMaids =[];
                cb()
            }
        },

        listOtherMaids: function (listSuggestedMaids, cb) {
            // if(response.requestedMaids.length == 0 && response.suggestedMaids.length == 0){
            let geoNear = {
                near: {type: "Point", coordinates: [payloadData.long, payloadData.lat]},
                spherical: true,
                distanceField: "distance",
                distanceMultiplier: 0.000621371,                   //convert to miles  3963.2miles 0.000621371
                maxDistance: 8046720,                            //in metres
                query: {
                    isDeleted: false,
                    isBlocked: false,
                    acceptAgreement:true,
                    isVerified: true,
                    countryName: {$regex: payloadData.country, $options: 'i'},
                    agencyId: {$in: validAgency},
                    },
            };

            let criteria = {

            };
            if (payloadData.nationality) {
                geoNear.query.nationality = {$in: payloadData.nationality}
            }
            if (payloadData.searchMaidName) {

                geoNear.query.$or = [
                    {fullName: new RegExp(payloadData.searchMaidName,'i')},
                    {firstName: new RegExp(payloadData.searchMaidName,'i')}
                ]
            }

            if (maidIdToIgnore && maidIdToIgnore.length) {

                let combineArr = inValidMaids.concat(maidIdToIgnore)
                geoNear.query._id = {$nin: combineArr}
            }
            else {
                if (inValidMaids && inValidMaids.length) {
                    geoNear.query._id = {$nin: inValidMaids}
                } else {

                }
            }

            let startHour = moment.tz(payloadData.hour,payloadData.timeZone).get('hour');

            console.log('startHourrrrrrrr', startHour)

            for (let i = startHour; i < (startHour + 1 + payloadData.duration); i++) {
                (function (i) {
                    //   console.log("_i_", i);
                    if (i > 24) {
                        daysGreater24.push(i - 24)
                    } else {
                        days.push(i)
                    }
                }(i))
            }
            let criteriaDay = moment(parseInt(payloadData.hour)).tz(payloadData.timeZone).day();

            console.log('criteriaDay........', criteriaDay,days)

            key = 'timeSlot.' + criteriaDay;
            keyGreater24 = 'timeSlot.' + (criteriaDay + 1);

            console.log('key...........', key);

            console.log('keyGreater24', keyGreater24,'daysGreater24',daysGreater24);

            geoNear.query[key] = {$all: days};
            if (daysGreater24 && daysGreater24.length) {
                geoNear.query[keyGreater24] = {$all: daysGreater24};
            }

            //console.log("___criteria____geoNear______", geoNear.query.$or);
            Models.Maids.aggregate([
                {
                    $geoNear: geoNear
                },
              /*  {
                    $match: criteria
                },*/
                {$skip: ((payloadData.pageNo - 1) * payloadData.limit)},
                {$limit: payloadData.limit},
                {
                    $lookup: {
                        from: "languages",
                        localField: "languages",
                        foreignField: "_id",
                        as: "languages"
                    }
                },
                {
                    $lookup: {
                        from: "agencies",
                        localField: "agencyId",
                        foreignField: "_id",
                        as: "agencyId"
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        _id: 1,
                        gender: 1,
                        email: 1,
                        country: 1,
                        countryCode: 1,
                        phoneNo: 1,
                        makId: 1,
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                        agencyImage: "$agencyId.profilePicURL",
                        locationName: 1,
                        languages: 1,
                        price: 1,
                        actualPrice: 1,
                        makPrice: 1,
                        experience: 1,
                        nationality: 1,
                        dob: 1,
                        description: 1,
                        lastName: 1,
                        firstName: 1,
                        feedBack: 1,
                        currentLocation: 1,
                        documentPicURL: 1,
                        profilePicURL: 1,
                        distance: 1,
                        religion: 1,
                        currency: 1,
                        timeSlot:1,
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {
                            isDeleted: false,
                            isBlocked: false,
                            reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER
                        }
                    }
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "serviceData",
                        restrictSearchWithMatch: {
                            workDate:{$gte:new Date().setUTCHours(0,0,0,0)}
                        }
                    }
                },
                {
                    $project: {
                      /*  "timeSlot.0": {
                            $setDifference: [
                                "$timeSlot.0",
                                {
                                    $reduce: {
                                        input: {
                                            $filter: {
                                                input: "$serviceData",
                                                as: "item",
                                                cond: {
                                                    $eq: [
                                                        {$dayOfWeek: {$add: [new Date(0), "$$item.workDate"]}},
                                                        1,
                                                    ]
                                                }
                                            }
                                        },
                                        initialValue: [],
                                        in: {
                                            $concatArrays: [
                                                "$$value",
                                                [{$hour: {$add: [new Date(0), "$$this.startTime"]}}]
                                            ]
                                        }
                                    }
                                }
                            ]
                        },
                        "timeSlot.1": {
                            $setDifference: [
                                "$timeSlot.1",
                                {
                                    $reduce: {
                                        input: {
                                            $filter: {
                                                input: "$serviceData",
                                                as: "item",
                                                cond: {
                                                    $eq: [
                                                        {$dayOfWeek: {$add: [new Date(0), "$$item.workDate"]}},
                                                        1,
                                                    ]
                                                }
                                            }
                                        },
                                        initialValue: [],
                                        in: {
                                            $concatArrays: [
                                                "$$value",
                                                [{$hour: {$add: [new Date(0), "$$this.startTime"]}}]
                                            ]
                                        }
                                    }
                                }
                            ]
                        },
                        "timeSlot.2": {
                            $setDifference: [
                                "$timeSlot.2",
                                {
                                    $reduce: {
                                        input: {
                                            $filter: {
                                                input: "$serviceData",
                                                as: "item",
                                                cond: {
                                                    $eq: [
                                                        {$dayOfWeek: {$add: [new Date(0), "$$item.workDate"]}},
                                                        2,
                                                    ]
                                                }
                                            }
                                        },
                                        initialValue: [],
                                        in: {
                                            $concatArrays: [
                                                "$$value",
                                                [{$hour: {$add: [new Date(0), "$$this.startTime"]}}]
                                            ]
                                        }
                                    }
                                }
                            ]
                        },
                        "timeSlot.3": {
                            $setDifference: [
                                "$timeSlot.3",
                                {
                                    $reduce: {
                                        input: {
                                            $filter: {
                                                input: "$serviceData",
                                                as: "item",
                                                cond: {
                                                    $eq: [
                                                        {$dayOfWeek: {$add: [new Date(0), "$$item.workDate"]}},
                                                        3,
                                                    ]
                                                }
                                            }
                                        },
                                        initialValue: [],
                                        in: {
                                            $concatArrays: [
                                                "$$value",
                                                [{$hour: {$add: [new Date(0), "$$this.startTime"]}}]
                                            ]
                                        }
                                    }
                                }
                            ]
                        },
                        "timeSlot.4": {
                            $setDifference: [
                                "$timeSlot.4",
                                {
                                    $reduce: {
                                        input: {
                                            $filter: {
                                                input: "$serviceData",
                                                as: "item",
                                                cond: {
                                                    $eq: [
                                                        {$dayOfWeek: {$add: [new Date(0), "$$item.workDate"]}},
                                                        4,
                                                    ]
                                                }
                                            }
                                        },
                                        initialValue: [],
                                        in: {
                                            $concatArrays: [
                                                "$$value",
                                                [{$hour: {$add: [new Date(0), "$$this.startTime"]}}]
                                            ]
                                        }
                                    }
                                }
                            ]
                        },
                        "timeSlot.5": {
                            $setDifference: [
                                "$timeSlot.5",
                                {
                                    $reduce: {
                                        input: {
                                            $filter: {
                                                input: "$serviceData",
                                                as: "item",
                                                cond: {
                                                    $eq: [
                                                        {$dayOfWeek: {$add: [new Date(0), "$$item.workDate"]}},
                                                        5,
                                                    ]
                                                }
                                            }
                                        },
                                        initialValue: [],
                                        in: {
                                            $concatArrays: [
                                                "$$value",
                                                [{$hour: {$add: [new Date(0), "$$this.startTime"]}}]
                                            ]
                                        }
                                    }
                                }
                            ]
                        },
                        "timeSlot.6": {
                            $setDifference: [
                                "$timeSlot.6",
                                {
                                    $reduce: {
                                        input: {
                                            $filter: {
                                                input: "$serviceData",
                                                as: "item",
                                                cond: {
                                                    $eq: [
                                                        {$dayOfWeek: {$add: [new Date(0), "$$item.workDate"]}},
                                                        6,
                                                    ]
                                                }
                                            }
                                        },
                                        initialValue: [],
                                        in: {
                                            $concatArrays: [
                                                "$$value",
                                                [{$hour: {$add: [new Date(0), "$$this.startTime"]}}]
                                            ]
                                        }
                                    }
                                }
                            ]
                        },*/
                        "timeSlot.0": {
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                1,
                                            ]
                                        }
                                    }
                                },
                                initialValue:'$timeSlot.0',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]

                                        }
                                    }
                                }

                            }
                        },
                        'timeSlot.1' :{
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                2,
                                            ]
                                        }
                                    }
                                },
                                initialValue: '$timeSlot.1',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                    {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                        },
                        'timeSlot.2' :{
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                3,
                                            ]
                                        }
                                    }
                                },
                                initialValue: '$timeSlot.2',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                    {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                        },
                        'timeSlot.3' :{
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                4,
                                            ]
                                        }
                                    }
                                },
                                initialValue: '$timeSlot.3',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                    {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                        },
                        'timeSlot.4' :{
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                5,
                                            ]
                                        }
                                    }
                                },
                                initialValue: '$timeSlot.4',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                    {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                        },
                        'timeSlot.5' :{
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                6,
                                            ]
                                        }
                                    }
                                },
                                initialValue: '$timeSlot.5',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                    {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }

                                }

                            }
                        },
                        'timeSlot.6' :{
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                7,
                                            ]
                                        }
                                    }
                                },
                                initialValue: '$timeSlot.6',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                    {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                        },
                        _id: 1,
                        gender: 1,
                        email: 1,
                        country: 1,
                        countryCode: 1,
                        phoneNo: 1,
                        makId: 1,
                        agencyId: 1,
                        agencyName: 1,
                        agencyImage: 1,
                        locationName: 1,
                        languages: 1,
                        price: 1,
                        actualPrice: 1,
                        makPrice: 1,
                        experience: 1,
                        nationality: 1,
                        dob: 1,
                        description: 1,
                        lastName: 1,
                        firstName: 1,
                        feedBack: 1,
                        currentLocation: 1,
                        documentPicURL: 1,
                        profilePicURL: 1,
                        distance: 1,
                        religion: 1,
                        currency: 1,
                        reviewData:1,
                    }
                },
                {$sort: { 'reviewData.createdOn': -1 } },

            ], function (err, result) {
                if (result && result.length) {
                    let localFavourites;
                    if (userData.favouriteMaidId && userData.favouriteMaidId.length) {
                        localFavourites = userData.favouriteMaidId.map(obj => {
                            return JSON.stringify(obj)
                        });
                    } else {
                        localFavourites = []
                    }


                    for (let i = 0; i < result.length; i++) {
                        result[i].day=new Date().getDay()
                        result[i].count = count;
                        if (localFavourites && localFavourites.length) {
                            let flag = _.indexOf(localFavourites, JSON.stringify(result[i]._id));
                            if (flag >= 0) {
                                result[i].isFavourite = true;
                            } else {
                                result[i].isFavourite = false;
                            }
                        } else {
                            result[i].isFavourite = false;
                        }


                        if (result[i].experience === 2) {
                            result[i].experience1 = result[i].experience;
                            result[i].experience = 'Up to 2 years';
                        }
                        if (result[i].experience === 5) {
                            result[i].experience1 = result[i].experience;
                            result[i].experience = '3 to 5 years';
                        }
                        if (result[i].experience === 6) {
                            result[i].experience1 = result[i].experience;
                            result[i].experience = 'Above 5 years';
                        }
                        result[i].distance = result[i].distance.toFixed(1);

                        let reduceResult = {cleaning: 0, ironing: 0, cooking: 0, childCare: 0};
                        if (result[i].reviewData.length > 0) {
                            let countCleaning = 0;
                            let countIroning = 0;
                            let countCooking = 0;
                            let countChildCare = 0;
                            reduceResult = result[i].reviewData.reduce((prev, curr) => {
                                prev.cleaning += curr.maidRating.cleaning;
                                prev.ironing += curr.maidRating.ironing;
                                prev.cooking += curr.maidRating.cooking;
                                prev.childCare += curr.maidRating.childCare;

                                if (curr.maidRating.cleaning > 0) {
                                    countCleaning = countCleaning + 1;
                                }
                                if (curr.maidRating.ironing > 0) {
                                    countIroning = countIroning + 1;
                                }
                                if (curr.maidRating.cooking > 0) {
                                    countCooking = countCooking + 1;
                                }
                                if (curr.maidRating.childCare > 0) {
                                    countChildCare = countChildCare + 1;
                                }
                                return prev
                            }, reduceResult);
                            result[i].avgCleaning = (reduceResult.cleaning / countCleaning);
                            result[i].avgIroning = (reduceResult.ironing / countIroning);
                            result[i].avgCooking = (reduceResult.cooking / countCooking);
                            result[i].avgChildCare = (reduceResult.childCare / countChildCare);

                        }
                        else {
                            result[i].avgCleaning = 0;
                            result[i].avgIroning = 0;
                            result[i].avgCooking = 0;
                            result[i].avgChildCare = 0;
                        }
                    }

                    if (payloadData.sort == 'RATING') {
                        result = _.sortBy(result, ['avgCleaning', 'avgIroning', 'avgCooking', 'avgChildCare']);
                        response.otherMaids = _.reverse(result)
                    }
                    else if (payloadData.sort == 'EXPERIENCE') {
                        result = _.sortBy(result, ['experience1']);
                        response.otherMaids = _.reverse(result)
                    }
                    else if (payloadData.sort == 'DISTANCE') {
                        result = _.sortBy(result, ['distance']);
                        response.otherMaids = _.reverse(result)
                    }
                    else if (payloadData.sort == 'PRICE_LOW') {
                        result = _.sortBy(result, ['price']);
                        response.otherMaids = result
                    }
                    else if (payloadData.sort == 'PRICE_HIGH') {
                        result = _.sortBy(result, ['price']);
                        response.otherMaids = _.reverse(result)
                    }
                    else {
                        result = _.sortBy(result, ['distance']);
                        response.otherMaids = _.reverse(result)
                    }
                    cb()
                } else {
                    response.otherMaids = [];
                    cb(null)
                }
            })
        }

    }, function (err, result) {

        callback(err, response)
    });
};

const bookService = function (payloadData, userData, callback) {
    if (userData && userData._id) {

        let serviceId = [];
        let agencyId = null;
        let price = 0;
        let commission = 0;
        let actualPrice = 0;
        let makPrice = 0;
        let serviceMakId = parseInt(Date.now());
        let countryName = "";
        let maidRadius;
        let maidToken;
        let notificationDataMaid = {};
        let bookingNumber;
        let maidId;
        let hourToadd =0;
        async.autoInject({

            checkForUniqueKey: function (cb) {

                if(userData.profileComplete && !userData.isVerified)
                    return callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.VERIFY_USER_BOOKING, {})
                else {
                    let time1 = moment.tz(parseInt(payloadData.serviceData[0].startTime), payloadData.deviceTimeZone).get('hour');
                    let time2 = moment.tz(payloadData.timeZone).get('hour');

                    let minutes = moment.tz(payloadData.timeZone).get('minutes');

                    if (minutes > 0) hourToadd = 3;
                    else hourToadd = 2;

                    console.log('$$$$$$$$$$$$$$$$$$$$$', time1, time2, time1 >= time2 + hourToadd);

                    let date1 = moment.tz(parseInt(payloadData.serviceData[0].startTime), payloadData.deviceTimeZone);
                    let date2 = moment.tz(parseInt(payloadData.hour), payloadData.timeZone);

                    console.log('dayyyyyyyyyyyyyyyyyyy', date2 > new Date(),
                        new Date(), date2);

                    if (payloadData.timeZone === payloadData.deviceTimeZone) {
                        console.log('in first ifffffffffffffffff', parseInt(payloadData.serviceData[0].workDate) > +new Date())

                        if (parseInt(payloadData.serviceData[0].workDate) > +new Date()) {
                            if (time1 + payloadData.duration > 23) {
                                if(userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                    callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK);
                                else  callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK_AR);
                            } else cb()
                        }
                        else if (time1 >= time2 + hourToadd) {
                            console.log('timeeeeeeeeeeeeeeeeeeee', time1 + payloadData.duration)
                            if (time1 + parseInt(payloadData.serviceData[0].duration) > 23) {
                                if(userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                    callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK);
                                else  callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK_AR);
                            }
                            else {
                                console.log('herrrrrrrrrrrrrrrrrrr')
                                cb()
                            }
                        }
                        else {
                            if(userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TIME_ERROR);
                            else  callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TIME_ERROR_AR);
                        }
                    }
                    else if (parseInt(payloadData.serviceData[0].workDate) > new Date()) {
                        console.log('he111111111111111')
                        if (time1 + parseInt(payloadData.serviceData[0].duration) > 23) {
                            if(userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK);
                            else  callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK_AR);
                        } else cb()

                    }
                    else {
                        if (time1 >= time2 + hourToadd) {
                            if (time1 + parseInt(payloadData.serviceData[0].duration) > 23) {
                                if(userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                    callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK);
                                else  callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK_AR);
                            } else cb();
                        }
                        else {
                            if(userData.appLanguage === Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TIME_ERROR);
                            else  callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TIME_ERROR_AR);
                        }
                    }
                }
            },
            // checkMaid :(checkForUniqueKey,cb)=>{
            //     let criteria ={
            //         maidId : payloadData.maidId,
            //         startTime :parseInt(payloadData.serviceData[0].startTime),
            //         date:  moment.tz(parseInt(payloadData.serviceData[0].startTime),payloadData.timeZone).get('date'),
            //        // transactionId : {$ne : ''},
            //         agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
            //             Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
            //         deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
            //     };
            //     console.log('bokkkkkkkkkkkkkkk serviveeeeeeeee',JSON.stringify(criteria));
            //     Service.ServiceServices.getService(criteria,{}, {lean:true}, (err, result)=> {
            //         if (err) {cb(err)
            //         } else {
            //             if (result && result.length) {
            //                 callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ALREADY_BOOK)
            //             } else {
            //                 cb()
            //             }
            //         }
            //     })
            // },
            checkMaidAvailability: function (cb) {
                let criteria = {
                    maidId : payloadData.maidId,
                   // hour : payloadData.hour,
                    date:  moment.tz(payloadData.hour,payloadData.timeZone).get('date'),
                    $and: [
                        {transactionId: {$exists: true}},
                        {transactionId: {$nin: ["", null]}}
                    ],

                    agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                        Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
                };

                let projection = {};
                let option = {
                    lean: true
                };
                Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            let endTime = parseInt(payloadData.hour) + (parseInt(payloadData.serviceData[0].duration) * 60 * 60 * 1000);
                            let localArray = result.filter(obj => {
                                console.log("__________hello_____________",
                                    obj.startTime,endTime,payloadData.hour,obj.bufferEndTime,obj.hour)
                                return (obj.startTime <= endTime && payloadData.hour < obj.bufferEndTime)
                            });
                            if (localArray && localArray.length) {
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_BUSY)
                            } else {
                                cb(null)
                            }
                        } else {
                            cb(null)
                        }
                    }
                })
            },
            getMaidRadius:function (checkMaidAvailability, cb) {
                let criteria={
                    _id:payloadData.maidId
                };
                let key,keyGreater24,daysGreater24=[],days=[]
                let startHour = moment.tz(payloadData.hour,payloadData.timeZone).get('hour');

                console.log('startHourrrrrrrr', startHour)

                for (let i = startHour; i < (startHour + 1 + parseInt(payloadData.serviceData[0].duration)); i++) {
                    (function (i) {
                        //   console.log("_i_", i);
                        if (i > 24) {
                            daysGreater24.push(i - 24)
                        } else {
                            days.push(i)
                        }
                    }(i))
                }
                let criteriaDay = moment(parseInt(payloadData.hour)).tz(payloadData.timeZone).day();

                console.log('criteriaDay........', criteriaDay,days)

                key = 'timeSlot.' + criteriaDay;
                keyGreater24 = 'timeSlot.' + (criteriaDay + 1);

                console.log('key...........', key);

                console.log('keyGreater24', keyGreater24,'daysGreater24',daysGreater24);

                criteria[key] = {$all: days};
                if (daysGreater24 && daysGreater24.length) {
                    criteria[keyGreater24] = {$all: daysGreater24};
                }

                console.log('criteriaaaaaaaa...........', criteria);
                Service.MaidServices.getMaid(criteria,{}, {lean:true}, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            maidRadius = result[0].radius;
                            cb()
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_NOT_AVAILABLE);
                        }
                    }
                })
            },
            getMaidAmount: function (getMaidRadius,checkForUniqueKey, cb) {

                let criteria = {
                    currentLocation :{$nearSphere: {
                        $geometry: {
                            type: "Point",
                            //coordinates: [payloadData.long, payloadData.lat]
                            coordinates: [payloadData.long, payloadData.lat]
                        },
                        $maxDistance: maidRadius * 1609.34,
                    }},
                    _id: payloadData.maidId,
                    isDeleted: false,
                    isBlocked: false
                };

                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                Service.MaidServices.getMaid(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            agencyId = result[0].agencyId;
                            price = result[0].price;
                            maidToken = result[0].deviceToken;
                            commission = result[0].commission;
                            actualPrice = result[0].actualPrice;
                            makPrice = result[0].makPrice;
                            countryName = result[0].countryName;
                            notificationDataMaid.receiverId = result[0]._id;
                            notificationDataMaid.fullName = result[0].firstName + " " + result[0].lastName;
                            notificationDataMaid.profilePicURL = result[0].profilePicURL;
                            notificationDataMaid.deviceToken = result[0].deviceToken;
                            cb(null)
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_PROVIDE_SERVICE);
                        }
                    }
                })
            },
            checkMail :(getMaidAmount,cb)=>{

                //checkEmail in db
                if(!userData.actualSignupDone){
                    let criteria = {
                        email: userData.email,
                        isDeleted: false,
                    };

                    let projection = {};
                    let options = {
                        lean: true
                    };
                    Service.MaidServices.getMaid(criteria, projection, options, function (err, data) {
                        if (err) {
                            cb(err);
                        } else {
                            if (data && data.length) {
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MEMEBR_EMAIL_EXIST);
                            }
                            else cb()
                        }
                    });
                }
                else cb()

            },
            bookService: function (getMaidAmount,checkMail, cb) {

                async.eachSeries(payloadData.serviceData, function (obj, cb1) {

                    let startTime = obj.startTime;

                    let localCurency = getCountry(countryName);
                    let endTime = parseInt(payloadData.hour) + (parseInt(obj.duration) * 60 * 60 * 1000);
                    let bufferEndTime = parseInt(endTime) + (60 * 60 * 1000);
                    let dataTOSave = {
                        serviceMakId: serviceMakId,
                        maidId: payloadData.maidId,
                        userId: userData._id,
                        agencyId: agencyId,
                        workDate: parseInt(obj.workDate),
                        startTime: payloadData.hour , //(parseInt(startTime)),
                        endTime: endTime,
                        bufferEndTime: bufferEndTime,
                        duration: obj.duration,
                        commission: commission || process.env.commission,
                        price: price,
                        actualPrice: actualPrice,
                        makPrice: makPrice,
                        // agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                        amount: Math.round((actualPrice * obj.duration) * 100) / 100,
                        commissionToBePaid: Math.round((makPrice * obj.duration) * 100) / 100,
                        uniquieAppKey: UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME),
                        bookingLocation: [payloadData.long, payloadData.lat],
                        address: payloadData.address,
                        billingAddress: userData.billingAddress,
                        timeZone: payloadData.timeZone,
                        hour: payloadData.hour,
                        date : moment.tz(parseInt(payloadData.hour),payloadData.timeZone).get('date'),
                        currency: localCurency.currency,
                    };
                    if (payloadData.locationName) {
                        dataTOSave.locationName = payloadData.locationName;
                    }
                    Service.ServiceServices.createService(dataTOSave, function (err, data) {
                        if (err) {
                            cb1(err)
                        } else {
                            bookingNumber = data.bookingId;
                            maidId = data.maidId;
                            serviceId.push(data._id);
                            cb1(null)
                        }
                    })
                }, function (err1, result1) {
                    cb(err1, null)
                })
            },
            createNotificationForAgency: function (bookService, cb) {
                let dataToSet = {
                    senderId: userData._id,
                    receiverId: agencyId,
                    maidId: payloadData.maidId,
                    type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_REQUEST,
                    serviceId: serviceId[0],
                    bookingId: serviceMakId,
                    agencyMessage: userData.fullName + " has requested for a new service.",
                };
                Service.NotificationServices.createNotification(dataToSet, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null)
                    }
                })
            }

        }, function (err, result) {
            callback(err, {serviceId: serviceId[0]})
        })
    }
    else {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.VERIFY_USER, {})
    }
};

const createPaymentPaytabs = function (payloadData, userData, callback) {

    let bookingId = Math.floor(100000 + Math.random() * 900000);
    let data = [],bookingData,bookingTime,servicData;
    let notificationDataMaid = {};
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        getData :(cb)=>{

            Service.ServiceServices.getService({_id:payloadData.serviceId},{}, {lean:true}, (err, result)=> {
                if (err) {cb(err)
                } else {
                    if (result && result.length) {
                        servicData = result[0];
                        cb()
                    } else {
                        cb()
                    }
                }
            })
        },
        // checkMaid :(getData, checkForUniqueKey,cb)=>{
        //     if (!payloadData.isExtension ||
        //         payloadData.isExtension === null || payloadData.isExtension === undefined) {
        //         let criteria ={
        //             maidId : servicData.maidId,
        //             startTime : servicData.startTime,
        //             date:  moment.tz(servicData.workDate,servicData.timeZone).get('date'),
        //             //transactionId : {$ne : ''},
        //             agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT]},
        //             deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
        //         };
        //         console.log('crrrrrrrrrrrrr',JSON.stringify(criteria));
        //         Service.ServiceServices.getService(criteria,{}, {lean:true}, (err, result)=> {
        //             if (err) {cb(err)
        //             } else {
        //                 if (result && result.length) {
        //                     callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ALREADY_BOOK)
        //                 } else {
        //                     cb()
        //                 }
        //             }
        //         })
        //     }
        //     else cb()
        //
        // },
        updateService: function (checkForUniqueKey, cb) {
            if (!payloadData.isExtension ||
                payloadData.isExtension === null || payloadData.isExtension === undefined) {
                let criteria = {
                    _id: payloadData.serviceId
                };

                let dataToUpdate = {};
                dataToUpdate.cardToken = payloadData.transactionStatus;
                dataToUpdate.transactionId = payloadData.transactionId;
                dataToUpdate.bookingId = bookingId;
                dataToUpdate.agencyAction= Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT;

                if (userData.isGuestFlag) {
                    dataToUpdate.firstBookingDone = true
                }

                if(payloadData.cardType) dataToUpdate.cardType = payloadData.cardType;
                let option = {new: true,lean:true};
                Service.ServiceServices.updateService(criteria, dataToUpdate, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        Models.Maids.populate(result, [
                            {
                                path: 'maidId',
                                select: '', model :'Maids'
                                // model: 'Agency'
                            },{
                                path: 'userId',
                                select: '', model :'Users'
                                // model: 'Agency'
                            },{
                                path: 'agencyId',
                                select: '', model :'Agency'
                                // model: 'Agency'
                            }
                        ], function (err, populatedTransactions) {
                            if (err) {
                                cb(err)
                            }
                            else {
                                bookingData = populatedTransactions
                                data.push(populatedTransactions);
                                cb(null)
                            }
                        })
                        // cb(null)
                    }
                })
            }
            else {
                let criteria = {
                    _id: payloadData.serviceId
                };

                let dataToUpdate = {
                    "isExtend.transactionId": payloadData.transactionId,
                    "isExtend.cardToken": payloadData.transactionStatus,
                    "isExtend.bookingId": bookingId,
                    "isExtend.agencyAction": Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,

                };
                if(payloadData.cardType) dataToUpdate.cardType = payloadData.cardType;
                let option = {new: true,lean:true};
                Service.ServiceServices.updateService(criteria, dataToUpdate, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        Models.Maids.populate(result, [
                            {
                                path: 'maidId',
                                select: '', model :'Maids'
                                // model: 'Agency'
                            },{
                                path: 'userId',
                                select: '', model :'Users'
                                // model: 'Agency'
                            },{
                                path: 'agencyId',
                                select: '', model :'Agency'
                                // model: 'Agency'
                            }
                        ], function (err, populatedTransactions) {
                            if (err) {
                                cb(err)
                            }
                            else {
                                bookingData = populatedTransactions
                                data.push(populatedTransactions);
                                cb()
                            }
                        })


                    }
                })

            }
        },
        bookingDone :(updateService,cb)=>{

            let criteria={_id:userData._id};
            Service.UserServices.updateUser(criteria,{firstBookingDone:true} ,{},(err, result)=>{
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        },
        updateTimeForExtend:(updateService,cb)=>{
            if(payloadData.isExtension) {
                let criteria = {
                    _id: payloadData.serviceId
                };
                let endTime = (parseInt(bookingData.isExtend.extendDuration) * 60 * 60 * 1000);
                let bufferEndTime = ((bookingData.isExtend.extendDuration +1) * 60 * 60 * 1000);
                console.log('444444444444444',bufferEndTime, endTime)
                let dataToUpdate = {
                    $inc:{
                        duration : bookingData.isExtend.extendDuration,
                        endTime : endTime,
                        bufferEndTime : bufferEndTime
                    }
                };
                Service.ServiceServices.updateService(criteria, dataToUpdate, {new:true}, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb()
                    }
                })
            }
            else cb()
        },
        sendPushToMaid: function (updateService, cb) {
            bookingTime = moment.tz(bookingData.startTime,bookingData.timeZone).format("hh:mm a");
            let bookingDate = moment.tz(bookingData.startTime,bookingData.timeZone).format("LL");

            notificationDataMaid.requestId = bookingData.servciceId;
            notificationDataMaid.senderId = userData._id;
            notificationDataMaid.agencyName = bookingData.agencyId.agencyName;
            notificationDataMaid.senderProfilePicURL = bookingData.userId.profilePicURL;

            notificationDataMaid.receiverId = bookingData.maidId._id;
            notificationDataMaid.fullName = bookingData.maidId.firstName + " " + bookingData.maidId.lastName;
            notificationDataMaid.profilePicURL = bookingData.maidId.profilePicURL;
            notificationDataMaid.deviceToken = bookingData.maidId.deviceToken;

            notificationDataMaid.title = 'M.A.K.';
            notificationDataMaid.bookingNumber = bookingData.bookingId;
            notificationDataMaid.maidId = bookingData.maidId._id;
            notificationDataMaid.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_ACCEPT;

            if(!payloadData.isExtension)
                notificationDataMaid.body = " You have a new service request from " + bookingData.userId.fullName+' on '+bookingDate+' at '+bookingTime;
            else notificationDataMaid.body = " Your service request is extended by " + bookingData.userId.fullName+' on '+bookingDate+' for '+bookingData.isExtend.extendDuration+' hours';

            sendNotificationMaid(notificationDataMaid, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })
        },
        sendMail :(updateService, cb)=>{
            if(!payloadData.isExtension || payloadData.isExtension === null){

                let data ={
                    bookingNumber : bookingData.bookingId,
                    maidName : bookingData.maidId.firstName + " " + bookingData.maidId.lastName,
                    maidId :bookingData.maidId.makId,
                    agencyName : bookingData.agencyId.agencyName,
                    bookingTime : moment.tz(bookingData.startTime, bookingData.timeZone).format("hh:mm a"),
                    bookingDate : moment.tz(bookingData.startTime,bookingData.timeZone).format("LL"),
                    duration :bookingData.duration,
                    villa :bookingData.address.villaName,
                    block :bookingData.address.buildingName,
                    road :bookingData.address.streetName,
                    details :bookingData.address.moreDetailedaddress,
                    city :bookingData.address.city,
                    rate :bookingData.maidId.actualPrice,
                    amount :bookingData.amount,

                };
                acceptTemp(data,(err,res)=>{
                    if(err){cb(err)
                    }
                    else{
                        let content=res;
                        let subject = "MAK Booking Confirmation : Thank you!";
                        MailManager.sendMailBySES(userData.email, subject, content, function (err, res) {
                        });
                        cb(null)
                    }
                })

            }
            else {

                isExtended({duration :bookingData.isExtend.extendDuration },(err,res)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        let content=res;
                        let subject = "MAK Extended Booking Confirmation : Thank you!";
                        MailManager.sendMailBySES(userData.email, subject, content, function (err, res) {
                        });
                        cb(null)
                    }
                })
            }

        }
    }, function (err, result) {
        if (err) {
            callback(null)
        }
        else {
            data[0].bookingTime = bookingTime;
            callback(null, {
                trxId: bookingId,
                data: data,
            })
        }
    })
};

const listAllMaidsUserWeb = function (payloadData, userData, callback) {
    let response = [];
    let languages = [];
    let days = [];
    let daysGreater24 = [];
    let key = "";
    let keyGreater24 = "";
    let maidIdToIgnore = [];
    let startTime = payloadData.startTime;


    let requestedMaid = [];
    let count = 0;
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        updateUsersLocation: function (checkForUniqueKey, cb) {
            if (userData && userData._id) {
                let criteria = {
                    _id: userData._id
                };
                let dataToUpdate = {
                    currentLocation: [payloadData.long, payloadData.lat],
                };
                if (payloadData.locationName) {
                    dataToUpdate.locationName = payloadData.locationName;
                }
                let option = {new: true};
                Service.UserServices.updateUser(criteria, dataToUpdate, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null)
                    }
                })
            } else {
                cb(null)
            }
        },

        getAvailableSlots: function (updateUsersLocation, cb) {

            if (payloadData.workDate && payloadData.duration && payloadData.startTime) {
                let workDate = payloadData.workDate;
                let endTime = startTime + (payloadData.duration * 60 * 60 * 1000);
                let criteria = {
                    workDate: {$eq: workDate},
                    agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                    deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
                };

                let projection = {};
                let option = {
                    lean: true
                };
                console.log("___endTime and Start time__________", endTime, startTime);

                Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {

                        if (result && result.length) {
                            let localArray = result.filter(obj => {
                                return (obj.startTime <= endTime && startTime < obj.bufferEndTime)
                            });
                            localArray.map(obj => {
                                maidIdToIgnore.push(obj.maidId);
                            });
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        getMaidCount: function (getAvailableSlots, cb) {
            let geoNear = {
                near: {type: "Point", coordinates: [payloadData.long, payloadData.lat]},
                spherical: true,
                distanceField: "distance",
                //distanceMultiplier: 0.001,                   //convert to kilometre 6371km 0.001
                distanceMultiplier: 0.000621371,                   //convert to miles  3963.2miles 0.000621371
                //maxDistance: 1000000,
                query: {isDeleted: false},
            };

            let criteria = {
                isDeleted: false,
                isBlocked: false,
                isVerified: true,
                acceptAgreement: true,
                countryName: {$regex: payloadData.country, $options: 'i'},
            };

            if (maidIdToIgnore && maidIdToIgnore.length) {
                criteria._id = {$nin: maidIdToIgnore}
            }

            let startHour = parseInt(moment(parseInt(payloadData.startTime)).tz(payloadData.timeZone).hours());
            for (let i = startHour + 1; i < (startHour + 1 + payloadData.duration); i++) {
                (function (i) {
                    console.log("_i_", i);
                    if (i > 24) {
                        daysGreater24.push(i - 24)
                    } else {
                        days.push(i)
                    }
                }(i))
            }
            let criteriaDay = moment(parseInt(startTime)).tz(payloadData.timeZone).day();
            key = 'timeSlot.' + criteriaDay;
            keyGreater24 = 'timeSlot.' + (criteriaDay + 1);
            criteria[key] = {$all: days};
            if (daysGreater24 && daysGreater24.length) {
                criteria[keyGreater24] = {$all: daysGreater24};
            }

            if (payloadData.nationality) {
                criteria.nationality = {$in: payloadData.nationality}
            }
            if (payloadData.religion) {
                criteria.religion = {$in: payloadData.religion}
            }
            if (payloadData.languages) {
                async.each(payloadData.languages, function (file, cb) {
                    languages.push(mongoose.Types.ObjectId(file));
                    cb(null)
                }, function (err) {
                    criteria.languages = {$in: languages}
                })
            }
            if (payloadData.gender) {
                if (payloadData.gender == "MALE") {
                    criteria.gender = Config.APP_CONSTANTS.DATABASE.GENDER.MALE
                }
                else if (payloadData.gender == "FEMALE") {
                    criteria.gender = Config.APP_CONSTANTS.DATABASE.GENDER.FEMALE
                }
                else if (payloadData.gender == "BOTH") {
                    criteria.gender = {$in: [Config.APP_CONSTANTS.DATABASE.GENDER.MALE, Config.APP_CONSTANTS.DATABASE.GENDER.FEMALE]}
                }
            }
            if (payloadData.agencyId) {
                criteria.agencyId = {$in: payloadData.agencyId.map(id => mongoose.Types.ObjectId(id))}
            }
            if (payloadData.searchMaidName) {
                criteria.$or = [
                    {firstName: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {lastName: {$regex: payloadData.searchMaidName, $options: "si"}}
                ]
            }
            Models.Maids.aggregate([
                {
                    $geoNear: geoNear
                },
                {
                    $match: criteria
                },
                {
                    $lookup: {
                        from: "languages",
                        localField: "languages",
                        foreignField: "_id",
                        as: "languages"
                    }
                },
                {
                    $lookup: {
                        from: "agencies",
                        localField: "agencyId",
                        foreignField: "_id",
                        as: "agencyId"
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        _id: 1,
                        gender: 1,
                        email: 1,
                        country: 1,
                        countryCode: 1,
                        phoneNo: 1,
                        makId: 1,
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                        agencyImage: "$agencyId.profilePicURL",
                        locationName: 1,
                        languages: 1,
                        price: 1,
                        actualPrice: 1,
                        makPrice: 1,
                        experience: 1,
                        nationality: 1,
                        dob: 1,
                        description: 1,
                        lastName: 1,
                        firstName: 1,
                        feedBack: 1,
                        currentLocation: 1,
                        documentPicURL: 1,
                        profilePicURL: 1,
                        distance: 1,
                        religion: 1,
                        currency: 1,
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {
                            isDeleted: false,
                            isBlocked: false,
                            reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER
                        }
                    }
                }
            ], function (err, result) {
                if (result && result.length) {
                    count = result.length;
                    cb(null)
                } else {
                    count = 0;
                    cb(null)
                }

            })
        },

        listSuggestedMaids: function (getMaidCount, cb) {
            let geoNear = {
                near: {type: "Point", coordinates: [payloadData.long, payloadData.lat]},
                spherical: true,
                distanceField: "distance",
                //distanceMultiplier: 0.001,                   //convert to kilometre 6371km 0.001
                distanceMultiplier: 0.000621371,                   //convert to miles  3963.2miles 0.000621371
                //maxDistance: 1000000,
                query: {isDeleted: false},
            };

            let criteria = {
                isDeleted: false,
                isBlocked: false,
                countryName: {$regex: payloadData.country, $options: 'i'}
            };

            criteria[key] = {$all: days};
            if (daysGreater24 && daysGreater24.length) {
                criteria[keyGreater24] = {$all: daysGreater24};
            }
            if (maidIdToIgnore && maidIdToIgnore.length) {
                criteria._id = {$nin: maidIdToIgnore}
            }

            Models.Maids.aggregate([
                {
                    $geoNear: geoNear
                },
                {
                    $match: criteria
                },
                {
                    $sort: {
                        distance: -1,
                    }
                },
                {
                    $lookup: {
                        from: "languages",
                        localField: "languages",
                        foreignField: "_id",
                        as: "languages"
                    }
                },
                {
                    $lookup: {
                        from: "agencies",
                        localField: "agencyId",
                        foreignField: "_id",
                        as: "agencyId"
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        _id: 1,
                        gender: 1,
                        email: 1,
                        country: 1,
                        countryCode: 1,
                        phoneNo: 1,
                        makId: 1,
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                        agencyImage: "$agencyId.profilePicURL",
                        locationName: 1,
                        rating: 1,
                        languages: 1,
                        price: 1,
                        actualPrice: 1,
                        makPrice: 1,
                        experience: 1,
                        nationality: 1,
                        dob: 1,
                        description: 1,
                        lastName: 1,
                        firstName: 1,
                        feedBack: 1,
                        currentLocation: 1,
                        documentPicURL: 1,
                        profilePicURL: 1,
                        distance: 1,
                        religion: 1,
                        currency: 1
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {
                            isDeleted: false,
                            isBlocked: false,
                            reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER
                        }
                    }
                },
            ], function (err, result) {
                if (result && result.length) {
                    for (let i = 0; i < result.length; i++) {
                        result[i].count = count;
                        if (userData.favouriteMaidId) {
                            if (userData.favouriteMaidId.indexOf(result[i]._id)) {
                                result[i].isFavourite = true;
                            } else {
                                result[i].isFavourite = false;
                            }
                        }
                        else {
                            result[i].isFavourite = false;
                        }

                        if (result[i].experience == 2) {
                            result[i].experience = 'Up to 2 years';
                        }
                        if (result[i].experience == 5) {
                            result[i].experience = '3 to 5 years';
                        }
                        if (result[i].experience == 6) {
                            result[i].experience = 'Above 5 years';
                        }
                        result[i].distance = result[i].distance.toFixed(1);

                        let reduceResult = {cleaning: 0, ironing: 0, cooking: 0, childCare: 0};
                        if (result[i].reviewData.length > 0) {
                            let countCleaning = 0;
                            let countIroning = 0;
                            let countCooking = 0;
                            let countChildCare = 0;
                            reduceResult = result[i].reviewData.reduce((prev, curr) => {
                                prev.cleaning += curr.maidRating.cleaning;
                                prev.ironing += curr.maidRating.ironing;
                                prev.cooking += curr.maidRating.cooking;
                                prev.childCare += curr.maidRating.childCare;

                                if (curr.maidRating.cleaning > 0) {
                                    countCleaning = countCleaning + 1;
                                }
                                if (curr.maidRating.ironing > 0) {
                                    countIroning = countIroning + 1;
                                }
                                if (curr.maidRating.cooking > 0) {
                                    countCooking = countCooking + 1;
                                }
                                if (curr.maidRating.childCare > 0) {
                                    countChildCare = countChildCare + 1;
                                }
                                return prev
                            }, reduceResult);
                            result[i].avgCleaning = (reduceResult.cleaning / countCleaning);
                            result[i].avgIroning = (reduceResult.ironing / countIroning);
                            result[i].avgCooking = (reduceResult.cooking / countCooking);
                            result[i].avgChildCare = (reduceResult.childCare / countChildCare);

                        }
                        else {
                            result[i].avgCleaning = 0;
                            result[i].avgIroning = 0;
                            result[i].avgCooking = 0;
                            result[i].avgChildCare = 0;
                        }
                    }
                    result = _.sortBy(result, ['avgCleaning', 'avgIroning', 'avgCooking', 'avgChildCare']);
                    if (payloadData.pageNo == 1) {
                        response.push(result[result.length - 1]);
                    }
                    maidIdToIgnore.push(result[result.length - 1]._id)
                    cb(null)
                } else {
                    cb(null)
                }
            })

        },

        listMaids: function (listSuggestedMaids, cb) {
            let geoNear = {
                near: {type: "Point", coordinates: [payloadData.long, payloadData.lat]},
                spherical: true,
                distanceField: "distance",
                //distanceMultiplier: 0.001,                   //convert to kilometre 6371km 0.001
                distanceMultiplier: 0.000621371,                   //convert to miles  3963.2miles 0.000621371
                //maxDistance: 1000000,
                query: {isDeleted: false},
            };

            let criteria = {
                isDeleted: false,
                isBlocked: false,
                isVerified: true,
                acceptAgreement: true,
                countryName: {$regex: payloadData.country, $options: 'i'},
            };

            if (maidIdToIgnore && maidIdToIgnore.length) {
                criteria._id = {$nin: maidIdToIgnore}
            }

            let startHour = parseInt(moment(parseInt(payloadData.startTime)).tz(payloadData.timeZone).hours());
            for (let i = startHour + 1; i < (startHour + 1 + payloadData.duration); i++) {
                (function (i) {
                    console.log("_i_", i);
                    if (i > 24) {
                        daysGreater24.push(i - 24)
                    } else {
                        days.push(i)
                    }
                }(i))
            }
            let criteriaDay = moment(parseInt(startTime)).tz(payloadData.timeZone).day();
            key = 'timeSlot.' + criteriaDay;
            keyGreater24 = 'timeSlot.' + (criteriaDay + 1);
            criteria[key] = {$all: days};
            if (daysGreater24 && daysGreater24.length) {
                criteria[keyGreater24] = {$all: daysGreater24};
            }

            if (payloadData.nationality) {
                criteria.nationality = {$in: payloadData.nationality}
            }
            if (payloadData.religion) {
                criteria.religion = {$in: payloadData.religion}
            }
            if (payloadData.languages) {
                async.each(payloadData.languages, function (file, cb) {
                    languages.push(mongoose.Types.ObjectId(file));
                    cb(null)
                }, function (err) {
                    criteria.languages = {$in: languages}
                })
            }
            if (payloadData.gender) {
                if (payloadData.gender == "MALE") {
                    criteria.gender = Config.APP_CONSTANTS.DATABASE.GENDER.MALE
                }
                else if (payloadData.gender == "FEMALE") {
                    criteria.gender = Config.APP_CONSTANTS.DATABASE.GENDER.FEMALE
                }
                else if (payloadData.gender == "BOTH") {
                    criteria.gender = {$in: [Config.APP_CONSTANTS.DATABASE.GENDER.MALE, Config.APP_CONSTANTS.DATABASE.GENDER.FEMALE]}
                }
            }
            if (payloadData.agencyId) {
                criteria.agencyId = {$in: payloadData.agencyId.map(id => mongoose.Types.ObjectId(id))}
            }
            if (payloadData.searchMaidName) {
                criteria.$or = [
                    {firstName: {$regex: payloadData.searchMaidName, $options: "si"}},
                    {lastName: {$regex: payloadData.searchMaidName, $options: "si"}}
                ]
            }


            let limit = (payloadData.limit - 1);
            console.log("___criteria____geoNear______", limit, maidIdToIgnore, criteria, startHour);

            Models.Maids.aggregate([
                {
                    $geoNear: geoNear
                },
                {
                    $match: criteria
                },
                {
                    $lookup: {
                        from: "languages",
                        localField: "languages",
                        foreignField: "_id",
                        as: "languages"
                    }
                },
                {
                    $lookup: {
                        from: "agencies",
                        localField: "agencyId",
                        foreignField: "_id",
                        as: "agencyId"
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        _id: 1,
                        gender: 1,
                        email: 1,
                        country: 1,
                        countryCode: 1,
                        phoneNo: 1,
                        makId: 1,
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                        agencyImage: "$agencyId.profilePicURL",
                        locationName: 1,
                        languages: 1,
                        price: 1,
                        actualPrice: 1,
                        makPrice: 1,
                        experience: 1,
                        nationality: 1,
                        dob: 1,
                        description: 1,
                        lastName: 1,
                        firstName: 1,
                        feedBack: 1,
                        currentLocation: 1,
                        documentPicURL: 1,
                        profilePicURL: 1,
                        distance: 1,
                        religion: 1,
                        currency: 1,
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {
                            isDeleted: false,
                            isBlocked: false,
                            reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER
                        }
                    }
                },
                {$skip: ((payloadData.pageNo - 1) * limit)},
                {$limit: limit}
            ], function (err, result) {
                if (result && result.length) {
                    let localFavourites;
                    if (userData.favouriteMaidId && userData.favouriteMaidId.length) {
                        localFavourites = userData.favouriteMaidId.map(obj => {
                            return JSON.stringify(obj)
                        });
                    } else {
                        localFavourites = []
                    }

                    for (let i = 0; i < result.length; i++) {
                        result[i].count = count;
                        if (localFavourites && localFavourites.length) {
                            let flag = _.indexOf(localFavourites, JSON.stringify(result[i]._id));
                            if (flag >= 0) {
                                result[i].isFavourite = true;
                            } else {
                                result[i].isFavourite = false;
                            }
                        } else {
                            result[i].isFavourite = false;
                        }


                        if (result[i].experience == 2) {
                            result[i].experience1 = result[i].experience;
                            result[i].experience = 'Up to 2 years';
                        }
                        if (result[i].experience == 5) {
                            result[i].experience1 = result[i].experience;
                            result[i].experience = '3 to 5 years';
                        }
                        if (result[i].experience == 6) {
                            result[i].experience1 = result[i].experience;
                            result[i].experience = 'Above 5 years';
                        }
                        result[i].distance = result[i].distance.toFixed(1);

                        let reduceResult = {cleaning: 0, ironing: 0, cooking: 0, childCare: 0};
                        if (result[i].reviewData.length > 0) {
                            let countCleaning = 0;
                            let countIroning = 0;
                            let countCooking = 0;
                            let countChildCare = 0;
                            reduceResult = result[i].reviewData.reduce((prev, curr) => {
                                prev.cleaning += curr.maidRating.cleaning;
                                prev.ironing += curr.maidRating.ironing;
                                prev.cooking += curr.maidRating.cooking;
                                prev.childCare += curr.maidRating.childCare;

                                if (curr.maidRating.cleaning > 0) {
                                    countCleaning = countCleaning + 1;
                                }
                                if (curr.maidRating.ironing > 0) {
                                    countIroning = countIroning + 1;
                                }
                                if (curr.maidRating.cooking > 0) {
                                    countCooking = countCooking + 1;
                                }
                                if (curr.maidRating.childCare > 0) {
                                    countChildCare = countChildCare + 1;
                                }
                                return prev
                            }, reduceResult);
                            result[i].avgCleaning = (reduceResult.cleaning / countCleaning);
                            result[i].avgIroning = (reduceResult.ironing / countIroning);
                            result[i].avgCooking = (reduceResult.cooking / countCooking);
                            result[i].avgChildCare = (reduceResult.childCare / countChildCare);

                        }
                        else {
                            result[i].avgCleaning = 0;
                            result[i].avgIroning = 0;
                            result[i].avgCooking = 0;
                            result[i].avgChildCare = 0;
                        }
                    }

                    if (payloadData.sort == 'RATING') {
                        result = _.sortBy(result, ['avgCleaning', 'avgIroning', 'avgCooking', 'avgChildCare']);
                        requestedMaid = _.reverse(result)
                    }
                    else if (payloadData.sort == 'EXPERIENCE') {
                        result = _.sortBy(result, ['experience1']);
                        requestedMaid = _.reverse(result)
                    }
                    else if (payloadData.sort == 'DISTANCE') {
                        result = _.sortBy(result, ['distance']);
                        requestedMaid = _.reverse(result)
                    }
                    else if (payloadData.sort == 'PRICE_LOW') {
                        result = _.sortBy(result, ['price']);
                        requestedMaid = result
                    }
                    else if (payloadData.sort == 'PRICE_HIGH') {
                        result = _.sortBy(result, ['price']);
                        requestedMaid = _.reverse(result)
                    }
                    else {
                        result = _.sortBy(result, ['distance']);
                        requestedMaid = _.reverse(result)
                    }
                    cb()
                } else {
                    cb(null)
                }
            })
        },

        finalResponse: function (listMaids, cb) {
            if (payloadData.pageNo == 1) {
                response = response.concat(requestedMaid);
                cb(null)
            } else {

                response = requestedMaid;
                cb(null)
            }
        }

    }, function (err, result) {
        callback(err, response)
    });
};

const getMaidProfileDetail = function (payloadData, userData, callback) {

    let response = {};
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getMAidDetails: function (checkForUniqueKey, cb) {

            Models.Maids.aggregate([
                {
                    $match: {
                        _id: mongoose.Types.ObjectId(payloadData.maidId),
                        isDeleted: false,
                        isBlocked: false,
                        isVerified: true,
                        acceptAgreement: true,
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {
                            isDeleted: false,
                            isBlocked: false,
                            reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER
                        }
                    }
                },
                {
                    $unwind: {path: "$reviewData", preserveNullAndEmptyArrays: true}
                },
                {
                    $addFields: {
                        maidRating: "$reviewData.maidRating",
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        cleaningRating: {$sum: "$maidRating.cleaning"},
                        ironingRating: {$sum: "$maidRating.ironing"},
                        cookingRating: {$sum: "$maidRating.cooking"},
                        childCareRating: {$sum: "$maidRating.childCare"},

                        cleaningCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cleaning", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        ironingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.ironing", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        cookingCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.cooking", 0]},
                                    0,
                                    1
                                ]
                            }
                        },
                        childCareCount: {
                            $sum: {
                                $cond: [
                                    {$eq: ["$maidRating.childCare", 0]},
                                    0,
                                    1
                                ]
                            }
                        }
                    }
                },
                {
                    $project: {
                        maidId: "$_id",
                        avgCleaning: {$cond: [{$eq: ["$cleaningCount", 0]}, 0, {$divide: ["$cleaningRating", "$cleaningCount"]}]},
                        avgIroning: {$cond: [{$eq: ["$ironingCount", 0]}, 0, {$divide: ["$ironingRating", "$ironingCount"]}]},
                        avgCooking: {$cond: [{$eq: ["$cookingCount", 0]}, 0, {$divide: ["$cookingRating", "$cookingCount"]}]},
                        avgChildCare: {$cond: [{$eq: ["$childCareCount", 0]}, 0, {$divide: ["$childCareRating", "$childCareCount"]}]},
                    }
                },
                {
                    $lookup: {
                        from: 'maids',
                        localField: '_id',
                        foreignField: '_id',
                        as: 'maidData'
                    }
                },
                {
                    $unwind: "$maidData"
                },
                {
                    $project: {
                        _id: 1,
                        avgCleaning: 1,
                        avgIroning: 1,
                        avgCooking: 1,
                        avgChildCare: 1,
                        gender: "$maidData.gender",
                        email: "$maidData.email",
                        country: "$maidData.country",
                        countryCode: "$maidData.countryCode",
                        phoneNo: "$maidData.phoneNo",
                        makId: "$maidData.makId",
                        locationName: "$maidData.locationName",
                        languages: "$maidData.languages",
                        price: "$maidData.price",
                        actualPrice: "$maidData.actualPrice",
                        makPrice: "$maidData.makPrice",
                        experience: "$maidData.experience",
                        nationality: "$maidData.nationality",
                        dob: "$maidData.dob",
                        description: "$maidData.description",
                        lastName: "$maidData.lastName",
                        firstName: "$maidData.firstName",
                        feedBack: "$maidData.feedBack",
                        currentLocation: "$maidData.currentLocation",
                        documentPicURL: "$maidData.documentPicURL",
                        profilePicURL: "$maidData.profilePicURL",
                        religion: "$maidData.religion",
                        agencyId: "$maidData.agencyId",
                        currency: "$maidData.currency"
                    }
                },
                {
                    $lookup: {
                        from: "agencies",
                        localField: "agencyId",
                        foreignField: "_id",
                        as: "agencyId"
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $addFields: {
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                        agencyImage: "$agencyId.profilePicURL",
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {
                            isDeleted: false,
                            isBlocked: false,
                            reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER
                        }
                    }
                },
                {
                    $project: {
                        _id: 1,
                        reviewData: 1,
                        avgCleaning: 1,
                        avgIroning: 1,
                        avgCooking: 1,
                        avgChildCare: 1,
                        gender: 1,
                        email: 1,
                        country: 1,
                        countryCode: 1,
                        phoneNo: 1,
                        makId: 1,
                        agencyId: 1,
                        agencyName: 1,
                        agencyImage: 1,
                        locationName: 1,
                        languages: 1,
                        price: 1,
                        actualPrice: 1,
                        makPrice: 1,
                        experience: 1,
                        nationality: 1,
                        dob: 1,
                        description: 1,
                        lastName: 1,
                        firstName: 1,
                        feedBack: 1,
                        currentLocation: 1,
                        documentPicURL: 1,
                        profilePicURL: 1,
                        religion: 1,
                        currency: 1,
                    }
                },

            ], function (err, result) {
                if (result && result.length) {

                    let populateArray = [];
                    if (userData.appLanguage == Config.APP_CONSTANTS.DATABASE.LANGUAGE.AR) {
                        populateArray.push({
                            path: 'reviewData.userId',
                            select: '_id fullNameAr email',
                            model: 'Users'
                        })
                    } else {
                        populateArray.push({
                            path: 'reviewData.userId',
                            select: '_id fullName email',
                            model: 'Users'
                        })
                    }
                    Models.Users.populate(result, populateArray, function (e1, r1) {
                        if (e1) {
                            cb(e1)
                        } else {
                            Models.Languages.populate(r1, [{
                                path: 'languages',
                                select: '_id languageName',
                                model: 'Languages'
                            }], function (err, populatedTransactions) {
                                if (err) {
                                    cb(err)
                                }
                                else {
                                    let suggestedMaid = populatedTransactions[0];
                                    let localFavourites;
                                    if (userData.favouriteMaidId && userData.favouriteMaidId.length) {
                                        localFavourites = userData.favouriteMaidId.map(obj => {
                                            return JSON.stringify(obj)
                                        });
                                    } else {
                                        localFavourites = []
                                    }
                                    if (localFavourites && localFavourites.length) {
                                        let localFavourites = userData.favouriteMaidId.map(obj => {
                                            return JSON.stringify(obj)
                                        })
                                        let flag = _.indexOf(localFavourites, JSON.stringify(suggestedMaid._id));

                                        if (flag >= 0) {
                                            suggestedMaid.isFavourite = true;
                                        } else {
                                            suggestedMaid.isFavourite = false;
                                        }
                                    } else {
                                        suggestedMaid.isFavourite = false;
                                    }
                                    if (suggestedMaid.experience === 2) {
                                        suggestedMaid.experience = 'Up to 2 years';
                                    }
                                    if (suggestedMaid.experience === 5) {
                                        suggestedMaid.experience = '3 to 5 years';
                                    }
                                    if (suggestedMaid.experience === 6) {
                                        suggestedMaid.experience = 'Above 5 years';
                                    }
                                    response = suggestedMaid;
                                    cb(null)
                                }
                            })
                        }
                    })
                } else {
                    cb(null)
                }
            })
        }

    }, function (err, result) {
        response.reviewData.sort(function (a,b) {
            return b.createdOn - a.createdOn
        });
        callback(err, response)
    })
};

const listAllServiceUser = function (payloadData, userData, callback) {
    let response = {};
    let offset = momentTz.tz(payloadData.timeZone).utcOffset() * 60 * 1000;

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        listServices: function (checkForUniqueKey, cb) {
            let criteria = {
                userId: userData._id,
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ]
            };
            let option = {
                lean: true,
                skip: ((payloadData.pageNo - 1) * payloadData.limit),
                limit: payloadData.limit,
            };

            if (payloadData.onBasisOfDate === "ON_GOING") {
                criteria.$or = [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT}
                        ]
                    }
                ];
                criteria.startTime = {$lte: +new Date()};
                criteria.endTime = {$gte: +new Date()};
                criteria.isCompleted = false;
                option.sort ={startTime:1}
            }
            else if (payloadData.onBasisOfDate === "UPCOMING") {
                criteria.$or = [
                    {
                        $and: [
                            {"isExtend.requested": true},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]}}
                        ]
                    },
                    {
                        $and: [
                            {"isExtend.requested": false},
                            {agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]}}
                        ]
                    }
                ];
                criteria.startTime = {$gte: +new Date()};
                criteria.isCompleted = false;
                option.sort ={startTime:1}
            }
            else if (payloadData.onBasisOfDate === "COMPLETED") {
                criteria.agencyAction = Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT;
                criteria.isCompleted = true;
                option.sort ={startTime: -1}
            }


            let projection = {};

            let populateArray = [
                {
                    path: 'agencyId',
                    match: {},
                    select: '_id agencyName profilePicURL',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '',
                    option: {},
                    populate : [{path : "languages", select : "languageName languageCode"}],
                }
            ];
            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        response.data = result;
                        response.data.map((obj)=>{
                            obj.bookingTime = moment.tz(obj.startTime,obj.timeZone).format("hh:mm a")
                            obj.maidId.agencyId = obj.agencyId._id;
                            obj.maidId.agencyName = obj.agencyId.agencyName;
                            obj.maidId.agencyImage = obj.agencyId.profilePicURL;
                            if (obj.maidId.experience === 2) {
                                obj.maidId.experience = 'Up to 2 years';
                            }
                            if (obj.maidId.experience === 5) {
                                obj.maidId.experience = '3 to 5 years';
                            }
                            if (obj.maidId.experience === 6) {
                                obj.maidId.experience = 'Above 5 years';
                            }
                        });

                        cb(null)
                    } else {
                        response.count = 0;
                        response.data = [];
                        cb(null)
                    }

                }
            })
        }
    }, function (err, result) {
        callback(err, response)
    });
};

const cancelService = function (payloadData, userData, callback) {
    let response = {};
    let canBeDeleted = false;
    let canRefund = false;
    let offset = momentTz.tz(payloadData.timeZone).utcOffset() * 60 * 1000;

    let notificationData = {};
    let maidId = "";
    let agencyId = "";
    let serviceMakId = "";

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkIfItcanBecanceled: function (checkForUniqueKey, cb) {
            let today = new Date();
          //  let currentTimePlusOne = (today.setHours(today.getHours() - 1)) + offset;
            let currentTimePlusOne = +moment.tz(payloadData.timeZone).toDate();

            console.log('4444444444444444',currentTimePlusOne)
            Models.Service.aggregate([
                {
                    $match: {
                        _id: mongoose.Types.ObjectId(payloadData.serviceId),
                        userId: mongoose.Types.ObjectId(userData._id),
                        agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                        deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                        //startTime: {$gt: currentTimePlusOne}
                    }
                }
            ], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    let diff  = (result[0].startTime - currentTimePlusOne) /3600000
                    console.log('55555555555555',diff);
                    if(diff > 24) canRefund = true

                    if (result && result.length) {
                        canBeDeleted = true;
                        response = result[0]
                        maidId = result[0].maidId;
                        agencyId = result[0].agencyId;
                        serviceMakId = result[0].serviceMakId;
                        cb(null)
                    } else {
                        canBeDeleted = false;
                        cb(null)
                    }
                }
            })
        },
        refundAmount: function (checkIfItcanBecanceled, cb) {
            if (canRefund) {

                console.log('33333333333333333',response.startTime)
                var formData = {
                    merchant_email: 'KAM@MAK.TODAY',
                   /// secret_key: Config.APP_CONSTANTS.SERVER.SECRET_KEY_PAYTAB,
                    refund_amount : response.actualPrice - (response.actualPrice * 3.33/100),
                    refund_reason : 'user cancelled the service',
                    transaction_id : response.transactionId,
                };

                if(response.cardType && response.cardType === 1)
                    formData.secret_key = Config.APP_CONSTANTS.SERVER.SECRET_KEY_PAYTAB_DEBIT;
                else  formData.secret_key = Config.APP_CONSTANTS.SERVER.SECRET_KEY_PAYTAB;

                request.post({
                url: 'https://www.paytabs.com/apiv2/refund_process',
                form: formData
                },
                function (err, httpResponse, body) {
                let saveData ={

                    responseCode : body.response_code,
                    userId : userData._id,
                    serviceId : payloadData.serviceId,
                    transactionId : response.transactionId,
                    refundAmount : formData.refund_amount,
                    actualPrice : response.actualPrice,
                    result : body.result,
                    createdOn : new Date(),
                }
                 Service.RefundServices.createRefund(saveData,function (err, result) {
                     cb()
                    })
                });
            }
            else cb()
        },

        cancelServiceByUser: function (checkIfItcanBecanceled, cb) {
            if (canBeDeleted) {
                let criteria = {
                    _id: payloadData.serviceId,
                };
                let dataToUpdate = {
                    deleteAction: Config.APP_CONSTANTS.DATABASE.ACTION.DELETED,
                    deleteReason: payloadData.deleteReason,
                    deleteTimeStamp: Date.now(),
                    deleteRequestByUser: true,
                };
                let option = {};
                Service.ServiceServices.updateService(criteria, dataToUpdate, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null)
                    }
                })
            } else {
                cb(null)
            }
        },

        getMaidAmount: function (cancelServiceByUser, cb) {
            if (canBeDeleted) {
                let criteria = {
                    _id: maidId,
                    isDeleted: false,
                    isBlocked: false
                };

                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };
                let populate=[{
                    path : 'agencyId',
                    select : 'agencyName'
                }]
                Service.MaidServices.getMaidPopulate(criteria, projection, options,populate, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            notificationData.receiverId = result[0]._id;
                            notificationData.fullName = result[0].firstName + " " + result[0].lastName;
                            notificationData.profilePicURL = result[0].profilePicURL;
                            notificationData.deviceToken = result[0].deviceToken;
                            notificationData.makId = result[0].makId;
                            notificationData.actualPrice = result[0].actualPrice;
                            notificationData.agencyName = result[0].agencyId.agencyName;
                            cb(null)
                        } else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        sendPushToMaid: function (getMaidAmount,checkIfItcanBecanceled, cb) {
            if (canBeDeleted) {
                notificationData.requestId = payloadData.serviceId;
                notificationData.senderId = userData._id;
                notificationData.fullName = userData.fullName;
                notificationData.senderProfilePicURL = userData.profilePicURL;

                notificationData.title = 'M.A.K.';
                notificationData.bookingNumber = response.bookingId;
                notificationData.maidId = payloadData.maidId;
                notificationData.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_DELETE;
                notificationData.body = userData.fullName + " has canceled a service with booking number : "+response.bookingId;


                sendNotificationMaid(notificationData, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null)
                    }
                })
            } else {
                cb(null)
            }
        },

        sendMail : (getMaidAmount,checkIfItcanBecanceled,cb)=>{
            let data ={
                userName : userData.fullName,
                bookingNumber : response.bookingId,
                maidName : notificationData.fullName,
                maidId : notificationData.makId,
                agencyName : notificationData.agencyName,
                bookingTime : moment.tz(response.startTime, response.timeZone).format("hh:mm a"),
                bookingDate : moment.tz(response.startTime,response.timeZone).format("LL"),
                duration :response.duration,
                villa : response.address.villaName,
                block : response.address.buildingName,
                road : response.address.streetName,
                details : response.address.moreDetailedaddress || 'NA',
                city : response.address.city,
                rate : notificationData.actualPrice,
                amount :  response.amount,

            };
            if(canRefund){
                bookingCanBeCancel(data,(err,res)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        let content=res;
                        let subject = "MAK Booking Cancelled!";
                        MailManager.sendMailBySES(userData.email, subject, content, function (err, res) {
                        });
                        cb(null)
                    }
                })
            }
            else {
                bookingNotToCancel(data,(err,res)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        let content=res;
                        let subject = "MAK Booking Cancelled!";
                        MailManager.sendMailBySES(userData.email, subject, content, function (err, res) {
                        });
                        cb(null)
                    }
                })
            }
        },

        createNotificationForAgency: function (checkIfItcanBecanceled,sendPushToMaid, cb) {
            if (canBeDeleted) {
                let dataToSet = {
                    senderId: userData._id,
                    receiverId: agencyId,
                    type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_DELETE,
                    serviceId: payloadData.serviceId,
                    agencyMessage: userData.fullName + " has cancelled a service with booking number : "+response.bookingId,
                    bookingId: serviceMakId,
                    maidId: maidId
                };
                Service.NotificationServices.createNotification(dataToSet, function (err, result) {
                    if (err) {
                        callback(err)
                    } else {
                        cb(null)
                    }
                })
            } else {
                cb(null)
            }
        },
        deleteChat: function (checkIfItcanBecanceled,sendPushToMaid, cb) {

            let criteria = {
                serviceId: response._id
            };
            let dataToSet = {
                isExpired:true,
            };
            let options = {multi: true};
            Service.ChatServices.updateAllChat(criteria, dataToSet, options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb()
                }
            })

        }
    }, function (err, result) {
        if (canBeDeleted) {
            callback(err, response)
        } else {
            callback(err, "This service cannot be canceled")
        }
    })
};

const bookingCanBeCancel =  function(data,callback){

    let content = `
  <head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width"> <title></title> <style type="text/css"> body, #bodyTable { height: 100%!important; margin: 0; padding: 0; width: 100%!important } #bodyTable { font-family: Helvetica Neue, Helvetica, Arial, sans-serif; min-height: 100%; background: #eee; color: #333 } body, table, td, p, a, li, blockquote { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100% } table, td { mso-table-lspace: 0; mso-table-rspace: 0 } img { -ms-interpolation-mode: bicubic } body { margin: 0; padding: 0; background: #eee } img { border: 0; height: auto; line-height: 100%; outline: 0; text-decoration: none } table { border-collapse: collapse!important; max-width: 100%!important } img { border: 0!important } h1 { font-family: Helvetica; font-size: 42px; font-style: normal; font-weight: bold; text-align: center; margin: 30px auto 0 } h2 { font-size: 32px; font-style: normal; font-weight: bold; margin: 50px auto 0 } h3 { font-size: 20px; font-weight: bold; margin: 25px 0; letter-spacing: normal; text-align: left } a h3 { color: #444!important; text-decoration: none!important } .titleLink { text-decoration: none!important } .preheaderContent { color: #808080; font-size: 10px; line-height: 125% } .preheaderContent a:link, .preheaderContent a:visited, .preheaderContent a .yshortcuts { color: #606060; font-weight: normal; text-decoration: underline } #emailHeader, #tacoTip { color: #fff } #emailHeader { background-color: #fff } #content p { color: #4d4d4d; margin: 20px 70px 30px; font-size: 24px; line-height: 32px; text-align: left } #button { display: inline-block; margin: 10px auto; background: #fff; border-radius: 4px; font-weight: bold; font-size: 18px; padding: 15px 20px; cursor: pointer; color: #0079bf; margin-bottom: 50px } #socialIconWrap img { line-height: 35px!important; padding: 0 5px } .footerContent div { color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center; max-width: 100%!important } .footerContent div a:link, .footerContent div a:visited { color: #369; font-weight: normal; text-decoration: underline } .footerContent img { display: inline } #socialLinks img { margin: 0 2px } #utility { border-top: 1px solid #ddd } #utility div { text-align: center } #monkeyRewards img { max-width: 160px } #emailFooter { max-width: 100%!important } #footerTwitter a, #footerFacebook a { text-decoration: none!important; color: #fff!important; font-size: 14px } #emailButton { border-radius: 6px; background: #70b500!important; margin: 0 auto 60px; box-shadow: 0 4px 0 #578c00 } #socialLinks a { width: 40px } #socialLinks #blogLink { width: 80px!important } .sectionWrap { text-align: center } #header { color: #fff!important } </style></head><body> <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%"> <tbody> <tr> <td align="center" valign="top"> <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px"> <tbody> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%"> <tbody> <tr> <td> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td> </tr> </tbody> </table> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px"> <tbody> <tr> <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="https://s3-us-west-2.amazonaws.com/mak-s3/makLogo.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120"> <h1 style="font-size: 30px;"></h1> <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p> </td> </tr> 

	<tr> <td align="left" style="background:#FFFFFF;"> <p> Dear ${data.userName},<br>We have received your request for MAK booking cancellation. As you cancelled the job prior to 24 hours of when the job was due to start, and as per our agreed Terms & Conditions, you will receive a full refund.

</p> </td> </tr> 


	<tr> 
		 <td style="text-align: left; color:#000; font-weight: bold; padding: 0 10px">

             <table style="width: 100%; border: solid 1px #ccc; margin-top: 20px; text-align: center;">
             <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Number</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.bookingNumber}</td>
             	</tr>
             	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Maid Name</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.maidName}</td>
             	</tr>
             	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Maid ID</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.maidId}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Agency Name</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.agencyName}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Time</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.bookingTime}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Date</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.bookingDate}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Duration</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.duration}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Villa Number</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.villa}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Block Number</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.block}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Road Number/Name</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.road}</td>
             	</tr>
               <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Additional Details</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.details}</td>
             	</tr>
               <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">City</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.city}</td>
             	</tr>
                 <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Rate/Hour</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.rate}</td>
             	</tr>
                 <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Total Amount</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.amount}</td>
             	</tr>
             </table>

		 </td>
	</tr>


	
	<tr>  </div> </td> </tr> <tr> <tr> <td align="left" style="background:#FFFFFF;"> <p>Please contact us if you have any questions.<br>
Sincerely,<br>MAK Team </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px"> <tbody> <tr> <td class="footerContent" valign="top"> <table border="0" cellpadding="10" cellspacing="0" style="width:100%"> <tbody> <tr> <td valign="top">&nbsp; <div mc:edit="std_footer"><em>Copyright � 2018 MAK, All rights reserved.</em></div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table></body>

    `
    callback(null,content)
};

const bookingNotToCancel =  function(data,callback){

    let content = `
<head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width"> <title></title> <style type="text/css"> body, #bodyTable { height: 100%!important; margin: 0; padding: 0; width: 100%!important } #bodyTable { font-family: Helvetica Neue, Helvetica, Arial, sans-serif; min-height: 100%; background: #eee; color: #333 } body, table, td, p, a, li, blockquote { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100% } table, td { mso-table-lspace: 0; mso-table-rspace: 0 } img { -ms-interpolation-mode: bicubic } body { margin: 0; padding: 0; background: #eee } img { border: 0; height: auto; line-height: 100%; outline: 0; text-decoration: none } table { border-collapse: collapse!important; max-width: 100%!important } img { border: 0!important } h1 { font-family: Helvetica; font-size: 42px; font-style: normal; font-weight: bold; text-align: center; margin: 30px auto 0 } h2 { font-size: 32px; font-style: normal; font-weight: bold; margin: 50px auto 0 } h3 { font-size: 20px; font-weight: bold; margin: 25px 0; letter-spacing: normal; text-align: left } a h3 { color: #444!important; text-decoration: none!important } .titleLink { text-decoration: none!important } .preheaderContent { color: #808080; font-size: 10px; line-height: 125% } .preheaderContent a:link, .preheaderContent a:visited, .preheaderContent a .yshortcuts { color: #606060; font-weight: normal; text-decoration: underline } #emailHeader, #tacoTip { color: #fff } #emailHeader { background-color: #fff } #content p { color: #4d4d4d; margin: 20px 70px 30px; font-size: 24px; line-height: 32px; text-align: left } #button { display: inline-block; margin: 10px auto; background: #fff; border-radius: 4px; font-weight: bold; font-size: 18px; padding: 15px 20px; cursor: pointer; color: #0079bf; margin-bottom: 50px } #socialIconWrap img { line-height: 35px!important; padding: 0 5px } .footerContent div { color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center; max-width: 100%!important } .footerContent div a:link, .footerContent div a:visited { color: #369; font-weight: normal; text-decoration: underline } .footerContent img { display: inline } #socialLinks img { margin: 0 2px } #utility { border-top: 1px solid #ddd } #utility div { text-align: center } #monkeyRewards img { max-width: 160px } #emailFooter { max-width: 100%!important } #footerTwitter a, #footerFacebook a { text-decoration: none!important; color: #fff!important; font-size: 14px } #emailButton { border-radius: 6px; background: #70b500!important; margin: 0 auto 60px; box-shadow: 0 4px 0 #578c00 } #socialLinks a { width: 40px } #socialLinks #blogLink { width: 80px!important } .sectionWrap { text-align: center } #header { color: #fff!important } </style></head><body> <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%"> <tbody> <tr> <td align="center" valign="top"> <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px"> <tbody> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%"> <tbody> <tr> <td> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td> </tr> </tbody> </table> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px"> <tbody> <tr> <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="https://s3-us-west-2.amazonaws.com/mak-s3/makLogo.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120"> <h1 style="font-size: 30px;"></h1> <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p> </td> </tr> 

	<tr> <td align="left" style="background:#FFFFFF;"> <p> Dear ${data.userName},<br>We have received your request for a MAK booking cancellation.  As you cancelled the job within 24 hours of when the job was due to start, and as per our agreed Terms & Conditions, you are not entitled to a refund.
</p> </td> </tr> 


	<tr> 
		 <td style="text-align: left; color:#000; font-weight: bold; padding: 0 10px">

             <table style="width: 100%; border: solid 1px #ccc; margin-top: 20px; text-align: center;">
             <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Number</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.bookingNumber}</td>
             	</tr>
             	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Maid Name</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.maidName}</td>
             	</tr>
             	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Maid ID</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.maidId}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Agency Name</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.agencyName}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Time</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.bookingTime}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Date</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.bookingDate}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Duration</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.duration}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Villa Number</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.villa}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Block Number</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.block}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Road Number/Name</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.road}</td>
             	</tr>
               <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Additional Details</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.details}</td>
             	</tr>
               <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">City</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.city}</td>
             	</tr>
                 <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Rate/Hour</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.rate}</td>
             	</tr>
                 <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Total Amount</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.amount}</td>
             	</tr>
             </table>

		 </td>
	</tr>


	
	<tr>  </div> </td> </tr> <tr> <tr> <td align="left" style="background:#FFFFFF;"> <p>Please contact us if you have any questions.<br>
Sincerely,<br>MAK Team </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px"> <tbody> <tr> <td class="footerContent" valign="top"> <table border="0" cellpadding="10" cellspacing="0" style="width:100%"> <tbody> <tr> <td valign="top">&nbsp; <div mc:edit="std_footer"><em>Copyright � 2018 MAK, All rights reserved.</em></div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table></body>

    `
    callback(null,content)
};

const listAllCanceledServiceUsers = function (payloadData, userData, callback) {

    let response = {};
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        listServices: function (checkForUniqueKey, cb) {
            let criteria = {
                deleteAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                isCompleted: false,

            };


            let projection = {};
            let option = {
                lean: true,
                skip: ((payloadData.pageNo - 1) * payloadData.limit),
                limit: payloadData.limit
            };
            let populateArray = [
                {
                    path: 'userId',
                    match: {},
                    select: '_id fullName phoneNo email',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '_id firstName lastName makId',
                    option: {}
                }
            ];
            console.log("_______________criteria", criteria);

            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("_______________result");
                    if (result && result.length) {
                        response.count = result.length;
                        response.data = result;
                        cb(null)
                    } else {
                        response.count = 0;
                        response.data = [];
                        cb(null)
                    }

                }
            })
        }

    }, function (err, result) {
        callback(err, response)
    });
};

const requestForExtendService = function (payloadData, userData, callback) {

    let workDate = "";
    let startTime = "";
    let endTime = "";
    let maidId = "";
    let agencyId = "";
    let serviceMakId = "";
    let amount = 0;
    let commission = 0;
    let extendService = false;
    let bookingTime;

    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getServiceForAvailablity: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.serviceId,
            };
            let projection = {};
            let option = {
                lean: true
            };
            Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        let time = parseInt(result[0].endTime) + parseInt(payloadData.duration) * 3600000;

                        let hour = moment.tz(time, result[0].timeZone).get('hour');
                        let date =  moment.tz(time, result[0].timeZone).get('date');
                        console.log('#######################',time, date)

                        if(date > result[0].date || hour > 23){
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_BOOK);
                        }
                        else {
                            maidId = result[0].maidId;
                            agencyId = result[0].agencyId;
                            serviceMakId = result[0].serviceMakId;
                            bookingTime = result[0].hour;
                            workDate = result[0].workDate;
                            startTime = result[0].startTime;
                            commission = result[0].commission;
                            endTime = bookingTime + ((payloadData.duration + result[0].duration) * 60 * 60 * 1000);
                            cb(null)
                        }
                    } else {
                        cb(null)
                    }
                }
            })
        },

        checkMaidAvailability: function (getServiceForAvailablity, cb) {
            let criteria = {
                _id: {$nin: [payloadData.serviceId]},
                maidId : maidId,
                workDate: {$eq: workDate},
                $and: [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ],

                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                    Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
            };
            let projection = {};
            let option = {
                lean: true
            };
            console.log("_________startTime____________", startTime, endTime)

            Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {

                    if (result && result.length) {
                        let localArray = result.filter(obj => {
                            console.log("__________hello_____________",
                                obj.startTime,endTime,bookingTime,obj.bufferEndTime)
                            return (obj.startTime <= endTime && bookingTime < obj.bufferEndTime)
                        });
                        if (localArray && localArray.length) {
                            if (result[0].isExtend.requested || (result[0].isExtend.transactionId !== "" ||
                                    result[0].isExtend.transactionId !== undefined)) {
                                if(userData.appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_BUSY);
                                else   callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_BUSY_AR)
                            }
                            else {
                                extendService = true;
                                cb(null)
                            }
                        } else {
                            extendService = true;
                            cb(null)
                        }
                    } else {
                        extendService = true;
                        cb(null)
                    }
                }
            })
        },

        getMaidAmount: function (checkMaidAvailability, cb) {

            if (extendService) {
                let criteria = {
                    _id: maidId,
                    isDeleted: false,
                    isBlocked: false
                };

                let projection = {__v: 0, password: 0};
                let options = {
                    lean: true,
                };

                Service.MaidServices.getMaid(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            amount = result[0].price;

                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb()
            }
        },

        updateServiceDb: function (getMaidAmount, getServiceForAvailablity,cb) {
            if (extendService) {
                let criteria = {
                    _id: payloadData.serviceId,
                };

                let isExtend = {
                    reason: payloadData.reason,
                    extendDuration: payloadData.duration,
                    requested: true,
                    extendAmount: (amount * payloadData.duration) + ((commission * amount * payloadData.duration) / 100),
                    agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.PENDING,
                };

                let dataToUpdate = {
                    isExtend: isExtend,
                     // $inc :{
                     //    duration : payloadData.duration,
                     //     endTime : endTime,
                     //     bufferEndTime : bufferEndTime
                     // }
                    // agencyAction: Config.APP_CONSTANTS.DATABASE.ACTION.PENDING,
                };

                let options = {
                    new: true,
                };
                Service.ServiceServices.updateService(criteria, dataToUpdate, options, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },

        createNotificationForAgency: function (updateServiceDb, cb) {
            if (extendService) {
                let dataToSet = {
                    senderId: userData._id,
                    receiverId: agencyId,
                    maidId: maidId,
                    type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_REQUEST,
                    serviceId: payloadData.serviceId,
                    bookingId: serviceMakId,
                    agencyMessage: userData.fullName + " has requested for extension of service.",
                };
                Service.NotificationServices.createNotification(dataToSet, function (err, result) {
                    if (err) {
                        callback(err)
                    } else {
                        cb(null)
                    }
                })
            } else {
                cb(null)
            }

        }

    }, function (err, result) {
        callback(err, {})
    })
};

const addReview = function (payloadData, userData, callback) {

    let maidId = "",maidData;
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getForDuplicateReview: function (checkForUniqueKey, cb) {
            let criteria = {
                serviceId: payloadData.serviceId,
                reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER,
                userId: userData._id,
                isDeleted: false
            };
            let projection = {};
            let option = {
                lean: true
            };

            Service.ReviewServices.getReview(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        if(userData.appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_CANNOT_REVIEW_AGAIN)
                        else callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_CANNOT_REVIEW_AGAIN_AR)
                    }
                    else {
                        cb(null)
                    }
                }
            })
        },

        getServiceDetails: function (getForDuplicateReview, cb) {
            let criteria = {
                _id: payloadData.serviceId
            };
            let projection = {};
            let option = {
                lean: true
            };

            Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        if (result[0].userId.toString() == userData._id.toString()) {
                            maidId = result[0].maidId;
                            cb(null)
                        } else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
                        }
                    }
                    else {
                        cb(null)
                    }
                }
            })
        },

        createReviewByUser: function (getServiceDetails, cb) {
            let dataToSave = {
                uniquieAppKey: UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME),
                userId: userData._id,
                maidId: maidId,
                serviceId: payloadData.serviceId,
                feedBack: payloadData.feedBack || "",
                createdOn : +new Date(),
                reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER,
            };

            if (payloadData.maidRating) {
                dataToSave.maidRating = payloadData.maidRating
            }
            Service.ReviewServices.createReview(dataToSave, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    Models.Maids.populate(result,[
                        {
                            path :'maidId',
                            select :'firstName'
                        }
                    ],(err,result)=>{
                        maidData = result
                        cb(null)
                    })

                }
            })
        },
        updateNotification:function (createReviewByUser,cb) {
            let criteria = {
                serviceId: payloadData.serviceId,
                type: Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_COMPLETE,
                receiverId: userData._id
            };
            let dataToUp = {
                reviewSubmitted: true,
                userMessage: maidData.maidId.firstName + ' completed your service request, review submitted.'
            };

            Service.NotificationServices.updateNotification(criteria, dataToUp, {new: true}, (err) => {
                if (err) {
                    cb(err)
                } else cb()
            });
        }
    }, function (err, result) {
        callback(err, {})
    })
};

const changePassword = function (payloadData, userData, callback) {
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkCurrentPasswordAndUpdate: function (checkForUniqueKey, cb) {
            if (UniversalFunctions.CryptData(payloadData.oldPassword) === userData.password) {

                let criteria = {
                    _id: userData._id,
                    isDeleted: false,
                    isBlocked: false,
                    isDeactivated: false,
                };
                let projection = {
                    password: UniversalFunctions.CryptData(payloadData.newPassword),
                };
                let option = {new: true};

                Service.UserServices.updateUser(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        callback(null, Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT)
                    }
                })
            } else {
                if(userData.appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASSWORD)
                else callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASSWORD_AR)
            }
        },

    }, function (err, result) {
        callback(err, {})
    })
};

const forgetPassword = function (payloadData, callback) {
    let userData = {};
    let passwordResetToken = "";
    let response = {};
    let content;
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        verifyEmail: function (checkForUniqueKey, cb) {
            let criteria = {
                email: payloadData.email
            };

            let projection = {};
            let option = {lean: true};
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result && result.length) {
                        userData = result[0];
                        cb(null)
                    } else {

                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_REGISTERED)
                    }
                }
            })
        },

        generatePasswordResetToken: function (verifyEmail, cb) {
            passwordResetToken = (otpGenerator.generate(6, {specialChars: false}));
            let criteria = {
                email: payloadData.email,
                isDeleted: false,
                isDeactivated: false,
            };

            let projection = {
                $set: {
                    password: UniversalFunctions.CryptData(passwordResetToken),
                }
            };
            let option = {
                new: true
            };
            Service.UserServices.updateUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    response.passwordResetToken = passwordResetToken;
                    cb(null)
                }
            })
        },
        emailTemplate:function(generatePasswordResetToken,cb){
            let data={};
            data.password = passwordResetToken;
            
            //console.log("===========userData=======================",userData)
            data.fullName =   userData.fullName;

            MaidController.emailTemplateForgetPassword(data,(err,result)=>{
                if(err){
                    cb(null)
                }
                else{
                    content=result;
                    cb(null)
                }
            })
        },
        sendMail: function (emailTemplate, cb) {
            if (payloadData.email && (payloadData.email === userData.email)) {
                let subject = "M.A.K Temporary Password";

                MailManager.sendMailBySES(payloadData.email, subject, content, function (err, res) {
                });
                cb()
            }
            else {
                cb(null)
            }
        },
    }, function (err, result) {
        callback(err, {})
    })
};

const raiseAnIssue = function (payloadData, userData, callback) {

    let count;
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },
        checkTicketNo:function (cb) {
            let criteria = {
                isDeleted:false
            };
            let projection = {};
            let option = {
                lean: true
            };
            criteria.userType="USER";
            Service.IssueServices.getIssue(criteria,{},{},(err,result)=>{
                if(err){
                    cb(err)
                }
                else{
                    count= result.length+1;
                    cb(null);
                }
            })
        },
        createReviewUser: function (checkForUniqueKey,checkTicketNo, cb) {
            let dataToInsert = {
                ticketNo:count,
                userType: 'USER',
                userId: userData._id,
                issueDescription: payloadData.issue,
                uniquieAppKey: UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME),
            };

            Service.IssueServices.createIssue(dataToInsert, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result) {
                        cb(null)
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                    }
                }
            })
        }

    }, function (err, result) {
        callback(err, {})
    })
};

const getNotifications = function (payloadData, userData, callback) {
    let response = [];

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getNotification: function (checkForUniqueKey, cb) {
            console.log("____________userData..............", userData._id);
            if(userData._id){
                let criteria = {
                    $or: [
                        {receiverId: userData._id,},
                        {receiverArray: userData._id}
                    ],
                    isDeleted: {$nin: [userData._id]}
                    // read:false,
                };
                let projection = {};

                let option = {
                    lean: true,
                    sort: {'timeStamp': -1},
                };
                let populate = [
                    {
                        path: 'senderId',
                        match: {},
                        select: 'agencyName profilePicURL',
                        options: {}
                    },
                    {
                        path: 'maidId',
                        match: {},
                        select: 'firstName lastName profilePicURL',
                        options: {}
                    },
                ]

                Service.NotificationServices.getNotificationPopulate(criteria, projection, option, populate, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        response = data;
                        cb(null);
                    }
                })
            }
            else cb()

        },

        updateNotificationToRead: function (getNotification, cb) {
            if(userData._id){
                let criteria = {
                    $or: [
                        {receiverId: userData._id,},
                        {receiverArray: userData._id}
                    ],
                    read: false
                };
                let dataToSet = {
                    $addToSet: {
                        read: userData._id
                    }
                };
                let option = {
                    new: true,
                    multi: true
                };
                Service.NotificationServices.updateAllNotification(criteria, dataToSet, option, function (err, data) {
                    if (err) {
                        callback(err)
                    } else {
                        cb(null);
                    }
                })
            }
            else cb()

        },

    }, function (err, result) {
        callback(err, response)
    })
};

const sendNotification = function (notificationData, callback) {

    async.auto({
        sendPushFinally: function (cb) {
            if (!(notificationData.deviceToken == null)) {
                if (notificationData.deviceToken.length > 15) {
                    async.parallel([
                        function (cb) {
                            NotificationManager.sendPushUser(notificationData, function (err, result) {
                                if (err) {
                                    cb(null)
                                } else {
                                    cb(null)
                                }
                            })
                        },

                        function (cb) {
                            let dataToSet = {
                                senderId: notificationData.senderId,
                                receiverId: notificationData.receiverId,
                                type: notificationData.type,
                                requestId: notificationData.requestId,
                                message: notificationData.body,
                                bookingId: notificationData.bookingNumber,
                                maidId: notificationData.maidId
                            };

                            if (notificationData.type == Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_DELETE) {
                                dataToSet.adminMessage = notificationData.fullName + " deleted a service request."
                            }
                            Service.NotificationServices.createNotification(dataToSet, function (err, result) {
                                if (err) {
                                    callback(err)
                                } else {
                                    cb(null)
                                }
                            })
                        },
                    ], function (err, result) {
                        callback(err, result)
                    })
                } else {
                    callback()
                }
            } else {
                callback()
            }
        }
    }, function (err, res) {
        callback()
    })
};

const sendNotificationMaid = function (notificationData, callback) {

    async.auto({
        sendPushFinally: function (cb) {
            if (!(notificationData.deviceToken == null)) {
                if (notificationData.deviceToken.length > 15) {
                    async.parallel([
                        function (cb) {
                            NotificationManager.sendPushMaid(notificationData, function (err, result) {
                                if (err) {
                                    cb(null)
                                } else {
                                    cb(null)
                                }
                            })
                        },

                        function (cb) {
                            let dataToSet = {
                                senderId: notificationData.senderId,
                                receiverId: notificationData.receiverId,
                                maidId: notificationData.maidId,
                                type: notificationData.type,
                                serviceId: notificationData.requestId,
                                bookingId: notificationData.bookingNumber,
                                maidMessage: notificationData.body,
                            };

                            Service.NotificationServices.createNotification(dataToSet, function (err, result) {
                                if (err) {
                                    callback(err)
                                } else {
                                    cb(null)
                                }
                            })
                        },
                    ], function (err, result) {
                        callback(err, result)
                    })
                } else {
                    callback()
                }
            } else {
                callback()
            }
        }
    }, function (err, res) {
        callback()
    })
};

const addFavouriteMaid = function (payloadData, userData, callback) {

    if (userData && userData._id) {
        async.autoInject({

            checkForUniqueKey: function (cb) {
                let uniquieAppKey = payloadData.uniquieAppKey;
                if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                    cb(null)
                } else {
                    callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
                }
            },

            addToFavouriteMaids: function (checkForUniqueKey, cb) {
                let criteria = {
                    _id: userData._id
                };
                let dataToUpdate = {
                    $addToSet: {
                        favouriteMaidId: payloadData.maidId
                    }
                };
                let option = {
                    lean: true,
                };
                Service.UserServices.updateUser(criteria, dataToUpdate, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            response.count = result.length;
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            }

        }, function (err, result) {
            callback(err, {})
        })
    } else {

        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PLEASE_SIGNUP, null)
    }

};

const listFavouriteMaid = function (payloadData, userData, callback) {
    let response = [];
    let offset = moment.tz(userData.timeZone).utcOffset();
    offset = offset * 60000;
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        listMaids: function (checkForUniqueKey, cb) {
            let criteria = {
                isDeleted: false,
                isBlocked: false,
                isVerified: true,
                acceptAgreement: true,
                _id: {$in: userData.favouriteMaidId}
            };

            Models.Maids.aggregate([
                {
                    $match: criteria
                },
                {
                    $lookup: {
                        from: "languages",
                        localField: "languages",
                        foreignField: "_id",
                        as: "languages"
                    }
                },
                {
                    $lookup: {
                        from: "agencies",
                        localField: "agencyId",
                        foreignField: "_id",
                        as: "agencyId"
                    }
                },
                {
                    $unwind: "$agencyId"
                },
                {
                    $project: {
                        _id: 1,
                        gender: 1,
                        email: 1,
                        country: 1,
                        countryCode: 1,
                        phoneNo: 1,
                        makId: 1,
                        agencyId: "$agencyId._id",
                        agencyName: "$agencyId.agencyName",
                        agencyImage: "$agencyId.profilePicURL",
                        locationName: 1,
                        languages: 1,
                        price: 1,
                        actualPrice: 1,
                        makPrice: 1,
                        experience: 1,
                        nationality: 1,
                        dob: 1,
                        description: 1,
                        lastName: 1,
                        firstName: 1,
                        feedBack: 1,
                        currentLocation: 1,
                        documentPicURL: 1,
                        profilePicURL: 1,
                        distance: 1,
                        religion: 1,
                        currency: 1,
                        timeSlot:1,
                    }
                },
                {
                    $graphLookup: {
                        from: "reviews",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "reviewData",
                        restrictSearchWithMatch: {
                            isDeleted: false,
                            isBlocked: false,
                            reviewByType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER
                        }
                    }
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "serviceData",
                        restrictSearchWithMatch: {
                           startTime:{$gte:new Date().setUTCHours(0,0,0,0)},
                            "agencyAction" : "ACCEPT",
                        }
                    }
                },
                {
                    $project: {
                        // "timeSlot.0": {
                        //     $setDifference: [
                        //         "$timeSlot.0",
                        //         {
                        //             $reduce: {
                        //                 input: {
                        //                     $filter: {
                        //                         input: "$serviceData",
                        //                         as: "item",
                        //                         cond: {
                        //                             $eq: [
                        //                                 {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                        //                                 1,
                        //                             ]
                        //                         }
                        //                     }
                        //                 },
                        //                 initialValue: [],
                        //                 in: {
                        //                     $concatArrays: [
                        //                         "$$value",
                        //                         [{$hour: {$add: [new Date(0),"$$this.hour",offset]}}],
                        //                         [{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}],
                        //                     ]
                        //                 }
                        //             }
                        //         }
                        //     ]
                        // },
                        // "timeSlot.1": {
                        //     $setDifference: [
                        //         "$timeSlot.1",
                        //         {
                        //             $reduce: {
                        //                 input: {
                        //                     $filter: {
                        //                         input: "$serviceData",
                        //                         as: "item",
                        //                         cond: {
                        //                             $eq: [
                        //                                 {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                        //                                 2,
                        //                             ]
                        //                         }
                        //                     }
                        //                 },
                        //                 initialValue: [],
                        //                 in: {
                        //                     $concatArrays: [
                        //                         "$$value",
                        //                         [{$hour: {$add: [new Date(0),"$$this.hour",offset]}}],
                        //                         [{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}],
                        //                     ]
                        //                 }
                        //             }
                        //         }
                        //     ]
                        // },
                        // "timeSlot.2": {
                        //     $setDifference: [
                        //         "$timeSlot.2",
                        //         {
                        //             $reduce: {
                        //                 input: {
                        //                     $filter: {
                        //                         input: "$serviceData",
                        //                         as: "item",
                        //                         cond: {
                        //                             $eq: [
                        //                                 {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                        //                                 3,
                        //                             ]
                        //                         }
                        //                     }
                        //                 },
                        //                 initialValue: [],
                        //                 in: {
                        //                     $concatArrays: [
                        //                         "$$value",
                        //                         [{$hour: {$add: [new Date(0), "$$this.hour",offset]}}],
                        //                         [{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}],
                        //                     ]
                        //                 }
                        //             }
                        //         }
                        //     ]
                        // },
                        // "timeSlot.3": {
                        //     $setDifference: [
                        //         "$timeSlot.3",
                        //         {
                        //             $reduce: {
                        //                 input: {
                        //                     $filter: {
                        //                         input: "$serviceData",
                        //                         as: "item",
                        //                         cond: {
                        //                             $eq: [
                        //                                 {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                        //                                 4,
                        //                             ]
                        //                         }
                        //                     }
                        //                 },
                        //                 initialValue: [],
                        //                 in: {
                        //                     $concatArrays: [
                        //                         "$$value",
                        //                         [{$hour: {$add: [new Date(0),"$$this.hour",offset]}}],
                        //                         [{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]
                        //                     ]
                        //                 }
                        //             }
                        //         }
                        //     ]
                        // },
                        // "timeSlot.4": {
                        //     $setDifference: [
                        //         "$timeSlot.4",
                        //         {
                        //             $reduce: {
                        //                 input: {
                        //                     $filter: {
                        //                         input: "$serviceData",
                        //                         as: "item",
                        //                         cond: {
                        //                             $eq: [
                        //                                 {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                        //                                 5,
                        //                             ]
                        //                         }
                        //                     }
                        //                 },
                        //                 initialValue: [],
                        //                 in: {
                        //                     $concatArrays: [
                        //                         "$$value",
                        //                         [{$hour: {$add: [new Date(0), "$$this.hour",offset]}}],
                        //                        // [{$hour: {$add: [new Date(0), {$multiply:["$$this.hour",'$$this.duration']},offset]}}],
                        //                        // [{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}],
                        //                        [{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]
                        //
                        //                     ]
                        //                 }
                        //             }
                        //         }
                        //     ]
                        // },
                        // "timeSlot.5": {
                        //     $setDifference: [
                        //         "$timeSlot.5",
                        //         {
                        //             $reduce: {
                        //                 input: {
                        //                     $filter: {
                        //                         input: "$serviceData",
                        //                         as: "item",
                        //                         cond: {
                        //                             $eq: [
                        //                                 {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                        //                                 6,
                        //                             ]
                        //                         }
                        //                     }
                        //                 },
                        //                 initialValue: [],
                        //                 in: {
                        //                     $concatArrays: [
                        //                         "$$value",
                        //                         [{$hour: {$add: [new Date(0),"$$this.hour",offset]}}],
                        //                         [{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}],
                        //                     ]
                        //                 }
                        //             }
                        //         }
                        //     ]
                        // },
                        // "timeSlot.6": {
                        //     $setDifference: [
                        //         "$timeSlot.6",
                        //         {
                        //             $reduce: {
                        //                 input: {
                        //                     $filter: {
                        //                         input: "$serviceData",
                        //                         as: "item",
                        //                         cond: {
                        //                             $eq: [
                        //                                 {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                        //                                 7,
                        //                             ]
                        //                         }
                        //                     }
                        //                 },
                        //                 initialValue: [],
                        //                 in: {
                        //                     $concatArrays: [
                        //                         "$$value",
                        //                         [{$hour: {$add: [new Date(0), "$$this.hour",offset]}}],
                        //                         [{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}],
                        //                     ]
                        //                 }
                        //             }
                        //         }
                        //     ]
                        // },
                        // 'timeSlot.0' :{
                        //           $reduce: {
                        //                     input: {
                        //                         $filter: {
                        //                             input: "$serviceData",
                        //                             as: "item",
                        //                             cond: {
                        //                                 $eq: [
                        //                                     {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                        //                                     1,
                        //                                 ]
                        //                             }
                        //                         }
                        //                     },
                        //                     initialValue: [],
                        //                     in: {
                        //                         $concatArrays :[
                        //                             "$$value",
                        //                             [{$hour: {$add: [new Date(0), "$$this.hour",offset]}}],
                        //                             [{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}],
                        //                         ]}
                        //
                        //             }
                        // },
                "timeSlot.0": {
                    $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                1,
                                            ]
                                        }
                                    }
                                },
                                initialValue:'$timeSlot.0',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                              {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]}]
                                        }
                                    }
                                }

                }
                },
                'timeSlot.1' :{
                            $reduce: {
                                    input: {
                                        $filter: {
                                            input: "$serviceData",
                                            as: "item",
                                            cond: {
                                                $eq: [
                                                    {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                    2,
                                                ]
                                            }
                                        }
                                    },
                                    initialValue: '$timeSlot.1',
                                    in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                },
                'timeSlot.2' :{
                            $reduce: {
                                    input: {
                                        $filter: {
                                            input: "$serviceData",
                                            as: "item",
                                            cond: {
                                                $eq: [
                                                    {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                    3,
                                                ]
                                            }
                                        }
                                    },
                                initialValue: '$timeSlot.2',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                             {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                },
                'timeSlot.3' :{
                            $reduce: {
                                    input: {
                                        $filter: {
                                            input: "$serviceData",
                                            as: "item",
                                            cond: {
                                                $eq: [
                                                    {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                    4,
                                                ]
                                            }
                                        }
                                    },
                                initialValue: '$timeSlot.3',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                              {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                },
                'timeSlot.4' :{
                            $reduce: {
                                    input: {
                                        $filter: {
                                            input: "$serviceData",
                                            as: "item",
                                            cond: {
                                                $eq: [
                                                    {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                    5,
                                                ]
                                            }
                                        }
                                    },
                                initialValue: '$timeSlot.4',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                              {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                },
                'timeSlot.5' :{
                            $reduce: {
                                    input: {
                                        $filter: {
                                            input: "$serviceData",
                                            as: "item",
                                            cond: {
                                                $eq: [
                                                    {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                    6,
                                                ]
                                            }
                                        }
                                    },
                                    initialValue: '$timeSlot.5',
                                    in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                               {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]}]
                                        }
                                    }

                                }

                            }
                },
                'timeSlot.6' :{
                            $reduce: {
                                    input: {
                                        $filter: {
                                            input: "$serviceData",
                                            as: "item",
                                            cond: {
                                                $eq: [
                                                    {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                    7,
                                                ]
                                            }
                                        }
                                    },
                                initialValue: '$timeSlot.6',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                              {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.bufferEndTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                },
                        _id: 1,
                        gender: 1,
                        //serviceData: 1,
                        time :{$hour: {$add: [new Date(0), 1527231600000,offset]}},
                        email: 1,
                        country: 1,
                        countryCode: 1,
                        phoneNo: 1,
                        makId: 1,
                        agencyId: 1,
                        agencyName: 1,
                        agencyImage: 1,
                        locationName: 1,
                        languages: 1,
                        price: 1,
                        actualPrice: 1,
                        makPrice: 1,
                        experience: 1,
                        nationality: 1,
                        dob: 1,
                        description: 1,
                        lastName: 1,
                        firstName: 1,
                        feedBack: 1,
                        currentLocation: 1,
                        documentPicURL: 1,
                        profilePicURL: 1,
                        distance: 1,
                        religion: 1,
                        currency: 1,
                        reviewData:1,
                    }
                },
                {$sort :{firstName :1}},
                {$skip: ((payloadData.pageNo - 1) * payloadData.limit)},
                {$limit: payloadData.limit}
            ], function (err, result) {
                console.log('4444444444444444',err)
                if (result && result.length) {
                    for (let i = 0; i < result.length; i++) {
                        if (result[i].experience == 2) {
                            result[i].experience1 = result[i].experience;
                            result[i].experience = 'Up to 2 years';
                        }
                        if (result[i].experience == 5) {
                            result[i].experience1 = result[i].experience;
                            result[i].experience = '3 to 5 years';
                        }
                        if (result[i].experience == 6) {
                            result[i].experience1 = result[i].experience;
                            result[i].experience = 'Above 5 years';
                        }

                        let reduceResult = {cleaning: 0, ironing: 0, cooking: 0, childCare: 0};
                        if (result[i].reviewData.length > 0) {
                            let countCleaning = 0;
                            let countIroning = 0;
                            let countCooking = 0;
                            let countChildCare = 0;
                            reduceResult = result[i].reviewData.reduce((prev, curr) => {
                                prev.cleaning += curr.maidRating.cleaning;
                                prev.ironing += curr.maidRating.ironing;
                                prev.cooking += curr.maidRating.cooking;
                                prev.childCare += curr.maidRating.childCare;

                                if (curr.maidRating.cleaning > 0) {
                                    countCleaning = countCleaning + 1;
                                }
                                if (curr.maidRating.ironing > 0) {
                                    countIroning = countIroning + 1;
                                }
                                if (curr.maidRating.cooking > 0) {
                                    countCooking = countCooking + 1;
                                }
                                if (curr.maidRating.childCare > 0) {
                                    countChildCare = countChildCare + 1;
                                }
                                return prev
                            }, reduceResult);
                            result[i].avgCleaning = (reduceResult.cleaning / countCleaning);
                            result[i].avgIroning = (reduceResult.ironing / countIroning);
                            result[i].avgCooking = (reduceResult.cooking / countCooking);
                            result[i].avgChildCare = (reduceResult.childCare / countChildCare);

                        }
                        else {
                            result[i].avgCleaning = 0;
                            result[i].avgIroning = 0;
                            result[i].avgCooking = 0;
                            result[i].avgChildCare = 0;
                        }
                    }
                    response = result;
                    cb()
                } else {
                    cb(null)
                }
            })
        },

    }, function (err, result) {
        callback(err, response)
    })
};

const checkMaidAvailable = function (payloadData, userData, callback) {

    let startTime = payloadData.startTime;
    let days = [];
    let daysGreater24 = [];
    let key = "";
    let keyGreater24 = "";
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        checkAvailabiltyOfMaid: function (checkForUniqueKey, cb) {
            let workDate = payloadData.workDate;
            let endTime = startTime + (payloadData.duration * 60 * 60 * 1000);
            let criteria = {
                maidId: payloadData.maidId,
                workDate: {$eq: workDate},
                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]}
            };

            let projection = {};
            let option = {
                lean: true
            };

            Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {

                    if (result && result.length) {
                        let localArray = result.filter(obj => {
                            return (obj.startTime <= endTime && startTime < obj.bufferEndTime)
                        });
                        if (localArray && localArray.length) {
                            if(userData.appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_BUSY);
                            else callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_BUSY_AR);
                        } else {
                            cb(null)
                        }
                    } else {
                        cb(null)
                    }
                }
            })
        },

        listMaids: function (checkAvailabiltyOfMaid, cb) {
            let criteria = {
                _id: mongoose.Types.ObjectId(payloadData.maidId),
                isDeleted: false,
                isBlocked: false,
                isVerified: true,
                acceptAgreement: true,
            };

            let startHour = parseInt(moment(parseInt(payloadData.startTime)).tz(payloadData.timeZone).hours());
            for (let i = startHour + 1; i < (startHour + 1 + payloadData.duration); i++) {
                (function (i) {
                    console.log("_i_", i);
                    if (i > 24) {
                        daysGreater24.push(i - 24)
                    } else {
                        days.push(i)
                    }
                }(i))
            }
            let criteriaDay = moment(parseInt(startTime)).tz(payloadData.timeZone).day();
            key = 'timeSlot.' + criteriaDay;
            keyGreater24 = 'timeSlot.' + (criteriaDay + 1);
            criteria[key] = {$all: days};
            if (daysGreater24 && daysGreater24.length) {
                criteria[keyGreater24] = {$all: daysGreater24};
            }

            Models.Maids.aggregate([
                {
                    $match: criteria
                },
            ], function (err, result) {
                if (result && result.length) {
                    cb()
                } else {
                    if(userData.appLanguage ===  Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN)
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_BUSY);
                    else callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_BUSY_AR);
                }
            })
        },

    }, function (err, result) {
        callback(err, {})
    })
};

const removeFavouriteMaid = function (payloadData, userData, callback) {

    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey === UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        removeFromFavouriteMaids: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: userData._id
            };
            let dataToUpdate = {
                $pull: {
                    favouriteMaidId: payloadData.maidId
                }
            };
            let option = {
                lean: true,
            };
            Service.UserServices.updateUser(criteria, dataToUpdate, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }

    }, function (err, result) {
        callback(err, {})
    })
};

const addCard = function (payloadData, userData, callback) {

    let cardFingerPrint;
    let cardToken;
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        /*createSource: function (checkForUniqueKey, cb) {
            stripe.customers.createSource(
                userData.customerStripeId,
                {source: payloadData.cardToken},
                function (err, card) {
                    console.log("_________________1_______________", err)
                    if (err) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_CARD);
                    }
                    else {
                        cardFingerPrint = card.fingerprint;
                        cardToken = card.id;
                        cb(null);
                    }
                });
        },
*/
       /* getDuplicateCard: function (createSource, cb) {

            let criteria = {
                _id: userData._id,
                "cardDetails.cardFingerPrint": cardFingerPrint,
                "cardDetails.isDeleted": false
            };
            let dataToSet = {
                "cardDetails.$": 1
            };
            console.log("_________________crietreia_______________", criteria, dataToSet)

            Service.UserServices.getUsers(criteria, dataToSet, {lean: true}, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    console.log("_________________2_______________", result)

                    if (result && result.length) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_CARD);
                    }
                    else {
                        cb(null);
                    }
                }
            })
        },*/

        addCardDetailsIndb: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: userData._id
            };
            let setQuery = {
                $addToSet: {
                    cardDetails: {
                        cardHolderName: payloadData.name,
                        cardNumber: payloadData.cardNumber,
                        cardType: payloadData.cardNumber,
                        validTill: payloadData.cardNumber,
                        customerEmail: payloadData.customerEmail || '',
                        customerPassword: payloadData.customerPassword || '',
                        cardFingerPrint: '',
                        cardToken: payloadData.cardToken
                    }
                }
            };
            let options = {new: true};
            Service.UserServices.updateUser(criteria, setQuery, options, function (err, data) {
                if (err) {
                    cb(err)
                }
                else {
                    cb(null)
                }
            })
        }

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            console.log("_______________finalResponse add card", {cardToken: cardToken})
            callback(null, {cardToken: cardToken});
        }
    })
};

const getCards = function (payloadData, userData, callback) {

    let cardDetails = [];
    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getCardDetails: function (checkForUniqueKey, cb) {
            if (userData.cardDetails && userData.cardDetails.length > 0) {

                userData.cardDetails.map(function (obj) {
                    if (obj.isDeleted == false) {
                        cardDetails.push(obj)
                    }
                });
                cb(null)
            }
            else {
                cb()
            }
        }

    }, function (err, result) {
        callback(err, cardDetails)
    })
};

const checkVersion = function (payloadData, callback) {

    let criteria ={
        appType : payloadData.appType
    };

    Service.UserServices.getVersion(criteria, {}, {}, (err, data)=> {
        if (err) {
            callback(err)
        }
        else {
            if(payloadData.deviceType === 'IOS'){
                if(data[0].latestIOSVersion > payloadData.appVersion)
                    callback(null,{status :1});   //any update
                else if(data[0].criticalIOSVersion > payloadData.appVersion)
                    callback (null,{status : 2});   //force update

                else  callback (null,{status : 3});   //no update
            }
            else {
                if(data[0].latestAndroidVersion > payloadData.appVersion)
                    callback(null,{status :1});
                else if(data[0].criticalAndroidVersion > payloadData.appVersion)
                    callback (null,{status : 2});

                else callback (null,{status : 3});
            }
        }
    })
};

const deleteCard = function (payloadData, userData, callback) {

    async.autoInject({
        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        deletCardDetails: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: userData._id
            };

            var setQuery = {
                $pull: {cardDetails: {_id: payloadData.cardId}}
            };

            var options = {new: true};
            Service.UserServices.updateUser(criteria, setQuery, options, function (err, data) {
                if (err) {
                    cb(err)
                }
                else {
                    cb(null);
                }
            })
        }

    }, function (err, result) {
        callback(err, {})
    })

};

const createPayment = function (payloadData, userData, callback) {

    let bookingId = Math.floor(100000 + Math.random() * 900000);
    let trxId = "";
    let paymentUrl = "";
    let data;
    let paymentType = "";
    async.autoInject({

        checkForUniqueKey: function (cb) {
            let uniquieAppKey = payloadData.uniquieAppKey;
            if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                cb(null)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            }
        },

        getServiceData: function (checkForUniqueKey, cb) {
            let criteria = {
                _id: payloadData.serviceId
            };
            let projection = {};
            let option = {
                lean: true
            };
            let populateArray = [
                {
                    path: 'agencyId',
                    match: {},
                    select: '_id agencyName',
                    option: {},
                },
                {
                    path: 'maidId',
                    match: {},
                    select: '',
                    option: {}
                },
                {
                    path: 'userId',
                    match: {},
                    select: 'fullName',
                    option: {}
                },

            ];
            Service.ServiceServices.getServicePopulate(criteria, projection, option, populateArray, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        if (result[0].currency == "BHD") {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.COUNTRY_NOT_AVAILABLE)
                        } else {
                            paymentType = "stripe";
                            data = result;
                            data[0].bookingId = bookingId;
                            cb(null)
                        }

                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                    }

                }
            })
        },

        checkIfPaymentCanBeDoneOrNot: function (getServiceData, cb) {
            let workDate = data[0].workDate;
            let startTime = data[0].startTime;
            let endTime = startTime + ((data[0].duration + data[0].isExtend.extendDuration) * 60 * 60 * 1000);
            let criteria = {
                _id: {$nin: [payloadData.serviceId]},
                maidId: data[0].maidId,
                workDate: {$eq: workDate},
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                agencyAction: {$in: [Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT, Config.APP_CONSTANTS.DATABASE.ACTION.PENDING]},
            };

            if (!payloadData.isExtension || payloadData.isExtension == null || payloadData.isExtension == undefined) {
                criteria.$and = [
                    {transactionId: {$exists: true}},
                    {transactionId: {$nin: ["", null]}}
                ]
            }
            else {
                criteria.$and = [
                    {"isExtend.requested": true},
                    {"isExtend.agencyAction": "ACCEPT"},
                    {"isExtend.transactionId": {$exists: true}},
                    {"isExtend.transactionId": {$nin: ["", null]}}
                ]
            }

            let projection = {};
            let option = {
                lean: true
            };

            Service.ServiceServices.getService(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        let localArray = result.filter(obj => {
                            return (obj.startTime <= endTime && startTime < obj.bufferEndTime)
                        });
                        if (localArray && localArray.length) {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.MAID_BUSY);
                        } else {
                            cb(null)
                        }
                    } else {
                        cb(null)
                    }
                }
            })

        },

        createChargesStripe: function (checkIfPaymentCanBeDoneOrNot, cb) {
            if (paymentType == "stripe") {
                console.log('3333333333333333',data[0])
                let paymentParameters = {
                    currency: "aed",
                    source: payloadData.cardToken,
                    description: userData.fullName + " successfull payment for service"
                };
                if (payloadData.saveCards == false) {

                } else {
                    paymentParameters.customer = userData.customerStripeId
                }

                if (!payloadData.isExtension || payloadData.isExtension == null || payloadData.isExtension == undefined) {
                    paymentParameters.amount = data[0].amount * 100
                } else {
                    paymentParameters.amount = data[0].isExtend.extendAmount * 100
                }
                console.log('paymentParameters',paymentParameters)
                stripe.charges.create(
                    paymentParameters,
                    function (err, stripeResponse) {
                        if (err) {
                            if (err) {
                                console.log("error", err);
                                callback({
                                    statusCode: 400,
                                    customMessage: err.message,
                                    type: err.code
                                });
                            }
                        } else {
                            trxId = stripeResponse.id || null;
                            cb(null);
                        }
                    }
                );
            } else {
                cb(null)
            }
        },

        createChargesPayTabs: function (checkIfPaymentCanBeDoneOrNot, cb) {
            cb(null)
            /*
             if(paymentType == "payTabs"){
             let nameArray=data[0].userId.fullName.split(" ");
             let formData = {
             merchant_email: "ankitk@code-brew.com",
             secret_key: "0myG5bJxCYveCSYAKVvEr1vwRuFzXnJXJhEPson5nSjZLiAvd6aF8FbZaRz7ei5YAKO38b8w4svPK6lYcwEkcPkmGTtVczUMjIPs",
             site_url: "http://52.36.127.111//MAK/AgencyPanel/#/login",
             return_url: "http://52.36.127.111//MAK/AgencyPanel/#/login",
             title: data[0].bookingId,
             cc_first_name:nameArray[0],
             cc_last_name: nameArray[1] || "abc",
             cc_phone_number: data[0].billingAddress.phoneNo,
             phone_number:  data[0].billingAddress.phoneNo,
             email:  data[0].billingAddress.email,
             products_per_title: data[0].maidId.makId,
             // unit_price: data[0].actualPrice,
             // quantity: data[0].duration,
             other_charges: "0",
             // amount: data[0].amount,
             discount: "0",
             currency: "BHD",
             reference_no: data[0].maidId.makId,
             ip_customer: "1.1.1.0",
             ip_merchant: "1.1.1.0",
             billing_address:data[0].address.streetName+" "+data[0].address.buildingName+" "+data[0].address.villaName ,
             city: data[0].address.city,
             state: data[0].address.city,
             postal_code: "12345",
             country: "BHR",
             shipping_first_name: data[0].billingAddress.billingName,
             shipping_last_name: "",
             address_shipping: data[0].billingAddress.streetName+" "+data[0].billingAddress.buildingName+" "+data[0].billingAddress.villaName,
             state_shipping: data[0].billingAddress.city,
             city_shipping: data[0].billingAddress.city,
             postal_code_shipping: "1234",
             country_shipping: "BHR",
             msg_lang: "English",
             cms_with_version: "WordPress4.0-WooCommerce2.3.9",
             is_tokenization: "TRUE",
             is_existing_customer: "FALSE",
             };

             if (!payloadData.isExtension || payloadData.isExtension == null || payloadData.isExtension == undefined) {
             formData.unit_price = data[0].actualPrice
             formData.quantity = data[0].duration
             formData.amount = data[0].amount
             } else {
             formData.unit_price = data[0].actualPrice
             formData.amount = data[0].isExtend.extendDuration
             formData.quantity = data[0].isExtend.extendAmount
             }

             request.post({
             url: 'https://www.paytabs.com/apiv2/create_pay_page',
             formData: formData
             }, function (err, httpResponse, body) {
             if (err) {
             cb(err)
             } else {
             console.log('____________SUCCESSFULL API HIT____________',body);
             let result=JSON.parse(body);
             if(!result.payment_url && !result.p_id){
             callback({
             statusCode: 400,
             customMessage: result.result,
             type: result.response_code
             });
             }else{
             paymentUrl=result.payment_url;
             trxId=result.p_id;
             cb(null)
             }
             }
             });
             }
             else{
             cb(null)
             }*/
        },

        updateService: function (createChargesStripe, createChargesPayTabs, cb) {
            if (!payloadData.isExtension || payloadData.isExtension == null || payloadData.isExtension == undefined) {
                let criteria = {
                    _id: payloadData.serviceId
                };

                let dataToUpdate = {};

                if (paymentType == "stripe") {
                    dataToUpdate.cardToken = payloadData.cardToken;
                    dataToUpdate.transactionId = trxId;
                    dataToUpdate.bookingId = bookingId;
                } else {
                    // dataToUpdate.transactionId=trxId;
                    // dataToUpdate.bookingId=bookingId;
                }

                if (userData.isGuestFlag) {
                    dataToUpdate.firstBookingDone = true
                }

                let option = {new: true};
                Service.ServiceServices.updateService(criteria, dataToUpdate, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null)
                    }
                })
            }
            else {
                let criteria = {
                    _id: payloadData.serviceId
                };

                let dataToUpdate;

                if (paymentType == "stripe") {
                    dataToUpdate = {
                        "isExtend.transactionId": trxId,
                        "isExtend.cardToken": payloadData.cardToken,
                        "isExtend.bookingId": bookingId
                    };
                } else {
                    /*dataToUpdate = {
                     "isExtend.transactionId": trxId,
                     "isExtend.bookingId": bookingId
                     };*/
                }

                let option = {new: true};
                Service.ServiceServices.updateService(criteria, dataToUpdate, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null)
                    }
                })
            }
        },

    }, function (err, result) {
        if (err) {
            callback(null)
        }
        else {
            callback(null, {
                trxId: bookingId,
                // paymentUrl:paymentUrl,
                data: data,
            })
        }
    })

};

const acceptTemp =  function(data,callback){

    let content = `
  <head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width"> <title></title> <style type="text/css"> body, #bodyTable { height: 100%!important; margin: 0; padding: 0; width: 100%!important } #bodyTable { font-family: Helvetica Neue, Helvetica, Arial, sans-serif; min-height: 100%; background: #eee; color: #333 } body, table, td, p, a, li, blockquote { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100% } table, td { mso-table-lspace: 0; mso-table-rspace: 0 } img { -ms-interpolation-mode: bicubic } body { margin: 0; padding: 0; background: #eee } img { border: 0; height: auto; line-height: 100%; outline: 0; text-decoration: none } table { border-collapse: collapse!important; max-width: 100%!important } img { border: 0!important } h1 { font-family: Helvetica; font-size: 42px; font-style: normal; font-weight: bold; text-align: center; margin: 30px auto 0 } h2 { font-size: 32px; font-style: normal; font-weight: bold; margin: 50px auto 0 } h3 { font-size: 20px; font-weight: bold; margin: 25px 0; letter-spacing: normal; text-align: left } a h3 { color: #444!important; text-decoration: none!important } .titleLink { text-decoration: none!important } .preheaderContent { color: #808080; font-size: 10px; line-height: 125% } .preheaderContent a:link, .preheaderContent a:visited, .preheaderContent a .yshortcuts { color: #606060; font-weight: normal; text-decoration: underline } #emailHeader, #tacoTip { color: #fff } #emailHeader { background-color: #fff } #content p { color: #4d4d4d; margin: 20px 10px 0px; font-size: 12px; line-height: 20px; text-align: left !important; } #button { display: inline-block; margin: 10px auto; background: #fff; border-radius: 4px; font-weight: bold; font-size: 18px; padding: 15px 20px; cursor: pointer; color: #0079bf; margin-bottom: 50px } #socialIconWrap img { line-height: 35px!important; padding: 0 5px } .footerContent div { color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center; max-width: 100%!important } .footerContent div a:link, .footerContent div a:visited { color: #369; font-weight: normal; text-decoration: underline } .footerContent img { display: inline } #socialLinks img { margin: 0 2px } #utility { border-top: 1px solid #ddd } #utility div { text-align: center } #monkeyRewards img { max-width: 160px } #emailFooter { max-width: 100%!important } #footerTwitter a, #footerFacebook a { text-decoration: none!important; color: #fff!important; font-size: 14px } #emailButton { border-radius: 6px; background: #70b500!important; margin: 0 auto 60px; box-shadow: 0 4px 0 #578c00 } #socialLinks a { width: 40px } #socialLinks #blogLink { width: 80px!important } .sectionWrap { text-align: center } #header { color: #fff!important } </style></head><body> <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%"> <tbody> <tr> <td align="center" valign="top"> <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px"> <tbody> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%"> <tbody> <tr> <td> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td> </tr> </tbody> </table> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px"> <tbody> <tr> <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="https://s3-us-west-2.amazonaws.com/mak-s3/makLogo.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120"> <h1 style="font-size: 30px;"></h1> <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p> </td> </tr> 

	<tr> <td align="left" style="background:#FFFFFF;"> <p style="font-size:17px"> <br>Thank you for MAKing it ! Your booking details are shown below.</p> </td> </tr> 


	<tr> 
		 <td style="text-align: left; color:#000; font-weight: bold; padding: 0 10px">

             <table style="width: 100%; border: solid 1px #ccc; margin-top: 20px; text-align: center;">
             <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Number</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.bookingNumber}</td>
             	</tr>
             	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Maid Name</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.maidName}</td>
             	</tr>
             	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Maid ID</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.maidId}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Agency Name</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.agencyName}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Time</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.bookingTime}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Date</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.bookingDate}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Booking Duration</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.duration}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Villa Number</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.villa}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Block Number</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.block}</td>
             	</tr>
               	<tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Road Number/Name</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.road}</td>
             	</tr>
               <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Additional Details</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.details}</td>
             	</tr>
               <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">City</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.city}</td>
             	</tr>
                 <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Rate/Hour</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.rate}</td>
             	</tr>
                 <tr>
             		 <td style="width:50%; font-weight: bold; padding: 10px;">Total Amount</td>
             		 <td style="width:50%; color:#999; padding: 10px;">${data.amount}</td>
             	</tr>
             </table>

		 </td>
	</tr>
<tr> 
  <td align="left" style="background:#FFFFFF;";><br><p style="font-size:12px">
  As a platform providing the marketplace for maids and those who wish to engage their services, MAK cannot be held responsible for MAK Registered Maids who do not fulfill their commitment. However, in case we have been made aware of a MAK Registered Maid�s failure to fulfill her commitment, MAK will undertake to find a replacement for its Users from among their pool of MAK Registered Maids or Agency Maids, which have been rated on the same level as the maid originally booked. Please refer to the Terms & Condition in our policies in these particular cases.</p>
  </td>
</tr>

 <tr> <td align="left" style="background:#FFFFFF;"> <p style="font-size:17px">Sincerely,<br>MAK Team </tbody> </table> </td> </tr> <tr> 

<td align="center" valign="top"> <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px"> <tbody> <tr> <td class="footerContent" valign="top"> <table border="0" cellpadding="10" cellspacing="0" style="width:100%"> <tbody> <tr> <td valign="top">&nbsp; <div mc:edit="std_footer"><em>Copyright � 2018 MAK, All rights reserved.</em></div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table></body>
    `
    callback(null,content)
};

const isExtended =  function(data,callback){

    let content = `
    <head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width"> <title></title> <style type="text/css"> body, #bodyTable { height: 100%!important; margin: 0; padding: 0; width: 100%!important } #bodyTable { font-family: Helvetica Neue, Helvetica, Arial, sans-serif; min-height: 100%; background: #eee; color: #333 } body, table, td, p, a, li, blockquote { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100% } table, td { mso-table-lspace: 0; mso-table-rspace: 0 } img { -ms-interpolation-mode: bicubic } body { margin: 0; padding: 0; background: #eee } img { border: 0; height: auto; line-height: 100%; outline: 0; text-decoration: none } table { border-collapse: collapse!important; max-width: 100%!important } img { border: 0!important } h1 { font-family: Helvetica; font-size: 42px; font-style: normal; font-weight: bold; text-align: center; margin: 30px auto 0 } h2 { font-size: 32px; font-style: normal; font-weight: bold; margin: 50px auto 0 } h3 { font-size: 20px; font-weight: bold; margin: 25px 0; letter-spacing: normal; text-align: left } a h3 { color: #444!important; text-decoration: none!important } .titleLink { text-decoration: none!important } .preheaderContent { color: #808080; font-size: 10px; line-height: 125% } .preheaderContent a:link, .preheaderContent a:visited, .preheaderContent a .yshortcuts { color: #606060; font-weight: normal; text-decoration: underline } #emailHeader, #tacoTip { color: #fff } #emailHeader { background-color: #fff } #content p { color: #4d4d4d; margin: 20px 70px 30px; font-size: 24px; line-height: 32px; text-align: left } #button { display: inline-block; margin: 10px auto; background: #fff; border-radius: 4px; font-weight: bold; font-size: 18px; padding: 15px 20px; cursor: pointer; color: #0079bf; margin-bottom: 50px } #socialIconWrap img { line-height: 35px!important; padding: 0 5px } .footerContent div { color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center; max-width: 100%!important } .footerContent div a:link, .footerContent div a:visited { color: #369; font-weight: normal; text-decoration: underline } .footerContent img { display: inline } #socialLinks img { margin: 0 2px } #utility { border-top: 1px solid #ddd } #utility div { text-align: center } #monkeyRewards img { max-width: 160px } #emailFooter { max-width: 100%!important } #footerTwitter a, #footerFacebook a { text-decoration: none!important; color: #fff!important; font-size: 14px } #emailButton { border-radius: 6px; background: #70b500!important; margin: 0 auto 60px; box-shadow: 0 4px 0 #578c00 } #socialLinks a { width: 40px } #socialLinks #blogLink { width: 80px!important } .sectionWrap { text-align: center } #header { color: #fff!important } </style></head><body> <table border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="height:100%; width:100%"> <tbody> <tr> <td align="center" valign="top"> <table align="center" border="0" cellspacing="0" id="emailContainer" style="width:600px"> <tbody> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="margin:15px 0 10px; width:100%"> <tbody> <tr> <td> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="left" class="preheaderContent" mc:edit="preheader_content00" style="text-align:left!important;" valign="top">Welcome to MAK</td> </tr> </tbody> </table> <table align="left" border="0" cellspacing="0" style="width:50%"> <tbody> <tr> <td align="right" class="preheaderContent" mc:edit="preheader_content01" style="text-align:right!important;" valign="top"><a href="" target="_blank">View this email in your browser</a>.</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" class="sectionWrap" id="content" style="background:#FFFFFF; border-radius:4px; box-shadow:0px 3px 0px #DDDDDD; overflow:hidden; width:600px"> <tbody> <tr> <td id="header" style="background-color:#141a29;color:#FFFFFF!important;" valign="top"><img alt="Lukiwave Logo" src="https://s3-us-west-2.amazonaws.com/mak-s3/makLogo.png" style="display:block;margin:40px auto 0;width:170px!important;" width="120"> <h1 style="font-size: 30px;"></h1> <p style="display:block;margin-bottom:50px;color:#FFFFFF;"></p> </td> </tr> 

	<tr> <td align="left" style="background:#FFFFFF;"> <p> 
	<br>Thank you for MAKing it ! Your booking is extended successfully for ${data.duration} hours.</p> </td> </tr> 

	
 <tr> <td align="left" style="background:#FFFFFF;"> <p>Sincerely,<br>MAK Team </tbody> </table> </td> </tr> <tr> <td align="center" valign="top"> <table align="center" border="0" cellpadding="10" cellspacing="0" id="emailFooter" style="width:600px"> <tbody> <tr> <td class="footerContent" valign="top"> <table border="0" cellpadding="10" cellspacing="0" style="width:100%"> <tbody> <tr> <td valign="top">&nbsp; <div mc:edit="std_footer"><em>Copyright � 2018 MAK, All rights reserved.</em></div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table></body>
    `
    callback(null,content)
};

const payTabs = function (payloadData, userData, callback) {

    let paymentUrl = "";
    async.autoInject({
        /*
         validateSecretKey:function (checkForUniqueKey, cb) {
         var formData = {
         merchant_email:"ankitk@code-brew.com",
         secret_key:"0myG5bJxCYveCSYAKVvEr1vwRuFzXnJXJhEPson5nSjZLiAvd6aF8FbZaRz7ei5YAKO38b8w4svPK6lYcwEkcPkmGTtVczUMjIPs",
         };
         request.post({url:'https://www.paytabs.com/apiv2/validate_secret_key', formData: formData}, function (err, httpResponse, body) {
         if (err) {
         cb(err)
         }else {
         console.log('____________SUCCESSFULL API HIT____________', httpResponse, body);
         cb(null)
         }
         });
         },
         */
        createPayPage: function (cb) {
            var formData = {
                merchant_email: "ankitk@code-brew.com",
                secret_key: "0myG5bJxCYveCSYAKVvEr1vwRuFzXnJXJhEPson5nSjZLiAvd6aF8FbZaRz7ei5YAKO38b8w4svPK6lYcwEkcPkmGTtVczUMjIPs",
                site_url: "http://52.36.127.111/MAK/AgencyPanel/#/login",
                return_url: "http://52.36.127.111/MAK/AgencyPanel/#/login",
                title: "JohnDoe And Co.",
                cc_first_name: "John",
                cc_last_name: "Doe",
                cc_phone_number: "00973",
                phone_number: "123123123456",
                email: "johndoe@example.com",
                products_per_title: "MobilePhone",
                unit_price: "12.123",
                quantity: "1",
                other_charges: "0",
                amount: "12.123",
                discount: "0",
                currency: "BHD",
                reference_no: "ABC-123",
                ip_customer: "1.1.1.0",
                ip_merchant: "1.1.1.0",
                billing_address: "Flat 3021 Manama Bahrain",
                city: "Manama",
                state: "Manama",
                postal_code: "12345",
                country: "BHR",
                // shipping_first_name: "John",
                // shipping_last_name: "Doe",
                address_shipping: "Flat 3021 Manama Bahrain",
                state_shipping: "Manama",
                city_shipping: "Manama",
                postal_code_shipping: "1234",
                country_shipping: "BHR",
                msg_lang: "English",
                cms_with_version: "WordPress4.0-WooCommerce2.3.9",
                is_tokenization: "TRUE",
                is_existing_customer: "FALSE",
                /*pt_token: "dp425YJr6ro-hoIHCTdj8jPNnn1Cd8VSw-djJZmUEilExaI",
                 pt_customer_email: "johndoe@example.com",
                 pt_customer_password: "123456"*/
            };
            request.post({
                url: 'https://www.paytabs.com/apiv2/create_pay_page',
                formData: formData
            }, function (err, httpResponse, body) {
                if (err) {
                    cb(err)
                } else {
                    let result = JSON.parse(body);
                    if (result.response_code >= 4000) {
                        callback({
                            statusCode: 400,
                            customMessage: result.result,
                            type: result.response_code
                        });
                    } else {
                        console.log('____________SUCCESSFULL API HIT____________', body);
                        paymentUrl = JSON.parse(body).payment_url;
                        cb(null)
                    }
                }
            });
            /*
             var formData = {
             merchant_email: "ankitk@code-brew.com",
             secret_key: "0myG5bJxCYveCSYAKVvEr1vwRuFzXnJXJhEPson5nSjZLiAvd6aF8FbZaRz7ei5YAKO38b8w4svPK6lYcwEkcPkmGTtVczUMjIPs",
             payment_reference:":114917"
             };
             request.post({
             url: 'https://www.paytabs.com/apiv2/verify_payment',
             formData: formData
             }, function (err, httpResponse, body) {
             if (err) {
             cb(err)
             } else {
             console.log('____________SUCCESSFULL API HIT____________', httpResponse, body);
             cb(null)
             }
             });*/
        }

    }, function (err, result) {
        callback(err, {paymentUrl: paymentUrl})
    })
};

const searchMaidAndAgencyWeb = function (payloadData, userData, callback) {
    let response = {};
    async.autoInject({
        getAllMaids: function (cb) {
            let criteria = {
                isBlocked: false,
                isDeleted: false,
            };
            let project = {};
            let option = {
                lean: true,
                skip: ((payloadData.pageNo - 1) * payloadData.limit),
                limit: payloadData.limit
            };
            Service.MaidServices.getMaid(criteria, project, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.data = result;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getAllMaidCount: function (cb) {
            let criteria = {
                isBlocked: false,
                isDeleted: false,
            };
            let project = {};
            let option = {
                lean: true,
            };
            Service.MaidServices.getMaid(criteria, project, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        response.count = result.length;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        }
    }, function (err, result) {
        callback(err, response)
    })
};

const getSlots = function (payloadData, userData, callback) {
    let response = {};
    let offset = moment.tz(payloadData.timeZone).utcOffset();
    offset = offset * 60000;
    async.autoInject({
        getdata: function (cb) {
            Models.Maids.aggregate([
                {
                    $match :{_id :mongoose.Types.ObjectId(payloadData.maidId)}
                },
                {
                    $graphLookup: {
                        from: "services",
                        startWith: "$_id",
                        connectFromField: "_id",
                        connectToField: "maidId",
                        as: "serviceData",
                        restrictSearchWithMatch: {
                            startTime:{$gte:new Date().setUTCHours(0,0,0,0)},
                            "agencyAction" : "ACCEPT",
                            deleteAction : {$ne : 'DELETED'}
                        }
                    }
                },

                {
                    $project: {
                       // serviceData :1,
                        "timeSlot.0": {
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                1,
                                            ]
                                        }
                                    }
                                },
                                initialValue:'$timeSlot.0',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                               {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }
                                }

                            }
                        },
                        'timeSlot.1' :{
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                2,
                                            ]
                                        }
                                    }
                                },
                                initialValue: '$timeSlot.1',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                               {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                        },
                        'timeSlot.2' :{
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                3,
                                            ]
                                        }
                                    }
                                },
                                initialValue: '$timeSlot.2',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                        },
                        'timeSlot.3' :{
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                4,
                                            ]
                                        }
                                    }
                                },
                                initialValue: '$timeSlot.3',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                               {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                        },
                        'timeSlot.4' :{
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                5,
                                            ]
                                        }
                                    }
                                },
                                initialValue: '$timeSlot.4',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                        },
                        'timeSlot.5' :{
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                6,
                                            ]
                                        }
                                    }
                                },
                                initialValue: '$timeSlot.5',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }

                                }

                            }
                        },
                        'timeSlot.6' :{
                            $reduce: {
                                input: {
                                    $filter: {
                                        input: "$serviceData",
                                        as: "item",
                                        cond: {
                                            $eq: [
                                                {$dayOfWeek: {$add: [new Date(0), "$$item.startTime"]}},
                                                7,
                                            ]
                                        }
                                    }
                                },
                                initialValue: '$timeSlot.6',
                                in: {
                                    $filter: {
                                        input: "$$value",
                                        as: "item",
                                        cond: {
                                            $or:[{$and:[{$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$lt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]},
                                                {$and:[{$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.hour",offset]}}]},
                                                {$gt:['$$item',{$hour: {$add: [new Date(0),"$$this.endTime",offset]}}]}]}]
                                        }
                                    }
                                }
                            }
                        },
                    }
                }
            ],(err,result)=>{
                if(err)
                    cb(err);
                else {
                    response = result;
                    cb()
                }
            });
        }
    }, function (err, result) {
        callback(err, response[0])
    })
};

const credimaxSession = function (payloadData, userData, callback) {
    let response = {};
    let canBeDeleted = false;
    let canRefund = false;
    let offset = momentTz.tz(payloadData.timeZone).utcOffset() * 60 * 1000;

    let notificationData = {};
    let maidId = "";
    let agencyId = "";
    let serviceMakId = "";

    async.autoInject({
        // checkForUniqueKey: function (cb) {
            // let uniquieAppKey = payloadData.uniquieAppKey;
            // if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                // cb(null)
            // } else {
                // callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            // }
        // },

        // checkIfItcanBecanceled: function (checkForUniqueKey, cb) {
        checkIfItcanBecanceled: function (cb) {
           var username = "merchant.E13730950",
			password = "03687a9cc02ed4588917fee0b49b56ff",
			url = "http://www.example.com",
			auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
            var options = {
				method: 'POST',
				uri: 'https://credimax.gateway.mastercard.com/api/rest/version/49/merchant/E13730950/session',
				body: {
					"apiOperation":"CREATE_CHECKOUT_SESSION",
					"order":{"currency":"BHD",
					"id":"43185owf44d4447",
					"amount":payloadData.amount
					},
						"interaction":{
						"returnUrl":"https://www.google.com"
						}
				},
				// qs: {
					// 'username': 'merchant.E13730950',
					// 'password': '03687a9cc02ed4588917fee0b49b56ff',
				// },
				headers: {
					"Authorization" : auth
				},
				json: true // Automatically stringifies the body to JSON
			};
			 
			rp(options)
				.then(function (data) {
					console.log("******** enter in success ****"+data)
					response = data;
					cb(null);
					// if(data.result=='SUCCESS'){
						// cb(data);
					// }
					// if(data.result=='ERROR'){
						// cb(data.error);
					// }
					
				})
				.catch(function (err) {
					console.log("******** enter in error ****"+err)
					cb(err);
				});
        },
      
        
       
    }, function (err, result) {
		console.log("******** enter in result ****"+result)
        if (result) {
            // callback(err, result)
            callback(err, response)
        } else {
            callback(err, "This service cannot be canceled")
        }
    })
};

const credimaxPayment = function (payloadData, userData, callback) {
    let response = {};
    let canBeDeleted = false;
    let canRefund = false;
    let offset = momentTz.tz(payloadData.timeZone).utcOffset() * 60 * 1000;

    let notificationData = {};
    let maidId = "";
    let agencyId = "";
    let serviceMakId = "";

    async.autoInject({
        // checkForUniqueKey: function (cb) {
            // let uniquieAppKey = payloadData.uniquieAppKey;
            // if (uniquieAppKey == UniversalFunctions.CryptData(UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.APP_NAME)) {
                // cb(null)
            // } else {
                // callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED)
            // }
        // },

        // checkIfItcanBecanceled: function (checkForUniqueKey, cb) {
        checkIfItcanBecanceled: function (cb) {	
			
           const randomString = crypto.randomBytes(Math.ceil(20/2)).toString('hex').slice(0,20);
			const randomString2 = crypto.randomBytes(Math.ceil(20/2)).toString('hex').slice(0,20);
			var username = "merchant.E13730950",
			password = "03687a9cc02ed4588917fee0b49b56ff",
			url = "https://credimax.gateway.mastercard.com/api/rest/version/49/merchant/E13730950/order/order-"+randomString2+"/transaction/trans-"+randomString,
			auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
            var options = {
				method: 'PUT',
				uri: url,
				body:{
					  "apiOperation": "AUTHORIZE",
					  "session": {
						"id": payloadData.session_id
					  },
					  "sourceOfFunds": {
						"provided": {
						  "card": {
							"expiry": {
							  "month": payloadData.exp_month,
							  "year": payloadData.exp_year
							},
							"number": payloadData.credit_card_number,
							"securityCode":payloadData.security_code
						  }
						},
						"type": "CARD"

					  },
					  "order": {
						"amount": payloadData.amount,
						"currency": "BHD"
					  }
					},
				// qs: {
					// 'username': 'merchant.E13730950',
					// 'password': '03687a9cc02ed4588917fee0b49b56ff',
				// },
				headers: {
					"Authorization" : auth
				},
				json: true // Automatically stringifies the body to JSON
			};
			 
			rp(options)
				.then(function (data) {
					// console.log("******** enter in success ****"+data.result)
					// console.log("******** enter in success ****"+JSON.stringify(data));
					// resolve(data);
					response = data;
					cb(null);
					// if(data.result=='FAILURE'){
						// reject(data.response);						
					// }
					// if(data.result=='ERROR'){
						// reject(data.error);						
					// }
					
					// if(data.return==1){
						// resovle(data);
					// }
					
				})
				.catch(function (err) {
					console.log("******** enter in error ****"+JSON.stringify(err.error));
					response = err;
					cb(null);
					
					// if(err.error.result=="ERROR"){
						// reject(err.error);
					// }
					
				});
        },
      
        
       
    }, function (err, result) {
		console.log("******** enter in result ****"+result)
        if (result) {
            // callback(err, result)
            callback(err, response)
        } else {
            callback(err, "This service cannot be canceled")
        }
    })
};

const testMail = function (payloadData, userData, callback) {
    let response = {};
    let canBeDeleted = false;
    let canRefund = false;
    let offset = momentTz.tz(payloadData.timeZone).utcOffset() * 60 * 1000;

    let notificationData = {};
    let maidId = "";
    let agencyId = "";
    let serviceMakId = "";

    async.autoInject({
      
        checkIfItcanBecanceled: function (cb) {
			let email = 'nitinsehgal@xperge.com';
			let subject = 'TEST MAK';
			// let subject = 'TEST MAK';
			let  result = 'success';
		  MailManager.sendMailBySES(email, subject, result, function (err, res) {
                cb(null);
			});
           
        }
      
        
       
    }, function (err, result) {
		console.log("******** enter in result ****"+JSON.stringify(result));
        if (result) {
            // callback(err, result)
            callback(err, response)
        } else {
            callback(err, "This service cannot be canceled")
        }
    })
};

module.exports = {

    signUp1: signUp1,
    signUp2: signUp2,
    updateDeviceToken: updateDeviceToken,
    verifyUser: verifyUser,
    addBillingInfo: addBillingInfo,
    emailLogin: emailLogin,
    facebookLogin: facebookLogin,
    updateUserProfile: updateUserProfile,
    saveAddress: saveAddress,
    deleteAddress: deleteAddress,
    getAllAddress: getAllAddress,
    userLogout: userLogout,
    listAllLanguagesUser: listAllLanguagesUser,
    changeLanguageUser: changeLanguageUser,
    listAllNationalityUser: listAllNationalityUser,
    listAllReligion: listAllReligion,
    listAllAgencyUser: listAllAgencyUser,
    listAllMaidsUser: listAllMaidsUser,
    listAllMaidsUserWeb: listAllMaidsUserWeb,
    getMaidProfileDetail: getMaidProfileDetail,
    bookService: bookService,
    listAllServiceUser: listAllServiceUser,
    cancelService: cancelService,
    listAllCanceledServiceUsers: listAllCanceledServiceUsers,
    requestForExtendService: requestForExtendService,
    addReview: addReview,
    changePassword: changePassword,
    forgetPassword: forgetPassword,
    raiseAnIssue: raiseAnIssue,
    getNotifications: getNotifications,
    sendNotification: sendNotification,
    sendNotificationMaid: sendNotificationMaid,
    addFavouriteMaid: addFavouriteMaid,
    listFavouriteMaid: listFavouriteMaid,
    checkMaidAvailable: checkMaidAvailable,
    removeFavouriteMaid: removeFavouriteMaid,
    addCard: addCard,
    getCards: getCards,
    deleteCard: deleteCard,
    createPayment: createPayment,
    createPaymentPaytabs: createPaymentPaytabs,
    payTabs: payTabs,
    getSlots: getSlots,
    checkVersionT: checkVersion,
    searchMaidAndAgencyWeb: searchMaidAndAgencyWeb,
    credimaxSession: credimaxSession,
    credimaxPayment: credimaxPayment,
    testMail: testMail,
};
