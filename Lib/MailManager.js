'use strict';

const Config = require('../Config');
const async = require('async');
var nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport('SES', {
    AWSAccessKeyID: Config.awsS3Config.s3BucketCredentials.accessKeyId,
    AWSSecretKey: Config.awsS3Config.s3BucketCredentials.secretAccessKey,
    ServiceUrl: 'email-smtp.us-west-2.amazonaws.com'
});

var sendMailBySES = function (email, subject, content, cb) {
    console.log('******************* Enter in mail Send*************')
    console.log(email);

    var nodemailer = require('nodemailer');
    var sesTransport = require('nodemailer-ses-transport');
    // var transporter = nodemailer.createTransport(sesTransport({
        // accessKeyId: Config.awsS3Config.s3BucketCredentials.accessKeyId,
        // secretAccessKey: Config.awsS3Config.s3BucketCredentials.secretAccessKey,
        // region:'us-west-2'
    // }));
	
	
        // rateLimit: 5 // do not send more than 5 messages in a second

	
	// var transporter = nodemailer.createTransport("SES", {
		// AWSAccessKeyID: Config.awsS3Config.s3BucketCredentials.accessKeyId,
		// AWSSecretKey: Config.awsS3Config.s3BucketCredentials.secretAccessKey,
		// SeviceUrl:"https://email.us-west-1.amazonaws.com"
	// });

	
	/*  var transporter = nodemailer.createTransport(sesTransport({
         accessKeyId: Config.awsS3Config.s3BucketCredentials.accessKeyId,
        secretAccessKey: Config.awsS3Config.s3BucketCredentials.secretAccessKey,
        rateLimit: 5
    }));
 */

	 var transporter = nodemailer.createTransport('SES', {
        AWSAccessKeyID: Config.awsS3Config.s3BucketCredentials.accessKeyId,
        AWSSecretKey: Config.awsS3Config.s3BucketCredentials.secretAccessKey,
        ServiceUrl: 'email-smtp.us-west-2.amazonaws.com'
    });
	
	
    var mailOptions = {
        // from: "shumi.gupta@code-brew.com",// sender address
        from: "MAK info@mak.today",// sender address
        to: email, // list of receivers
        bcc :'ritesh@xperge.com,kam@mak.today,nitinsehgal@xperge.com',
        subject: subject, // Subject line
        html: content,
        // attachments: [
        //     {   // file on disk as an attachment
        //         filename: 'tc.docx',
        //         path: '/var/app/dev/mak_backend/tc.docx' // stream this file
        //     }
        // ]
    };
	
	
	// nodemailer.sendMail({
        // transport : transporter, //pass your transport
        // sender : 'info@mak.today' ,
        // to : 'nitinsehgal@xperge.com',
        // subject : "SUBJECT",
        // html: '<p> Hello World </p>'
      // })
	  
	  
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log("Error is email: "+error);
            cb(null)
        }
        else {
            console.log('Message sent:');
            console.log(info)
            cb(null);
        }
    });
};


var sendMailBySESBooking = function (email, email_agency ,subject, content, cb) {
    console.log('*********** sendMailBySESBooking *******')
    console.log(email_agency);

    var mailOptions = {      
        from: "MAK info@mak.today",// sender address
        to: email+','+email_agency, // list of receivers
        bcc :'ritesh@xperge.com,makbookingconfirmation@gmail.com,nitinsehgal@xperge.com',
        subject: subject, // Subject line
        html: content,        
    };
	
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log("Error is email: "+error);
            cb(null)
        }
        else {
            console.log('Message sent:');
            console.log(info)
            cb(null);
        }
    });
};

var sendMailBySESBookingCancel = function (email, email_agency ,subject, content, cb) {
    console.log('*********** sendMailBySESBookingCancel*******')
    console.log(email_agency);

    var mailOptions = {      
        from: "MAK info@mak.today",// sender address
        to: email+','+email_agency, // list of receivers
        bcc :'ritesh@xperge.com,nitinsehgal@xperge.com',
        subject: subject, // Subject line
        html: content,        
    };
	
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log("Error is email: "+error);
            cb(null)
        }
        else {
            console.log('Message sent:');
            console.log(info)
            cb(null);
        }
    });
};

module.exports = {
    sendMailBySES: sendMailBySES,
    sendMailBySESBooking: sendMailBySESBooking,
    sendMailBySESBookingCancel: sendMailBySESBookingCancel
};