'use strict';

const async = require('async');

const FCM = require('fcm-node');
const serverKeyUser = 'AAAAZzfT5eE:APA91bHp1ItbBCKKyeDs2MAKdUsSsjCJuYwOpLWQLh9WabB2dwIYEVUCD3E2y3XfGv4SlZdrxeY92pQqR5zvgHlke42DrkGgoUeR4e9QMbwJx8-dHkkTrVLiib_N615VqIPUQIAF-bMu';
const serverKeyMaid = 'AAAAul7iVhg:APA91bGEv2S9-dEyvd9D_-Us5cvr7pLcjyIAQS4rqn5f7UbRJXh17p2tBOq8T9iaeCRGe6e2SApjmwTJZjNgmf6TIGhSMyLNEUfjtPLf4ssm4whXcI_CnQvDvw15wKvminp1iymONzKn';

const sendPushUser = function (notificationData, callback) {
    const fcm = new FCM(serverKeyUser);
    async.auto({
        sendPush: function (cb) {
            const message = {
                to: notificationData.deviceToken,
                // collapse_key: 'demo',
                notification : {
                    sound: 'default',
                    badge: 0,
                    title: notificationData.title || "",
                    body: notificationData.body|| "",
                    type: notificationData.type|| "",
                    typeId: notificationData.requestId|| "",
                    receiverId: notificationData.receiverId|| "",
                    fullName: notificationData.fullName|| "",
                    profilePicURL: notificationData.profilePicURL|| "",
                    requestId: notificationData.requestId|| "",
                    senderId: notificationData.senderId|| "",
                    maidId: notificationData.maidId|| "",
                    maidName: notificationData.maidName|| "",
                    maidProfile: notificationData.maidProfile|| "",
                    senderProfilePicURL: notificationData.senderProfilePicURL|| "",
                    bookingNumber: notificationData.bookingNumber|| "",
                },
                data: {
                    sound: 'default',
                    badge: 0,
                    title: notificationData.title|| "",
                    body: notificationData.body|| "",
                    type: notificationData.type|| "",
                    typeId: notificationData.requestId|| "",
                    receiverId: notificationData.receiverId|| "",
                    fullName: notificationData.fullName|| "",
                    profilePicURL: notificationData.profilePicURL|| "",
                    requestId: notificationData.requestId|| "",
                    senderId: notificationData.senderId|| "",
                    maidId: notificationData.maidId|| "",
                    maidName: notificationData.maidName|| "",
                    maidProfile: notificationData.maidProfile|| "",
                    senderProfilePicURL: notificationData.senderProfilePicURL|| "",
                    bookingNumber: notificationData.bookingNumber|| "",
                },
                priority: 'high'
            };

            fcm.send(message, function (err, result) {
                if (err) {
                    console.log("Something has gone wrong!", err);
                    callback(err);
                } else {
                    console.log("Successfully sent with response: ", result);
                    cb(null, result);
                }
            });
        }
    }, function (err, result) {
        callback(err, result);
    })
};

const sendPushMaid = function (notificationData, callback) {
    const fcm1 = new FCM(serverKeyMaid);
    async.auto({
        sendPush: function (cb) {
            const message = {
                to: notificationData.deviceToken,
                // collapse_key: 'demo',
                notification : {
                    sound: 'default',
                    badge: 0,
                    title: notificationData.title|| "",
                    body: notificationData.body|| "",
                    type: notificationData.type|| "",
                    typeId: notificationData.requestId|| "",
                    receiverId: notificationData.receiverId|| "",
                    fullName: notificationData.fullName|| "",
                    profilePicURL: notificationData.profilePicURL|| "",
                    requestId: notificationData.requestId|| "",
                    senderId: notificationData.senderId|| "",
                    maidId: notificationData.maidId|| "",
                    maidName: notificationData.maidName|| "",
                    maidProfile: notificationData.maidProfile|| "",
                    senderProfilePicURL: notificationData.senderProfilePicURL|| "",
                    bookingNumber: notificationData.bookingNumber|| "",
                },
                data: {
                    sound: 'default',
                    badge: 0,
                    title: notificationData.title|| "",
                    body: notificationData.body|| "",
                    type: notificationData.type|| "",
                    typeId: notificationData.requestId|| "",
                    receiverId: notificationData.receiverId|| "",
                    fullName: notificationData.fullName|| "",
                    profilePicURL: notificationData.profilePicURL|| "",
                    requestId: notificationData.requestId|| "",
                    senderId: notificationData.senderId|| "",
                    maidId: notificationData.maidId|| "",
                    maidName: notificationData.maidName|| "",
                    maidProfile: notificationData.maidProfile|| "",
                    senderProfilePicURL: notificationData.senderProfilePicURL|| "",
                    bookingNumber: notificationData.bookingNumber|| "",
                },
                priority: 'high'
            };
            fcm1.send(message, function (err, result) {
                if (err) {
                    console.log("Something has gone wrong!", err);
                    callback(err);
                } else {
                    console.log("Successfully sent with response: ", result);
                    cb(null, result);
                }
            });
        }
    }, function (err, result) {
        callback(err, result);
    })
};

const sendPushInChat = function (notificationData, callback) {
    const fcm = new FCM(serverKeyUser);
    const fcm1 = new FCM(serverKeyMaid);

    async.auto({
        sendPush: function (cb) {
            const message = {
                to: notificationData.deviceToken,
                // collapse_key: 'demo',
                notification : {
                    sound: 'default',
                    badge: 0,
                    title: notificationData.title,
                    body: notificationData.body,
                    type: notificationData.type,
                    serviceId: notificationData.serviceId,
                    senderId: notificationData.senderId,
                    fullName: notificationData.fullName,
                    receiverId: notificationData.receiverId,
                    receiverName: notificationData.receiverName,
                    message: notificationData.message,
                    location: notificationData.location,
                },
                data: {
                    sound: 'default',
                    badge: 0,
                    title: notificationData.title,
                    body: notificationData.body,
                    type: notificationData.type,
                    serviceId: notificationData.serviceId,
                    senderId: notificationData.senderId,
                    fullName: notificationData.fullName,
                    receiverId: notificationData.receiverId,
                    receiverName: notificationData.receiverName,
                    message: notificationData.message,
                    location: notificationData.location,
                },
                priority: 'high'
            };

            if(notificationData.senderType == "USER"){
                fcm1.send(message, function (err, result) {
                    if (err) {
                        console.log("Something has gone wrong!", err);
                        callback(err);
                    } else {
                        console.log("Successfully sent with response: ", result);
                        cb(null, result);
                    }
                });
            }else if(notificationData.senderType == "MAID"){
                fcm.send(message, function (err, result) {
                    if (err) {
                        console.log("Something has gone wrong!", err);
                        callback(err);
                    } else {
                        console.log("Successfully sent with response: ", result);
                        cb(null, result);
                    }
                });
            }

        }
    }, function (err, result) {
        callback(err, result);
    })
};

module.exports = {
    sendPushUser: sendPushUser,
    sendPushMaid:sendPushMaid,
    sendPushInChat:sendPushInChat,
};