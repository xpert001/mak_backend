'use strict';

const Config = require('../Config');
const async = require('async');

const client = require('twilio')(Config.smsConfig.twilioCredentials.accountSid, Config.smsConfig.twilioCredentials.authToken);


let sendSMSToUser = function (five_digit_verification_code, senderName, countryCode, phoneNo, callback) {

    if(five_digit_verification_code == 'INVITE'){
        var From= Config.smsConfig.twilioCredentials.smsFromNumber;
        var To= countryCode + phoneNo.toString();
        var Body= senderName+" invited you to download Kabootz application";
    }
    else if(senderName == 'FORGOT_PASSWORD'){
        var From= Config.smsConfig.twilioCredentials.smsFromNumber;
        var To= countryCode + phoneNo.toString();
        var Body = "Hi, there was a request to change your password. Your new temporary password is " + five_digit_verification_code + ". " +"\n"+
            "Please use this to login with your existing email address or phone number. " +"\n"+
            "Thank you. " +"\n"+
            "Team Kabootz. "
    }
    else{
        var From= Config.smsConfig.twilioCredentials.smsFromNumber;
        var To= countryCode + phoneNo.toString();
        var Body= "Your 5 digit verification code for Kabootz is "+five_digit_verification_code;
    }

    async.series([
        function (cb) {
            client.messages.create({
                to: To,
                from: From,
                body: Body,
            }, function(err, message) {
                if (err) {
                    console.log("error in sending sms........",err)
                }
                else {
                    console.log("message sid..................",message);
                }
            });
            cb();
        }
    ],function (err, result) {
        if(err){
            callback()
        }else{
            callback()
        }
    })
};

module.exports = {
    sendSMSToUser: sendSMSToUser,
};