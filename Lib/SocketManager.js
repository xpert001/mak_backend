/*
'use strict';

let TokenManager = require('../Lib/TokenManager');
let Service = require('../Services');
let NotificationManager = require('../Lib/NotificationManager');
let async = require('async');
let io = null;
let accessToken = "";
let socketId = "";
let _ = require('lodash');
let mongoose = require('mongoose');

exports.connectSocket = function (server) {
    if (!server.app) {
        server.app = {}
    }
    server.app.socketConnections = {};
    io = require('socket.io').listen(server.listener);

    io.on('connection', function (socket) {
        console.log("...........................CONNECTION..................................");

        if (socket.handshake.query && socket && socket.id) {
            if(socket.handshake.query.userAccessToken){
                accessToken = socket.handshake.query.userAccessToken;
                socketId = socket.id;
                verifyUser(accessToken, socketId, function (err, result) {
                    if (err) {
                        console.log("___________NOT VERIFIED_____________");
                    } else {
                        console.log("___________VERIFIED_____________");
                    }
                });
            }
            else if(socket.handshake.query.maidAccessToken){
                accessToken = socket.handshake.query.maidAccessToken;
                socketId = socket.id;
                verifyMaid(accessToken, socketId, function (err, result) {
                    if (err) {
                        console.log("___________NOT VERIFIED_____________");
                    } else {
                        console.log("___________VERIFIED_____________");
                    }
                });
            }
        }

        else if (!(socket.handshake.query && (socket.handshake.query.userAccessToken || socket.handshake.query.maidAccessToken))) {
            console.log("___________NO ACCESS TOKEN____________");
        }

        else if (!(socket && socket.id)) {
            console.log("___________NO SOCKET ID____________");
        }

        else {
            console.log("___________UNEXPECTED ERROR____________");
        }


        socket.on('sendMessage', function (data, callback) {
            console.log("____________sendMessage______________", data);

            /!*var abc={
             senderType:["MAID","USER"],
             senderId:"",
             receiverId:"",
             messageType:['MESSAGE','LOCATION'],
             message:"",
             timeStamp:"",
             };*!/
            callback("acknowledge");

            let dataToSend = {
                senderId: data.senderId,
                receiverId: data.receiverId,
                message: data.message,
                messageType: data.messageType,
                timeStamp: data.timeStamp,
            };

            if (data.senderType == "MAID") {
                getUser(data.receiverId, function (err, res) {
                    if (err) {
                        console.log("___________NO RECEIVER FOUND____________");
                    } else {
                        updateChat(data, function (err, res1) {
                            if (err) {
                                console.log("err 2....................", err)
                            } else {
                                if (res.socketId) {
                                    if (res1 && res1._id) {
                                        dataToSend.messageId = res1._id;
                                        socket.to(res.socketId).emit("messageFromServer", dataToSend);
                                        console.log("_________________messageFromServer", dataToSend)
                                    } else {
                                        console.log("__________MESSAGE NOT SAVED IN DB____________");
                                    }
                                } else {
                                    console.log("___________NO SOCKET ID____________");
                                }
                            }
                        })
                    }
                });
            }

            else if (data.senderType == "USER") {
                getMaid(data.receiverId, function (err, res) {
                    if (err) {
                        console.log("___________NO RECEIVER FOUND____________");
                    } else {
                        updateChat(data, function (err, res1) {
                            if (err) {
                                console.log("err 2....................", err)
                            } else {
                                if (res.socketId) {
                                    if (res1 && res1._id) {
                                        dataToSend.messageId = res1._id;
                                        socket.to(res.socketId).emit("messageFromServer", dataToSend);
                                        console.log("_________________messageFromServer", dataToSend)
                                    } else {
                                        console.log("__________MESSAGE NOT SAVED IN DB____________");
                                    }
                                } else {
                                    console.log("___________NO SOCKET ID____________");
                                }
                            }
                        })
                    }
                });
            }
        });

        socket.on('disconnect', function () {
            console.log("________________DISCONNECT_______________");
        });

    });
};


const verifyUser = function (accessToken, socketId, cb) {
    let criteria = {
        accessToken: accessToken,
    };
    let projection = {
        socketId: socketId
    };
    let option = {lean: true, new: true};
    Service.UserServices.updateUser(criteria, projection, option, function (err, result) {
        if (err) {
            cb()
        } else {
            cb(null, result)
        }
    })
};

const verifyMaid = function (accessToken, socketId, cb) {
    let criteria = {
        accessToken: accessToken,
    };
    let projection = {
        socketId: socketId
    };
    let option = {lean: true, new: true};
    Service.MaidServices.updateMaid(criteria, projection, option, function (err, result) {
        if (err) {
            cb()
        } else {
            cb(null, result)
        }
    })
};

const getUser = function (_id, cb) {
    let criteria = {
        _id: _id,
        isDeleted: false,
    };
    let projection = {
        _id: 1,
        socketId: 1,
        firstName: 1,
        lastName: 1,
        profilePicURL: 1
    };
    let option = {lean: true};
    Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
        if (err) {
            cb()
        } else {
            if (result && result.length) {
                cb(null, result[0])
            } else {
                cb(null)
            }
        }
    })
};

const getMaid = function (_id, cb) {
    let criteria = {
        _id: _id,
        isDeleted: false,
    };
    let projection = {
        _id: 1,
        socketId: 1,
        firstName: 1,
        lastName: 1,
        profilePicURL: 1
    };
    let option = {lean: true};
    Service.MaidServices.getMaid(criteria, projection, option, function (err, result) {
        if (err) {
            cb()
        } else {
            if (result && result.length) {
                cb(null, result[0])
            } else {
                cb(null)
            }
        }
    })
};

const updateChat = function (chatdata, callback) {
    let message = {};
    async.auto({

        createChatInDb: function (cb) {

            var dataToSave = {};
            dataToSave.senderType = chatdata.senderType;
            dataToSave.senderId = chatdata.senderId;
            dataToSave.receiverId = chatdata.receiverId;
            dataToSave.conversationId = [chatdata.senderId, chatdata.receiverId].sort().join('.');
            dataToSave.message = chatdata.message;
            dataToSave.messageType = chatdata.messageType;
            dataToSave.timeStamp = chatdata.timeStamp;
            dataToSave.isDelivered = true;

            Service.ChatServices.createChat(dataToSave, function (err, res) {
                if (err) {
                    cb(err)
                } else {
                    // console.log("chat data is saved");
                    message = res;
                    cb(null)
                }
            })
        }

    }, function (err, res) {
        callback(err, message)
    })
};*/
