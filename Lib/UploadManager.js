var Config = require('../Config');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var Path = require('path');
var knox = require('knox');
let fs= require('fs');
var fsExtra = require('fs-extra');
var im = require('imagemagick');
let AWS = require("aws-sdk");
var mime = require('mime-types')


/*
 1) Save Local Files
 2) Create Thumbnails
 3) Upload Files to S3
 4) Delete Local files
 */



var awsUpload= function (fname,data,callback){
    console.log("bnzc.....",fname)
    var imgLink;
    AWS.config.update({
        accessKeyId: Config.awsS3Config.s3BucketCredentials.accessKeyId,
        secretAccessKey: Config.awsS3Config.s3BucketCredentials.secretAccessKey,
        region:'us-west-2'
    });
    var s3 = new AWS.S3();
    var cType=fname.split('.');
    let keyname=new Date().getTime()+fname;
    async.auto({
        uploadImage:function (cb) {
            var params = {
                Bucket: Config.awsS3Config.s3BucketCredentials.bucket,
                Key: keyname,
                Body: data,
                // ACL: 'public-read',
                // ContentType: cType[(cType.length)-1],
                ContentType: mime.lookup(fname)

            };
            s3.putObject(params, function (err, res) {
                if (err) {
                    cb(err)
                } else {
                    imgLink=Config.awsS3Config.s3BucketCredentials.s3URL+keyname;
                    cb(null)
                }
            });
        },
    },function (err,result) {
        if(err){
            callback(err)
        }else{
            callback(null,imgLink)
        }
    })
};

//Set Base URL for Images
var baseFolder = Config.awsS3Config.s3BucketCredentials.folder.profilePicture + '/';

var baseURL = Config.awsS3Config.s3BucketCredentials.s3URL + '/' + baseFolder;

function uploadFileToS3WithThumbnail(fileData, userId, callbackParent) {
    //Verify File Data
    var profilePicURL = {
        original: null,
        thumbnail: null
    };
    var originalPath = null;
    var thumbnailPath = null;
    var originalPath1 = null;
    var thumbnailPath1 = null;
    var dataToUpload = [];

    async.series([
        function (cb) {
            //Validate fileData && userId
            if (!userId || !fileData || !fileData.filename) {
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            } else {
                cb();
            }
        }, function (cb) {
            //Set File Names
            profilePicURL.original = getFileNameWithUserId(false, fileData.filename, userId);
            profilePicURL.thumbnail = getFileNameWithUserId(true, fileData.filename, userId);
            cb();

        },
        function (cb) {
            //Save File
            var path = Path.resolve(".") + "/uploads/" + profilePicURL.original;
            saveFile(fileData.path, path, function (err, data) {
                cb(err, data)
            })
        },
        function (cb) {
            //Create Thumbnail
            originalPath = Path.resolve(".") + "/uploads/" + profilePicURL.original;
            thumbnailPath = Path.resolve(".") + "/uploads/" + profilePicURL.thumbnail;
            createThumbnailImage(originalPath, thumbnailPath, function (err, data) {
                dataToUpload.push({
                    originalPath: originalPath,
                    nameToSave: profilePicURL.original
                });
                dataToUpload.push({
                    originalPath: thumbnailPath,
                    nameToSave: profilePicURL.thumbnail
                });
                cb(err, data)
            })
        },
        function (cb) {
            //Upload both images on S3
            parallelUploadTOS3(dataToUpload, cb);
        }
    ], function (err, result) {
        callbackParent(err, profilePicURL)
    });
}


var getFileNameWithUserId = function (thumbFlag, fullFileName, userId) {
    var prefix = Config.APP_CONSTANTS.DATABASE.PROFILE_PIC_PREFIX.ORIGINAL;
    var date=new Date().getTime()
    var ext = fullFileName && fullFileName.length > 0 && fullFileName.substr(fullFileName.lastIndexOf('.') || 0, fullFileName.length);
    if (thumbFlag) {
        prefix = Config.APP_CONSTANTS.DATABASE.PROFILE_PIC_PREFIX.THUMB;
    }
    return prefix + date + ext;
};

function uploadFile(fileData, userId, type, callback) {
    //Verify File Data
    var imageURL = {
        original: null,
        thumbnail: null
    };
    var logoURL = {
        original: null,
        thumbnail: null
    };
    var documentURL = null;

    var originalPath = null;
    var thumbnailPath = null;
    var dataToUpload = [];

    async.series([
        function (cb) {
            if (!userId || !fileData || !fileData.filename) {
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            } else {
                cb();
            }
        }, function (cb) {
            imageURL.original = UniversalFunctions.getFileNameWithUserIdWithCustomPrefix(false, fileData.filename, type,  userId);
            imageURL.thumbnail = UniversalFunctions.getFileNameWithUserIdWithCustomPrefix(true, fileData.filename, type, userId);
            cb();
        },
        function (cb) {
            var path = Path.resolve(".") + "/uploads/" + imageURL.original;
            saveFile(fileData.path, path, function (err, data) {
                cb(err, data)
            })
        },
        function (cb) {
            originalPath = Path.resolve(".") + "/uploads/" + imageURL.original;
            dataToUpload.push({
                originalPath: originalPath,
                nameToSave: imageURL.original
            });
            if (type == UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.FILE_TYPES.LOGO){
                thumbnailPath = Path.resolve(".") + "/uploads/" + imageURL.thumbnail;
                createThumbnailImage(originalPath, thumbnailPath, function (err, data) {
                    dataToUpload.push({
                        originalPath: thumbnailPath,
                        nameToSave: imageURL.thumbnail
                    });
                    cb(err, data)
                })
            }else {
                cb();
            }

        },
        function (cb) {
            parallelUploadTOS3(dataToUpload, cb);
        }
    ], function (err, result) {
        callback(err, imageURL)
    });
}


function parallelUploadTOS3(filesArray, callback) {
    var client = knox.createClient({
        key: Config.awsS3Config.s3BucketCredentials.accessKeyId,
        secret: Config.awsS3Config.s3BucketCredentials.secretAccessKey,
        bucket: Config.awsS3Config.s3BucketCredentials.bucket
    });
    var s3ClientOptions = {'x-amz-acl': 'public-read'};
    var taskToUploadInParallel = [];
    filesArray.forEach(function (fileData) {
        taskToUploadInParallel.push((function (fileData) {
            return function (internalCB) {
                if (!fileData.originalPath || !fileData.nameToSave) {
                    console.log("here in ....errrr......",fileData.originalPath, fileData.nameToSave)
                    internalCB(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                } else {
                    console.log("here in ..........",fileData.originalPath, fileData.nameToSave)
                    client.putFile(fileData.originalPath, fileData.nameToSave, s3ClientOptions, function (err, result) {
                        deleteFile(fileData.originalPath);
                        internalCB(err, result);
                    })
                }
            }
        })(fileData))
    });
    async.parallel(taskToUploadInParallel, callback)
}


function saveFile(fileData, path, callback) {
    fsExtra.copy(fileData, path, callback);
}

function deleteFile(path) {
    fsExtra.remove(path, function (err) {
    });
}


function createThumbnailImage(originalPath, thumbnailPath, callback) {
    im.resize({
        srcPath: originalPath,
        dstPath: thumbnailPath,
        width:   256
    }, function(err, data){
        if (err) {
            callback(err)
        }else{
            callback(null,thumbnailPath)
        }
    });
};


module.exports = {
    uploadFileToS3WithThumbnail: uploadFileToS3WithThumbnail,
    uploadFile: uploadFile,
    awsUpload:awsUpload
};
