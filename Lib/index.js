
'use strict';

module.exports = {
    UploadManager:require('./UploadManager'),
    SocketManager:require('./SocketManager'),
    SMSManager:require('./SMSManager'),
    MailManager:require('./MailManager'),
    NotificationManager:require('./NotificationManager'),
};