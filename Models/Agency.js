var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');

var Agency = new Schema({
    uniquieAppKey:{type:String, required:true},
    agencyName: {type: String, trim: true, index: true, required:true},
    adminNote: {type: String, trim: true},
    agencyWebsite: {type: String, trim: true},
    agencyOwnerName: {type: String, trim: true, required:true},
    email: {type: String, trim: true, index: true, sparse: true},
    isVerified: {type: Boolean, default: false, required: true},
    address: {
        streetName: {type: String, default: ''},
        buildingName: {type: String, default: ''},
        villaName: {type: String, default: ''},
        city: {type: String, default: ''},
        country:{type: String, default: ''},
       },
    countryName: {type: String, trim: true},                                           //INDIA where agency belong(UAE/ BAHRAIN)
    countryENCode: {type: String, trim: true},                                           //IN
    countryCode: {type: String, trim: true, min: 2, max: 5},                      //+91
    phoneNo: {type: String, trim: true, index: true, min: 5, max: 15},

    currentLocation: {type: [Number], index: '2dsphere'},                    //long,lat
    locationName:{type: String, default:""},
    radius:{type:Number,default:0},

    password: {type: String, default: ''},
    passwordResetToken: {type: String, trim: true},
    accessToken: {type: String, trim: true, index: true},
    deviceToken: {type: String, trim: true},
    profilePicURL: {
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''},
    },
    documentPicURL: {
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''},
    },
    nationalId: {type: String, trim: true, default:""},

    agencyType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.AGENCY_TYPE.MAK_REGISTERED_BAHRAIN,
            Config.APP_CONSTANTS.DATABASE.AGENCY_TYPE.MAK_REGISTERED_UAE,
            Config.APP_CONSTANTS.DATABASE.AGENCY_TYPE.NORMAL,
        ],
        default: Config.APP_CONSTANTS.DATABASE.ACTION.PENDING,
    },
    registrationDate: {type: Date, default: Date.now, required: true},
    lastTimeLogin: {type: Date, default: null},                           // update when logout
    isBlocked: {type: Boolean, default: false, required: true},
    isDeleted: {type: Boolean, default: false, required: true},
    isLogin: {type: Boolean, default: false, required: true},

    commission:{type: Number, default:0},
    // currency:{type:String},
});


module.exports = mongoose.model('Agency', Agency);
