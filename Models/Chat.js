var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');



var Chat = new Schema({
    senderType:{type:String,enum:['USER','MAID']},
    senderId: {type: String, required:true},
    receiverId: {type: String, required:true},
    serviceId: {type: String, required:true},
    conversationId : {type: String, required:true},
    messageType:{type:String,default:'MESSAGE',enum:['MESSAGE','LOCATION']},
    message: {type: String, default: ''},
    location: {type: [Number], index: '2dsphere'},                    //long,lat
    locationName: {type: String, default: ''},
    timeStamp: {type: Date, default: null},
    isDeleted:[{type : String}],
    isRead : [{type : String}],
    isExpired : {type : Boolean, default : false},
});

module.exports = mongoose.model('Chat', Chat);
