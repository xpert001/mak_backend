var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');



var ContactUs = new Schema({
    uniquieAppKey:{type:String, required:true},

    name: {type:String, default: '',  trim: true},
    email: {type: String, default: '', trim: true},
    comment:{type:String,default: ''},
    phoneNumber:{type:String,default:""},
    createdOn :{type:Number,default:0},
});


module.exports = mongoose.model('ContactUs', ContactUs);
