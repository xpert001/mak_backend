var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');



var Country = new Schema({

    num_code: {type:String, default: '',  trim: true},
    alpha_2_code: {type: String, default: '', trim: true},
    alpha_3_code: {type: String, default: '', trim: true},
    en_short_name: {type: String, default: '', trim: true},
    nationality:{type:String,default: ''}
});


module.exports = mongoose.model('Country', Country);
