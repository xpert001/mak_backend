var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');




var Issue = new Schema({

    uniquieAppKey:{type:String, required:true},
    ticketNo:{type:String},
    userType:{type:String,enum:['USER','MAID']},
    userId: {type: String, required:true},
    issueDescription:{type: String, trim: true},
    issueDate: {type: Date, default: Date.now, required: true},
    isDeleted:{type: Boolean, default: false, required: true},

});

module.exports = mongoose.model('Issue', Issue);