var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');




var Languages = new Schema({
    uniquieAppKey:{type:String, required:true},

    languageName:{type: String, default:'english', trim: true, required: true},
    languageCode:{type: String, default: 'EN', trim: true, required: true},
    registrationDate: {type: Date, default: Date.now, required: true},
    isDeleted:{type: Boolean, default: false, required: true},
});

module.exports = mongoose.model('Languages', Languages);