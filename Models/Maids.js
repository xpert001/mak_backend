var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');




var Maids = new Schema({
    uniquieAppKey:{type:String, required:true},
    appLanguage:{type:String,enum:[
        Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN,
        Config.APP_CONSTANTS.DATABASE.LANGUAGE.AR,
    ],default:Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN},

    step:{type: Number,default:0,required:true},
    maidAppStep:{type:Number,default:0,required:true},
    makId:{type:String,required:true,index:true,unique:true},
    firstName: {type: String, trim: true, index: true, default: ''},
    lastName: {type: String, trim: true, index: true, default: '', sparse: true},
    fullName: {type: String,sparse: true, default: ''},
    email: {type: String, trim: true,index: true,sparse:true, default: ''},
    password: {type: String, default: ''},
    passwordResetToken: {type: String, default:''},
    agencyId:{type: Schema.ObjectId, ref:'Agency', trim: true},
    countryENCode: {type: String, trim: true},                    //IN
    countryCode: {type: String, trim: true, min:2, max:5},       //+91
    phoneNo: {type: String, trim: true, index: true, min: 5, max: 15, default: ''},
    profilePicURL: {
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''},
    },
    dob: {type:Number,default: 0},
    dateJoinedAgency: {type:Number,default: 0},
    gender:{
        type:String,
        enum:[
            Config.APP_CONSTANTS.DATABASE.GENDER.MALE,
            Config.APP_CONSTANTS.DATABASE.GENDER.FEMALE,
            ''
        ],
        default: ''
    },
    nationality: {type: String, trim: true, default:""},
    religion: {type: String, trim: true, default:""},
    nationalId: {type: String, trim: true, default:""},
    material: {type: Number, trim: true, default:1},
    documentPicURL: {
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''},
    },
    nationalityFlag:{
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''},
    },
    experience:{
        type: Number, enum: [
            Config.APP_CONSTANTS.DATABASE.EXPERIENCE.UPTO_2_YEARS,
            Config.APP_CONSTANTS.DATABASE.EXPERIENCE.UPTO_5_YEARS,
            Config.APP_CONSTANTS.DATABASE.EXPERIENCE.ABOVE_6_YEARS,
        ],
       // default: Config.APP_CONSTANTS.DATABASE.EXPERIENCE.UPTO_2_YEARS,
    },
    languages: [{type: Schema.ObjectId, ref:'Languages', trim: true}],
    description:{type: String, trim: true, default:""},

    commission:{type: Number, default:0},
    price:{type: Number,default:0},                              /////price that agency entered
    actualPrice:{type: Number,default:0},                        ////// price that we calculate to show user
    makPrice:{type: Number,default:0},                           ////// commission price
    currency:{type:String},

    currentLocation: {type: [Number], index: '2dsphere'},                    //long,lat
    locationName:{type: String, default:""},
    radius:{type:Number,default:0},

    countryName: {type: String, trim: true},
    maidAddress:{
        streetName: {type: String, default: ''},
        buildingName: {type: String, default: ''},
        villaName: {type: String, default: ''},
        city: {type: String, default: ''},
        countryName: {type: String, default: ''},
        moreDetailedaddress:{type: String, default:""},                          //according to user input
    },
    timeZone:{type:String, required:true},


    accessToken: {type: String, trim: true, index: true},
    deviceToken: {type: String, trim: true},
    deviceType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
        ]
    },
    lastTimeLogin: {type:Date, default:null},                                // update when logout
    registrationDate: {type: Date, default: Date.now, required: true},
    isBlocked: {type: Boolean, default: false, required: true},              ///enable and disable in admin pannel
    isDeleted: {type: Boolean, default: false, required: true},
    isLogin: {type: Boolean, default: false, required: true},
    isVerified: {type: Boolean, default: false, required: true},
    acceptAgreement: {type: Boolean, default: false, required: true},
    rejectReasonForAggrementByMaid: {type: String, trim: true},
    rejectReason: {type: String, default:''},

    // 0 for sunday
    timeSlot: {
        0: [{
            type: Number,
            enum: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
        }],
        1: [{
            type: Number,
            enum: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
        }],
        2: [{
            type: Number,
            enum: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
        }],
        3: [{
            type: Number,
            enum: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
        }],
        4: [{
            type: Number,
            enum: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
        }],
        5: [{
            type: Number,
            enum: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
        }],
        6: [{
            type: Number,
            enum: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
        }],
    }
});



module.exports = mongoose.model('Maids', Maids);
