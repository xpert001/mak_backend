var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');


var Notification = new Schema({
    senderId: {type:String, default: null},
    receiverId: {type:String, default: null},
    receiverArray:[{type:String}],
    maidId: {type: Schema.ObjectId, ref: "Maids", default: null},
    timeStamp: {type: Date, default: Date.now, required: true},
    type: {
        type: String,
        enum: [
            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_REQUEST,
            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_ACCEPT,
            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_REJECT,
            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_COMPLETE,
            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_EXTEND,
            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_EXTEND_ACCEPT,
            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_EXTEND_CANCEL,
            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_DELETE,

            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.MESSAGE,

            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.ALL_USER_TYPE,
            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.NEW_MAID,

        ],
        required: true
    },
    serviceId: {type: String, trim: true},
    bookingId:{type:Number},

    fullName: {type:String, default: ""},
    reviewSubmitted : {type:Boolean, default: false},
    maidReviewSubmitted : {type:Boolean, default: false},

    userMessage: {type: String, default: '', trim: true},
    maidMessage: {type: String, default: '', trim: true},
    agencyMessage: {type: String, default: '', trim: true},
    adminMessage: {type: String, default: '', trim: true},

    isDeleted:[{type: String}],
    read: [{type: String}],
    actionPerformed: [{type: String}],
});


module.exports = mongoose.model('Notification', Notification);


