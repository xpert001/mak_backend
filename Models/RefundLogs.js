var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');




var RefundLogs = new Schema({

    responseCode:{type:String},
    userId:{type:Schema.ObjectId,ref:'Users'},
    serviceId:{type:Schema.ObjectId,ref:'Service'},
    transactionId:{type:String},
    refundAmount : {type:Number},
    actualPrice : {type:Number},
    result: {type: String},
    createdOn : {type:Date},
});

module.exports = mongoose.model('RefundLogs', RefundLogs);


/*
responseCode of paytabs :

4001 Missing parameters
4002 Invalid Credentials
810 You already requested Refund for this Transaction ID
811 Refund amount you requested is greater than transaction amount
Your balance is insufficient to cover the Refund Amount
812 Refund request is sent to Operation for Approval. You can track the Status
813 You are not authorized to view this transaction

 */