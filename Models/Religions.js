var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');



var Religions = new Schema({
    uniquieAppKey:{type:String, required:true},
    name: {type:String,required:true},
    isBlocked :{type:Boolean,default:false},
    isDeleted :{type:Boolean,default:false}
});


module.exports = mongoose.model('Religions', Religions);
