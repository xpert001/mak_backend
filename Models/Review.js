var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');


/////maidReview////////

var Review = new Schema({
    uniquieAppKey:{type:String, required:true},

    userId: {type: Schema.ObjectId, ref:'Users', trim: true},
    serviceId: {type: Schema.ObjectId, ref:'Service', trim: true},
    maidId : {type: Schema.ObjectId, ref:'Maids', trim: true},
    reviewByType:{type:String,enum:[
        Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER,
        Config.APP_CONSTANTS.DATABASE.USER_ROLES.MAID,
    ]},
    userRating:{type: Number, default: 0},
    maidRating: {
        cleaning: {type: Number, default: 0},
        ironing: {type: Number, default: 0},
        cooking: {type: Number, default: 0},
        childCare: {type: Number, default: 0},
    },
    feedBack: {type: String, trim: true},
    isDeleted: {type: Boolean, default: false, required: true},
    isBlocked: {type: Boolean, default: false, required: true},
    createdOn :{type: Number, default: 0},
    timeStamp: {type: Date, default: Date.now, required: true},
});



module.exports = mongoose.model('Review', Review);
