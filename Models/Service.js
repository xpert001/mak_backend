var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');




var Service = new Schema({
    uniquieAppKey:{type:String, required:true},

    serviceMakId:{type:String,required:true,index:true,unique:true},
    userId: {type: Schema.ObjectId, ref:'Users', trim: true, required:true},
    maidId: {type: Schema.ObjectId, ref:'Maids', trim: true, required:true},
    agencyId: {type: Schema.ObjectId, ref:'Agency', trim: true, required:true},
    workDate:{type: Number, required:true},          //milliseconds
    startTime:{type:Number, required:true},          //milliseconds
    endTime:{type:Number, required:true},            //milliseconds
    bufferEndTime:{type:Number, required:true},      //milliseconds
    serviceStartByMaid:{type:Number},                //milliseconds
    isStart:{type:Boolean,default:false,required:true},
    duration:{type:Number, required:true},
    hour :{type:Number},
    date :{type:Number},
    timeZone:{type:String, required:true},

    commission:{type: Number, default:0},
    price:{type: Number,default:0},                              /////price that agency entered

    actualPrice:{type: Number,default:0},                        ////// price per hour that we calculate to show user
    makPrice:{type: Number,default:0},                           ///////commission price
    amount:{type:Number},                                        ///////total price after multiplying duration
    commissionToBePaid:{type:Number},                            //////total commission on amount to be paid
    currency:{type:String},

    bookingLocation: {type: [Number], index: '2dsphere'},                    //long,lat
    locationName:{type: String, default:""},                                 //according to users lat long where the maid goes for work
    address:{
        streetName: {type: String, default: ''},
        buildingName: {type: String, default: ''},
        villaName: {type: String, default: ''},
        city: {type: String, default: ''},
        country: {type: String, default: ''},
        moreDetailedaddress:{type: String, default:""},                          //according to user input where the maid goes for work
    },
    billingAddress: {
        billingName: {type: String, default: ''},
        email: {type: String, trim: true},
        streetName: {type: String, default: ''},
        buildingName: {type: String, default: ''},
        villaName: {type: String, default: ''},
        city: {type: String, default: ''},
        country: {type: String, default: ''},
        countryENCode: {type: String, trim: true},
        countryCode: {type: String, trim: true, min:2, max:5},
        phoneNo: {type: String, trim: true, index: true, min: 5, max: 15},
        nationalId: {type: String, trim: true},
        moreDetailedaddress:{type: String, default:""},                          //according to user input as billing address
    },

    agencyAction: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.ACTION.PENDING,
            Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
            Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE,
        ],
        default: Config.APP_CONSTANTS.DATABASE.ACTION.PENDING,
    },
    isCompleted:{type:Boolean,default:false,required:true},
    declineReason:{type: String, default:""},

    deleteAction: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.ACTION.PENDING,
            Config.APP_CONSTANTS.DATABASE.ACTION.DELETED,
        ],
        default:Config.APP_CONSTANTS.DATABASE.ACTION.PENDING,
    },
    deleteRequestByUser:{type:Boolean,default:false,required:true},

    deleteTimeStamp:{type:Date},
    deleteReason:{type: String, default:""},
    deleteFromList : {type:Boolean,default:false},
    isExtend:{
        reason:{type: String, default:""},
        extendDuration:{type:Number,default:0,required:true},
        requested:{type:Boolean,default:false,required:true},
        agencyAction: {
            type: String, enum: [
                Config.APP_CONSTANTS.DATABASE.ACTION.PENDING,
                Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                Config.APP_CONSTANTS.DATABASE.ACTION.DECLINE,
                ""
            ],
            default: "",
        },
        extendAmount:{type:Number,default:0,required:true},
        declineReason:{type: String, default:""},
        transactionId: {type: String, default: ""},
        cardToken: {type: String, default: ""},
        bookingId:{type:Number,unique:true,sparse:true},
    },

    transactionId: {type: String, default: ""},
    cardToken: {type: String, default: ""},
    cardType: {type: Number, default: 1},  // 1- debit ,2 - credit
    bookingId:{type:Number,unique:true,sparse:true},
});



module.exports = mongoose.model('Service', Service);
