var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');



var cardDetails = new Schema({
    cardHolderName:{type: String, default: null},
    cardNumber:{type: String, default: null},
    cardToken:{type: String, default: null},
    cardFingerPrint:{type: String, default: null},
    cardType:{type: String, default: ''},
    validTill:{type: String, default: ''},
    customerEmail:{type: String, default: ''},
    customerPassword:{type: String, default: ''},
    Date: {type: Date, default: Date.now, required: true},
    isDeleted:{type: Boolean, default: false, required: true},
});

var multipleAddress = new Schema({
    streetName: {type: String, default: ''},
    buildingName: {type: String, default: ''},
    villaName: {type: String, default: ''},
    city: {type: String, default: ''},
    country: {type: String, default: ''},
    moreDetailedaddress:{type: String, default:""},
    isDeleted: {type: Boolean, default: false, required: true},   //according to user input,
    lat: {type: Number},
    long: {type: Number},
});



var Users = new Schema({
    appLanguage:{type:String,enum:[
        Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN,
        Config.APP_CONSTANTS.DATABASE.LANGUAGE.AR,
    ],default:Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN},

    fullName: {type: String, trim: true, index: true, default: '', sparse: true},
    // fullNameAr: {type: String, trim: true, index: true, default: '', sparse: true},
    email: {type: String, trim: true, index: true,sparse: true},
    password: {type: String,default:''},
    nationalId: {type: String, trim: true},

    currentLocation: {type: [Number], index: '2dsphere'},                    //long,lat
    locationName:{type: String, default:""},
    usersAddress:{
        streetName: {type: String, default: ''},
        buildingName: {type: String, default: ''},
        villaName: {type: String, default: ''},
        city: {type: String, default: ''},
        country: {type: String, default: ''},
        moreDetailedaddress:{type: String, default:""},                          //according to user input
        lat: {type: Number},
        long: {type: Number},
    },
    billingAddress: {
        billingName: {type: String, default: ''},
        email: {type: String, trim: true},
        streetName: {type: String, default: ''},
        buildingName: {type: String, default: ''},
        villaName: {type: String, default: ''},
        city: {type: String, default: ''},
        country: {type: String, default: ''},
        countryENCode: {type: String, trim: true},
        countryCode: {type: String, trim: true, min:2, max:5},
        phoneNo: {type: String, trim: true, min: 5, max: 15},
        nationalId: {type: String, trim: true},
        moreDetailedaddress:{type: String, default:""},                          //according to user input
    },
    multipleAddress:[multipleAddress],

    profilePicURL: {
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''},
    },
    documentPicURL: {
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''},
    },

    countryENCode: {type: String, trim: true},
    countryCode: {type: String, trim: true, min:2, max:5},
    phoneNo: {type: String, trim: true,/* index: true,*/ min: 5, max: 15},

    accessToken: {type: String, trim: true, index: true},
    deviceToken: {type: String, trim: true},
    deviceType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
        ]
    },

    registrationDate: {type: Date, default: Date.now, required: true},
    isVerified: {type: Boolean, default: false},
    isLogin: {type: Boolean, default: false, required: true},
    firstTimeLogin: {type: Boolean, default: false},
    lastTimeLogin: {type:Date, default:null},                                // update when logout
    isBlocked: {type: Boolean, default: false, required: true},
    isDeleted: {type: Boolean, default: false, required: true},
    isDeactivated: {type: Boolean, default: false, required: true},
    passwordResetToken: {type: String, trim: true},
    profileComplete:{type: Boolean, default: false, required: true},

    facebookId: {type: String, trim: true,index: true,sparse: true},
    faceBookLogin:{type: Boolean, default: false},

    isGuestFlag: {type: Boolean, default: false, required: true},
    firstBookingDone: {type: Boolean, default: false, required: true},
    actualSignupDone: {type: Boolean, default: false, required: true},

    timeZone:{type:String},
    customerStripeId:{type:String},
    cardDetails:[cardDetails],
    uniquieAppKey:{type:String, required:true},

    favouriteMaidId: {type: [Schema.ObjectId], ref:'Maids'},
});



module.exports = mongoose.model('Users', Users);
