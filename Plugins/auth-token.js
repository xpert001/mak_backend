'use strict';

var TokenManager = require('../Lib/TokenManager');
var UniversalFunctions = require('../Utils/UniversalFunctions');

exports.register = function(server, options, next){

//Register Authorization Plugin
    server.register(require('hapi-auth-bearer-token'), function (err) {
        server.auth.strategy('AdminAuth', 'bearer-access-token', {

            allowQueryToken: false,
            allowMultipleHeaders: true,
            accessTokenName: 'accessToken',
            validateFunc: function (token, callback) {
                TokenManager.verifyToken(token,'ADMIN', function (err,response) {
                    if (err || !response || !response.userData){
                        callback(null, false, {token: token, userData: null})
                    }else {
                        callback(null, true, {token: token, userData: response.userData})
                    }
                });

            }
        });

        server.auth.strategy('AgencyAuth', 'bearer-access-token', {

            allowQueryToken: false,
            allowMultipleHeaders: true,
            accessTokenName: 'accessToken',
            validateFunc: function (token, callback) {
                TokenManager.verifyToken(token,'AGENCY', function (err,response) {
                    if (err || !response || !response.userData){
                        //callback(null, false, {token: token, userData: null})
                        callback(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_ALREADY_EXPIRED))
                    }else {
                        if(response.userData.isBlocked == true){
                            callback(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.BLOCK_USER));
                        }else if(response.userData.isDeleted == true){
                            callback(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.USER_DELETED));
                        }
                        else{
                            callback(null, true, {token: token, userData: response.userData})
                        }
                    }
                });
            }
        });

        server.auth.strategy('MaidAuth', 'bearer-access-token', {


            allowQueryToken: false,
            allowMultipleHeaders: true,
            accessTokenName: 'accessToken',
            validateFunc: function (token, callback) {
                // console.log("____MAID________in plugins ",token)

                TokenManager.verifyToken(token,'MAID', function (err,response) {
                    if (err || !response || !response.userData){
                        //callback(null, false, {token: token, userData: null})
                        callback(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_ALREADY_EXPIRED))
                    }else {
                        if(response.userData.isBlocked == true){
                            callback(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.BLOCK_USER));
                        }else if(response.userData.isDeleted == true){
                            callback(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.USER_DELETED));
                        }
                        else{
                            callback(null, true, {token: token, userData: response.userData})
                        }
                    }
                });
            }
        });

        server.auth.strategy('UserAuth', 'bearer-access-token', {

            allowQueryToken: false,
            allowMultipleHeaders: true,
            accessTokenName: 'accessToken',
            validateFunc: function (token, callback) {
                if(token == undefined){
                    callback(null, true, {token: "", userData: {}})
                }else{
                    TokenManager.verifyToken(token,'USER', function (err,response) {
                        if (err || !response || !response.userData){
                            //callback(null, false, {token: token, userData: null})
                            callback(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_ALREADY_EXPIRED))
                        }else {
                            if(response.userData.isBlocked == true){
                                callback(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.BLOCK_USER));
                            }else if(response.userData.isDeleted == true){
                                callback(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.USER_DELETED));
                            }
                            else{
                                callback(null, true, {token: token, userData: response.userData})
                            }
                        }
                    });
                }
            }
        });



    });

    next();
};

exports.register.attributes = {
    name: 'auth-token-plugin'
};