'use strict';

const pack = require('../package');
const Inert = require('inert');
const Vision = require('vision');


//Register All Plugins
exports.register = function(server, options, next){
    options={
        info: {
            'title': 'MAK API',
            'version': pack.version
        }
    }
    server.register([
        Inert,
        Vision,
        {
            'register': require('hapi-swagger'),
            'options': options}
    ], function (err) {
        if (err) {
            server.log(['error'], 'hapi-swagger load error: ' + err)
        }else{
            server.log(['start'], 'hapi-swagger interface loaded')
        }
    });
    next()
};



exports.register.attributes = {
    name: 'swagger-plugin'
};