"use strict";

const Controller = require('../Controllers');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const Joi = require('joi');
const Config = require('../Config');

module.exports = [
    {
        method: 'POST',
        path: '/admin/login',
        handler: function (request, reply) {
            let userPayload = {
                email:request.payload.email,
                password:request.payload.password,
                ipAddress: request.info.remoteAddress || null,
                uniquieAppKey:request.payload.uniquieAppKey
            };
            Controller.AdminController.adminLogin(userPayload, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Admin login',
            tags: ['api', 'admin'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    email: Joi.string().trim().required(),
                    password: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    apayloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/admin/agencySignUp',
        handler: function (request, reply) {
            let userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData._id) {
                var payloadData = request.payload;
                Controller.AdminController.agencySignUp(payloadData,userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                console.log(".........Access Token Not verified..............");
            }
        },
        config: {
            description: 'Agency Register',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    agencyName: Joi.string().trim().required(),
                    agencyOwnerName: Joi.string().trim().optional(),
                    adminNote: Joi.string().trim().optional(),
                    agencyWebsite: Joi.string().trim().optional(),
                    email: Joi.string().trim().required(),
                    address:{
                        streetName:Joi.string().optional(),
                        buildingName:Joi.string().optional(),
                        villaName:Joi.string().optional(),
                        city:Joi.string().required(),
                        country:Joi.string().required(),
                    },

                    long: Joi.number().required(),
                    lat: Joi.number().required(),
                    locationName: Joi.string().required(),
                    radius: Joi.number().min(1).optional(),

                    countryENCode:Joi.string().required(),
                    countryCode: Joi.string().max(4).required().trim(),
                    phoneNo: Joi.string().required(),
                    nationalId: Joi.string().required(),

                    commission: Joi.string().optional(),
                    countryName: Joi.string().optional(),
                    agencyType:Joi.string().valid(["MAK_REGISTERED_UAE", "MAK_REGISTERED_BAHRAIN", "NORMAL"]).required(),
                    documentPicURL:{
                        original:Joi.string().optional(),
                        thumbnail:Joi.string().optional(),
                    },
                    profilePicURL:{
                        original:Joi.string().optional(),
                        thumbnail:Joi.string().optional(),
                    },
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/admin/deleteFromList',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AgencyController.deleteFromList(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'deleteFromList',
            auth: 'AdminAuth',
            tags: ['api','agency'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    serviceId:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/admin/addImage',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AgencyController.addObjectsToS3(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'uploads files to aws',
            auth: 'AdminAuth',
            tags: ['api','agency'],
            payload: {
                maxBytes: 100000000,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    image: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file'),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/admin/updateAgencyProfile',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AdminController.updateAgencyProfile(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Update agency profile',
            auth: 'AdminAuth',
            tags: ['api','admin'],
            validate: {
                payload: {

                    uniquieAppKey:Joi.string().trim().required(),
                    agencyId: Joi.string().trim().required(),
                    agencyName: Joi.string().trim().optional(),
                    agencyOwnerName: Joi.string().trim().optional(),
                    adminNote: Joi.string().trim().optional(),
                    agencyWebsite: Joi.string().trim().optional(),
                    nationalId: Joi.string().trim().optional(),
                    address:{
                        streetName:Joi.string().required(),
                        buildingName:Joi.string().required(),
                        villaName:Joi.string().required(),
                        city:Joi.string().required(),
                        country:Joi.string().required(),
                    },
                    commission:Joi.string().optional(),
                    radius:Joi.number().optional(),
                    countryENCode:Joi.string().optional(),
                    countryCode: Joi.string().max(4).optional().trim(),
                    phoneNo: Joi.string().optional(),
                    profilePicURL:{
                        original:Joi.string().optional(),
                        thumbnail:Joi.string().optional(),
                    },
                    documentPicURL:{
                        original:Joi.string().optional(),
                        thumbnail:Joi.string().optional(),
                    },
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/admin/addAndUpdateCommissionFeeForTheAgency',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.AdminController.addAndUpdateCommissionFeeForTheAgency(userData, userPayload, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(data)).code(201)
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Block Users',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    agencyId:Joi.string().required(),
                    commission:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/getAllLanguages',
        handler: function (request, reply) {
            var data = request.query;
            Controller.AdminController.getAllLanguages(data, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Language Listing',
            tags: ['api', 'admin'],

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/getAllAgency',
        handler: function (request, reply) {
            var data = request.query;
            Controller.AdminController.getAllAgency(data, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Agency Listing',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    agencyId:Joi.string().trim().optional(),
                    searchAgencyName:Joi.string().optional(),

                    searchByCountryName:Joi.string().optional(),
                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/admin/blockAgencyByAdmin',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.AdminController.blockAgencyByAdmin(userData, userPayload, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(data)).code(201)
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Block Agency',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    agencyId:Joi.string().required(),
                    action:Joi.valid(['BLOCK','UNBLOCK']).required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/getAllCustomerDetails',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.AdminController.getAllCustomerDetails(userData, data, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }

        },
        config: {
            description: 'Customer Listing',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    userType:Joi.number().valid([1,2]).description('1 - normal,2 - guest'),
                    searchByCountryName:Joi.string().optional(),
                    searchCustomerName:Joi.string().optional(),
                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/admin/blockUserByAdmin',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.AdminController.blockUserByAdmin(userData, userPayload, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(data)).code(201)
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Block Users',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    userId:Joi.string().required(),
                    action:Joi.valid(['BLOCK','UNBLOCK']).required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/admin/addEditReligion',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.AdminController.addEditReligion(userPayload,userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(data)).code(201)
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'add EditReligion',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    religionId:Joi.string(),
                    name:Joi.string().required(),

                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/admin/changePassword',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData._id){
                Controller.AdminController.changePass(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(null, data)
                    }
                });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED));
            }
        },
        config: {
            description: "Change password",
            auth: 'AdminAuth',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    oldPassword:Joi.string().trim().required(),
                    newPassword:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/admin/forgetPassword',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.forgetPassword(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(null)
                }
            });
        },
        config: {
            description: "Forgot password",
            tags: ['api', 'user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    email:Joi.string().trim().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/getAllMaidDetails',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.AdminController.getAllMaidDetails(userData, data, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }

        },
        config: {
            description: 'Maid Listing',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    searchByCountryName:Joi.string().optional(),
                    searchNationality:Joi.string().optional(),
                    experience:Joi.number().optional(),
                    agencyId:Joi.string().optional(),
                    search:Joi.string().optional(),
                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional(),
                    isRejected : Joi.boolean().optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getAllReligion',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.AdminController.getAllReligion(userData, data, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }

        },
        config: {
            description: 'Maid Listing',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    search:Joi.string().optional(),
                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/listAllServiceByAdmin',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.listAllServiceByAdmin(userData, data, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Service Listing By Admin',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    onBasisOfDate:Joi.string().required().valid([
                        "ON_GOING", "COMPLETED", "UPCOMING"
                    ]),

                    searchByCountryName:Joi.string().optional(),
                    search: Joi.string().optional(),
                    userId: Joi.string().optional(),
                    timeZone:Joi.string().optional(),

                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/listAllCanceledServiceAdmin',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.listAllCanceledServiceAdmin(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Cancelled Service Listing for admin',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    search: Joi.string().optional(),
                    countryName:Joi.string().optional(),

                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/revenueReport',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.revenueReportAdmin(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Revenue report listing admin',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    year : Joi.number().optional(),
                    month : Joi.number().optional(),
                    startDate: Joi.number().optional(),
                    endDate: Joi.number().optional(),

                    long: Joi.number().optional(),
                    lat: Joi.number().optional(),

                    search: Joi.string().optional(),
                    maidId: Joi.string().optional(),
                    userId: Joi.string().optional(),
                    rating:Joi.number().optional(),
                    timeZone:Joi.string().optional(),
                    countryName:Joi.string().optional(),

                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/lifeTimeSummaryAdmin',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.lifeTimeSummaryAdmin(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'LifeTime Summary of revenue report for admin',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    timeZone:Joi.string().required(),
                    search: Joi.string().optional(),
                    countryName:Joi.string().optional(),


                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/currentMonthSummaryAdmin',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.currentMonthSummaryAdmin(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Current Month Summary for admin',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    timeZone:Joi.string().required(),
                    search: Joi.string().optional(),
                    countryName:Joi.string().optional(),

                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/detailedMonthlyInvoiceAdmin',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.detailedMonthlyInvoiceAdmin(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Detailed Monthly Invoice for admin',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    timeZone:Joi.number().required(),
                    search: Joi.string().optional(),
                    countryName:Joi.string().optional(),
                    agencyId:Joi.string().optional(),
                    monthFilter:Joi.number().optional(),
                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/dashBoardData',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.AdminController.dashboardAdmin(payloadData, userData,function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Dashboard details',
            auth: 'AdminAuth',
            tags: ['api', 'admin'],
            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    timeZone:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/listAllReviewsAdmin',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.listAllReviewsAdmin(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Review and rating Listing By Admin',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    search: Joi.string().optional(),
                    agencyId: Joi.string().optional(),
                    reviewFilterType:Joi.string().valid(['cleaning','ironing','cooking','childCare']),
                    starFilter:Joi.number().optional(),
                    countryName:Joi.string().optional(),

                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/listAllNotificationAdmin',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.listAllNotificationAdmin(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List all notification Admin',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/getNotificationUnredCountAdmin',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.getNotificationUnredCountAdmin(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get unread notification Count admin',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/admin/sendNotificationToAll',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.sendNotificationToAll(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Send notification to multiple users',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),

                    userArray:Joi.array(),
                    maidArray:Joi.array(),
                    agencyArray:Joi.array(),
                    message:Joi.string(),
                    subject:Joi.string(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/createXlsxAdmin',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.createXlsxAdmin(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'create Xlsx sheet in admin',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    jsonData:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/issueList',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.issueListAdmin(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'get Issue List ',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    type:Joi.string().valid(["USER","MAID"]).optional(),
                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/contactList',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AdminController.contactList(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'get Contact List ',
            tags: ['api', 'admin'],
            auth: 'AdminAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


];