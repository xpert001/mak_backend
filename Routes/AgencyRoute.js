"use strict";

const Controller = require('../Controllers');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const Joi = require('joi');
const Config = require('../Config');

module.exports = [
    ////////////////////////////////////PUT APIS///////////////////////////////////////////////
    {
        method: 'PUT',
        path: '/agency/deleteMaid',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.AgencyController.deleteMaidByAgency(userPayload, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(data)).code(201)
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Delete maid',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    maidId:Joi.string().required(),
                    action:Joi.valid(['DELETE','UNDELETE']).required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/agency/blockMaid',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.AgencyController.blockMaidByAgency(userPayload, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(data)).code(201)
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Block maid',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    maidId:Joi.string().required(),
                    action:Joi.valid(['BLOCK','UNBLOCK']).required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/listAllReligion',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.listAllReligion(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Religion Listing for User App',
            tags: ['api', 'user'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    // pageNo: Joi.number().required().min(1),
                    // limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },



    ////////////////////////////////////POST APIS///////////////////////////////////////////////
    {
        method: 'POST',
        path: '/agency/addImage',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AgencyController.addObjectsToS3(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'uploads files to aws',
            auth: 'AgencyAuth',
            tags: ['api','agency'],
            payload: {
                maxBytes: 100000000,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    image: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file'),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/agency/agencyLogin',
        handler: function (request, reply) {
            let userPayload = request.payload;
            Controller.AgencyController.agencyLogin(userPayload, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Agency login',
            tags: ['api', 'agency'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    email: Joi.string().trim().required(),
                    password: Joi.string().trim().required(),
                    // countryName: Joi.string().trim().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    apayloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/agency/addMaid',
        handler: function (request, reply) {
            var payloadData = request.payload;
            console.log("payloadData*********in addMaid ********agency",payloadData);
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AgencyController.addMaid(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Add Maid (dob in milliseconds)'+'required Fields: firstName, lastName, description, gender,'+
            ' email, password, country, countryCode, phoneNo',
            auth: 'AgencyAuth',
            tags: ['api','agency'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().required(),

                    firstName: Joi.string().required(),
                    lastName: Joi.string().required(),
                    religion:Joi.string(),
                    gender: Joi.string().valid(['MALE','FEMALE']),
                    email: Joi.string().allow('').optional(),
                    nationality: Joi.string(),
                    nationalId: Joi.string().allow('').optional(),
                    documentPicURL:{
                        original:Joi.string().allow('').optional(),
                        thumbnail:Joi.string().allow('').optional(),
                    },
                    password:Joi.string().allow('').optional(),
                    countryENCode: Joi.string().allow('').optional().trim(),
                    countryName: Joi.string().trim(),
                    countryCode: Joi.string().max(4).optional().allow('').trim(),
                    dob: Joi.number(),
                    phoneNo: Joi.string().regex(/^[0-9]+$/).min(5).allow('').optional(),
                    timeZone:Joi.string().optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/agency/updateMaidProfile',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AgencyController.updateMaidProfile(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Update Maid profile',
            auth: 'AgencyAuth',
            tags: ['api','agency'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    step:Joi.number().valid([0,1,2,3]).optional(),
                    maidId:Joi.string().required(),
                    nationalId: Joi.string().optional(),
                    religion:Joi.string().optional(),
                    firstName: Joi.string().trim().optional(),
                    lastName: Joi.string().trim().optional(),
                    description: Joi.string().trim().optional(),
                    gender: Joi.string().valid([Config.APP_CONSTANTS.DATABASE.GENDER.MALE,Config.APP_CONSTANTS.DATABASE.GENDER.FEMALE]).optional(),
                    email: Joi.string().trim().optional(),
                    // password: Joi.string().trim().optional(),
                    countryENCode: Joi.string().trim().optional(),
                    countryCode: Joi.string().trim().optional(),
                    phoneNo: Joi.string().trim().optional(),
                    nationality: Joi.string().optional(),
                    dob: Joi.number().optional(),
                    material: Joi.number().optional(),
                    dateJoinedAgency: Joi.number().optional(),
                    experience: Joi.string().optional().valid([
                        Config.APP_CONSTANTS.DATABASE.EXPERIENCE.UPTO_2_YEARS,
                        Config.APP_CONSTANTS.DATABASE.EXPERIENCE.UPTO_5_YEARS,
                        Config.APP_CONSTANTS.DATABASE.EXPERIENCE.ABOVE_6_YEARS,
                    ]),
                    languages:Joi.array().optional(),
                    price:Joi.number().optional(),
                    // currency:Joi.string().optional().allow(""),
                    profilePicURL:{
                        original:Joi.string().optional(),
                        thumbnail:Joi.string().optional(),
                    },
                    documentPicURL:{
                        original:Joi.string().optional(),
                        thumbnail:Joi.string().optional(),
                    },


                    long: Joi.number().optional(),
                    lat: Joi.number().optional(),
                    locationName: Joi.string().optional(),
                    radius:Joi.number().min(1).optional(),

                    maidAddress:{
                        streetName:Joi.string().optional(),
                        buildingName:Joi.string().optional(),
                        villaName:Joi.string().optional(),
                        city:Joi.string().optional(),
                        countryName: Joi.string().trim().optional(),
                        moreDetailedaddress:Joi.string().optional(),
                    },
                    timeZone:Joi.string().optional(),

                    timeSlot:{
                        0:Joi.array(),
                        1:Joi.array(),
                        2:Joi.array(),
                        3:Joi.array(),
                        4:Joi.array(),
                        5:Joi.array(),
                        6:Joi.array(),
                    },
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/agency/approveService',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AgencyController.approveService(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Approve upcoming services',
            auth: 'AgencyAuth',
            tags: ['api','agency'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    servciceId:Joi.string().required(),
                    agencyAction: Joi.string().required().valid(['ACCEPT', 'DECLINE']),
                    declineReason:Joi.string().optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/agency/deleteService',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AgencyController.deleteService(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Delete on-going and upcoming services',
            auth: 'AgencyAuth',
            tags: ['api','agency'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    servciceId:Joi.string().required(),
                    deleteReason:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/agency/deleteFromList',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AgencyController.deleteFromList(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'deleteFromList',
            auth: 'AgencyAuth',
            tags: ['api','agency'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    serviceId:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/agency/approveExtendedService',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AgencyController.approveExtendedService(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Approve extended services',
            auth: 'AgencyAuth',
            tags: ['api','agency'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    serviceId:Joi.string().required(),
                    agencyAction: Joi.string().required().valid(['ACCEPT','DECLINE']),
                    declineReason:Joi.string(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/agency/changePassword',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AgencyController.changePasswordByAgency(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(null, data)
                }
            });
        },
        config: {
            description: "Change password by agency",
            auth: 'AgencyAuth',
            tags: ['api', 'agency'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    oldPassword:Joi.string().trim().required(),
                    newPassword:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/agency/forgetPassword',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AgencyController.forgetPasswordByAgency(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(null, data)
                }
            });
        },
        config: {
            description: "Forgot password by Agency",
            tags: ['api', 'agency'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    email:Joi.string().trim().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/agency/contactUs',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AgencyController.contactUs(payloadData,{}, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'contact',
            tags: ['api','agency'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    name:Joi.string().required(),
                    email:Joi.string().required(),
                    comment:Joi.string().required(),
                    phoneNumber:Joi.string().optional(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    ////////////////////////////////////GET APIS///////////////////////////////////////////////
    {
        method: 'GET',
        path: '/agency/dashBoardData',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.AgencyController.dashBoardData(payloadData, userData,function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Dashboard details',
            auth: 'AgencyAuth',
            tags: ['api', 'agency'],
            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    timeZone:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/getAgencyProfile',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.getAgencyProfile(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List service scheduling as per maids',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/listAllUnVerifiedMaids',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.listAllUnVerifiedMaids(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'UnVerified Maids Listing',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    maidId:Joi.string().optional(),
                    searchMaidName: Joi.string().optional(),
                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/agency/verifyMaid',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.verifyMaid(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'verify maid',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    maidId:Joi.string().trim().required(),
                    reject:Joi.boolean().valid([true,false]).optional(),
                    rejectReason:Joi.string().optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/listAllMaids',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.listAllMaids(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Maids Listing',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    rejectMaids:Joi.boolean(),
                    maidId:Joi.string().optional(),
                    nationality:Joi.array().optional(),
                    languages:Joi.array().optional(),
                    gender:Joi.array().optional(),
                    sort:Joi.string().optional().valid([
                        "experience", "rating", "distance"
                    ]),
                    searchMaidName: Joi.string().optional(),
                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/listAllUsers',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.listAllUsers(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Users Listing',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/listAllService',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.listAllService(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Service Listing',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    onBasisOfDate:Joi.string().required().valid([
                        "ON_GOING", "COMPLETED", "UPCOMING"
                    ]),

                    search: Joi.string().optional(),
                    timeZone:Joi.string().optional(),

                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/listAllCanceledService',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.listAllCanceledService(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Service Listing',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    search: Joi.string().optional(),

                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/getCalender',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.getCalender(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List service scheduling as per maids',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    maidId:Joi.string().optional(),
                    timeZone:Joi.string().optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/listAllExtendedService',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.listAllExtendedService(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List extended service for approval by agency',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    search: Joi.string().optional(),

                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
         method: 'GET',
         path: '/agency/revenueReport',
         handler: function (request, reply) {
             var data = request.query;
             var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

             Controller.AgencyController.revenueReport(data, userData, function (err, data) {
                 if (err) {
                     reply(UniversalFunctions.sendError(err));
                 } else {
                     reply(UniversalFunctions.sendSuccess(null, data))
                 }
             });
         },
         config: {
             description: 'Revenue report listing',
             tags: ['api', 'agency'],
             auth: 'AgencyAuth',

             validate: {
                 query: {
                     uniquieAppKey:Joi.string().trim().required(),

                     year : Joi.number().optional(),
                     month : Joi.number().optional(),
                     startDate: Joi.number().optional(),
                     endDate: Joi.number().optional(),

                     long: Joi.number().optional(),
                     lat: Joi.number().optional(),

                     search: Joi.string().optional(),
                     maidId: Joi.string().optional(),
                     userId: Joi.string().optional(),
                     /*reviewFilterType:Joi.string().valid(['cleaning','ironing','cooking','childCare']),
                     starFilter:Joi.number().optional(),  */
                     timeZone:Joi.string().optional(),

                     pageNo: Joi.number().optional().min(1),
                     limit: Joi.number().optional()
                 },
                 headers: UniversalFunctions.authorizationHeaderObj,
                 failAction: UniversalFunctions.failActionFunction
             },
             plugins: {
                 'hapi-swagger': {
                     responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                 }
             }
         }
     },

    {
        method: 'GET',
        path: '/agency/lifeTimeSummary',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.lifeTimeSummary(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'LifeTime Summary of revenue report',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    timeZone:Joi.string().required(),
                    search: Joi.string().optional(),

                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/currentMonthSummary',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.currentMonthSummary(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Current Month Summary',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    timeZone:Joi.string().required(),
                    search: Joi.string().optional(),

                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/detailedMonthlyInvoice',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.detailedMonthlyInvoice(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Detailed Monthly Invoice',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    timeZone:Joi.number().required(),
                    search: Joi.string().optional(),
                    monthFilter:Joi.number().optional(),
                    pageNo: Joi.number().optional().min(1),
                    limit: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/listAllReviewsAgency',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.listAllReviewsAgency(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List All Revenue of agency and maids',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    search: Joi.string().optional(),
                    reviewFilterType:Joi.string().valid(['cleaning','ironing','cooking','childCare']),
                    starFilter:Joi.number().optional(),
                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/listAllNotificationAgency',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.listAllNotificationAgency(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List all notification Agency',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    type:Joi.string().valid(["NEW_REGISTRATION","JOB"])
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/agency/clearNotificationAgency',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.clearNotificationAgency(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'clear all notification Agency',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    type:Joi.string().valid(["NEW_REGISTRATION","JOB"])
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/getNotificationUnredCount',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.getNotificationUnredCount(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get unread notification Count',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/agency/actionPerformOnNotification',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.actionPerformOnNotification(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Perform Action on notification',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    notificationId:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/issueList',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.issueList(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'get Issue List ',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/getAllCountry',
        handler: function (request, reply) {
            var payloadData = request.query;

            Controller.AgencyController.countryList(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List all countries',
            tags: ['api', 'agency'],

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/createXlsx',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.createXlsx(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'create Xlsx sheet',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    jsonData:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/agency/listAllAgency',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.listAllAgency(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Maids Listing for User App',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/agency/switchMAKAgency',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.switchMAKAgency(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Change Mak agency in maids',
            tags: ['api', 'agency'],
            auth: 'AgencyAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    maidId:Joi.string().trim().required(),
                    agencyId:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


];