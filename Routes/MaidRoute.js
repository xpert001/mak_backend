"use strict";

const Controller = require('../Controllers');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const Joi = require('joi');
const Config = require('../Config');

module.exports = [
    {
        method: 'POST',
        path: '/maid/addImage',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AgencyController.addObjectsToS3(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'uploads files to aws',
            auth: 'MaidAuth',
            tags: ['api','maid'],
            payload: {
                maxBytes: 100000000,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    image: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file'),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/maid/maidSignup1',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.MaidController.maidSignup1(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Add Maid country(EN code)',
            tags: ['api','agency'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().required(),

                    email: Joi.string().required(),
                    password:Joi.string(),
                    firstName: Joi.string().trim().optional(),
                    lastName: Joi.string().trim().optional(),
                    agencyId:Joi.string().required(),
                    countryENCode: Joi.string().required().trim(),
                    countryCode: Joi.string().max(4).optional().trim(),
                    phoneNo: Joi.string().regex(/^[0-9]+$/).min(5).optional(),
                    timeZone:Joi.string().optional(),
                    deviceToken: Joi.string().optional().allow(""),
                    deviceType: Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/maid/updateMaidProfileInApp',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.MaidController.updateMaidProfileInApp(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Update Maid profile',
            auth: 'MaidAuth',
            tags: ['api','maid'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    step:Joi.number().valid([0,1,2,3]).optional(),
                    maidId:Joi.string().required(),


                    firstName: Joi.string().trim().optional().allow(null,""),
                    lastName: Joi.string().trim().optional().allow(null,""),
                    email: Joi.string().trim().optional().allow(null,""),
                    password: Joi.string().trim().optional().allow(null,""),
                    phoneNo: Joi.string().trim().optional().allow(null,""),
                    countryENCode: Joi.string().trim().optional().allow(null,""),
                    countryCode: Joi.string().trim().optional().allow(null,""),


                    profilePicURL:{
                        original:Joi.string().optional(),
                        thumbnail:Joi.string().optional(),
                    },
                    dob: Joi.number().optional().allow(null,""),
                    gender: Joi.string().valid([Config.APP_CONSTANTS.DATABASE.GENDER.MALE,Config.APP_CONSTANTS.DATABASE.GENDER.FEMALE]).optional().allow(null,""),
                    nationality: Joi.string().optional().allow(null,""),
                    religion:Joi.string().optional().allow(null,""),
                    nationalId: Joi.string().optional().allow(null,""),
                    documentPicURL:{
                        original:Joi.string().optional(),
                        thumbnail:Joi.string().optional(),
                    },
                    experience: Joi.string().optional().valid(["2","5","6"]).allow(null,""),
                    languages:Joi.array().optional().allow(null,""),
                    description: Joi.string().trim().optional().allow(null,""),
                    price:Joi.number().optional().allow(null,""),
                    currency:Joi.string().optional().allow(""),

                    long: Joi.number().optional().allow(null,""),
                    lat: Joi.number().optional().allow(null,""),
                    locationName: Joi.string().optional().allow(null,""),
                    radius:Joi.number().min(1).optional(),

                    maidAddress:{
                        streetName:Joi.string().required(),
                        buildingName:Joi.string().required(),
                        villaName:Joi.string().required(),
                        city:Joi.string().required(),
                        countryName: Joi.string().trim().optional().allow(null,""),
                        moreDetailedaddress:Joi.string().optional().allow(null,""),
                    },
                    timeZone:Joi.string().optional().allow(null,""),


                    timeSlot:{
                        0:Joi.array(),
                        1:Joi.array(),
                        2:Joi.array(),
                        3:Joi.array(),
                        4:Joi.array(),
                        5:Joi.array(),
                        6:Joi.array(),
                    },

                    deviceToken: Joi.string().optional(),
                    deviceType: Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/maid/listAllReligion',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.listAllReligion(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Religion Listing for User App',
            tags: ['api', 'user'],
            auth: 'MaidAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    // pageNo: Joi.number().required().min(1),
                    // limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/maid/acceptAgreement',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.MaidController.acceptAgreements(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Accept Agreement by maid',
            tags: ['api', 'maid'],

            validate: {
                payload: {
                    email:Joi.string().trim().required(),
                    rejectReasonForAggrementByMaid:Joi.string().optional(),
                    action:Joi.string().valid(["ACCEPT","REJECT"]),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/maid/updateDeviceTokenMaid',
        handler: function (request, reply) {

            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData._id) {
                var payloadData = request.payload;
                Controller.MaidController.updateDeviceTokenMaid(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            }else{
                console.log(".........Access Token Not verified..............");
            }
        },
        config: {
            description: 'Update Device Token for maid',
            tags: ['api', 'maid'],
            auth: 'MaidAuth',
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    deviceToken:Joi.string().optional(),
                    deviceType:Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/maid/changeLanguageMaid',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData._id) {
                Controller.MaidController.changeLanguageMaid(request.payload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(data)
                    }
                });
            }else{
                console.log(".........Access Token Not verified..............");
            }
        },
        config: {
            description: 'Change application language by maid',
            tags: ['api', 'maid'],
            auth: 'MaidAuth',
            validate: {
                payload:{
                    uniquieAppKey:Joi.string().trim().required(),
                    language:Joi.string().valid(['EN','AR']).required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/maid/login',
        handler: function (request, reply) {
            let userPayload = request.payload;
            Controller.MaidController.maidLogin(userPayload, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Maid login',
            tags: ['api', 'maid'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),

                    email: Joi.string().trim().required(),
                    password: Joi.string().trim().required(),
                    timeZone:Joi.string().required(),
                    deviceToken: Joi.string().optional().allow(""),
                    deviceType: Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    apayloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/maid/listMakAgencyForMaid',
        handler: function (request, reply) {
            var data = request.query;

            Controller.MaidController.listMakAgencyForMaid(data, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List MAK registered agency for maid',
            tags: ['api', 'user'],

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/maid/listAllService',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.MaidController.listAllServiceMaid(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Service Listing for maids',
            tags: ['api', 'maid'],
            auth: 'MaidAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    onBasisOfDate:Joi.string().required().valid([
                        "ON_GOING", "COMPLETED", "UPCOMING"
                    ]),
                    timeZone:Joi.string().optional(),
                    workDate:Joi.number().optional(),

                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/maid/getAllCountryMaid',
        handler: function (request, reply) {
            var payloadData = request.query;
            //var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.countryList(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List all countries for maid',
            tags: ['api', 'agency'],
            //auth: 'AgencyAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required()
                },
                // headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/maid/listAllReviewsMaid',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.MaidController.listAllReviewsMaid(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Review Listing for maids',
            tags: ['api', 'maid'],
            auth: 'MaidAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/maid/wallet',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.MaidController.wallet(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'wallet',
            tags: ['api', 'maid'],
            auth: 'MaidAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    timeZone:Joi.string().trim().required(),
                    dayFilter:Joi.string().valid(["today", "weekly", "monthly"]).required().lowercase(),
                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/maid/addReviewForUser',
        handler: function (request, reply) {
            var data = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.MaidController.addReviewForUser(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'wallet',
            tags: ['api', 'maid'],
            auth: 'MaidAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    serviceId:Joi.string().required(),
                    // reviewByType:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER, Config.APP_CONSTANTS.DATABASE.USER_ROLES.MAID,]).required(),
                    userRating: Joi.number().optional(),
                    feedBack: Joi.string().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/maid/startService',
        handler: function (request, reply) {
            var data = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.MaidController.startService(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Start Service',
            tags: ['api', 'maid'],
            auth: 'MaidAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    serviceId:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/maid/createChat',
        handler: function (request, reply) {
            var data = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.MaidController.createChat(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Create Chat',
            tags: ['api', 'maid'],
            auth: 'MaidAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    serviceId:Joi.string().trim().required(),
                    receiverId:Joi.string().trim().required(),
                    message:Joi.string().trim().optional(),
                    locationName:Joi.string().trim().optional(),
                    lat:Joi.number().optional(),
                    lng:Joi.number().optional(),
                    messageType:Joi.string().valid(['MESSAGE','LOCATION']).required(),
                    senderType:Joi.string().valid(['MAID','USER']).required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/maid/getAllChat',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.MaidController.getAllChat(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get main chat screen',
            tags: ['api', 'maid'],
            auth: 'MaidAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    userType:Joi.string().valid(["MAID", "USER"]).required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/maid/getChatHistory',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.MaidController.getChatHistory(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get chat history',
            tags: ['api', 'maid'],
            auth: 'MaidAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    userId:Joi.string().trim().required(),
                    lastMessageCount:Joi.number().required(),
                    userType:Joi.string().valid(["MAID", "USER"]).required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/maid/listAllNotificationsMaids',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.MaidController.listAllNotificationsMaids(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List All Notifications for Maids',
            tags: ['api', 'maid'],
            auth: 'MaidAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/maid/changePasswordMaid',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData._id){
                Controller.MaidController.changePasswordMaid(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(null, data)
                    }
                });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED));
            }
        },
        config: {
            description: "Change password by maid",
            auth: 'MaidAuth',
            tags: ['api', 'maid'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    oldPassword:Joi.string().trim().required(),
                    newPassword:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/maid/forgetPasswordMaid',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.MaidController.forgetPasswordMaid(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(null, data)
                }
            });
        },
        config: {
            description: "Forgot password by maid",
            tags: ['api', 'maid'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    email:Joi.string().trim().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/maid/maidLogout',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData._id) {
                Controller.MaidController.maidLogout(request.payload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(data)
                    }
                });
            }else{
                console.log(".........Access Token Not verified..............");
            }
        },
        config: {
            description: 'Logout by maid',
            tags: ['api', 'maid'],
            auth: 'MaidAuth',
            validate: {
                payload:{
                    uniquieAppKey:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/maid/raiseAnIssue',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData._id && userData){
                Controller.MaidController.raiseAnIssueMaid(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED));
            }

        },
        config: {
            description: "Raise an issue by maid",
            tags: ['api', 'maid'],
            auth: 'MaidAuth',
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    issue:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

];