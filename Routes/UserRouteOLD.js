"use strict";

const Controller = require('../Controllers');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const Joi = require('joi');
const Config = require('../Config');

module.exports = [
    {
        method: 'POST',
        path: '/user/addImage',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.AgencyController.addObjectsToS3(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'uploads files to aws',
            auth: 'UserAuth',
            tags: ['api','user'],
            payload: {
                maxBytes: 100000000,
                parse: true,
                timeout:false,
                output: 'file'
            },
            validate: {
                payload: {
                    image: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file'),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/signUp1',
        handler: function (request, reply) {
            let userPayload = request.payload;
            Controller.UserController.signUp1(userPayload, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'User Registration part 1',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),

                    fullName: Joi.string().trim().required(),
                    email: Joi.string().trim().required(),
                    password: Joi.string().trim().required(),
                    deviceToken: Joi.string().optional(),
                    deviceType: Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/checkVersionT',
        handler: function (request, reply) {
            let userPayload = request.payload;
            Controller.UserController.checkVersion(userPayload, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            notes:'status : 1 - any update, 2- force update, 3- no update',
            description: 'checkVersion',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    appType : Joi.string().valid(['USER','MAID']).required(),
                    deviceType : Joi.string().valid(['IOS','ANDROID']).required(),
                    appVersion : Joi.number().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/signUp2',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.UserController.signUp2(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
           //description: 'User Registration part 1',
            description: 'User registration part 2 (update Profile)'+'required Fields: nationalId,streetName,buildingName,'+
            'villaName,city,country,countryENCode,countryCode,phoneNo',
            auth: 'UserAuth',
            tags: ['api','user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),

                    timeZone:Joi.string().optional(),
                    nationalId:Joi.string().required(),
                    email: Joi.string().email().optional().allow(''),

                    streetName:Joi.string().required(),
                    buildingName:Joi.string().required(),
                    villaName:Joi.string().required(),
                    city:Joi.string().required(),
                    country:Joi.string().required(),
                    moreDetailedaddress:Joi.string().optional(),

                    countryENCode:Joi.string().required(),
                    countryCode:Joi.string().required(),
                    phoneNo:Joi.string().required(),
                    profilePicURL:{
                        original:Joi.string().optional(),
                        thumbnail:Joi.string().optional(),
                    },
                    documentPicURL:{
                        original:Joi.string().optional(),
                        thumbnail:Joi.string().optional(),
                    },
                    deviceToken: Joi.string().optional(),
                    deviceType: Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/updateDeviceToken',
        handler: function (request, reply) {

            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData._id) {
                var payloadData = request.payload;
                Controller.UserController.updateDeviceToken(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            }else{
                console.log(".........Access Token Not verified..............");
            }
        },
        config: {
            description: 'Update Device Token',
            tags: ['api', 'user'],
            auth: 'UserAuth',
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    deviceToken:Joi.string().optional(),
                    deviceType:Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/verifyUser',
        handler: function (request, reply) {
                var payloadData = request.payload;
                Controller.UserController.verifyUser(payloadData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
        },
        config: {
            description: 'verifyUser',
            tags: ['api', 'user'],
            validate: {
                payload: {
                   // uniquieAppKey:Joi.string().trim().required(),
                    userId:Joi.string().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/addBillingInfo',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.UserController.addBillingInfo(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Add Billing Information'+'required Fields: timeZone,isGuestFlag, fullName, nationalId,streetName,buildingName,'+
            'villaName,city,country,countryENCode,countryCode,phoneNo',
            auth: 'UserAuth',
            tags: ['api','user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),

                    timeZone:Joi.string().required(),
                    isGuestFlag: Joi.boolean().required().valid([true,false]),
                    email:Joi.string().trim().optional(),
                    fullName:Joi.string().required(),
                    nationalId:Joi.string().required(),
                    streetName:Joi.string().required(),
                    buildingName:Joi.string().required(),
                    villaName:Joi.string().required(),
                    city:Joi.string().required(),
                    country:Joi.string().required(),
                    moreDetailedaddress:Joi.string().optional(),

                    countryENCode:Joi.string().required(),
                    countryCode:Joi.string().required(),
                    phoneNo:Joi.string().required(),
                    deviceToken: Joi.string().optional().allow(""),
                    deviceType: Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/user/emailLogin',
        handler: function (request, reply) {
            var userPayload = request.payload;
            Controller.UserController.emailLogin(userPayload, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'User Login via email and password',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),

                    email: Joi.string().trim().required(),
                    password: Joi.string().trim().required(),
                    timeZone:Joi.string().required(),
                    deviceToken: Joi.string().optional(),
                    deviceType: Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/user/facebookLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.facebookLogin(payloadData, function (err, data){
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                }else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Login Via Facebook',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),

                    facebookId:Joi.string().required(),
                    fullName: Joi.string().required(),
                    email: Joi.string().email().optional().allow(''),
                    profilePicURL:{
                        original:Joi.string().optional(),
                        thumbnail:Joi.string().optional(),
                    },
                    timeZone:Joi.string().optional(),
                    deviceToken:Joi.string().optional(),
                    deviceType:Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/user/updateUserProfile',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.UserController.updateUserProfile(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            //description: 'User Registration part 1',
            description: 'User update Profile',
            auth: 'UserAuth',
            tags: ['api','user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),

                    city:Joi.string().optional(),
                    country:Joi.string().optional(),
                    streetName:Joi.string().optional(),
                    buildingName:Joi.string().optional(),
                    villaName:Joi.string().optional(),

                    timeZone:Joi.string().optional(),
                    fullName: Joi.string().trim().optional(),
                    moreDetailedaddress:Joi.string().optional(),
                    countryENCode:Joi.string().optional(),
                    countryCode:Joi.string().optional(),
                    phoneNo:Joi.string().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/saveAddress',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.UserController.saveAddress(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'User saveAddress'+'required Fields: streetName,buildingName,'+
            'villaName,city,country',
            auth: 'UserAuth',
            tags: ['api','user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),

                    streetName:Joi.string().required(),
                    buildingName:Joi.string().required(),
                    villaName:Joi.string().required(),
                    city:Joi.string().required(),
                    country:Joi.string().required(),
                    moreDetailedaddress:Joi.string().optional(),
                    lat: Joi.number().required(),
                    long: Joi.number().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/deleteAddress',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.UserController.deleteAddress(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'User deleteAddress'+'required Fields: streetName,buildingName,'+
            'villaName,city,country',
            auth: 'UserAuth',
            tags: ['api','user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    id:Joi.string().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/listAllUserAddress',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.getAllAddress(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Address Listing for User App',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    // pageNo: Joi.number().required().min(1),
                    // limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/user/userLogout',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData._id) {
                Controller.UserController.userLogout(request.payload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(data)
                    }
                });
            }else{
                console.log(".........Access Token Not verified..............");
            }
        },
        config: {
            description: 'Logout by user',
            tags: ['api', 'user'],
            auth: 'UserAuth',
            validate: {
                payload:{
                    uniquieAppKey:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/listAllLanguagesUser',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.listAllLanguagesUser(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Languages Listing for User App',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    // pageNo: Joi.number().required().min(1),
                    // limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/user/changeLanguageUser',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData._id) {
                Controller.UserController.changeLanguageUser(request.payload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            }else{
                reply(UniversalFunctions.sendSuccess(null))
                console.log(".........Access Token Not verified..............");
            }
        },
        config: {
            description: 'Change application language by user',
            tags: ['api', 'user'],
            auth: 'UserAuth',
            validate: {
                payload:{
                    uniquieAppKey:Joi.string().trim().required(),
                    language:Joi.string().valid(['EN','AR']).required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'GET',
        path: '/user/getAllCountryUser',
        handler: function (request, reply) {
            // var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.AgencyController.countryList( request.query, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List all countries for maid',
            tags: ['api', 'agency'],
            // auth: 'UserAuth',

            validate: {
                query:{
                    uniquieAppKey:Joi.string().trim().required(),
                },
                // headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/listAllNationalityUser',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.listAllNationalityUser(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Nationality Listing for User App',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    // pageNo: Joi.number().required().min(1),
                    // limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/listAllReligion',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.listAllReligion(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Religion Listing for User App',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    // pageNo: Joi.number().required().min(1),
                    // limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/listAllAgencyUser',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.listAllAgencyUser(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Maids Listing for User App',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    searchAgencyName:Joi.string().trim().optional(),
                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required(),
                    long: Joi.number().required(),
                    lat: Joi.number().required(),
                    country: Joi.string().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/listAllMaidsUser',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.listAllMaidsUser(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Maids Listing for User App (workDate in milliseconds)',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    nationality:Joi.array().single().optional(),
                    languages:Joi.array().single().optional(),
                    religion:Joi.array().single().optional(),
                    gender:Joi.string().optional().valid([
                        "MALE", "FEMALE", "BOTH"
                    ]),
                    agencyId:Joi.array().single().optional(),
                    searchMaidName: Joi.string().optional(),
                    workDate:Joi.number().optional(),
                    startTime:Joi.number().optional(),
                    hour:Joi.number().optional(),
                    duration:Joi.number().optional(),
                    timeZone:Joi.string().optional().trim(),
                    deviceTimeZone:Joi.string().optional().trim(),
                    long: Joi.number().required(),
                    lat: Joi.number().required(),
                    country: Joi.string().required(),
                    locationName: Joi.string().optional(),

                    sort:Joi.string().optional().valid([
                        "EXPERIENCE", "RATING", "DISTANCE", "PRICE_LOW", "PRICE_HIGH"
                    ]),

                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/listAllMaidsUserWeb',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.listAllMaidsUserWeb(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Maids Listing for User App (workDate in milliseconds)',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    nationality:Joi.array().single().optional(),
                    languages:Joi.array().single().optional(),
                    religion:Joi.array().single().optional(),
                    gender:Joi.string().optional().valid([
                        "MALE", "FEMALE", "BOTH"
                    ]),
                    agencyId:Joi.array().single().optional(),
                    searchMaidName: Joi.string().optional(),
                    workDate:Joi.number().optional(),
                    startTime:Joi.number().optional(),
                    duration:Joi.number().optional(),
                    timeZone:Joi.string().optional(),

                    long: Joi.number().required(),
                    lat: Joi.number().required(),
                    country: Joi.string().required(),
                    locationName: Joi.string().optional(),

                    sort:Joi.string().optional().valid([
                        "EXPERIENCE", "RATING", "DISTANCE", "PRICE_LOW", "PRICE_HIGH"
                    ]),

                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/getMaidProfileDetail',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.getMaidProfileDetail(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Maids profile Information',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    maidId:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/bookService',
        handler: function (request, reply) {
            var payloadData = request.payload;
            console.log("data******************",payloadData);
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.UserController.bookService(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Book time slots of maid'+'required Fields: long, lat, timeZone, streetName, buildingName,'+
            'villaName, city, country, maidId, timeZone, serviceData[arrayFileds required:-workDate,startTime,duration]',
            auth: 'UserAuth',
            tags: ['api','user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),

                    long: Joi.number().required(),
                    lat: Joi.number().required(),
                    hour: Joi.number(),
                    locationName: Joi.string().optional(),

                    address:{
                        streetName:Joi.string().required(),
                        buildingName:Joi.string().required(),
                        villaName:Joi.string().required(),
                        city:Joi.string().required(),
                        country:Joi.string().required(),
                        moreDetailedaddress:Joi.string().optional(),
                    },
                    maidId:Joi.string().required(),
                    timeZone:Joi.string().required(),
                    deviceTimeZone:Joi.string(),
                    currency:Joi.string().optional(),
                    serviceData:Joi.array().single().items({
                        workDate:Joi.string().required(),
                        startTime:Joi.string().required(),
                        duration:Joi.number().required(),
                    })
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/listAllServiceUser',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.listAllServiceUser(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Service Listing for User App',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    onBasisOfDate:Joi.string().required().valid([
                        "ON_GOING", "COMPLETED", "UPCOMING"
                    ]),
                    timeZone:Joi.string().required(),
                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/cancelService',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.UserController.cancelService(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: "Cancel service by user(not after service starts)",
            auth: 'UserAuth',
            tags: ['api','user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    serviceId:Joi.string().required(),
                    deleteReason:Joi.string().required(),
                    timeZone:Joi.string().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    // payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/listAllCanceledServiceUsers',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.listAllCanceledServiceUsers(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Service Listing for user',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {

                    uniquieAppKey:Joi.string().trim().required(),

                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
         method: 'POST',
         path: '/user/requestForExtendService',
         handler: function (request, reply) {
             var payloadData = request.payload;
             var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
             Controller.UserController.requestForExtendService(payloadData, userData, function (err, data) {
                 if (err) {
                     reply(UniversalFunctions.sendError(err));
                 } else {
                     reply(UniversalFunctions.sendSuccess(null, data))
                 }
             });
         },
         config: {
             description: "Request for extending service of maid",
             auth: 'UserAuth',
             tags: ['api', 'user'],
             validate: {
                 payload: {
                     uniquieAppKey:Joi.string().trim().required(),
                     serviceId:Joi.string().required(),
                     duration: Joi.number().required(),
                     reason: Joi.string().required(),
                     // timeZone:Joi.string().required(),
                 },
                 headers: UniversalFunctions.authorizationHeaderObj,
                 failAction: UniversalFunctions.failActionFunction
             },
             plugins: {
                 'hapi-swagger': {
                     payloadType: 'form',
                     responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                 }
             }
         }
    },

    {
        method: 'POST',
        path: '/user/addReview',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.UserController.addReview(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: "Add maid and agency review",
            auth: 'UserAuth',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    serviceId:Joi.string().required(),
                    // reviewByType:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER, Config.APP_CONSTANTS.DATABASE.USER_ROLES.MAID,]).required(),
                    maidRating: {
                        cleaning:Joi.number().optional(),
                        ironing:Joi.number().optional(),
                        cooking:Joi.number().optional(),
                        childCare:Joi.number().optional(),
                    },
                    feedBack: Joi.string().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/user/changePassword',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData._id){
                Controller.UserController.changePassword(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(null, data)
                    }
                });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED));
            }
        },
        config: {
            description: "Change password by user",
            auth: 'UserAuth',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    oldPassword:Joi.string().trim().required(),
                    newPassword:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/forgetPassword',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.forgetPassword(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(null, data)
                }
            });
        },
        config: {
            description: "Forgot password by user",
            tags: ['api', 'user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    email:Joi.string().trim().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/raiseAnIssue',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData._id && userData){
                Controller.UserController.raiseAnIssue(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_ABLE_TO_RAISE));
            }

        },
        config: {
            description: "Raise an issue by user",
            tags: ['api', 'user'],
            auth: 'UserAuth',
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    issue:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/contactUs',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AgencyController.contactUs(payloadData, {}, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Request for maid on website',
            tags: ['api','user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    name:Joi.string().required(),
                    email:Joi.string().required(),
                    comment:Joi.string().required(),
                    phoneNumber:Joi.string().optional(),
                    type:Joi.string().default('USER'),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/getNotifications',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                    Controller.UserController.getNotifications(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
            },
            description: 'Get all notification',
            notes: 'Get notification',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/addFavouriteMaid',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.UserController.addFavouriteMaid(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: "Add to favorite",
            auth: 'UserAuth',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    maidId:Joi.string().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/listFavouriteMaid',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData){
                Controller.UserController.listFavouriteMaid(data, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            }
            else reply(UniversalFunctions.sendSuccess(null, []))

        },
        config: {
            description: 'List favourite Maids',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/checkMaidAvailable',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.checkMaidAvailable(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Maids Listing for User App (workDate in milliseconds)',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    workDate:Joi.number().required(),
                    startTime:Joi.number().required(),
                    duration:Joi.number().required(),
                    timeZone:Joi.string().required(),
                    maidId:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/getSlots',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.getSlots(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Maids Listing for User App (workDate in milliseconds)',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    timeZone:Joi.string().required(),
                    maidId:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/user/removeFavouriteMaid',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.UserController.removeFavouriteMaid(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: "Add to favorite",
            auth: 'UserAuth',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    maidId:Joi.string().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/createChat',
        handler: function (request, reply) {
            var data = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.MaidController.createChat(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Create Chat',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    serviceId:Joi.string().trim().required(),
                    receiverId:Joi.string().trim().required(),
                    message:Joi.string().trim().optional(),
                    locationName:Joi.string().trim().optional(),
                    lat:Joi.number().optional(),
                    lng:Joi.number().optional(),
                    messageType:Joi.string().valid(['MESSAGE','LOCATION']).required(),
                    senderType:Joi.string().valid(['MAID','USER']).required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/getAllChat',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData.id){
                Controller.MaidController.getAllChat(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            }
            else {
                Controller.MaidController.getAllChat(payloadData, {}, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            }

        },
        config: {
            description: 'Get main chat screen',
            tags: ['api', 'user'],
            auth: 'UserAuth',
            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    userType:Joi.string().valid(["MAID", "USER"]).required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/getChatHistory',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.MaidController.getChatHistory(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get chat history',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                    userId:Joi.string().trim().required(),
                    lastMessageCount:Joi.number().required(),
                    userType:Joi.string().valid(["MAID", "USER"]).required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/addCard',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.UserController.addCard(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'add cards',
            notes: 'add cards',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().optional(),
                    name:Joi.string().optional(),
                    cardNumber:Joi.string().optional(),
                    cardToken:Joi.string().optional(),
                    cardType:Joi.string().optional(),
                    customerEmail:Joi.string().optional(),
                    customerPassword:Joi.string().optional(),
                    validTill:Joi.string().optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/getCards',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.UserController.getCards(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }

            },
            description: 'get card details',
            notes: 'get card details',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/deleteCard',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.UserController.deleteCard(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }

            },
            description: 'delete card details',
            notes: 'delete card details',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    cardId : Joi.string().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/createPayment',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.UserController.createPayment(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'create payment',
            notes: 'create payment',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    serviceId:Joi.string().required(),
                    cardToken:Joi.string().optional(),
                    saveCards:Joi.boolean().valid([true,false]).required(),
                    isExtension:Joi.boolean().valid([true,false]).optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/createPaymentPaytabs',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.UserController.createPaymentPaytabs(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'create payment via paytabs',
            notes: 'create payment',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    uniquieAppKey:Joi.string().trim().required(),
                    serviceId:Joi.string().required(),
                    cardType: Joi.number().valid([1,2]).description('1- debit,2- credit'),
                    transactionId:Joi.string().optional(),
                    transactionStatus:Joi.string().optional(),
                    isExtension:Joi.boolean().valid([true,false]).optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/payTabs',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.UserController.payTabs(payloadData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: "Integrating pay tabs",
            auth: 'UserAuth',
            tags: ['api', 'user'],
            validate: {
                payload: {},
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/searchMaidAndAgencyWeb',
        handler: function (request, reply) {
            var data = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            Controller.UserController.searchMaidAndAgencyWeb(data, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Search for maid or agency',
            tags: ['api', 'user'],
            auth: 'UserAuth',

            validate: {
                query: {
                    uniquieAppKey:Joi.string().trim().required(),

                    pageNo: Joi.number().required().min(1),
                    limit: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

{
        method: 'POST',
        path: '/user/credimaxSession',
        handler: function (request, reply) {

            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData._id) {
                var payloadData = request.payload;
                Controller.UserController.credimaxsession(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            }else{
                console.log(".........Access Token Not verified..............");
            }
        },
        config: {
            description: 'Credimax',
            tags: ['api', 'user'],
            auth: 'UserAuth',
            validate: {
                payload: {
	                    uniquieAppKey:Joi.string().trim().required(),
                    amount:Joi.number().required()                   
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    /*{
        method: 'POST',
        path: '/user/languageConvertor',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.UserController.languageConvertor(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: "languageConvertor",
            auth: 'UserAuth',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    string:Joi.string().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },*/
];
