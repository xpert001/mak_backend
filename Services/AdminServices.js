'use strict';

var Models = require('../Models');


//Insert Admin in DB
var createAdmin= function (objToSave, callback) {
    new Models.Admins(objToSave).save(callback)
};

//Update Admin in DB
var updateAdmin = function (criteria, dataToSet, options, callback) {
    Models.Admins.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete Admin in DB
var deleteAdmin = function (criteria, callback) {
    Models.Admins.findOneAndRemove(criteria, callback);
};

//Get Admin from DB
var getAdmin = function (criteria, projection, options, callback) {
    Models.Admins.find(criteria, projection, options, callback);
};



module.exports = {
    createAdmin: createAdmin,
    updateAdmin:updateAdmin,
    deleteAdmin:deleteAdmin,
    getAdmin:getAdmin
};

