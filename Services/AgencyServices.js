'use strict';

var Models = require('../Models');

//Insert Agency in DB
var createAgency= function (objToSave, callback) {
    new Models.Agency(objToSave).save(callback)
};

//Update Agency in DB
var updateAgency = function (criteria, dataToSet, options, callback) {
    Models.Agency.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Update All Agency in DB
var updateAllAgency = function (criteria, dataToSet, options, callback) {
    Models.Agency.update(criteria, dataToSet, options, callback);
};

//Delete Agency in DB
var deleteAgency = function (criteria, callback) {
    Models.Agency.findOneAndRemove(criteria, callback);
};

//Find Agency in DB
var getAgency = function (criteria, projection, options, callback) {
    Models.Agency.find(criteria, projection, options, callback);
};

//populate Agency
var getAgencyPopulate = function (criteria, project, options,populateArray, callback) {
    Models.Agency.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

let count = function (condition, callback) {
    Models.Agency.count(condition, function (error, count) {
        if (error) return callback(error);
        return callback(null, count);
    })
};


module.exports = {

    createAgency: createAgency,
    updateAgency:updateAgency,
    count:count,
    updateAllAgency:updateAllAgency,
    deleteAgency:deleteAgency,
    getAgency:getAgency,
    getAgencyPopulate:getAgencyPopulate
};

