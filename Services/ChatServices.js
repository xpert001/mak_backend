'use strict';

var Models = require('../Models');

//Insert Chats in DB
var createChat= function (objToSave, callback) {
    new Models.Chat(objToSave).save(callback)
};

//Update Chats in DB
var updateChat = function (criteria, dataToSet, options, callback) {
    Models.Chat.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Update All Chats in DB
var updateAllChat = function (criteria, dataToSet, options, callback) {
    Models.Chat.update(criteria, dataToSet, options, callback);
};

//Delete Chats in DB
var deleteChat = function (criteria, callback) {
    Models.Chat.findOneAndRemove(criteria, callback);
};

//Find Chats in DB
var getChat = function (criteria, projection, options, callback) {
    Models.Chat.find(criteria, projection, options, callback);
};

//populate Chats
var getChatPopulate = function (criteria, project, options,populateArray, callback) {
    Models.Chat.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};



module.exports = {

    createChat: createChat,
    updateChat:updateChat,
    updateAllChat:updateAllChat,
    deleteChat:deleteChat,
    getChat:getChat,
    getChatPopulate:getChatPopulate
};

