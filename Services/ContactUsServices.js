/**
 * Created by cbluser47 on 5/12/17.
 */

'use strict';

var Models = require('../Models');

//Insert ContactUs in DB
var createContactUs= function (objToSave, callback) {
    new Models.ContactUs(objToSave).save(callback)
};

//Update ContactUs in DB
var updateContactUs = function (criteria, dataToSet, options, callback) {
    Models.ContactUs.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Update All ContactUs in DB
var updateAllContactUs = function (criteria, dataToSet, options, callback) {
    Models.ContactUs.update(criteria, dataToSet, options, callback);
};

//Delete ContactUs in DB
var deleteContactUs = function (criteria, callback) {
    Models.ContactUs.findOneAndRemove(criteria, callback);
};

//Find ContactUs in DB
var getContactUs = function (criteria, projection, options, callback) {
    Models.ContactUs.find(criteria, projection, options, callback);
};

//populate ContactUs
var getContactUsPopulate = function (criteria, project, options,populateArray, callback) {
    Models.ContactUs.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};



module.exports = {

    createContactUs: createContactUs,
    updateContactUs:updateContactUs,
    updateAllContactUs:updateAllContactUs,
    deleteContactUs:deleteContactUs,
    getContactUs:getContactUs,
    getContactUsPopulate:getContactUsPopulate
};

