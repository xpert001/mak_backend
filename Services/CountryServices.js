/**
 * Created by cbluser47 on 9/12/17.
 */


'use strict';

var Models = require('../Models');

//Insert Country in DB
var createCountry= function (objToSave, callback) {
    new Models.Country(objToSave).save(callback)
};

//Update Country in DB
var updateCountry = function (criteria, dataToSet, options, callback) {
    Models.Country.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Update All Country in DB
var updateAllCountry = function (criteria, dataToSet, options, callback) {
    Models.Country.update(criteria, dataToSet, options, callback);
};

//Delete Country in DB
var deleteCountry = function (criteria, callback) {
    Models.Country.findOneAndRemove(criteria, callback);
};

//Find Country in DB
var getCountry = function (criteria, projection, options, callback) {
    Models.Country.find(criteria, projection, options, callback);
};

//populate Country
var getCountryPopulate = function (criteria, project, options,populateArray, callback) {
    Models.Country.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};



module.exports = {

    createCountry: createCountry,
    updateCountry:updateCountry,
    updateAllCountry:updateAllCountry,
    deleteCountry:deleteCountry,
    getCountry:getCountry,
    getCountryPopulate:getCountryPopulate
};

