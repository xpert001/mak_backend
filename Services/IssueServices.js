'use strict';

var Models = require('../Models');

//Insert Issue in DB
var createIssue= function (objToSave, callback) {
    new Models.Issue(objToSave).save(callback)
};

//Update Issue in DB
var updateIssue= function (criteria, dataToSet, options, callback) {
    Models.Issue.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Update All Issue in DB
var updateAllIssue= function (criteria, dataToSet, options, callback) {
    Models.Issue.update(criteria, dataToSet, options, callback);
};

//Delete Issue in DB
var deleteIssue= function (criteria, callback) {
    Models.Issue.findOneAndRemove(criteria, callback);
};

//Find Issue in DB
var getIssue= function (criteria, projection, options, callback) {
    Models.Issue.find(criteria, projection, options, callback);
};

//populate Issue
var getIssuePopulate = function (criteria, project, options,populateArray, callback) {
    Models.Issue.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

let IssueAggregation=function (pipeline,callback) {
    Models.Issue.aggregate(pipeline)
        .exec(function(error, result) {
            //console.log(result)
            callback(error,result);
        });
};

module.exports = {
    createIssue: createIssue,
    updateIssue:updateIssue,
    updateAllIssue:updateAllIssue,
    deleteIssue:deleteIssue,
    getIssue:getIssue,
    getIssuePopulate:getIssuePopulate,
    IssueAggregation:IssueAggregation
};

