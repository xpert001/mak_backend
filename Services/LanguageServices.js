'use strict';

var Models = require('../Models');


//Insert Language in DB
var createLanguage= function (objToSave, callback) {
    new Models.Languages(objToSave).save(callback)
};

//Update Language in DB
var updateLanguage = function (criteria, dataToSet, options, callback) {
    Models.Languages.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete Language in DB
var deleteLanguage = function (criteria, callback) {
    Models.Languages.findOneAndRemove(criteria, callback);
};

//Get Language from DB
var getLanguage = function (criteria, projection, options, callback) {
    Models.Languages.find(criteria, projection, options, callback);
};



module.exports = {
    createLanguage: createLanguage,
    updateLanguage:updateLanguage,
    deleteLanguage:deleteLanguage,
    getLanguage:getLanguage
};

