'use strict';

var Models = require('../Models');

//Insert Maid in DB
var createMaid= function (objToSave, callback) {
    new Models.Maids(objToSave).save(callback)
};

//Update Maid in DB
var updateMaid = function (criteria, dataToSet, options, callback) {
    Models.Maids.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Update All Maid in DB
var updateAllMaid = function (criteria, dataToSet, options, callback) {
    Models.Maids.update(criteria, dataToSet, options, callback);
};


//Update All Maid in DB
var updateAll= function (criteria, dataToSet, options, callback) {
    Models.Maids.updateMany(criteria, dataToSet, options, callback);
};


//Delete Maid in DB
var deleteMaid = function (criteria, callback) {
    Models.Maids.findOneAndRemove(criteria, callback);
};

//Find Maid in DB
var getMaid = function (criteria, projection, options, callback) {
    Models.Maids.find(criteria, projection, options, callback);
};

//populate Maid
var getMaidPopulate = function (criteria, project, options,populateArray, callback) {
    Models.Maids.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};
let count = function (condition, callback) {
    Models.Maids.count(condition, function (error, count) {
        if (error) return callback(error);
        return callback(null, count);
    })
};


module.exports = {

    createMaid: createMaid,
    updateMaid:updateMaid,
    updateAllMaid:updateAllMaid,
    deleteMaid:deleteMaid,
    getMaid:getMaid,
    count:count,
    getMaidPopulate:getMaidPopulate,
    updateAll:updateAll
};

