'use strict';

var Models = require('../Models');

//Insert Notification in DB
var createNotification= function (objToSave, callback) {
    new Models.Notification(objToSave).save(callback)
};

//Update Notification in DB
var updateNotification = function (criteria, dataToSet, options, callback) {
    Models.Notification.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Update All Notification in DB
var updateAllNotification = function (criteria, dataToSet, options, callback) {
    Models.Notification.update(criteria, dataToSet, options, callback);
};

//Delete Notification in DB
var deleteNotification = function (criteria, callback) {
    Models.Notification.findOneAndRemove(criteria, callback);
};

//Find Notification in DB
var getNotification = function (criteria, projection, options, callback) {
    Models.Notification.find(criteria, projection, options, callback);
};

//populate Users
var getNotificationPopulate = function (criteria, project, options,populateArray, callback) {
    Models.Notification.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};



module.exports = {
    createNotification: createNotification,
    updateNotification:updateNotification,
    updateAllNotification:updateAllNotification,
    deleteNotification:deleteNotification,
    getNotification:getNotification,
    getNotificationPopulate:getNotificationPopulate
};

