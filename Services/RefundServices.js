'use strict';

var Models = require('../Models');

//Insert Issue in DB
var createRefund = function (objToSave, callback) {
    new Models.RefundLogs(objToSave).save(callback)
};

//Update Issue in DB
var updateRefund= function (criteria, dataToSet, options, callback) {
    Models.RefundLogs.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Update All Issue in DB
var updateAllRefund = function (criteria, dataToSet, options, callback) {
    Models.RefundLogs.update(criteria, dataToSet, options, callback);
};

//Delete Issue in DB
var deleteRefund = function (criteria, callback) {
    Models.RefundLogs.findOneAndRemove(criteria, callback);
};

//Find Issue in DB
var getRefund= function (criteria, projection, options, callback) {
    Models.RefundLogs.find(criteria, projection, options, callback);
};

//populate Issue
var getRefundPopulate = function (criteria, project, options,populateArray, callback) {
    Models.RefundLogs.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

let RefundAggregation=function (pipeline,callback) {
    Models.RefundLogs.aggregate(pipeline)
        .exec(function(error, result) {
            //console.log(result)
            callback(error,result);
        });
};

module.exports = {
    createRefund: createRefund,
    updateRefund:updateRefund,
    updateAllRefund:updateAllRefund,
    deleteRefund:deleteRefund,
    getRefund:getRefund,
    getRefundPopulate:getRefundPopulate,
    RefundAggregation:RefundAggregation
};

