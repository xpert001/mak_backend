'use strict';

var Models = require('../Models');


//Insert Admin in DB
var createReligion= function (objToSave, callback) {
    new Models.Languages(objToSave).save(callback)
};

//Update Admin in DB
var updateReligion = function (criteria, dataToSet, options, callback) {
    Models.Religions.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete Admin in DB
var deleteReligion= function (criteria, callback) {
    Models.Religions.findOneAndRemove(criteria, callback);
};

//Get Admin from DB
var getReligion= function (criteria, projection, options, callback) {
    Models.Religions.find(criteria, projection, options, callback);
};



module.exports = {
    createReligion: createReligion,
    updateReligion:updateReligion,
    deleteReligion:deleteReligion,
    getReligion:getReligion
};

