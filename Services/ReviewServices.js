'use strict';

var Models = require('../Models');

//Insert Review in DB
var createReview = function (objToSave, callback) {
    new Models.Review(objToSave).save(callback)
};

//Update Review in DB
var updateReview = function (criteria, dataToSet, options, callback) {
    Models.Review.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Update All Review in DB
var updateAllReview  = function (criteria, dataToSet, options, callback) {
    Models.Review.update(criteria, dataToSet, options, callback);
};

//Delete Review in DB
var deleteReview = function (criteria, callback) {
    Models.Review.findOneAndRemove(criteria, callback);
};

//Find Review in DB
var getReview = function (criteria, projection, options, callback) {
    Models.Review.find(criteria, projection, options, callback);
};

//populate Review
var getReviewPopulate = function (criteria, project, options,populateArray, callback) {
    Models.Review.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};



module.exports = {

    createReview: createReview,
    updateReview:updateReview,
    updateAllReview:updateAllReview,
    deleteReview:deleteReview,
    getReview:getReview,
    getReviewPopulate:getReviewPopulate
};

