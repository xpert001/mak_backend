'use strict';

var Models = require('../Models');

//Insert Service in DB
var createService= function (objToSave, callback) {
    new Models.Service(objToSave).save(callback)
};

//Update Service in DB
var updateService = function (criteria, dataToSet, options, callback) {
    Models.Service.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Update All Service in DB
var updateAllService = function (criteria, dataToSet, options, callback) {
    Models.Service.update(criteria, dataToSet, options, callback);
};


//Find Service in DB
var getService = function (criteria, projection, options, callback) {
    Models.Service.find(criteria, projection, options, callback);
};

//populate Service
var getServicePopulate = function (criteria, project, options,populateArray, callback) {
    Models.Service.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};



module.exports = {

    createService: createService,
    updateService:updateService,
    updateAllService:updateAllService,
    getService:getService,
    getServicePopulate:getServicePopulate
};

