'use strict';

var Models = require('../Models');

//Insert User in DB
var createUser= function (objToSave, callback) {
    new Models.Users(objToSave).save(callback)
};

//Update User in DB
var updateUser = function (criteria, dataToSet, options, callback) {
    Models.Users.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Update All User in DB
var updateAllUser = function (criteria, dataToSet, options, callback) {
    Models.Users.update(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteUser = function (criteria, callback) {
    Models.Users.findOneAndRemove(criteria, callback);
};

//Find User in DB
var getUsers = function (criteria, projection, options, callback) {
    Models.Users.find(criteria, projection, options, callback);
};

//Find version in DB
var getVersion= function (criteria, projection, options, callback) {
    Models.AppVersions.find(criteria, projection, options, callback);
};

//populate Users
var getUsersPopulate = function (criteria, project, options,populateArray, callback) {
    Models.Users.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

let userAggregation=function (pipeline,callback) {
    Models.Users.aggregate(pipeline)
        .exec(function(error, result) {
            //console.log(result)
            callback(error,result);
        });
};

module.exports = {

    createUser: createUser,
    updateUser:updateUser,
    updateAllUser:updateAllUser,
    deleteUser:deleteUser,
    getUsers:getUsers,
    getUsersPopulate:getUsersPopulate,
    getVersion:getVersion,
    userAggregation:userAggregation
};

