module.exports = {
    AdminServices:require('./AdminServices'),
    AgencyServices:require('./AgencyServices'),
    MaidServices: require('./MaidServices'),
    UserServices:require('./UserServices'),
    LanguageServices:require('./LanguageServices'),
    ServiceServices:require('./ServiceServices'),
    ReviewServices:require('./ReviewServices'),
    NotificationServices:require('./NotificationServices'),
    ContactUsServices:require('./ContactUsServices'),
    CountryServices:require('./CountryServices'),
    ChatServices:require('./ChatServices'),
    IssueServices:require('./IssueServices'),
    RefundServices:require('./RefundServices'),
    ReligionServices:require('./ReligionServices')

};
