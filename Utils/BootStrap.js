'use strict';

const mongoose = require('mongoose');
const Config = require('../Config');
const SocketManager = require('../Lib/SocketManager');
const Service = require('../Services');
const async = require('async');
const UniversalFunctions = require('../Utils/UniversalFunctions');


//Connect to MongoDB
mongoose.connect(Config.dbConfig.mongo.URI, function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected');
    }
});

exports.bootstrapAdmin = function (callback) {
    var adminData1 = {
        email: 'ankit@gmail.com',
        password: '14e1b600b1fd579f47433b88e8d85291',
        name: 'Ankit',
        superAdmin:true,
        uniquieAppKey:UniversalFunctions.CryptData(Config.APP_CONSTANTS.SERVER.APP_NAME)
    };

    var adminData2 = {
        email: 'admin@gmail.com',
        password: '14e1b600b1fd579f47433b88e8d85291',
        name: 'Admin',
        superAdmin:false,
        uniquieAppKey:UniversalFunctions.CryptData(Config.APP_CONSTANTS.SERVER.APP_NAME)
};
    async.parallel([
        function (cb) {
            insertData(adminData1.email, adminData1, cb)
        },
        function (cb) {
              insertData(adminData2.email, adminData2, cb)
        },
    ], function (err, done) {
        callback(err, 'Bootstrapping finished');
    })
};

function insertData(email, adminData, callback) {
    var needToCreate = true;
    async.series([function (cb) {
        var criteria = {
            email: email
        };
        Service.AdminServices.getAdmin(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
            }
            cb()
        })
    }, function (cb) {
        if (needToCreate) {
            Service.AdminServices.createAdmin(adminData, function (err, data) {
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function (err, data) {
        console.log('Bootstrapping finished for ' + email);
        callback(err, 'Bootstrapping finished')
    })
};


exports.bootstrapLanguages = function (callback) {
    var languageData1 = {
        languageName: 'English',
        languageCode: 'EN',
        uniquieAppKey:UniversalFunctions.CryptData(Config.APP_CONSTANTS.SERVER.APP_NAME)

    };

    var languageData2 = {
        languageName: 'Arabic',
        languageCode: 'AR',
        uniquieAppKey:UniversalFunctions.CryptData(Config.APP_CONSTANTS.SERVER.APP_NAME)

    };
    async.parallel([
        function (cb) {
            insertDataLanguages(languageData1.languageCode, languageData1, cb)
        },
        function (cb) {
            insertDataLanguages(languageData2.languageCode, languageData2, cb)
        },
    ], function (err, done) {
        callback(err, 'Bootstrapping finished');
    })
};

function insertDataLanguages(languageCode, languageData, callback) {
    var needToCreate = true;
    async.series([function (cb) {
        var criteria = {
            languageCode: languageCode
        };
        Service.LanguageServices.getLanguage(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
            }
            cb()
        })
    }, function (cb) {
        if (needToCreate) {
            Service.LanguageServices.createLanguage(languageData, function (err, data) {
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function (err, data) {
        console.log('Bootstrapping finished for ' + languageCode);
        callback(err, 'Bootstrapping finished')
    })
}

exports.connectSocket = SocketManager.connectSocket;



exports.createServiceId = function (callback) {

    Service.ServiceServices.getService({},{},{lean:true},function (err, res) {
        process.env.serviceId = 100000+res.length || 100000;
        // console.log("Service Id created..",process.env.serviceId);
        callback(err,res)
    })
};
exports.getAdminCommission = function (callback) {

    Service.AdminServices.getAdmin({superAdmin:false},{},{lean:true},function (err, res) {
        process.env.commission = res[0].commission;
        console.log("Service Id created..",process.env.commission);
        callback(err,res)
    })
};


