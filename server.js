'use strict';

var Hapi = require('hapi');

//Internal Dependencies
var Routes = require('./Routes');
var Plugins = require('./Plugins');
var Config = require('./Config');
var Bootstrap = require('./Utils/BootStrap');
const async = require('async');
const SocketManager = require('./Lib/SocketManager');
const moment = require('moment');
const request = require('request');
const momentTz=require('moment-timezone');
const Service = require('./Services');
const Controller = require('./Controllers');
const MailManager = require('./Lib').MailManager;

/*MailManager.sendMailBySES('rohit@code-brew.com','sdsf',';fdsfsdfsdf',(err)=>{

})*/
//Create Server
var server = new Hapi.Server({
    app: {
        name: Config.APP_CONSTANTS.SERVER.APP_NAME
    }
});
server.connection({
    port: Config.APP_CONSTANTS.SERVER.PORTS.HAPI,
    routes: {cors: true}
});

server.register(Plugins, function (err) {
    if (err) {
        server.error('Error while loading plugins : ' + err)
    } else {
        server.route(Routes);
        server.route(
            {
                method: 'GET',
                path: '/',
                handler: function (req, res) {
                    //TODO Change for production server
                    res.view("index");
                }
            }
        );
        server.log('info', 'Plugins Loaded')
    }
});
console.log('455555555555555',moment.tz('Asia/Bahrain').utcOffset(),moment.tz(1527156000000,'Asia/Bahrain'))
/*server.register(Inert, function () {
    server.route( {
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: { path: Path.normalize(__dirname + '/') }
        }
    });
})*/

//rendering of html pages






// server.views({
    // engines: {
        // html: require('handlebars')        
    // },
    // relativeTo: __dirname,
    // path: './views'
// });


 server.views({
	engines: { ejs: require('ejs') },
	relativeTo: __dirname,
	 path: './views'
});

Bootstrap.bootstrapAdmin(function (err, message) {
    if (err) {
        console.log('Error while bootstrapping admin : ' + err)
    } else {
        // console.log(message);
    }
});

Bootstrap.bootstrapLanguages(function (err, message) {
    if (err) {
        console.log('Error while bootstrapping languages : ' + err)
    } else {
        // console.log(message);
    }
});

Bootstrap.createServiceId(function (err, message) {
    if (err) {
        console.log('Error while creating Service Id: ' + err)
    } else {
        // console.log(message);
    }
})

Bootstrap.getAdminCommission(function (err, message) {
    if (err) {
        console.log('Error while creating Service Id: ' + err)
    } else {
        // console.log(message);
    }
})

server.start(
    console.log('Server running at:', server.info.uri)
);
server.on('response', function (request) {
    console.log(request.info.remoteAddress + ': ' + request.method.toUpperCase() +
        ' ' + request.url.path + ' --> ' + request.response.statusCode);
    console.log('Request payload:', request.payload);
});


// var getCountry = require('country-currency-map').getCountry;
// console.log("___________jhjh",getCountry('United Arab Emirates'))
// getCountry('Canada');


var completeServiceCron = setInterval(function () {
    console.log('in completed cron.................')
    let userData=[];
    async.auto({

        getAcceptedService:function (cb) {
            let criteria = {
                deleteAction: {$nin: [Config.APP_CONSTANTS.DATABASE.ACTION.DELETED]},
                agencyAction : Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT,
                isCompleted : false,
                endTime : {$lte: (Date.now())}
            };
            let projection = {};
            let option = {lean: true};
            Service.ServiceServices.getService(criteria, projection, option,function (err, res) {
                if(err){
                    cb(err)
                } else{
                    if(res && res.length){
                        console.log("____________lol____________________result",res.length);
                        userData=res ;
                        cb(null)
                    } else {
                        cb(null)
                    }

                }
            })
        },

        completeAcceptedService:['getAcceptedService',function (err, cb) {
            userData.forEach(function (obj) {
                updateAcceptService(obj._id,function (err, result) {
                    console.log("_______________________________in function");
                });
            });
            cb()
        }]

    },function (err, res) {
        // console.log("____________cron 1 complete")
    })
},60000);

const updateAcceptService = function (payloadData, callback)  {
    let userId="";
    let agencyId="";
    let agencyName="";
    let maidId="";
    let serviceId="";
    let serviceMakId="";
    let notificationDataUser={};
    let notificationDataMaid={};
    let bookingNumber;
    async.autoInject({
        updateCompleteService: function (cb) {
            let criteria = {
                _id:payloadData,
                isCompleted: false,
                endTime : {$lte: (Date.now())}
            };
            let dataToSet = {
                isCompleted:true,
                agencyAction : Config.APP_CONSTANTS.DATABASE.ACTION.ACCEPT
            };
            let options = {new: true};
            Service.ServiceServices.updateService(criteria, dataToSet, options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                   // console.log("_________getCompleteService___________", result);
                    if (result) {
                        userId=result.userId;
                        maidId = result.maidId;
                        agencyId = result.agencyId;
                        bookingNumber = result.bookingId;
                        serviceId = result._id;
                        serviceMakId = result.serviceMakId;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },
        expireChat: function (updateCompleteService,cb) {
            let criteria = {
                serviceId: serviceId
            };
            let dataToSet = {
                isExpired:true,
            };
            let options = {multi: true};
            Service.ChatServices.updateAllChat(criteria, dataToSet, options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                  cb()
                }
            })
        },

        getAgencyData: function (updateCompleteService, cb) {
            let criteria = {
                _id: agencyId,
                isDeleted: false,
                //isDeactivated: false,
            };
            let projection = {};
            let option = {lean: true};
            Service.AgencyServices.getAgency(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        agencyName=result[0].agencyName;
                        notificationDataUser.senderProfilePicURL=result[0].profilePicURL.original;
                        notificationDataMaid.senderProfilePicURL=result[0].profilePicURL.original;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getUsersData: function (updateCompleteService, cb) {
            notificationDataUser.agencyName = agencyName;
            let criteria = {
                _id: userId,
                isDeleted: false,
                isDeactivated: false,
            };
            let projection = {};
            let option = {lean: true};
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result && result.length) {
                        notificationDataMaid.fullName = result[0].fullName;

                        notificationDataUser.receiverId = result[0]._id;
                        notificationDataUser.fullName = result[0].fullName;
                        notificationDataUser.profilePicURL = result[0].profilePicURL;
                        notificationDataUser.deviceToken = result[0].deviceToken;
                        cb(null)
                    } else {
                        cb(null)
                    }
                }
            })
        },

        getMaidsData: function (updateCompleteService, cb) {
            notificationDataMaid.agencyName = agencyName;
            let criteria = {
                    _id: maidId,
                    isDeleted: false,
                };
                let projection = {};
                let option = {lean: true};
                Service.MaidServices.getMaid(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result && result.length) {
                            notificationDataMaid.receiverId = result[0]._id;
                            notificationDataMaid.maidName = result[0].firstName;
                            notificationDataMaid.profilePicURL = result[0].profilePicURL;
                            notificationDataMaid.deviceToken = result[0].deviceToken;
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })

        },

        sendPushToUser: function (getAgencyData, getUsersData, getMaidsData,updateCompleteService, cb) {
            notificationDataUser.title = 'M.A.K.';
            notificationDataUser.bookingNumber = bookingNumber;
            notificationDataUser.requestId = serviceId;
            notificationDataUser.maidId= maidId;
            notificationDataUser.senderId=agencyId;
            notificationDataUser.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_COMPLETE;
            notificationDataUser.body =  notificationDataMaid.maidName+" completed your service request, click to submit review.";
            Controller.AgencyController.sendNotification(notificationDataUser, function (err, result) {
                if (err) {
                    console.log("1____err_____________SRVICE_ACCEPT_________push")
                    cb(err)
                } else {
                    console.log("2_____result_____________SRVICE_ACCEPT_________push")
                    cb(null)
                }
            })
        },

        sendPushToMaid : function (getUsersData, getAgencyData, getMaidsData,updateCompleteService, cb) {
            notificationDataMaid.title = 'M.A.K.';
            notificationDataMaid.bookingNumber = bookingNumber;
            notificationDataMaid.requestId = serviceId;
            notificationDataMaid.maidId= maidId;
            notificationDataMaid.senderId=agencyId;
            notificationDataMaid.type = Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.SERVICE_COMPLETE;
            notificationDataMaid.body =  "Your service for "+notificationDataUser.fullName+" is completed.";

            Controller.AgencyController.sendNotificationMaid(notificationDataMaid, function (err, result) {
                    if (err) {
                        console.log("1____err______maid_______SRVICE_ACCEPT_________push")
                        cb(err)
                    } else {
                        console.log("2_____result_____maid________SRVICE_ACCEPT_________push")
                        cb(null)
                    }
                })


        },

    }, function (err, result) {
        callback(err, result)
    })
};
